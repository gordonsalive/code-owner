
/*--- ROLREFTSTF ---*/
CREATE TABLE QGPL.ROLREFTSTF (
view_name FOR COLUMN view VARCHAR(64) NOT NULL,
view_item FOR COLUMN item VARCHAR(64) NOT NULL,
run_type FOR COLUMN type CHAR(4) NOT NULL,
fail_end FOR COLUMN end INTEGER NOT NULL,
fail_start FOR COLUMN start INTEGER NOT NULL,
view_variable FOR COLUMN variable CHAR(10) NOT NULL,
view_item_value FOR COLUMN value VARCHAR(256),
updated TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
) RCDFMT ROLREFTSTF

-- Long table name                                             
RENAME ROLREFTSTF TO rolling_refresh_failing_tests FOR SYSTEM NAME ROLREFTSTF;

-- Table label                                                 
LABEL ON TABLE QGPL.ROLREFTSTF IS 
'Jenkins Cucumber Failing Scenarios'

-- Table description                                              
COMMENT ON TABLE QGPL.ROLREFTSTF IS 
'Jenkins Cucumber Failing Scenarios'

-- Column labels                                                  
LABEL ON COLUMN rolreftstf
(                                                                 
    view_name TEXT IS 'Reporting View Name',
    view_item TEXT IS 'Reporting View Sub-Item Name',
    run_type TEXT IS 'Flat View or Tree/Grouped View',             
    fail_end TEXT IS 'Scenario Stopped Failing',
    fail_start TEXT IS 'Scenario Started Failing',
    view_variable TEXT IS 'Variable',
    View_item_value TEXT IS 'Failing Scenario' 
); 


/*---rolrefd table---*/
CREATE TABLE QGPL.ROLREFTST (
test_result_id FOR COLUMN id INT NOT NULL
GENERATED ALWAYS AS IDENTITY
(START WITH 1
INCREMENT BY 1
NO MAXVALUE
NO CYCLE
NO CACHE
ORDER),
view_name FOR COLUMN view VARCHAR(64) NOT NULL,
run_date FOR COLUMN date BIGINT NOT NULL,
run_time FOR COLUMN time BIGINT NOT NULL,
run_type FOR COLUMN type CHAR(4) NOT NULL,
view_variable FOR COLUMN variable CHAR(10) NOT NULL,
view_item FOR COLUMN item VARCHAR(64) NOT NULL,
view_item_value FOR COLUMN value VARCHAR(1024),
updated TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
) RCDFMT ROLREFTSTF
LABEL ON TABLE QGPL.ROLREFTST IS 'Jenkins Cucumber Test History'
COMMENT ON TABLE QGPL.ROLREFTST IS 'Jenkins Cucumber Test History '
LABEL ON COLUMN QGPL.ROLREFTST (
 )



/*---rolrefd table---*/
CREATE TABLE QGPL.ROLREFD (
CONSTRAINT PK_ROLREFD_VALUES PRIMARY KEY (id),
delete_library FOR COLUMN id CHAR(10) NOT NULL,
original_library FOR COLUMN library CHAR(10) NOT NULL,
date_added FOR COLUMN date INTEGER NOT NULL,
updated TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
) RCDFMT ROLREFDF
LABEL ON TABLE QGPL.ROLREFD IS 'DBPOPULATE Library Deletes'
COMMENT ON TABLE QGPL.ROLREFD IS 'DBPOPULATE Libraries to be deleted'
LABEL ON COLUMN QGPL.ROLREFD (
id TEXT IS 'Renamed Library',
library TEXT IS 'Original Library',
date TEXT IS 'Date Added' )
/*Examples:
try { figUtil.muffle {figUtil.runSQL("DROP TABLE tagval")}
} catch (java.sql.SQLException ex){ 
//tolerate failure in case of re-runs 
}
try {
    figUtil.runSQL("CREATE TABLE tagval ( " +
            " CONSTRAINT PK_TAGGED_VALUES PRIMARY KEY (item, tag), " +
            " data_layer   FOR COLUMN layer CHAR(20) NOT NULL, " +
            " data_item    FOR COLUMN item CHAR(20) NOT NULL, " +
            " lookup_key   FOR COLUMN tag  VARCHAR(128) NOT NULL, " +
            " lookup_value FOR COLUMN value VARCHAR(256) NOT NULL, " +
            " mutable_data FOR COLUMN mutable CHAR(1) NOT NULL " +
            " ) RCDFMT TAGVALF")
} catch (java.sql.SQLException ex) { //tolerate failure in case of re-runs }
try {
    //this rename statemet has been hanging, so commenting out for now
    //figUtil.runSQL("RENAME TAGVAL TO tagged_values FOR SYSTEM NAME TAGVAL")
} catch (java.sql.SQLException ex) { //tolerate failure in case of re-runs }
try {
    figUtil.runSQL("LABEL ON TABLE tagval IS 'Tagged Values'")
} catch (java.sql.SQLException ex) { //tolerate failure in case of re-runs }
try {
    figUtil.runSQL("COMMENT ON TABLE tagval IS 'Tagged values for automated testing'")
} catch (java.sql.SQLException ex) { //tolerate failure in case of re-runs }
try {
    figUtil.runSQL("LABEL ON COLUMN tagval ( " +
            " layer TEXT IS 'db-populate Layer', " +
            " data_item TEXT IS 'Figaro Object', " +
            " lookup_key TEXT IS 'Lookup Key', " +
            " lookup_value TEXT IS 'Lookup Value' )")
} catch (java.sql.SQLException ex) { 
//tolerate failure in case of re-runs
}
*/
"CREATE TABLE "+ strDataLib +".pickup (" +
        "variable_name FOR COLUMN variable CHAR(20) NOT NULL," +
        "variable_value FOR COLUMN value CHAR(128) NOT NULL," +
        "sequence_no FOR COLUMN seqno CHAR(10) NOT NULL," +
        "updated TIMESTAMP NOT NULL GENERATED ALWAYS FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP" +
        ") RCDFMT PICKUPF"
/*--ROLREF--*/
CREATE TABLE rolref
(                                                                          
                                                                           
-- Primary Key                                                              

-- Columns                                                      
database_model   FOR COLUMN model CHAR(20) NOT NULL,          
promotion_level  FOR COLUMN level CHAR(10) NOT NULL,
database_name    FOR COLUMN database CHAR(10) NOT NULL,
variable_name    FOR COLUMN variable CHAR(20) NOT NULL,
variable_value   FOR COLUMN value CHAR(128) NOT NULL,
updated TIMESTAMP NOT NULL GENERATED ALWAYS                    
                 FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
)                                                              
RCDFMT ROLREFF                                                                                                          
                                                               
-- Long table name                                             
RENAME ROLREF TO rolling_refresh FOR SYSTEM NAME ROLREF;
                                                               
-- Table label                                                 
LABEL ON TABLE rolref                                   
IS 'Rolling Refresh' ;

-- Table description                                              
COMMENT ON TABLE rolref
IS 'Holds config for rolling refresh' ;
                                                                  
-- Column labels                                                  
LABEL ON COLUMN rolref
(                                                                 
    database_model TEXT IS 'Model or Project',
    promotion_level TEXT IS 'LIVE or HOLDING',
    database_name TEXT IS 'Environment Name',             
    variable TEXT IS 'Variable' ,
    value TEXT IS 'Value' 
);    



/*--ROLREFT--*/
CREATE TABLE QGPL.rolreft
(                                                                          
                                                                           
--Primary Key                                                              

--Columns                                                      
database_model   FOR COLUMN model CHAR(20) NOT NULL,          
promotion_level  FOR COLUMN level CHAR(10) NOT NULL,
database_name    FOR COLUMN database CHAR(10) NOT NULL,
run_start        FOR COLUMN start CHAR(20) NOT NULL,
variable_name    FOR COLUMN variable CHAR(20) NOT NULL,
variable_value   FOR COLUMN value VARCHAR(256) NOT NULL,
updated TIMESTAMP NOT NULL GENERATED ALWAYS                    
                 FOR EACH ROW ON UPDATE AS ROW CHANGE TIMESTAMP
)                                                              
RCDFMT ROLREFTF                                                                                                          
                                                               
-- Long table name                                             
RENAME ROLREFT TO rolling_refresh_times FOR SYSTEM NAME ROLREFT;
                                                               
-- Table label                                                 
LABEL ON TABLE rolreft                                   
IS 'Rolling Refresh Times' ;

-- Table description                                              
COMMENT ON TABLE rolreft
IS 'Holds times for rolling refresh' ;
                                                                  
-- Column labels                                                  
LABEL ON COLUMN rolreft
(                                                                 
    database_model TEXT IS 'Model or Project',
    promotion_level TEXT IS 'LIVE or HOLDING',
    database_name TEXT IS 'Environment Name',             
    run_start TEXT IS 'Run Start',
    variable_name TEXT IS 'Variable' ,
    variable_value TEXT IS 'Value' 
);          
/*--TAGVAL--*/
CREATE TABLE tagval
(

--Primary Key
CONSTRAINT PK_TAGGED_VALUES
PRIMARY KEY (item, tag),


--Columns
data_item    FOR COLUMN item CHAR(20) NOT NULL,
lookup_key   FOR COLUMN tag  CHAR(128) NOT NULL,
lookup_value FOR COLUMN database CHAR(256) NOT NULL
)
RCDFMT TAGVALF;

-- Long table name
RENAME TAGVAL TO tagged_values FOR SYSTEM NAME TAGVAL;

-- Table label
LABEL ON TABLE tagval
IS 'Tagged Values';

-- Table description
COMMENT ON TABLE tagval
IS 'Tagged values for automated testing';

-- Column labels
LABEL ON COLUMN tagval
(
    data_item TEXT IS 'Figaro Object',
    lookup_key TEXT IS 'Lookup Key',
    lookup_value TEXT IS 'Lookup Value'
);
