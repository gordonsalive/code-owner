/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var http = require('http');
var url = require('url');
var qs = require('querystring');
var path = require('path');
var fs = require('fs');
var ECT = require('ect');
var Q = require('q');
var Qfs = require("q-io/fs");
var _ = require('underscore');

//TODO: move the addHelper methods out into node modules, so this just pull things together.

var get_code_owners = require('./scripts/get-code-owners');
var get_quality_owners_view_data = require('./scripts/get-code-owners');
var get_db_populate_timings = require('./scripts/get-db-populate-timings');
var get_radiators_flat_view_data = require('./scripts/get-radiators-flat-view-data');
var get_cuke_results_data = require('./scripts/get-cuke-results-data');
var add_code_ownership_data_helper = require('./scripts/add-code-ownership-data-helper');
var add_timings_data_helper = require('./scripts/add-timings-data-helper');
var add_radiators_flat_view_data_helper = require('./scripts/add-radiators-flat-view-data-helper');
var add_quality_owners_grouped_data_helper = require('./scripts/add-quality-owners-grouped-data-helper');
var get_test_history = require('./scripts/get-test-history');
var add_test_history_helper = require('./scripts/add-test-history-helper');

var renderer = ECT({
    root: __dirname + '/views'
});

//TODO - use express and express-router to simplify this server and make it more REST in style

http.createServer(function (request, response) {

    var requestUrl = url.parse(request.url);
    var action = requestUrl.pathname;
    var filename = path.join(process.cwd(), action);

    var contentTypesByExtension = {
        '.html': "text/html",
        '.css': "text/css",
        '.js': "text/javascript"
    };

    //console.log(request.method + ':' + action);

    fs.exists(filename, function (exists) {
        //if file doesn't exist, is it one of my dynamic files
        if (!exists) {

            var params;
            if (request.method == 'POST') {
                var body = '';
                request.on('data', function (data) {
                    body += data;
                });
                request.on('end', function () {
                    params = qs.parse(body);
                });
            } else if (request.method == 'GET') {
                params = url.parse(request.url, true).query;
            }


            if (_.contains(['/code-owners.html'], action)) {
                // === CODE OWNERS ===
                response.writeHead(200, {
                    "Content-Type": "text/html"
                });

                //get_code_owners('code-owners.json', 'code-owners', 'view')
                get_code_owners('json/code-owners.json')
                    .then(add_code_ownership_data_helper)
                    .then(function (data) {
                        try {
                            //addCodeOwnerDataHelper(data);
                            //console.log(data);
                            var html = renderer.render('code-owners-page.ect', data);
                            response.write(html);
                        } finally {
                            response.end();
                        }
                    })
                    .fail(console.error);

            } else if (action.includes('/db-populate-timings.html')) {
                // === DBPOPULATE TIMINGS ===
                if (action.includes('/db-populate-timings.html/data/')) {
                    //pick up and return the json
                    response.writeHead(200, {
                        "Content-Type": "text/json"
                    });

                    //return cuke data
                    get_db_populate_timings('json/timings-results.json')
                        .then(function (data) {
                            try {
                                //console.log(JSON.stringify(data, null, "\t"));
                                var nav = action.replace('/db-populate-timings.html/data/', '');
                                var navItems = nav.split('/');
                                var resultData = data;
                                for (var x = 0;
                                    (x < navItems.length) && (navItems[x] !== ''); x++) {
                                    var navItem = navItems[x].split('%20').join(' ');
                                    resultData = resultData[navItem];
                                }

                                response.write(JSON.stringify(resultData, null, "\t"));
                            } finally {
                                response.end();
                            }
                        })
                        .fail(console.error);
                } else {
                    // TODO - move this into quality-owners, rest stylie
                    response.writeHead(200, {
                        "Content-Type": "text/html"
                    });

                    //                get_db_populate_timings('json/timings-results.json')
                    //                    .then(function (results) {
                    //                        //response.write(JSON.stringify(results, null, "  "));
                    //                        return results;
                    //                    })
                    //                    .then(add_timings_data_helper)
                    //                    .then(function (results) {
                    //                        response.write(JSON.stringify(results, null, "  "));
                    //                    });

                    get_db_populate_timings('json/timings-results.json')
                        .then(add_timings_data_helper)
                        .then(function (data) {
                            try {
                                var html = renderer.render('db-populate-timings-page.ect', data);
                                response.write(html);
                            } finally {
                                response.end();
                            }
                        })
                        .fail(console.err);
                }
            } else if (action.includes('/test-history.html')) {
                // === TEST HISTORY ===
                if (action.includes('/test-history.html/data')) {
                    //console.log('asking for test history data');
                    //pick up and return the json
                    response.writeHead(200, {
                        "Content-Type": "text/json"
                    });

                    //return cuke data
                    get_test_history('json/test-history-results.json')
                        .then(function (data) {
                            try {
                                //console.log(JSON.stringify(data, null, "\t"));
                                var nav = action.replace('/test-history.html/data/', '');
                                var navItems = nav.split('/');
                                var resultData = data;
                                for (var x = 0;
                                    (x < navItems.length) && (navItems[x] !== ''); x++) {
                                    var navItem = navItems[x].split('%20').join(' ');
                                    resultData = resultData[navItem];
                                }

                                response.write(JSON.stringify(resultData, null, "\t"));
                            } finally {
                                response.end();
                            }
                        })
                        .fail(console.error);
                } else {
                    //params: charttype=
                    //            twocharts: show 2 charts (pass/fail vs fail/missing)
                    //            subitemresults: show 1 chart with total (passed+failed) for sub-items
                    //            totalsonly: show 1 chart with totals only (passed+failed)
                    //            else (singlechart): show 1 chart with passed/failed
                    //        viewfilter: filter list to just views in view filter (normally just 1)
                    //            
                    /////////////////
                    //set up default params if not passed in
                    if (!params.charttype) {
                        params.charttype = 'singlechart'; //single chart with passed/faied
                    }
                    if (!params.viewfilter) {
                        params.viewfilter = []; //empty set means all views
                    } else {
                        //convert view filter from the form string:'[something,something,something]' to array of string
                        params.viewfilter = params.viewfilter.replace('[', '').replace(']', '').split(',');
                        //console.log('viewFilter=' + params.viewfilter);
                    }
                    ///////////////////

                    response.writeHead(200, {
                        "Content-Type": "text/html"
                    });

                    get_test_history('json/test-history-results.json')
                        .then(add_test_history_helper)
                        .then(function (data) {
                            data.helper.params = params; //add params to the data I pass on
                            //console.log('about to render the page');
                            //console.log(JSON.stringify(data, null, '  '));
                            try {
                                var html = renderer.render('test-history-page.ect', data);
                                response.write(html);
                            } finally {
                                response.end();
                            }
                        })
                        .fail(console.err);
                }
            } else if (action.includes('/quality-owners.html')) {
                // === QUALITY OWNERS ===

                if (action.includes('/quality-owners.html/data/')) {
                    //pick up and return the json
                    response.writeHead(200, {
                        "Content-Type": "text/json"
                    });

                    if (action.includes('/quality-owners.html/data/views/')) {
                        var nav = action.replace('/quality-owners.html/data/views/', '');
                        var navItems = nav.split('/');
                        var view = 'Automated Testing - Quality Owner View';
                        if (navItems[0] !== '') {
                            view = navItems[0].split('%20').join(' ');
                            //console.log(view);
                        }
                        //return view data
                        get_radiators_flat_view_data(view)
                            .then(function (data) {
                                try {
                                    //console.log(JSON.stringify(data, null, "\t"));
                                    var resultData = data;
                                    if (navItems.length > 1) {
                                        for (var x = 1;
                                            (x < navItems.length) && (navItems[x] !== ''); x++) {
                                            var navItem = navItems[x].split('%20').join(' ');
                                            resultData = resultData[navItem];
                                        }
                                    }

                                    response.write(JSON.stringify(resultData, null, "\t"));
                                } finally {
                                    response.end();
                                }
                            })
                            .fail(console.error);
                    } else if (action.includes('/quality-owners.html/data/cukes/')) {
                        //return cuke data
                        get_cuke_results_data()
                            .then(function (data) {
                                try {
                                    //console.log(JSON.stringify(data, null, "\t"));
                                    var nav = action.replace('/quality-owners.html/data/cukes/', '');
                                    var navItems = nav.split('/');
                                    var resultData = data;
                                    for (var x = 0;
                                        (x < navItems.length) && (navItems[x] !== ''); x++) {
                                        var navItem = navItems[x].split('%20').join(' ');
                                        resultData = resultData[navItem];
                                    }

                                    response.write(JSON.stringify(resultData, null, "\t"));
                                } finally {
                                    response.end();
                                }
                            })
                            .fail(console.error);
                    }
                } else {

                    //set up default params if not passed in
                    if (!params.view) {
                        params.view = 'Automated Testing - Quality Owner View';
                    }
                    if (!params.orientation) {
                        params.orientation = 'partrait';
                    }
                    if (!params.xmas) {
                        params.xmas = false;
                    }
                    if (!params.grouped) {
                        params.grouped = false;
                    }

                    response.writeHead(200, {
                        "Content-Type": "text/html"
                    });
                    if (params.grouped) {
                        // Currently, there must always be a Jenkins view that we are presenting data for
                        //(i) get the grouped cuke results, this is in the correct shape
                        //(2) get the associated view results (jobs json stright from Jenkins) 
                        //    and enrich cuke results with this info (attach it as a node called jobs) to give us the pass/fail status,etc.
                        addCukeResultsToGroupedViewData({}, params.view)
                            .then(function (data) {
                                return addJobStatusToGroupedViewData(data, params.view);
                            })
                            .then(function (data) {
                                return [data, params]; //add_quality_owners_grouped_data_helper takes two parameters and this seems like the neatest way of handling it
                            })
                            .spread(add_quality_owners_grouped_data_helper)
                            .then(function (data) {
                                try {
                                    var html;
                                    html = renderer.render('quality-owners-grouped-page.ect', data);
                                    response.write(html);
                                } finally {
                                    response.end();
                                }
                            })
                            .fail(console.error);
                    } else {
                        get_radiators_flat_view_data(params.view)
                            .then(addCukeResultsToFlatViewData)
                            .then(function (data) {
                                return [data, params]; //add_radiators_flat_view_data_helper takes two parameters and this seems like the neatest way of handling it
                            })
                            .spread(add_radiators_flat_view_data_helper)
                            .then(function (data) {
                                try {
                                    var html;
                                    html = renderer.render('radiators-flat-page.ect', data);
                                    response.write(html);
                                } finally {
                                    response.end();
                                }
                            })
                            .fail(console.error);
                    }
                }
            } else {
                //if not return 404
                response.writeHead(404, {
                    "Content-Type": "text/plain"
                });
                response.write("404 Not Found\n");
                response.end();
                return;
            }
        } else {
            //handle static files
            if (fs.statSync(filename).isDirectory()) filename += '/index.html';
            //TODO: add an index that explains what you can call and how.

            fs.readFile(filename, "binary", function (err, file) {
                if (err) {
                    response.writeHead(500, {
                        "Content-Type": "text/plain"
                    });
                    response.write(err + "\n");
                    response.end();
                    return;
                }

                var headers = {};
                var contentType = contentTypesByExtension[path.extname(filename)];
                if (contentType) headers["Content-Type"] = contentType;
                response.writeHead(200, headers);
                response.write(file, "binary");
                response.end();
            });
        }
    });
}).listen(8084);

function addCukeResultsToFlatViewData(data) {
    //read in the cuke results...
    return get_cuke_results_data()
        .then(function (cukeResults) {
            //...then, for each job, add totals
            _.each(data.jobs, function (job) {
                job.results = cukeResults.flat[job.name]; //result property will only be added if this is not undefined
            });

            //console.log(JSON.stringify(data, null, '\t'));

            return data;
        });
}

function addCukeResultsToGroupedViewData(data, view) {
    //read in the cuke results...
    return get_cuke_results_data() //returns a promise
        .then(function (cukeData) {
            data.view = cukeData.tree[view];
            return data;
        });
}

function addJobStatusToGroupedViewData(data, view) {
    //find the accociated view(s) and add a jobs node to the data (with combined results if there are multiple views)
    data.jobs = [];
    //(i) get the associated view(s)
    return Qfs.read('json/cukes.json')
        .then(function (cukesFile) {
            //console.log(cukesFile);
            //parse the file into json.
            var cukeJson = JSON.parse(cukesFile);
            var associatedViews = cukeJson['code-owners-tree'][view].info.associatedViews;
            data.displayName = cukeJson['code-owners-tree'][view].info.displayName;
            //these are the view id's within my JSON, I need to lookup real view names
            var associatedViewNames = _.map(associatedViews, function (viewID) {
                return cukeJson.jenkins.views[viewID].name;
            });
            //(ii) get an array of the job results and then flatten it
            return Q.all(
                    _.map(associatedViewNames, function (viewName) {
                        return get_radiators_flat_view_data(viewName); //returns a promise
                    }))
                .then(function (jobs) {
                    //have an array of the form [{jobs:[...]}, {jobs:[...]}]
                    //(iia) create an array of the jobs arrays and flatten it
                    var jobsArray = _.map(jobs, function (jobArray) {
                        return jobArray.jobs;
                    }).flatten();
                    data.jobs = jobsArray;
                    return data;
                });
        });
}