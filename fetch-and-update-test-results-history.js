/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");

var update_test_results_history = require('./scripts/update-test-results-history');

(function doMainStuff() {

    update_test_results_history('json/cuke-results.json', 'json/cukes.json')
        .then(function (result) {
            var d = new Date();
            console.log('Completed: ' + d.toLocaleString() + '. Wait 15 minutes before repeating...');
            setTimeout(doMainStuff, 900 * 1000); /*try again in 15 minutes*/
        })
        .fail(function (err) {
            console.error(err);
            var d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 20 minutes');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 1200 * 1000); //try again in 20 minutes
        });

})();