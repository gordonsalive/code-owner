/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 19/7/2016
// Module to fetch the timings information from QGPL.ROLREFT and save it as JSON for display
//**********

console.log("about to connect the pool");
var Database = require('jt400');
var database = new Database();

var config = {
    libpath: __dirname + '/jt400.jar',
    drivername: 'com.ibm.as400.access.AS400JDBCDriver',
    //url: 'jdbc:as400://127.0.0.1/QGPL;user=GOLDSTAR;password=JHCJHC'
    url: 'jdbc:as400://172.26.27.15/QGPL;user=GOLDSTAR;password=JHCJHC'
};

database.initialize(config);

var timingsConfig = {

};

var DEBUG = 0; //off

var update_status = require('./scripts/update-status');

var resultsJson = {};

writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {
    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    console.log("about to make use of the pool");
    // SELECT statements must be run with execute()
    database.execute("SELECT model, level, database, start, variable FROM qgpl.rolreft WHERE database='FGBLIVZn1'");

    database.on('execute', function (error, results) {
        if (error) {
            console.log(error);
        } else {
            console.log(results);
        }
    });
    console.log("back from call to pool.query");


})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'timings');
}