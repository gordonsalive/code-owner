/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 28/10/2016
// uses git to fetch figaro-features documentation, this can then be published by code-owners
//**********

var Git = require("nodegit");
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var read_json = require("./scripts/read-json");
var promisify = require("./scripts/promisify");

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');

var resultsJson = {};


writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    //console.log('--parsefile..');
    read_json('json/features.json')
        .then(function (featuresFile) {
            var results = featuresFile.documentation;
            return results;
        })
        //get the totals history for each (run), with flat as a special case
        .then(function (documentation) {

            // Set the URL that NodeGit will connect to clone.
            var cloneURL = documentation.repo;
            // Ensure that the `tmp` directory is local to this file and not the CWD.
            var localPath = require("path").join(__dirname, documentation.name);
            // Simple object to store clone options.
            var cloneOptions = {};
            // This is a required callback for OS X machines.  There is a known issue
            // with libgit2 being able to verify certificates from GitHub.
            cloneOptions.fetchOpts = {
                callbacks: {
                    callbacks: {
                        certificateCheck: function () {
                            console.log('certificate check');
                            return 1;
                        }
                    }
                    //                    certificateCheck: function () {
                    //                        console.log('certificate check');
                    //                        return 1;
                    //                    },
                    //
                    //                    // Credentials are passed two arguments, url and username. We forward the
                    //                    // `userName` argument to the `sshKeyFromAgent` function to validate
                    //                    // authentication.
                    //                    credentials: function (url, userName) {
                    //                        console.log('get credentials');
                    //                        return Git.Cred.sshKeyFromAgent(userName);
                    //                    }
                }
            };
            console.log('about to attempt to use git to get my repo(' + cloneURL + ')(' + localPath + ')');
            //Git.Clone("https://github.com/nodegit/nodegit", "nodegit").then(function(repository) {
            // Invoke the clone operation and store the returned Promise.
            var cloneRepository = Git.Clone(cloneURL, localPath, cloneOptions).then(function (repository) {
                // Work with the repository object here.
                console.log('using git to get my repo');
            });
            // If the repository already exists, the clone above will fail.  You can simply
            // open the repository in this case to continue execution.
            var errorAndAttemptOpen = function () {
                return Git.Repository.open(localPath);
            };
            var getMostRecentCommit = function (repository) {
                console.log('get most recent commit for master');
                return repository.getBranchCommit("master");
            };
            var getCommitMessage = function (commit) {
                console.log('get commit message');
                return commit.message();
            };
            Git.Repository.open("nodegit")
                .then(getMostRecentCommit)
                .then(getCommitMessage)
                .then(function (message) {
                    console.log(message);
                });

            //useful code excerpt:
            //var sys = require('sys')
            //var exec = require('child_process').exec;
            //function puts(error, stdout, stderr) { sys.puts(stdout) }
            //exec("git status", puts);
            return cloneRepository;
        })
        .then(function (result) {
            console.log('--update status..');
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            console.log('..repeat in 2 minutes..');
            /*repeat in 30 seconds*/
            setTimeout(doMainStuff, 120 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 5 minutes');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 300 * 1000); //try again in 5 minutes
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'status');
}