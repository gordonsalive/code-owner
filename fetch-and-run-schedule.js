/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 19/4/'17
// runs all the programs that act on a schedule, one after another, evey hour.
//**********

//var Qfs = require("q-io/fs");
//var Q = require("q");
//var _ = require("underscore");
//Q.longStackSupport = true;

//var read_json = require("./scripts/read-json");
var promisify = require("./scripts/promisify");

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');
var send_scheduled_code_review_emails = require('./scripts/send-scheduled-code-review-emails');
var send_scheduled_testing_summary_emails = require('./scripts/send-scheduled-testing-summary-emails');

writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    promisify.spread(
        [ //list of scripts that check a schedule and run if required:
            send_scheduled_code_review_emails(),
            send_scheduled_testing_summary_emails()
        ],
            function (result) {
                //console.log('--update status..');
                //update status
                return writeOutStatus('succeeded');
            })
        .then(function (result) {
            //console.log('..repeat in 1 hour..');
            setTimeout(doMainStuff, 60 * 60 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 2 hours');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 2 * 60 * 60 * 1000);
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'schedule');
}