/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//TODO: add if (DEBUG) style logging and anything else from original worth keeping
//TODO: remove the parse function into a node modele I can write test for it (passing in hardcoded json to parse)
//TODO: what should update status do?  does it update as successful if at least one job updated, or only if all updated?

var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;
var js2xmlparser = require("js2xmlparser");

var DEBUG = 0; //0=off

var fetch_and_parse_cuke = require('./scripts/fetch-and-parse-cuke');
var update_status = require('./scripts/update-status');
var delete_folder_recursive_promise = require("./scripts/delete-folder-recursive-promise");
var summarise_cucumber_tests_promise = require("./scripts/summarise-cucumber-tests-promise");
var summarise_junit_tests_promise = require("./scripts/summarise-junit-tests-promise");
var summarise_cucumber_tests_deep_promise = require("./scripts/summarise-cucumber-tests-deep-promise");

var resultsJson = {};

writeOutStatus('failed');

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) {
        console.log('--mainLoop:' + d.toTimeString());
    }

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    Qfs.read('json/cukes.json')
        .then(function (result) {
            //NEED TO CLEAR AWAY FILES FROM LAST RUN AT START AS WELL AS END 
            //(in run fails half way through because of something in the files!)
            if (DEBUG) {
                console.log('--(0a) clear away cuke files');
            }
            var directories = fs.readdirSync("cukes");
            return Q.all(
                    _.map(directories, function (dir) {
                        if (fs.lstatSync('cukes/' + dir).isDirectory()) {
                            return delete_folder_recursive_promise('cukes/' + dir);
                        }
                    })
                )
                .then(function (results) {
                    if (DEBUG) {
                        console.log("deleted:" + results);
                    }
                    return result;
                });
        })
        .then(function (result) {
            if (DEBUG) {
                console.log('--(0b) clear away junit files');
            }
            var directories = fs.readdirSync("junit");
            return Q.all(
                    _.map(directories, function (dir) {
                        if (fs.lstatSync('junit/' + dir).isDirectory()) {
                            return delete_folder_recursive_promise('junit/' + dir);
                        }
                    })
                )
                .then(function (results) {
                    if (DEBUG) {
                        console.log("deleted:" + results);
                    }
                    return result;
                });
        })
        .then(function (cukesFile) {
            function pathsForJenkinsJob(job, cukesJenkins) {
                if (cukesJenkins.jobs[job]) {
                    return cukesJenkins.jobs[job].paths;
                }
            }
            //(1) parse cukes.json and return all the cuke paths
            var cukesJson = JSON.parse(cukesFile);
            var cukesJenkins = cukesJson.jenkins;
            if (DEBUG) {
                console.log('--(1) parsefile..complete.');
            }
            var result = {
                jobsArray: _.map(cukesJenkins.jobs, function (job) {
                    return _.map(job.paths, function (path) {
                        return {
                            name: job.name,
                            hostname: job.hostname,
                            path: path,
                            resultsStyle: job.resultsStyle ? job.resultsStyle : 'cucumber',
                            "git-repo-branch": job["git-repo-branch"]
                        };
                    });
                }).flatten(),
                hostname: cukesJenkins.hostname
            };
            //(1b) for the grouped views, return, by view, the cuke paths and the tags to filter by 
            //- TODO: handle tree structure, basically flatten out any sub-nodes by carrying their data up to the node above them
            var cukesTree = cukesJson["code-owners-tree"];
            var expandedTree = _.mapObject(cukesTree, function (viewVal, viewKey) {
                //mapping: code-owners-tree item like "quality-owners-flat-rest"
                var viewItems = _.map(viewVal, function (project, projectKey) {
                    //mapping: view item like "documents"
                    if ((projectKey === 'info') || (!(project.info))) {
                        //return null;
                        return [];
                    }

                    //a project like "documents" might have a sub-project like "f153"
                    //..in this case, we want to keep cycling down into the sub-project,
                    //but we need to carry down any tags or Jenkins jobs from the project into the sub-project
                    var associatedViewName = null;
                    if (cukesTree[viewKey].info.associatedViews && cukesTree[viewKey].info.associatedViews.length > 0) {
                        var associatedView = cukesTree[viewKey].info.associatedViews[0];
                        associatedViewName = cukesJson.jenkins.views[associatedView].name;
                    }
                    var projectInfo = {
                        jenkinsJobs: project.info.jenkinsJobs,
                        tags: project.info.tags,
                        displayName: project.info.displayName,
                        jenkinsViewName: associatedViewName,
                        stickyGroups: cukesTree[viewKey].info.stickyGroups
                    };

                    var subProjectInfo = _.map(project, function (subProject) {
                        //mapping a sub-project like "f153"
                        if (subProject.info) {
                            var associatedJobName = null;
                            if (subProject.info.jenkinsJobs && subProject.info.jenkinsJobs.length > 0) {
                                var associatedJob = subProject.info.jenkinsJobs[0];
                                //console.log("looking for matching job:" + associatedJob);
                                if (!cukesJson.jenkins.jobs[associatedJob]) {
                                    console.log('undefined for cukesJson.jenkins.jobs[' + associatedJob + ']');
                                }
                                associatedJobName = cukesJson.jenkins.jobs[associatedJob].name;
                            }
                            return {
                                jenkinsJobs: _.union(subProject.info.jenkinsJobs, projectInfo.jenkinsJobs),
                                tags: _.union(subProject.info.tags, projectInfo.tags),
                                displayName: projectInfo.displayName + ':' + subProject.info.displayName,
                                jenkinsViewName: associatedViewName,
                                jenkinsJobName: associatedJobName,
                                stickyGroups: projectInfo.stickyGroups
                            };
                        }
                    });
                    subProjectInfo = _.compact(_.without(subProjectInfo, []));
                    //console.log("subProjectInfo:" + JSON.stringify(subProjectInfo, null, "  "));

                    //we want to avoid duplicating work, so
                    // if there are no sub-projects, we do our work on the cukes and tags specified at the project level
                    // if there are sub-projects, we do our work on the cukes and tags of the sub-project and simply
                    //  aggregate these results to present the information at the project level (we don't repeat the parsing, etc.)
                    if (subProjectInfo && (subProjectInfo.length > 0)) {
                        projectInfo = subProjectInfo;
                    } else {
                        projectInfo = [projectInfo];
                    }
                    //console.log("projectInfo:" + JSON.stringify(projectInfo, null, "  "));

                    return _.map(projectInfo, function (projectParams) {
                        return _.map(projectParams.jenkinsJobs, function (job) {
                            //mapping: jenkinsJobs like "rest-api-f153" - look up the cukes for this Jenkins job
                            return _.map(pathsForJenkinsJob(job, cukesJenkins), function (path) {
                                //mapping: cuke paths for each Jenkins job like "cucumber-json-reports/cucumber153.json"
                                return {
                                    //view: viewKey,
                                    name: cukesJenkins.jobs[job].name,
                                    path: path,
                                    hostname: cukesJenkins.jobs[job].hostname,
                                    resultsStyle: cukesJenkins.jobs[job].resultsStyle ? cukesJenkins.jobs[job].resultsStyle : 'cucumber', //cukesJenkins.jobs[job].resultsStyle,
                                    "git-repo-branch": cukesJenkins.jobs[job]["git-repo-branch"],
                                    tags: projectParams.tags,
                                    displayName: projectParams.displayName ? projectParams.displayName : cukesJenkins.jobs[job].name,
                                    jenkinsViewName: projectParams.jenkinsViewName,
                                    jenkinsJobName: projectParams.jenkinsJobName,
                                    stickyGroups: projectParams.stickyGroups
                                };
                            });
                        }).flatten();
                    }).flatten();
                });
                //console.log("viewItems:" + JSON.stringify(viewItems, null, " "));
                return _.compact(viewItems.flatten());
            });
            //console.log("\n\n\n\nexpanded-tree:" + JSON.stringify(expandedTree, null, " "));
            //now we have a list of view-viewItem-jenkinsJob-cukePath combinations
            //BUT, we only want to do 1 pass through each cuke file,
            //SO, we want to group up by cuke file, and tidy the structure
            // grouping by the property path will give me an object with each value of that property
            // and against each an array of the object
            var compressedTree = _.mapObject(expandedTree, function (viewVal, viewKey) {
                //mapping each view, e.g. "quality-owners-flat-rest"
                return _.groupBy(viewVal, function (item) {
                    return item.name + ':' + item.path;
                });
            });
            //console.log("\n\n\n\ncompressed-tree:" + JSON.stringify(compressedTree, null, " "));
            //finally collapse the different parsing parameter objects for each job-path combination 
            // down to a single object for that job-path combination with a filter property holding
            // an array of filter objects, e.g. [{tags: ["@qualityOwnerDocuments"], displayName: "Documents"}, ...]
            // include and optional "other" filter object for the results of "other" (if view specifies it)
            var collapsedTree = _.mapObject(compressedTree, function (viewVal, viewKey) {
                //mapping each view, e.g. "quality-owners-flat-rest"
                return _.map(viewVal, function (cuke) {
                    //mapping each job-path combination, e.g. "Rest API - F153:cucumber-json-reports/cucumber161.json"
                    return _.reduce(cuke, function (parsingParams, item) {
                        parsingParams.name = item.name;
                        parsingParams.path = item.path;
                        parsingParams.hostname = item.hostname;
                        parsingParams.resultsStyle = item.resultsStyle;
                        parsingParams["git-repo-branch"] = item["git-repo-branch"];
                        parsingParams.jenkinsViewName = item.jenkinsViewName;
                        parsingParams.jenkinsJobName = item.jenkinsJobName;
                        parsingParams.stickyGroups = item.stickyGroups;
                        parsingParams.filterArray.push({
                            tags: item.tags,
                            displayName: item.displayName
                        });
                        return parsingParams;
                    }, {
                        name: '',
                        path: '',
                        filterArray: []
                    });
                });
            });
            //console.log("\n\n\n\ncollapsed-tree:" + JSON.stringify(collapsedTree, null, " "));

            result.tree = collapsedTree;
            return result;
        })
        .then(function (jobsObj) {
            //we have a list of job and path to cuke
            //at this point it needs to split in two - one way to create the flat results {flat: {rsults}, ...}
            // the other way to parse the cukes based on tags and group up based on structure within view 
            // in code-owners-tree in cukes.json {flat:{...}, tree: {view1: {results1}, view2: {results2}}
            if (DEBUG) {
                console.log('--(2) received jobsObj working on flat results:');
                //console.log(JSON.stringify(jobsObj, null, "  "));
            }
            //TODO: break this up so that we get the flat results forst (downloading the latest cuke files for everyting)
            //then we do all the grouped views in parallel afterward
            return createFlatResults(jobsObj)
                .then(function (flatResults) {
                    return {
                        flat: flatResults,
                        jobsObj: jobsObj
                    };
                });
        })
        .then(function (flatResultsObj) {
            if (DEBUG) {
                console.log('--(2b) received flatResultsObj working on grouped results:');
                //console.log(JSON.stringify(flatResultsObj, null, "  "));
            }
            return createGroupedResults(flatResultsObj.jobsObj)
                .then(function (groupedResults) {
                    return {
                        flat: flatResultsObj.flat,
                        tree: groupedResults
                    };
                });
        })
        .then(function (results) {
            //if (DEBUG) console.log(JSON.stringify(results, null, "  "));
            return results;
        })
        .then(function (resultObject) {
            if (DEBUG) {
                console.log('--(5) write out results file..');
            }
            return Qfs.write("json/cuke-results.json", JSON.stringify(resultObject, null, '\t'));
        })
        .then(function (result) {
            //Before we delete the cuke files and junit xml files, create a summary of all features and scenarios
            if (DEBUG) {
                console.log('--(101) read cucumber json files and summarise');
            }
            var cukeDirectories = fs.readdirSync("cukes");
            var junitDirectories = fs.readdirSync("junit");
            return summarise_tests_promise(cukeDirectories, junitDirectories);
        })
        .then(function (result) {
            if (DEBUG) {
                console.log('--(5a) clear away cuke files');
            }
            var directories = fs.readdirSync("cukes");
            return Q.all(
                    _.map(directories, function (dir) {
                        if (fs.lstatSync('cukes/' + dir).isDirectory()) {
                            return delete_folder_recursive_promise('cukes/' + dir);
                        }
                    })
                )
                .then(function (results) {
                    if (DEBUG) {
                        console.log("deleted:" + results);
                    }
                    return results;
                });
        })
        .then(function (result) {
            if (DEBUG) {
                console.log('--(5b) clear away junit files');
            }
            var directories = fs.readdirSync("junit");
            return Q.all(
                    _.map(directories, function (dir) {
                        if (fs.lstatSync('junit/' + dir).isDirectory()) {
                            return delete_folder_recursive_promise('junit/' + dir);
                        }
                    })
                )
                .then(function (results) {
                    if (DEBUG) {
                        console.log("deleted:" + results);
                    }
                    return results;
                });
        })
        .then(function (result) {
            if (DEBUG) {
                console.log('--(6) update status..');
            }
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            var d = new Date();
            if (DEBUG) {
                console.log('Completed: ' + d.toLocaleString() + '. Wait 7 minutes before repeating...');
            }
            setTimeout(doMainStuff, 7 * 60 * 1000); /*try again in 7 minutes*/
        })
        .fail(function (err) {
            console.error(err);
            //update status (even if done already)
            writeOutStatus('failed');
            var d = new Date();
            console.log('\n\n\n\n' + d.toLocaleString() + ': error, try again in 15 minutes\n\n\n\n');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 15 * 60 * 1000); //try again in 5 minutes
        });

})();

function summarise_tests_promise(cukeDirectories, junitDirectories) {
    // Create 2 sets of summaries: 
    //   a shallow one that goes down to scenario, and 
    //   a deep one that goes down to individual step also.
    return Q.all(
            _.flatten(
                _.map(cukeDirectories, function (dir) {
                    //console.log('dir=' + dir);
                    if (fs.lstatSync('cukes/' + dir).isDirectory() && (dir !== '.git')) {
                        return summarise_cucumber_tests_promise('cukes/' + dir, dir);
                    } else {
                        return [];
                    }
                }).concat(
                    _.map(junitDirectories, function (dir) {
                        if (fs.lstatSync('junit/' + dir).isDirectory() && (dir !== '.git')) {
                            return summarise_junit_tests_promise('junit/' + dir, dir);
                        } else {
                            return [];
                        }
                    })
                )
            )
        )
        .then(function (results) {
            if (DEBUG); //console.log("summarised:" + JSON.stringify(results, null, '  '));
            return Qfs.write("json/testing-summary.json", JSON.stringify(results, null, '\t'))
                .then(function (writeResult) {
                    //before we export to xml the tags need to be converted to xml 'attributes'
                    var results2 = _.map(results, function (job) {
                        return {
                            job: replaceSpecialChars(job.job),
                            features: _.map(job.features, function (feature) {
                                return {
                                    featureName: replaceSpecialChars(feature.name),
                                    featureFile: replaceSpecialChars(feature.file),
                                    "@": {
                                        featureTags: feature.tags ? replaceSpecialChars(feature.tags.join(',')) : ""
                                    },
                                    scenarios: _.map(feature.scenarios, function (scenario) {
                                        return {
                                            scenarioName: replaceSpecialChars(scenario.name),
                                            "@": {
                                                scenarioTags: scenario.tags ? replaceSpecialChars(scenario.tags.join(',')) : ""
                                            },
                                            result: replaceSpecialChars(scenario.result)
                                        };
                                    })
                                };
                            })
                        };
                    });
                    //console.log('next step, write json/testing-summary.xml, results2 = ');// + JSON.stringify(results2));
                    return Q.all(_.map(results2, function(job) {
                        return Qfs.write(
                            "json/" + job.job.split(' ').join('') + '.xml', 
                            js2xmlparser.parse("testingSummary", job.features));
                    }))
                    .then(function(writeResults) {
                        return Qfs.write("json/testing-summary.xml", js2xmlparser.parse("testingSummary", results2));
                    });
                });
        })
        .then(function (writeResults) {
            return Q.all(
                _.flatten(
                    _.map(cukeDirectories, function (dir) {
                        if (fs.lstatSync('cukes/' + dir).isDirectory() && (dir !== '.git')) {
                            return summarise_cucumber_tests_deep_promise('cukes/' + dir, dir);
                        } else {
                            return [];
                        }
                    }).concat(
                        _.map(junitDirectories, function (dir) {
                            if (fs.lstatSync('junit/' + dir).isDirectory() && (dir !== '.git')) {
                                return summarise_junit_tests_promise('junit/' + dir, dir);
                            } else {
                                return [];
                            }
                        })
                    )
                )
            );
        })
        .then(function (results) {
            //console.log('next step, results = ' + results);
            //there shouldn't be any tags against steps?  so no need to convert?
            if (DEBUG); //console.log("summarised:" + JSON.stringify(results, null, '  '));
            return Qfs.write("json/testing-summary-deep.json", JSON.stringify(results, null, '\t'))
                .then(function (writeResult) {
                    //before we export to xml the tags need to be converted to xml 'attributes'
                    var results2 = _.map(results, function (job) {
                        return {
                            job: replaceSpecialChars(job.job),
                            features: _.map(job.features, function (feature) {
                                return {
                                    featureName: replaceSpecialChars(feature.name),
                                    featureFile: replaceSpecialChars(feature.file),
                                    "@": {
                                        featureTags: feature.tags ? replaceSpecialChars(feature.tags.join(',')) : ""
                                    },
                                    scenarios: _.map(feature.scenarios, function (scenario) {
                                        return {
                                            scenarioName: replaceSpecialChars(scenario.name),
                                            result: replaceSpecialChars(scenario.result),
                                            "@": {
                                                scenarioTags: scenario.tags ? replaceSpecialChars(scenario.tags.join(',')) : ""
                                            },
                                            steps: _.map(scenario.steps, function (step) {
                                                return {
                                                    stepName: replaceSpecialChars(step.step),
                                                    result: replaceSpecialChars(step.result)
                                                };
                                            })
                                        };
                                    })
                                };
                            })
                        };
                    });
                    console.log('about to parse json/testing-summary-deep.xml');
                    return Q.all(_.map(results2, function(job) {
                        return Qfs.write(
                            "json/" + job.job.split(' ').join('') + '-deep.xml', 
                            js2xmlparser.parse("testingSummary", job.features));
                    }))
                    .then(function(writeResults) {
                        return Qfs.write("json/testing-summary-deep.xml", js2xmlparser.parse("testingSummary", results2));
                    });
                });
        });
    
    function replaceSpecialChars(name) {
        if (DEBUG);// console.log('replaceSpecialChars(' + name + ')');
        return (name === '') || !name ? '_blank_' : name
            //.replace(/[`¬|¦!"£$%^&*()+={[\]};@'~#<,>?\/]/g, '_');
            .replace(/&/g, 'and')
            .replace(/\\n/g, 'N')
            .replace(/\\t/g, 'T')
            .replace(/\\u0000/g, 'U')
            .replace(/\n/g, 'N')
            .replace(/\r/g, 'R')
            //.replace(/\t/g, 'T')
            .replace(/\u0000/g, 'U')
            .replace(/`"'/g, '~');
    }
}

function fetch_and_parse_cuke_pooled(view, hostname) {
    //console.log('fetch_and_accumulate_reviews_pooled');
    //split the id's out into n different batches, or just 1 if there are less than n items
    var n = 5;
    var items = _.map(view);
    if (items.length <= n) {
        //console.log('small number so proceed as a single batch');
        return fetch_and_parse_cuke_acc(items, hostname);
    } else {
        //console.log('items = ' + items.length);
        //console.log('splitting into batches, n = ' + n);
        var batch = Math.floor(items.length / n);
        //console.log('batch = ' + batch);
        var batches = [];
        var tail = items;
        var head = [];
        //allocate n-1 batches and the remainder goes in the last batch
        for (var x = 0; x < n - 1; x++) {
            head = _.first(tail, batch);
            tail = _.difference(tail, head);
            batches.push(head);
        }
        batches.push(tail);
        //console.log(batches);

        return Q.all(_.map(batches, function (batch) {
                return fetch_and_parse_cuke_acc(batch, hostname);
            }))
            .then(function (results) {
                //console.log(JSON.stringify(results));
                //console.log('flatten the results');
                return _.flatten(results);
            });
    }
}

function fetch_and_parse_cuke_acc(items, hostname) {
    //console.log('fetch_and_parse_cuke_acc');
    //console.log('creating array of promises');
    var dots = '.';
    var arrayOfPromises = _.map(items, function (cukeParams) {
        dots += '.';
        return fetch_and_parse_cuke_acc_function_promise(cukeParams, hostname);
    });
    //console.log(dots);

    //convert the array of promises into a sequence of thens...
    return arrayOfPromises.reduce(function (soFar, f) {
        return soFar.then(f);
    }, Q([]));
}

function fetch_and_parse_cuke_acc_function_promise(cukeParams, hostname) {
    //console.log('fetch_junit_xml_function_promise');
    return function (resultsSoFar) {
        return fetch_and_parse_cuke_acc_promise(resultsSoFar, cukeParams, hostname);
    };
}

function fetch_and_parse_cuke_acc_promise(resultsSoFar, cukeParams, hostname) {
    return fetch_and_parse_cuke(cukeParams, hostname, cukeParams.filterArray)
        .then(function (results) {
            //console.log('push the results onto results so far');
            resultsSoFar.push(results);
            return resultsSoFar;
        });
}

function createGroupedResults(jobsObj) {
    //(2) for each grouped view, for each cuke item,
    //    we want to fetch the cucumber json, filtering for the tags passed in 
    //    and then condense this into passed/failed stats
    //    this combined step can be done in parallel
    return Q.all(_.map(jobsObj.tree, function (view, viewName) {
            //console.log('mapping each view, viewName=' + viewName);
            //mapping each view, e.g. "quality-owners-flat-rest"
            //do I wan't to keep nodes/sub-nodes with no results?
            return fetch_and_parse_cuke_pooled(view, jobsObj.hostname)
                //            return Q.all(_.map(view, function (cukeParams) {
                //                    //mapping each cuke-file (job and path combination): 
                //                    //  { "name": "Rest AS - F161",
                //                    //    "path": "functional-tests/target/cucumber.json",
                //                    //    "resultsStyle": "cucumber",
                //                    //    "jenkinsViewName": "something",
                //                    //    "jenkinsJobName": "something",
                //                    //    "stickyGroups": true,
                //                    //    "filterArray": [ 
                //                    //      { "tags": [ "@qualityOwnerDocuments" ],
                //                    //         "displayName": "Documents" },
                //                    //      {...} ]
                //                    //  }
                //
                //                    //collapse
                //                    return fetch_and_parse_cuke(cukeParams, jobsObj.hostname, cukeParams.filterArray);
                //                }))
                .then(function (jobResultsArray) {
                    //(3) aggregate cuke results from different cucumber files for the same run (job name)
                    if (DEBUG) {
                        console.log('--(3) received jobs totals:' + viewName);
                        //console.log(JSON.stringify(jobResultsArray, null, "  "));
                    }
                    //at this point we have a list of cukes (job+path) and within each the totals broken out by filter category, e.g.:
                    //  [ {
                    //      "name": "Rest API - F153",
                    //      "path": "cucumber-json-reports/cucumber162.json",
                    //      "jenkinsViewName": "Some View Name",
                    //      "jenkinsJobName": "Some Job Name",
                    //      "totals": {
                    //        "other": {"scenariosPassed": 2153, "scenariosFailed": 5, "scenariosMissing": 1},
                    //        "Documents": {"scenariosPassed": 40, "scenariosFailed": 14, "scenariosMissing": 4},
                    //        ... },
                    //    {...} ]
                    //
                    //we want to group this out by filter category and then sum the totals therein (across multiple cuke files)
                    //spin through the array, spin through the items in totals, produced the summed totals for each category
                    var result = {};
                    result[viewName] = _.reduce(jobResultsArray, function (aggregatedResults, cukeResults) {
                        //(i) spin through aggregated totals, if there is matching node in this cuke's results then sum them
                        aggregatedResults = _.mapObject(aggregatedResults, function (item, key) {
                            if ((cukeResults.totals) && (cukeResults.totals[key])) {
                                if (!item.failedScenarios) {
                                    console.log("whey-hey!");
                                }
                                return {
                                    passed: item.passed + cukeResults.totals[key].scenariosPassed,
                                    failed: item.failed + cukeResults.totals[key].scenariosFailed,
                                    missing: item.missing + cukeResults.totals[key].scenariosMissing,
                                    info: item.info,
                                    isRunning: item.isRunning,
                                    failedScenarios: item.failedScenarios.concat(cukeResults.totals[key].failedScenarios)
                                };
                            } else {
                                return item;
                            }
                        });

                        var associatedViewName = cukeResults.jenkinsViewName;
                        var associatedJobName = cukeResults.jenkinsJobName;
                        //(ii) spin through cukeResult.totals, if there is a node that is missing from the aggregated restuls then add it in
                        _.each(cukeResults.totals, function (item, key) {
                            //console.log("item - each cukeResults.totals:" + JSON.stringify(item, null, "  "));
                            if (!aggregatedResults[key]) {
                                aggregatedResults[key] = {
                                    passed: item.scenariosPassed,
                                    failed: item.scenariosFailed,
                                    missing: item.scenariosMissing,
                                    isRunning: (item.isRunning === true),
                                    failedScenarios: item.failedScenarios
                                };
                                aggregatedResults[key].info = {};
                                aggregatedResults[key].info.jenkinsViewName = associatedViewName;
                                aggregatedResults[key].info.jenkinsJobName = associatedJobName;
                            }
                        });

                        return aggregatedResults;
                    }, {
                        // Other: {
                        //   passed: 0,
                        //   failed: 0,
                        //   missing: 0
                        // }
                    });
                    return result;
                });
        }))
        .then(function (viewsResults) {
            //(4) convert the array of result objects into an object with properties
            if (DEBUG) {
                console.log('\n\n--(4b) convert views array of objects into a map of view name against object holding jobs against their totals:');
                //console.log(JSON.stringify(viewsResults, null, "  "));
            }
            //convert data of the form:
            //[
            //  {
            //    "quality-owners-flat": {
            //      "other": {
            //        "passed": 14331,
            //        "failed": 272,
            //        "missing": 7
            //      },
            //..to data of the form
            //  {
            //    "quality-owners-flat": {
            //      "other": {
            //        "passed": 14331,
            //        "failed": 272,
            //        "missing": 7
            //      },
            return _.reduce(viewsResults, function (memo, item) {
                var key = Object.keys(item)[0];
                memo[key] = item[key];
                return memo;
            }, {});
        })
        .then(function (viewsResults) {
            //now have results of the form:
            //{"quailit-owners-flat-rest": 
            //  {"other": {"passed": 123, "failed": 123, "missing":123},
            //   "Documents:F153": {"passed": 123, "failed": 123, "missing":123},
            //   "Documents:F154": {"passed": 123, "failed": 123, "missing":123} } }
            //want to turn this into:
            //{"quality-owners-flat-rest":
            //    {"other": {
            //        "results": {"passed": 123, "failed": 123, "missing": 123}}},
            //    {"Documents": {
            //        "results": {"passed": 123, "failed": 123, "missing": 123}}
            //        "sub-items": {
            //            "F153": {"results": {"passed": 123, "failed": 123, "missing": 123}},
            //            "F154": {"results": {"passed": 123, "failed": 123, "missing": 123}} 
            //       } 
            //    }
            //}
            //(i)Spin through each item in view, if it has ":" then:
            //..mark it out as a sub item with parent name and sub-item-name
            //..else return it with parent name = name and no sub-item-name
            var result = _.mapObject(viewsResults, function (view) {
                return _.map(view, function (item, key) {
                    if (key.includes(":")) {
                        var names = key.split(":");
                        return {
                            "parent-name": names[0],
                            "sub-item-name": names[1],
                            results: item
                        };
                    } else {
                        return {
                            "parent-name": key,
                            results: item
                        };
                    }
                });
            });
            //console.log("result-1- = " + JSON.stringify(result, null, "  "));

            //(ii)group by parent name to give:
            // { "parent name": [{whole object as is}], "diff parent name": [{whole object}, {whole object}]}
            result = _.mapObject(result, function (view) {
                return _.groupBy(view, 'parent-name');
            });
            //console.log("result-2- = " + JSON.stringify(result, null, "  "));

            //(iii) map this structure into the structure I want, aggregating totals 
            // and sticking results in result property as I go
            result = _.mapObject(result, function (view) {
                return _.mapObject(view, function (itemArray) {
                    //console.log("itemArray" + JSON.stringify(itemArray, null, "  "));
                    if ((itemArray.length === 1) && !(itemArray[0]["sub-item-name"])) {
                        return {
                            results: itemArray[0].results
                        };
                    } else {
                        //console.log('createGroupedResults, map the structure, itemArray=' + JSON.stringify(itemArray, null, ' '));
                        return _.reduce(itemArray, function (memo, subItem) {
                            //console.log(JSON.stringify(memo, null, "  "));
                            if (!(memo["sub-items"])) {
                                memo["sub-items"] = {};
                            }
                            memo["sub-items"][subItem["sub-item-name"]] = {
                                results: subItem.results
                            };
                            if (!memo.results.failedScenarios) {
                                console.log("but, but, but...!");
                            }
                            memo.results = {
                                passed: memo.results.passed + subItem.results.passed,
                                failed: memo.results.failed + subItem.results.failed,
                                missing: memo.results.missing + subItem.results.missing,
                                isRunning: memo.results.isRunning || subItem.results.isRunning,
                                failedScenarios: memo.results.failedScenarios.concat(subItem.results.failedScenarios)
                            };
                            return memo;
                        }, {
                            results: {
                                passed: 0,
                                failed: 0,
                                missing: 0,
                                isRunning: false,
                                failedScenarios: []
                            }
                        });
                    }
                });
            });
            //console.log("result-3- = " + JSON.stringify(result, null, "  "));
            return result;
        })
        .then(function (viewsResultsArray) {
            if (DEBUG) {
                console.log("\n\n---view results array:---\n\n");
                //console.log(JSON.stringify(viewsResultsArray, null, "  "));
            }
            return viewsResultsArray;
        });
}

function createFlatResults(jobsObj) {
    //console.log('createFlatResults');
    //(2) for each, we want to fetch the cucumber json and condense this into passed/failed stats
    //    this combined step can be done in parallel
    return Q.all(_.map(jobsObj.jobsArray, function (job) {
            return fetch_and_parse_cuke(job, jobsObj.hostname);
        }))
        .then(function (jobResultsArray) {
            //(3) aggregate cuke results from different cucumber files for the same run (job name)
            if (DEBUG) {
                console.log('--(3-flat) received jobs totals:');
                //console.log(JSON.stringify(jobResultsArray, null, "  "));
            }
            var groupedByName = _.groupBy(jobResultsArray, 'name');
            return _.map(groupedByName, function (runResults) {
                var reduced = _.reduce(runResults, function (runTotals, runResult) {
                    if (runResult.totals) {
                        if (!runTotals.failedScenarios) {
                            console.log("whoa there boy!");
                        }
                        return {
                            passed: runTotals.passed + runResult.totals.Other.scenariosPassed,
                            failed: runTotals.failed + runResult.totals.Other.scenariosFailed,
                            missing: runTotals.missing + runResult.totals.Other.scenariosMissing,
                            isRunning: runTotals.isRunning || runResult.totals.Other.isRunning,
                            failedScenarios: runTotals.failedScenarios.concat(runResult.totals.Other.failedScenarios)
                        };
                    } else {
                        return runTotals; //was runResult;
                    }
                }, {
                    passed: 0,
                    failed: 0,
                    missing: 0,
                    isRunning: false,
                    failedScenarios: []
                });
                return {
                    name: runResults[0].name,
                    totals: reduced
                };
            });
        })
        .then(function (results) {
            //(4) convert the array of result objects into an object with properties for each job 
            // holding an object of passed, failed and missing totals for that job
            if (DEBUG) {
                console.log('--(4-flat) convert array of objects into a map of job name against totals:');
                //console.log(results);
            }
            var result = {};
            _.each(results, function (jobObj) {
                result[jobObj.name] = jobObj.totals;
            });
            return result;
        });
}

function writeOutStatus(connectionStatus) {
    update_status(connectionStatus, 'error');
}