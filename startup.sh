#useage: make sure shell script has execute rights: chmod 777 startup.sh
#        run, might have to include directory: ./startup.sh
#nvm use
#node start
# forever stopall
forever list
forever stop code-owners.js
forever stop fetch-view-results.js
forever stop fetch-cuke-results.js
forever stop fetch-timings-results.js
forever stop fetch-test-history.js
forever stop fetch-failing-tests.js
forever stop fetch-and-update-test-results-history.js
forever stop fetch-code-changes-and-reviews.js
forever stop fetch-and-run-schedule.js
#forever stop fetch-all-incident-changes.js
rm -rf /home/agordon/.forever/*
forever start code-owners.js
forever start fetch-view-results.js
forever start fetch-cuke-results.js
#forever start fetch-timings-results.js
forever start fetch-test-history.js
#forever start fetch-failing-tests.js
forever start fetch-and-update-test-results-history.js
forever start fetch-code-changes-and-reviews.js
forever start fetch-and-run-schedule.js
#forever start fetch-all-incident-changes.js
forever list