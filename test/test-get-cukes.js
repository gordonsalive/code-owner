/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
/*global it, describe*/

var test = require('unit.js');
var Q = require('q');
//Q.longStackSupport = true;

var get_cukes = require('../scripts/get-cukes');

//to run use :> mocha test/test-get-cukes.js, or just :> mocha to run all tests
// -w to watch for changes

describe('Getting all cukes, ', function () {
    var C_CUKES_FILE = './test/test-cukes.json';

    it('should bring back an object', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .then(function (cukes) {
                //test.undefined(codeOwners['ERROR']);
                test.value(cukes).exists();
                cukes.should.be.ok; //should be truthy
                cukes.should.not.be.String;
                cukes.should.be.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have the correct fields: jenkins-root, local, jenkinsJobs and code-owners-tree', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .then(function (cukes) {
                test.value(cukes).exists();
                cukes['jenkins-root'].should.be.a.String
                    .and.equal('.'); //would normally be jenkins.jhcllp.local/..., but for test we will get cukes from a local directory
                cukes.local.should.be.a.Boolean
                    .and.equal(true); //would normally be false, but for test we will pick up cukes locally
                cukes.jenkinsJobs.should.be.an.Object;
                cukes['code-owners-tree'].should.be.an.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have jenkinsJobs correctly set up with list of jenkins jobs and the paths to the cucumber json files in their work spaces', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes.jenkinsJobs;
            })
            .then(function (jenkinsJobs) {
                test.value(jenkinsJobs).exists();
                jenkinsJobs.items.should.be.an.Array
                    .and.containEql('neon-as-f63')
                    .and.have.length(3);
                jenkinsJobs.cukes.should.be.an.Object;
                jenkinsJobs.cukes['neon-as-f63'].job.should.be.a.String
                    .and.equal('Neon AS - F63');
                jenkinsJobs.cukes['neon-as-f63'].paths.should.be.an.Array
                    .and.containEql('cukes/cuke2')
                    .and.have.length(2);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have code-owners-tree correctly set up with views: quality-owners and code-owners', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree'];
            })
            .then(function (codeOwnersTree) {
                test.value(codeOwnersTree).exists();

                codeOwnersTree.items.should.be.an.Array
                    .and.containEql('quality-owners')
                    .and.containEql('code-owners')
                    .and.have.length(2);
                codeOwnersTree['quality-owners'].should.be.an.Object;
                codeOwnersTree['quality-owners'].type.should.equal('view');
                codeOwnersTree['code-owners'].should.be.an.Object;
                codeOwnersTree['code-owners'].type.should.equal('view');
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have code-owners-tree correctly set up with projects in quality-owners view', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['quality-owners'];
            })
            .then(function (qualityOwnersView) {
                test.value(qualityOwnersView).exists();

                qualityOwnersView.items.should.be.an.Array
                    .and.containEql('neon-as-f63')
                    .and.have.length(3);
                qualityOwnersView['neon-as-f63'].should.be.an.Object;
                qualityOwnersView['neon-as-f63'].type.should.equal('project');
                qualityOwnersView['neon-as-f63'].jenkinsJobs.should.an.Array
                    .and.containEql('neon-as-f63')
                    .and.have.length(1);
                qualityOwnersView['neon-as-f63'].tags.should.an.Array
                    .and.containEql('@zinc')
                    .and.have.length(2);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have code-owners-tree correctly set up with specialistions in code-owners view', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['code-owners'];
            })
            .then(function (codeOwnersView) {
                test.value(codeOwnersView).exists();

                codeOwnersView.items.should.be.an.Array
                    .and.containEql('delphi')
                    .and.containEql('rpg')
                    .and.have.length(3);
                codeOwnersView.delphi.should.be.an.Object;
                codeOwnersView.delphi.type.should.equal('specialisation');
                codeOwnersView.rpg.should.be.an.Object;
                codeOwnersView.rpg.type.should.equal('specialisation');
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have code-owners-tree correctly set up with components in the delphi and rpg specialisations in the code-owners view, with jenkinsJobs and tags set up on rpg.fees', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                var codeOwnersView = cukes['code-owners-tree']['code-owners'];
                return {
                    delphiSpecialisation: codeOwnersView.delphi,
                    rpgSpecialisation: codeOwnersView.rpg
                };
            })
            .then(function (specialisations) {
                var delphiSpecialisation = specialisations.delphiSpecialisation;
                var rpgSpecialisation = specialisations.rpgSpecialisation;

                test.value(delphiSpecialisation).exists();
                test.value(rpgSpecialisation).exists();

                delphiSpecialisation.items.should.be.an.Array
                    .and.containEql('orderManagement')
                    .and.have.length(5);
                delphiSpecialisation.orderManagement.should.be.an.Object;
                delphiSpecialisation.orderManagement.type.should.equal('component');

                rpgSpecialisation.items.should.be.an.Array
                    .and.containEql('fees')
                    .and.have.length(1);
                rpgSpecialisation.fees.should.be.an.Object;
                rpgSpecialisation.fees.type.should.equal('component');
                rpgSpecialisation.fees.jenkinsJobs.should.be.an.Array
                    .and.containEql('fees-f63')
                    .and.have.length(1);
                rpgSpecialisation.fees.tags.should.be.an.Array
                    .and.containEql('@fees')
                    .and.containEql('@cap(fees)')
                    .and.have.length(2);
                done();

            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should have code-owners-tree correctly set up with sub-component rpgTransactions in orderManagement in delphi in the code-owners view, with jenkinsJobs and tags set up', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['code-owners'].delphi.orderManagement;
            })
            .then(function (orderManagementComponent) {
                test.value(orderManagementComponent).exists();

                orderManagementComponent.items.should.be.an.Array
                    .and.containEql('rpgTransactions')
                    .and.have.length(1);
                orderManagementComponent.rpgTransactions.should.be.an.Object;
                orderManagementComponent.rpgTransactions.type.should.equal('sub-component');
                orderManagementComponent.rpgTransactions.jenkinsJobs.should.be.an.Array
                    .and.containEql('delphi-transactions-f63')
                    .and.have.length(1);
                orderManagementComponent.rpgTransactions.tags.should.be.an.Array
                    .and.containEql('@fot')
                    .and.containEql('@cap(fot)')
                    .and.have.length(2);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('where a node does not have cuke info defined, if all its sub-nodes do, it should infer it correctly (rpg node in test data)', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['code-owners'].rpg;
            })
            .then(function (rpgSpecialisation) {
                test.value(rpgSpecialisation).exists();
                test.value(rpgSpecialisation.jenkinsJobs).exists();
                test.value(rpgSpecialisation.tags).exists();

                rpgSpecialisation.jenkinsJobs.should.be.an.Array
                    .and.containEql('fees-f63')
                    .and.have.length(1);
                rpgSpecialisation.tags.should.be.an.Array
                    .and.containEql('@fees')
                    .and.containEql('@cap(fees)')
                    .and.have.length(2);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('where a node does not have cuke info defined, if all its sub-nodes do, it should infer it correctly (quality-owners node in test data)', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['quality-owners'];
            })
            .then(function (qualityOwnersView) {
                test.value(qualityOwnersView).exists();
                test.value(qualityOwnersView.jenkinsJobs).exists();
                test.value(qualityOwnersView.tags).exists();

                qualityOwnersView.jenkinsJobs.should.be.an.Array
                    .and.containEql('delphi-transactions-f63')
                    .and.have.length(3);
                qualityOwnersView.tags.should.be.an.Array
                    .and.containEql('@fot')
                    .and.have.length(4);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('where a node does not have cuke info defined and not all its sub-nodes do, it should not infer it (code-owners node in test data)', function (done) {
        test.promise
            .given(get_cukes(C_CUKES_FILE))
            .when(function (cukes) {
                return cukes['code-owners-tree']['code-owners'];
            })
            .then(function (codeOwnersView) {
                test.value(codeOwnersView).exists();
                test.undefined(codeOwnersView.jenkinsJobs);
                test.undefined(codeOwnersView.tags);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });
});