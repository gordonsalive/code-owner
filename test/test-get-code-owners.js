/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
/*global it, describe*/

//TODO: add jira-backlog and notes to the code owners data.

var test = require('unit.js');
var Q = require('q');
//Q.longStackSupport = true;

var get_code_owners = require('../scripts/get-code-owners');

//to run use :> mocha test/test-get-code-owners.js, or just :> mocha to run all tests
// -w to watch for changes

describe('Getting all code owners, ', function () {
    var C_VIEW = 'view';
    var C_SPEC = 'specialisation';
    var C_COMP = 'component';
    var C_SUB_COMP = 'sub-component';
    var C_CODE_OWNERS_FILE = './test/test-code-owners.json';

    it('should bring back an object', function (done) {
        //var views;
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE)) //returns a promise
            .then(function (views) {
                test.value(views).exists();
                views.should.be.ok; //should be truthy
                views.should.not.be.String;
                views.should.be.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should correctly contain view "code-owners", with the correct fields', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE)) //returns a promise
            .when(function (views) {
                return views["code-owners"];
            })
            .then(function (codeOwners) {
                test.value(codeOwners).exists();
                codeOwners.type.should.be.a.String
                    .and.equal(C_VIEW);
                codeOwners.description.should.be.a.String
                    .and.equal('Code Owner View');
                codeOwners.owners.should.be.an.Array
                    .and.not.be.empty
                    .and.containEql({
                        "name": "Alan Gordon"
                    })
                    .and.have.length(1); //length updates the object to number, so must come last
                codeOwners.items.should.be.an.Array
                    .and.containEql("rpg")
                    .and.have.length(3);
                codeOwners.rpg.should.be.an.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should correctly contain view "code-owners", with specialism "delphi", with the correct fields', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE)) //returns a promise
            //.given(views = get_code_owners(C_CODE_OWNERS_FILE))
            .when(function (views) {
                test.value(views).exists();
                var codeOwners = views['code-owners'];
                test.value(codeOwners).exists();
                return codeOwners.delphi;
            })
            .then(function (delphiSpec) {
                test.value(delphiSpec).exists();
                delphiSpec.type.should.be.a.String
                    .and.equal(C_SPEC);
                delphiSpec.description.should.be.a.String
                    .and.equal('Delphi');
                delphiSpec.owners.should.be.an.Array
                    .and.not.be.empty
                    .and.containEql({
                        "name": "Mark Humphreys"
                    })
                    .and.have.length(1); //length updates the object to number, so must come last
                delphiSpec.items.should.be.an.Array
                    .and.containEql("appLauncher")
                    .and.have.length(4);
                delphiSpec.appLauncher.should.be.an.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should correctly contain view "code-owners", with specialism "delphi", with component "appLaucher", with correct fields', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE)) //returns a promise
            .when(function (views) {
                test.value(views).exists();
                var codeOwners = views['code-owners'];
                test.value(codeOwners).exists();
                var delphiSpec = codeOwners.delphi;
                test.value(delphiSpec).exists();
                return delphiSpec.appLauncher;
            })
            .then(function (appLauncherComp) {
                test.value(appLauncherComp).exists();
                appLauncherComp.type.should.be.a.String
                    .and.equal(C_COMP);
                appLauncherComp.description.should.be.a.String
                    .and.equal("App Launcher");
                appLauncherComp.owners.should.be.an.Array
                    .and.be.empty;
                appLauncherComp.items.should.be.an.Array
                    .and.containEql("application")
                    .and.have.length(1);
                appLauncherComp.application.should.be.an.Object;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should correctly contain view "code-owners", with specialism "delphi", with component "appLaucher", with sub-component "application", with correct fields', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE)) //returns a promise
            .when(function (views) {
                get_code_owners(C_CODE_OWNERS_FILE);
                test.value(views).exists();
                var codeOwners = views['code-owners'];
                test.value(codeOwners).exists();
                var delphiSpec = codeOwners.delphi;
                test.value(delphiSpec).exists();
                var appLauncherComp = delphiSpec.appLauncher;
                test.value(appLauncherComp).exists();
                return appLauncherComp.application;
            })
            .then(function (subComp) {
                test.value(subComp).exists();
                subComp.type.should.be.a.String
                    .and.equal(C_SUB_COMP);
                subComp.description.should.be.a.String
                    .and.equal("Application");
                subComp.owners.should.be.an.Array
                    .and.be.empty;
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });
});

describe('Getting a sub-branch of the code owners tree, ', function () {
    var C_SPEC = 'specialisation';
    var C_COMP = 'component';
    var C_SUB_COMP = 'sub-component';
    var C_CODE_OWNERS_FILE = './test/test-code-owners.json';

    it('should bring back a whole specialisation', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE, 'delphi', C_SPEC)) //returns a promise
            .then(function (codeOwners) {
                test.value(codeOwners).exists();
                codeOwners.type.should.equal(C_SPEC);
                codeOwners.description.should.equal('Delphi');
                codeOwners.items.should.have.length(4);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should bring back a whole component', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE, 'bargainList', C_COMP)) //returns a promise
            .then(function (codeOwners) {
                test.value(codeOwners).exists();
                codeOwners.type.should.equal(C_COMP);
                codeOwners.description.should.equal('Bargain List');
                codeOwners.items.should.have.length(1);
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });

    it('should bring back a whole sub-component:application', function (done) {
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE, 'application', C_SUB_COMP)) //returns a promise
            .then(function (codeOwners) {
                test.value(codeOwners).exists();
                codeOwners.type.should.equal(C_SUB_COMP);
                codeOwners.description.should.equal('Application');
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();

    });

    it('should bring back a whole sub-component, from the json of component: bargainList', function (done) {
        //pass in the bargainList node
        test.promise
            .given(get_code_owners(C_CODE_OWNERS_FILE, 'bargainList', C_COMP)) //returns a promise
            .when(function (bargainList) {
                test.value(bargainList).exists();
                return get_code_owners(bargainList, 'application', C_SUB_COMP);
            })
            .then(function (codeOwners) {
                test.value(codeOwners).exists();
                codeOwners.type.should.equal(C_SUB_COMP);
                codeOwners.description.should.equal('Application');
                done();
            })
            .catch(function (err) {
                test.fail(err.message);
                done(err);
            })
            .done();
    });
});