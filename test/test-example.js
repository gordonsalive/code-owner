var test = require('unit.js');

describe('Learning by the example', function () {

    it('example variable', function () {

        // just for example of tested value
        var example = 'hello world';
        test.string(example)
            .startsWith('hello')
            .match(/[a-z]/)
            .given(example = 'you are welcome')
            .string(example)
            .endsWith('welcome')
            .contains('you')
            .when('"example" becomes an object', function () {
                example = {
                    message: 'hello world',
                    name: 'Nico',
                    job: 'developper',
                    from: 'France'
                };
            })
            .then('test the "example" object', function () {
                test.object(example)
                    .hasValue('developper')
                    .hasProperty('name')
                    .hasProperty('from', 'France')
                    .contains({
                        message: 'hello world'
                    });
            })
            .if(example = 'bad value')
            .error(function () {
                example.badMethod();
            });
        //
        test.given(1 == 1).assert(1 == 1)
        test.assert(1 == 1);
        //
        var example = 'hello';
        test
            .string(example)
            .startsWith('hello')

        .given(example = 'bye')
            .string(example)
            .endsWith('bye')

        .when('some description', function () {
            //do something
        })

        .then('some test', function () {
                test
                    .string(example)
                    .startsWith('bye')
                    //can use contains to check for a field on an object
                    //e.g. .contains({message: 'hello world'})
                    //see http://unitjs.com/guide/quickstart.html
                ;
            })
            //can also handle bad data returned with an if().error()
        ;

    });
    it('example of using should from import of unit.js', function () {
        // other tests ...

        // test 'string' type
        test.should('foobar').be.type('string');
        // then that actual value '==' expected value
        test.should('foobar' == 'foobar').be.ok;
        // then that actual value '===' expected value
        test.should('foobar').be.equal('foobar');
        // Should.js library (alternative style)
        var should = test.should;
        // test 'string' type
        ('foobar').should.be.type('string');
        // then that actual value '==' expected value
        ('foobar' == 'foobar').should.be.ok;
        // then that actual value '===' expected value
        ('foobar').should.be.equal('foobar');
        // this shortcut works also like this
        // test 'string' type
        should('foobar').be.type('string');
        // then that actual value '==' expected value
        should('foobar' == 'foobar').be.ok;
        // then that actual value '===' expected value
        should('foobar').be.equal('foobar');
    });
    it('example of using should', function () {
        // test 'string' type
        test.should('foobar').be.type('string');
        // then that actual value '==' expected value
        test.should('foobar' == 'foobar').be.ok;
        // then that actual value '===' expected value
        test.should('foobar').be.equal('foobar');
        // Should.js library (alternative style)
        var should = test.should;
        // test 'string' type
        ('foobar').should.be.type('string');
        // then that actual value '==' expected value
        ('foobar' == 'foobar').should.be.ok;
        // then that actual value '===' expected value
        ('foobar').should.be.equal('foobar');
        // this shortcut works also like this
        // test 'string' type
        should('foobar').be.type('string');
        // then that actual value '==' expected value
        should('foobar' == 'foobar').be.ok;
        // then that actual value '===' expected value
        should('foobar').be.equal('foobar');
    });
    it('example of using should promises', function (done) {
        //TODO: correct this example (it's wrong this is a synch function)
        //done is to return to Mocha, Jasmine and other runners
        function asyncFunction() {
            console.log('<an asynch function...>');
        }
        test.promise
            .given(asyncFunction)
            .when(function () {
                console.log('<when>');
            })
            .then(function (value) {
                // ...
            })
            .catch(function (err) {
                test.fail(err.message);
            })
            .finally(done) //finally done is in the cast of a catch error, else .done()
            .done();
    });
});