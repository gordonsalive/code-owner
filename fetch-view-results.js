/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

var http = require('http');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var DEBUG = 0; //0 = off

var update_status = require('./scripts/update-status');

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;

var resultsJson = {
    items: ['someViewName'],
    someViewName: {
        jobs: [
            {
                "name": "Some Job Name",
                "color": "blue",
                "lastSuccessfulBuild": {
                    "id": "2015-12-18_14-10-05"
                },
                "lastUnsuccessfulBuild": null
            }
        ]
    }
};

writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    //console.log('--parsefile..');
    Qfs.read('json/cukes.json')
        .then(function (cukesFile) {
            //console.log('--parse file..');
            //parse the file into json.
            var cukesJenkins = JSON.parse(cukesFile).jenkins;
            //console.log('--parsefile..complete.');
            return {
                viewsArray: _.map(cukesJenkins.views, function (view) {
                    return view;
                }),
                hostname: cukesJenkins.hostname
            };
        })
        .then(function (viewsObj) {
            //console.log('--fetch views from jenkins..');
            //console.log(viewsObj);
            //for each view, fetch the json from the jenkins server
            return Q.all(_.map(viewsObj.viewsArray, function (view) {
                return Q.Promise(function (resolve, reject, notify) {
                    var allOfBody = '';

                    var path = '/view/' + encodeURI(view.name) + '/api/json?tree=jobs[name,color,lastSuccessfulBuild[id],lastUnsuccessfulBuild[culprits[fullName,id],actions[claimedBy]]]';

                    var options = {
                        hostname: viewsObj.hostname,
                        port: 80,
                        path: path,
                        method: 'GET',
                        auth: userAndToken,
                        agent: false
                    };

                    //console.log(options);

                    var req = http.request(options, function (res) {
                        //console.log('STATUS: ' + res.statusCode);
                        if (res.statusCode != 200) {
                            writeOutStatus('failed');
                            console.log('--failed: ' + options.method + ' ' + options.hostname + options.path + ':' + options.port);
                            reject(new Error("Status code was " + res.statusCode));
                        } else {
                            res.setEncoding('utf8');
                            res.on('data', function (chunk) {
                                allOfBody = allOfBody + chunk;
                            });
                            res.on('end', function () {
                                //console.log('--fetch views from jenkins..complete.');
                                resolve({
                                    view: view,
                                    body: JSON.parse(allOfBody)
                                });
                            });
                        }
                    });
                    req.on('error', function (e) {
                        reject(e);
                    });
                    // write data to request body
                    req.write(' ');
                    req.end();
                });
            }));
        })
        .then(function (resultArray) {
            //console.log('--convert results to object..');
            //console.log(resultArray);
            //convert the array of results into our finished object
            var resultObject = {};
            _.each(resultArray, function (result) {
                resultObject[result.view.name] = result.body;
                resultObject[result.view.name].displayName = result.view.displayName;
            });
            //console.log('--convert results to object..complete.');
            return resultObject;
        })
        .then(function (resultObject) {
            //console.log('--write out results file..');
            //console.log(resultObject);
            //write out the results file
            return Qfs.write("json/view-results.json", JSON.stringify(resultObject, null, '\t'));
        })
        .then(function (result) {
            //console.log('--update status..');
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            //console.log('..repeat in 30 seconds..');
            /*repeat in 30 seconds*/
            setTimeout(doMainStuff, 30 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 5 minutes');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 300 * 1000); //try again in 5 minutes
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'status');
}