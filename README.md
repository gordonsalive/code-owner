# code-owners
Practice project of a web page showing code owners and test coverage

Shows:

* tests passing/tests failing for Jenkins cucumber test projects (and junit)
* same but grouped up by tags in to groups and sub-groups, configurable
* test history (total, passing and failing scenarios)
* pie chart of project test counts out of total
* test stability - weighted %age of test projects stable (i.e. all test passing at 11:59pm)
* failing scenarios - which test scenarios are failing and how long they've been failing for
* Also timings data for DBPOPULATE scripted data jobs
