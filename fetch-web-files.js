/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var http = require('http');
var fs = require('fs');
//var Q = require('q');
//var _ = require('underscore');

var hostname = '172.26.27.188';
var port = 8084;

var folder = '/json/';
var fileExt = '.xml';

var pathArray = [
    'ClientReportsAS-CoreLive', 'ClientReportsAS-CoreLive-deep',
    'DelphiTransactions-CoreLive', 'DelphiTransactions-CoreLive-deep',
    'Fees-CoreLive', 'Fees-CoreLive-deep',
    'NeonAS-CoreLive', 'NeonAS-CoreLive-deep',
    'PortfolioManagement-CoreLive', 'PortfolioManagement-CoreLive-deep',
    'R-and-PAS-CoreLive', 'R-and-PAS-CoreLive-deep',
    'RestAPI(dbpopulate)-F63Live', 'RestAPI(dbpopulate)-F63Live-deep',
    'TransactionReporting-CoreLive', 'TransactionReporting-CoreLive-deep'
].map(item => folder + item + fileExt);

//for each item, create a request, provide handlers and send it
pathArray.map(function (path) {
    var allOfBody = '';

    var options = {
        hostname: hostname,
        port: port,
        path: path,
        method: 'POST',
        //auth: userAndToken,
        agent: false,
        timeout: 300 * 1000
    };

    var req = http.request(options, function (res) {
        console.log('STATUS: ' + res.statusCode);
        if (res.statusCode != 200) {
            throw 'return status was not 200 (' + path + ')';
        }
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            allOfBody = allOfBody + chunk;
        });
        res.on('end', function () {
            console.log('all body received (' + path + ')');
            //console.log(allOfBody);
            var outputFilename = path.replace(folder, '');
            fs.writeFileSync(outputFilename, allOfBody);
            //resolve({
            //    job: job,
            //    body: allOfBodyJson,
            //    isRunning: isRunningJson.isRunning
            //});
            console.log('complete - file written out as (' + path + '): ' + outputFilename);
        });
        res.on('error', function (e) {
            console.log('result error (' + path + '):' + e);
            throw e;
            //reject(e);
        });
    });
    req.on('error', function (e) {
        console.log('received an error (' + path + '):' + e);
        throw e;
        //reject(e);
    });
    req.setTimeout(300 * 1000); //node default is 2 minutes
    // write data to request body
    req.write(' ');
    req.end();

    console.log('request sent (' + path + '): ' + JSON.stringify(options));
});