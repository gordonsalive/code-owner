/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

// AAG 28/3/'17
// This script call other scripts to:
// (1) fetch all the SVN code changes for each month for the last 12 months
// (2) fetch all the Crucible code reviews
// (3) match SVN change revision numbers and crucible review SVN revision numbers to get code review %ages


//var fs = require('fs');
//var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 1; //0=off

var update_status = require('./scripts/update-status');
var read_json = require('./scripts/read-json');
//var promisify = require('./scripts/promisify');
//var exec_promise = require('./scripts/exec-promise');
var fetch_code_changes = require('./scripts/fetch-code-changes');
var fetch_code_reviews = require('./scripts/fetch-code-reviews');
var calculate_code_review_percent = require('./scripts/calculate-code-review-percent');

var resultsJson = {};

writeOutStatus('failed');

(function doMainStuff() {
    var now = new Date();
    if (DEBUG) {
        console.log('--mainLoop:' + now.toTimeString());
    }

    read_json('json/repos.json')
        .then(fetch_code_changes)
        //.then(function (repos) {
        //    return fetch_code_changes(repos);
        //})
        .then(function (result) {
            return read_json('json/reviews.json');
        })
        .then(fetch_code_reviews)
        //.then(function (reviews) {
        //    return fetch_code_reviews(reviews);
        //})
        .then(calculate_code_review_percent)
        //.then(function (reviewdRevisions) {
        //    return calculate_code_review_percent();
        //})
        .then(function (result) {
            var d = new Date();
            if (DEBUG) console.log('Completed: ' + d.toLocaleString() + '. Wait 3 hours before repeating...');
            setTimeout(doMainStuff, 3 * 60 * 60 * 1000); 
        })
        .fail(function (err) {
            console.error(err);
            //update status (even if done already)
            writeOutStatus('failed');
            var d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 4 hours');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 4 * 60 * 60 * 1000); 
        });

})();

function writeOutStatus(connectionStatus) {
    update_status(connectionStatus, 'code-changes');
}