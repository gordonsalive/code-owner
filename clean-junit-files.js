/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 6/7/2017
// Module to read in a number junit.xml files, convert them to cucumber and then convert them back again 
// - in so doing ensuring we have a consistent format for junit.xml to import into, say, Silk Central.
//

var fs = require('fs');
var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

//var read_json = require('./scripts/read-json');
//var promisify = require('./scripts/promisify');
var get_junit_from_cucumber = require('./scripts/get-junit-from-cucumber');
var get_cucumber_from_junit = require('./scripts/get-cucumber-from-junit');

(function doMainStuff() {
    var now = new Date();
    console.log('--started:' + now.toTimeString());

    var rootDirectory = process.argv[2];
    var outputFilePrefix = process.argv[3];
    var recurse = process.argv[4];

    Q.all(convertJunitToCucumberAndSaveRecursive(rootDirectory, recurse))
        .then(function (cukedFiles) {
            //console.log(cukedFiles);
            return Q.all(convertCucumberToJunitAndSaveRecursive(rootDirectory, outputFilePrefix, recurse));
        })
        .then(function (results) {
            var end = new Date();
            console.log("--we're back:" + end.toTimeString());
        })
        .fail(console.log)
        .done();

})();

function convertJunitToCucumberAndSaveRecursive(path, recurse) {
    var result = [];
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                if (recurse) {
                    result.push(convertJunitToCucumberAndSaveRecursive(curPath, recurse));
                }
            } else {
                if (_.last(curPath.split('.')) === 'xml') {
                    result.push(
                        convertJunitToCucumberAndSave(curPath)
                    );
                }
            }
        });
    }
    return _.flatten(result);
}

function convertJunitToCucumberAndSave(inputFile) {
    console.log('convertJunitToCucumberAndSave(' + inputFile + ')');
    return get_cucumber_from_junit(inputFile)
        .then(function (cukeJson) {
            //console.log(cukeJson);
            var outputFilename = inputFile.slice(0, -4) + '.json';
            return Qfs.write(outputFilename, JSON.stringify(cukeJson, null, '\t'))
                .then(function (writeResult) {
                    //return output;
                    return outputFilename;
                });
        });
}

function convertCucumberToJunitAndSaveRecursive(path, outputFilePrefix, recurse) {
    var result = [];
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) {
                if (recurse) {
                    result.push(convertCucumberToJunitAndSaveRecursive(curPath, outputFilePrefix, recurse));
                }
            } else {
                if (_.last(curPath.split('.')) === 'json') {
                    result.push(
                        convertCucumberToJunitAndSave(curPath, outputFilePrefix)
                    );
                }
            }
        });
    }
    return _.flatten(result);
}

function convertCucumberToJunitAndSave(inputFile, outputFilePrefix) {
    console.log('convertCucumberToJunitAndSave(' + inputFile + ',' + outputFilePrefix + ')');
    return get_junit_from_cucumber(inputFile)
        .then(function (junitXml) {
            var inputFilePathElements = inputFile.split('/');
            var inputFileWithoutPath = _.last(inputFilePathElements);
            inputFilePathElements = inputFileWithoutPath.split('\\');
            inputFileWithoutPath = _.last(inputFilePathElements);
            var outputFileName;
            if (outputFilePrefix) {
                outputFileName = outputFilePrefix + '-' + inputFileWithoutPath.slice(0, -5) + '.xml';
            } else {
                outputFileName = inputFileWithoutPath.slice(0, -5) + '.xml';
            }

            return Qfs.write(outputFileName, junitXml);
            //.then(function (writeResult) {
            //    return output;
            //});
        });
}