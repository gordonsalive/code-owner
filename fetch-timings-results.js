/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 19/7/2016
// Module to fetch the timings information from QGPL.ROLREFT and save it as JSON for display
//**********
// Changes:
// 22/02/2017 AAG The data get truely massive.  I only need the entity results for the last week.

var Qfs = require("q-io/fs");
var _ = require("underscore");

var pool = require('./scripts/shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var timingsConfig = {};

var DEBUG = 0; //off

var update_status = require('./scripts/update-status');

var resultsJson = {};

writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {
    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    pool
    //        use a smaller select for testing    
    //.query("SELECT model, level, database, start, variable, value, updated FROM qgpl.rolreft WHERE database='FGBLIVZn1'")
        .query("SELECT model, level, database, start, variable, value, updated " +
            "FROM qgpl.rolreft " +
            "WHERE updated > current date - 7 DAYS " +
            "ORDER BY updated ASC ")
        .then(function (result) {
            //have data in the form 
            // [ { "DATABASE": "FGBLIVZn1", 
            //     "VARIABLE": "entity", 
            //     "UPDATED": "2016-07-20 06:38:50.329145", 
            //     "START": "2016/07/20-04:24:42", 
            //     "MODEL": "F162_zinc", "LEVEL": "F162", 
            //     "VALUE": "{\"order\":155,\"name\":\"Fee_SharingConfig\",\"start\":\"05:53:00\",\"total\":1734}" } ]
            if (DEBUG) {
                console.log('returned from main query - last 7 days');
                //console.log('Result of Query=' + JSON.stringify(result, null, "  "));
            }
            return result;
        })
        .then(function (result) {
            //add records for the last 6 months (these don't include entities, just layers and whole runs)
            return pool.query("SELECT model, level, database, start, variable, value, updated " +
                    "FROM qgpl.rolreft " +
                    "WHERE updated < current date - 6 DAYS " +
                    "AND updated > current date - 6 MONTHS " +
                    "AND variable IN ('run', 'layer') " +
                    "ORDER BY updated ASC")
                .then(function (rows) {
                    if (DEBUG) {
                        console.log('returned from main query - last 6 months');
                        //console.log('Result of Query=' + JSON.stringify(result, null, "  "));
                    }
                    return rows.concat(result);
                });
        })
        .then(function (results) {
            //convert the VALUE into JSON
            resultsJson = _.map(results, function (row) {
                row.VALUE = JSON.parse(row.VALUE);
                return row;
            });
            if (DEBUG) console.log('converted to JSON');
            return resultsJson;
        })
        .then(function (results) {
            //group the data out by model & level (e.g. 'zinc:QA')
            var byModelAndLevel = _.groupBy(results, function (row) {
                return row.MODEL + ':' + row.LEVEL;
            });
            if (DEBUG) {
                console.log('group data out by model and level');
                //console.log('grouped by model & level=' + JSON.stringify(byModelAndLevel, null, "  "));
            }
            return byModelAndLevel;
        })
        .then(function (byModelAndLevel) {
            //within each model-level, group out each individual run by start
            var byStart = _.mapObject(byModelAndLevel, function (modelLevelItems) {
                return _.groupBy(modelLevelItems, 'START');
            });
            if (DEBUG) {
                console.log('within each model-level, group each run by start');
                //console.log('byStart = ' + JSON.stringify(byStart, null, "  "));
            }
            return byStart;
        })
        .then(function (byModelLevelStart) {
            //within each model-level-start, group the data by variable (run, layer and entity)
            var byVariable = _.mapObject(byModelLevelStart, function (modelLevelItems) {
                return _.mapObject(modelLevelItems, function (modelLevelStartItems) {
                    var groupedByVariable = _.groupBy(modelLevelStartItems, 'VARIABLE');
                    //we can drop quite a bit of the data from each row now
                    return _.mapObject(groupedByVariable, function (rows) {
                        return _.map(rows, function (row) {
                            return {
                                updated: row.UPDATED,
                                order: row.VALUE.order,
                                name: row.VALUE.name,
                                total: row.VALUE.total
                            };
                        });
                    });
                });
            });
            if (DEBUG) {
                console.log('within each model-level-start, group by variable (run, layer, entity)');
                //console.log('byVariable = ' + JSON.stringify(byVariable, null, "  "));
            }
            return byVariable;
        })
        .then(function (results) {
            //now stick the result in a property called runs, next steps will be to compute averages
            return {
                runs: results
            };
        })
        .then(function (results) {
            //do the daily run and layer averages across all model-level's
            //runs node looks like this: 
            //{ "F162_zinc:F162": { "2016/06/24-04:40:44": { "layer": 
            //    [ { "updated": "2016-06-27 07:36:11.214895", "order": -1, "name": "always", "total": 1914 }, ...
            // take the raw data and filter out the entity data
            //var withoutEntityData = _.filter(resultsJson, function (row) {
            //    return _.contains(['run', 'layer'], row.VARIABLE);
            //});
            // Now take the raw data and group it by day and then compute averages for run and layer items
            if (DEBUG) {
                console.log('about to do averages');
            }
            var byDay = _.groupBy(resultsJson, function (row) {
                return row.START.substring(0, 10); //just the date part
            });
            byDay = _.mapObject(byDay, function (dayRows) {
                var totalRuns = 0;
                var totalLayers = {};
                var totalSlowEntities = {};
                var totals = _.reduce(dayRows, function (memo, item) {
                    //console.log(JSON.stringify(memo, null, "  ") + JSON.stringify(item, null, "  "));
                    if (item.VARIABLE === 'run') {
                        totalRuns++;
                        memo.run = memo.run + item.VALUE.total;
                    } else if (item.VARIABLE === 'layer') {
                        var itemName = item.VALUE.name;
                        if (!(memo.layers[itemName])) {
                            memo.layers[itemName] = item.VALUE.total;
                            totalLayers[itemName] = 1;
                        } else {
                            memo.layers[itemName] = memo.layers[itemName] + item.VALUE.total;
                            totalLayers[itemName]++;
                        }
                    } else if (item.VARIABLE === 'entity') {
                        var entityName = item.VALUE.name;
                        //(note: can't use where() as I want to update the original item, where creates copies.)
                        var entity = _.find(memo.slowEntities, function (item) {
                            return item.name === entityName;
                        });
                        if (!entity) {
                            memo.slowEntities.push({
                                name: entityName,
                                total: item.VALUE.total
                            });
                            totalSlowEntities[entityName] = 1;
                        } else {
                            entity.total = entity.total + item.VALUE.total;
                            totalSlowEntities[entityName]++;
                        }
                    }
                    return memo;
                }, {
                    run: 0,
                    layers: {},
                    slowEntities: []
                });
                //console.log('\ntotal = ' + JSON.stringify(totals, null, "  "));
                //console.log('\ntotalRuns = ' + totalRuns);
                //console.log('\ntotalLayers = ' + JSON.stringify(totalLayers, null, "  "));
                //console.log('\ntotalSlowEntities = ' + JSON.stringify(totalSlowEntities, null, "  "));

                //spin through the totals and turn them into averages
                var averages = {};
                averages.totalRuns = totalRuns;
                averages.run = totals.run / totalRuns;
                averages.layers = _.mapObject(totals.layers, function (layer, key) {
                    return layer / totalLayers[key];
                });
                averages.population = _.reduce(averages.layers, function (memo, item) {
                    return memo + item;
                }, 0);
                //console.log(totalSlowEntities);
                averages.slowEntities = _.map(totals.slowEntities, function (entity) {
                    entity.total = entity.total / totalSlowEntities[entity.name];
                    return entity;
                });
                //filter this down to the top 10 slowest entities
                averages.slowEntities = _.sortBy(averages.slowEntities, 'total').reverse();
                averages.slowEntities = _.first(averages.slowEntities, 10);
                //finally convert this array into object properties
                averages.slowEntities = _.reduce(averages.slowEntities, function (memo, entity) {
                    memo[entity.name] = entity.total;
                    return memo;
                }, {});
                //console.log(JSON.stringify(averages, null, "  "));
                return averages;
            });
            results.averages = {};
            results.averages.daily = byDay;
            return results;
        })
        .then(function (byModelLevelStartVariable) {
            //write it out to the results file
            return Qfs.write("json/timings-results.json", JSON.stringify(byModelLevelStartVariable, null, '\t'));
        })
        .then(function (result) {
            //console.log('--update status..');
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            console.log('..repeat in 2 hours..');
            setTimeout(doMainStuff, 60 * 60 * 2 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            console.log('error, try again in 1 hour');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 60 * 60 * 1000);
        });
})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'timings');
}