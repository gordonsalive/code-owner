/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 7/6/2017
// Module to read in a number of cucumber.json files (param1, command separated list) and write them out to 
// and output files - either <same name>.xml, or <param2>-<same name>.xml.
//


var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

//var read_json = require('./scripts/read-json');
//var promisify = require('./scripts/promisify');
var get_junit_from_cucumber = require('./scripts/get-junit-from-cucumber');

(function doMainStuff() {
    var now = new Date();
    console.log('--started:' + now.toTimeString());

    var inputFiles = process.argv[2].split(',');
    var outputFilePrefix = process.argv[3];

    Q.all(_.map(inputFiles, function (inputFile) {
            return convertCucumberToJunitAndSave(inputFile, outputFilePrefix);
        }))
        .then(function (results) {
            var end = new Date();
            console.log("--we're back:" + end.toTimeString());
        })
        .fail(console.log)
        .done();

})();

function convertCucumberToJunitAndSave(inputFile, outputFilePrefix) {
    return get_junit_from_cucumber(inputFile)
        .then(function (junitXml) {
            var inputFilePathElements = inputFile.split('/');
            var inputFileWithoutPath = _.last(inputFilePathElements);
            var outputFileName;
            if (outputFilePrefix) {
                outputFileName = outputFilePrefix + '-' + inputFileWithoutPath.slice(0, -5) + '.xml';
            } else {
                outputFileName = inputFileWithoutPath.slice(0, -5) + '.xml';
            }

            return Qfs.write(outputFileName, junitXml);
            //.then(function (writeResult) {
            //    return output;
            //});
        });
}