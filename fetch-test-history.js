/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 17/8/2016
// Module to fetch the test history from ROLREFTST and then convert it into a structure with 
// totals for each view, and totals for each item for each view, with an exponential axis against time:
//  - best results by clock hour so far today
//  - best results by calendar day for the last 4weeks
//  - best results by calendar week before then.
// NOTE, FOR NOW THIS WILL JUST RETURN RUN RESULTS AS-IS WITH NO COMPENSATION FOR TIME
//  With just raw data totals for each view and for each item by view it's 130,000 lines.
// I think I need to be sparing with the processed data.  Perhaps, lastweek (hourly), lastmonth (daily), lastyear(weekly)
// using best figure in the range and with a proper date so that I can display them on a date/time axis.
//
// In future may need to restrcit fetch of raw data to a single year, so constraint size of test-history-results.json file.
//**********

var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var pool = require('./scripts/shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var read_json = require("./scripts/read-json");
var promisify = require("./scripts/promisify");

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');

var resultsJson = {
    //raw data
    history: {
        totals: {
            views: {
                "flat": {
                    "2016/08/17-10:53": {
                        passed: 100,
                        failed: 10,
                        missing: 0
                    },
                    "2016/08/18-11:51": {
                        passed: 100,
                        failed: 10,
                        missing: 0
                    }
                },
                "view2": {}
            }
        },
        views: {
            "flat": {},
            "view1": {
                "item1": {
                    "2016/08/17-10:53": {
                        passed: 10,
                        failed: 1,
                        missing: 0
                    },
                    "2016/08/18-11:51": {
                        passed: 10,
                        failed: 1,
                        missing: 0
                    }
                },
                "item2": {}
            }
        },
        stability: {
            greenStatus: {
                views: {
                    "flat": {},
                    "view1": {
                        "2016/08/17": {
                            "item1": {
                                isAllGreen: "green",
                                testsInProject: 110
                            },
                            "item2": {
                                isAllGreen: "green",
                                testsInProject: 110
                            }
                        }
                    }
                }
            }
        }
    },
    //processed data
    lastweek: {},
    lastmonth: {},
    lastyear: {}
};


writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());
    //only get the last 6 months - get current date - 1 year + 6 months, convert to yyyymmdd and add to where
    var sixMonthsAgo = new Date();
    sixMonthsAgo.setFullYear(d.getFullYear() - 1);
    sixMonthsAgo.setMonth(d.getMonth() + 6);
    var dY = sixMonthsAgo.getFullYear() * 10000;
    var dM = (sixMonthsAgo.getMonth() + 1) * 100;
    var dD = sixMonthsAgo.getDate();
    var whileAgoInt = dY + dM + dD;

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    //console.log('--parsefile..');
    read_json('json/cukes.json')
        .then(function (cukesFile) {
            var results = {
                history: {
                    totals: {
                        views: {
                            flat: {}
                        }
                    },
                    views: {
                        flat: {}
                    }
                }
            };
            _.each(cukesFile["code-owners-tree"], function (viewItem, viewName) {
                results.history.views[viewName] = {};
            });
            return results;
        })
        //get the totals history for each (run), with flat as a special case
        .then(function (results) {
            if (DEBUG) console.log('\n-get the flat run results first...');

            return pool.query(
                    " SELECT view, date, time, type, variable, item, value " +
                    " FROM qgpl.rolreftst " +
                    " WHERE type='flat' " +
                    " AND variable='run' " +
                    " AND item ='flat' " +
                    " AND run_date > " + whileAgoInt)
                .then(function (rows) {
                    var value = {};
                    //have data in the form
                    // [ { "VIEW": "-",
                    //     "DATE": 160818,
                    //     "TIME": 154052, 
                    //     "TYPE": "flat", 
                    //     "VARIABLE": "run", 
                    //     "ITEM": "flat",
                    //     "VALUE": " {"results":{"passed":29401,"failed":3088,"missing":409},"items":{...}}" ]
                    if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                    if ((rows) && (rows.length > 0)) {
                        _.each(rows, function (row) {
                            value[row.DATE + ":" + ("000000" + row.TIME).slice(-6)] = JSON.parse(row.VALUE).results;
                        });
                    }
                    results.history.totals.views.flat = value;
                    return results;
                });

        })
        .then(function (results) {
            if (DEBUG) console.log('\n-get the tree views run results...');
            //use Q.all() to fetch these results in parallel (but not flat which we have already)
            //***NEED TO FILTER OUT flat***
            return Q.all(
                    _.map(results.history.views, function (viewItem, viewName) {
                        if (viewName === 'flat') {
                            return promisify({});
                        } else {

                            return pool.query(
                                    " SELECT view, date, time, type, variable, item, value " +
                                    " FROM qgpl.rolreftst " +
                                    " WHERE type='tree' " + 
                                    " AND variable='run' " + 
                                    " AND item='" + viewName + "' " +
                                    " AND run_date > " + whileAgoInt)
                                .then(function (rows) {
                                    var value = {};
                                    //have data in the form
                                    // [ { "VIEW": "-",
                                    //     "DATE": 160818,
                                    //     "TIME": 154052, 
                                    //     "TYPE": "flat", 
                                    //     "VARIABLE": "run", 
                                    //     "ITEM": "flat",
                                    //     "VALUE": " {"results":{"passed":29401,"failed":3088,"missing":409},"items":{...}}" ]
                                    if (DEBUG); // console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                    if ((rows) && (rows.length > 0)) {
                                        _.each(rows, function (row) {
                                            //console.log('adding item to value' + parseInt(row.DATE) + ":" + parseInt(row.TIME));
                                            value[row.DATE + ":" + ("000000" + row.TIME).slice(-6)] = JSON.parse(row.VALUE).results;
                                        });
                                    }
                                    var result = {
                                        view: viewName,
                                        results: value
                                    };
                                    return result;
                                });
                        }
                    })
                )
                .then(function (viewsArray) {
                    if (DEBUG) console.log('\n\n-got the tree views run results now filtering and tidying...'); // + JSON.stringify(viewsArray, null, ' '));

                    _.each(viewsArray, function (viewItem) {
                        if (viewItem.view && viewItem.results) {
                            results.history.totals.views[viewItem.view] = viewItem.results;
                            //                            results.history.totals.views[viewItem.view] = {};
                            //                            _.each(viewItem.results, function (resultsItem, resultsKey) {
                            //                                results.history.totals.views[viewItem.view][resultsKey] = resultsItem;
                            //                            });
                        }
                    });

                    //console.log('\n\n-after transpose:' + JSON.stringify(results, null, ' '));
                    return results;
                });
        })
        //get the totals history for each view sub-tem (results), with flat as a special case
        .then(function (results) {
            if (DEBUG) console.log('\n-get the flat view results first...');
            return pool.query(
                    " SELECT view, date, time, type, variable, item, value " +
                    " FROM qgpl.rolreftst " +
                    " WHERE type='flat' " + 
                    " AND variable='results' " + 
                    " AND view='flat' " +
                    " AND run_date > " + whileAgoInt +
                    " ORDER BY item, date, time")
                .then(function (rows) {
                    var value = {};
                    //have data in the form
                    // [ { "VIEW": "-",
                    //     "DATE": 160818,
                    //     "TIME": 154052, 
                    //     "TYPE": "flat", 
                    //     "VARIABLE": "results", 
                    //     "ITEM": "flat",
                    //     "VALUE": " {"passed":29401,"failed":3088,"missing":409}" ]
                    if (DEBUG); // console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                    if ((rows) && (rows.length > 0)) {
                        _.each(rows, function (row) {
                            if (!value[row.ITEM]) {
                                value[row.ITEM] = {};
                            }
                            value[row.ITEM][row.DATE + ":" + ("000000" + row.TIME).slice(-6)] = JSON.parse(row.VALUE);
                            //console.log('value = ' + JSON.stringify(value, null, ' '));
                        });
                    }
                    results.history.views.flat = value; //i.e. add .itemname.DATE:TIME: {passed..} on to flat node
                    return results;
                });

        })
        .then(function (results) {
            if (DEBUG) console.log('\n-get the tree views view results...');
            //use Q.all() to fetch these results in parallel (but not flat which we have already)
            //***NEED TO FILTER OUT flat***
            return Q.all(
                    _.map(results.history.views, function (viewItem, viewName) {
                        if (viewName === 'flat') {
                            return promisify({});
                        } else {

                            return pool.query(
                                    " SELECT view, date, time, type, variable, item, value " +
                                    " FROM qgpl.rolreftst " +
                                    " WHERE type='tree' " + 
                                    " AND variable='results' " + 
                                    " AND view='" + viewName + "' " +
                                    " AND run_date > " + whileAgoInt +
                                    " ORDER BY item, date, time")
                                .then(function (rows) {
                                    var value = {};
                                    //have data in the form
                                    // [ { "VIEW": "-",
                                    //     "DATE": 160818,
                                    //     "TIME": 154052, 
                                    //     "TYPE": "flat", 
                                    //     "VARIABLE": "run", 
                                    //     "ITEM": "flat",
                                    //     "VALUE": " {"results":{"passed":29401,"failed":3088,"missing":409},"items":{...}}" ]
                                    if (DEBUG); // console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                    if ((rows) && (rows.length > 0)) {
                                        _.each(rows, function (row) {
                                            if (!value[row.ITEM]) {
                                                value[row.ITEM] = {};
                                            }
                                            //console.log('adding item to value' + parseInt(row.DATE) + ":" + parseInt(row.TIME));
                                            value[row.ITEM][row.DATE + ":" + ("000000" + row.TIME).slice(-6)] = JSON.parse(row.VALUE).results;
                                        });
                                    }
                                    var result = {
                                        view: viewName,
                                        results: value
                                    };
                                    return result;
                                });
                        }
                    })
                )
                .then(function (viewsArray) {
                    if (DEBUG) console.log('\n\n-got the tree views run results now filtering and tidying...'); // + JSON.stringify(viewsArray, null, ' '));

                    _.each(viewsArray, function (viewItem) {
                        if (viewItem.view && viewItem.results) {
                            results.history.views[viewItem.view] = viewItem.results;
                            //                            results.history.totals.views[viewItem.view] = {};
                            //                            _.each(viewItem.results, function (resultsItem, resultsKey) {
                            //                                results.history.totals.views[viewItem.view][resultsKey] = resultsItem;
                            //                            });
                        }
                    });

                    //console.log('\n\n-after transpose:' + JSON.stringify(results, null, ' '));
                    return results;
                });
        })
        .then(function (results) {
            if (DEBUG) console.log('\n\n-now add the stability node...');
            //what views are we collating data for? each view in results.history.views
            var viewsToCollect = _.map(results.history.views, function (view, key) {
                return key;
            });
            console.log('viewsToCollect=' + viewsToCollect);
            results.history.stability = {};
            //for each view, from the earliest date until 3am this morning, 
            //for each sub item find it's red/green status and the number of testsInProject
            var today = new Date();
            results.history.stability = _.mapObject(results.history.views, function (view, viewName) {
                //spinning through each view
                //console.log('spinning through results.history.views=' + viewName);
                return _.mapObject(view, function (subItem, subItemName) {
                    //each sub item, contains a map of dates against passed, failed, missing
                    //console.log('spinning through subItems=' + subItemName);
                    var resultObj = {};
                    var earliestDateStr = _.min(_.map(subItem, function (result, key) {
                        return Number(key.slice(0, 8));
                    })).toString();
                    //console.log(viewName + ':' + subItemName + ':earliest date=' + earliestDateStr);
                    //each date will be the start of the day, so I need to add one
                    //create range of dates as ints
                    var startDate = new Date(
                        Number(earliestDateStr.slice(0, 4)),
                        Number(earliestDateStr.slice(-4).slice(0, 2)) - 1,
                        Number(earliestDateStr.slice(-2)));
                    var dateRange = [];
                    while (startDate <= today) {
                        dateRange.push(
                            startDate.getFullYear() * 10000 * 1000000 +
                            (startDate.getMonth() + 1) * 100 * 1000000 +
                            startDate.getDate() * 1000000 +
                            235959);
                        startDate.setDate(startDate.getDate() + 1);
                        //console.log('start date now = ' + startDate);
                    }
                    _.each(dateRange, function (aDate) {
                        //find the maximum value in subItems list of dates that is below aDate
                        var maxDate = _.max(_.map(subItem, function (item, key) {
                            //***EXCEPT I WANT THE DATE AS AN INT***
                            var dateStr = key.split(':')[0];
                            var timeStr = key.split(':')[1];
                            var dateInt = Number(dateStr + timeStr);
                            //var year = Number(key.substr(0, 4));
                            //var month = Number(key.substr(4, 2)) - 1;
                            //var day = Number(key.substr(6, 2));
                            //var hours = Number(timeStr.substr(0, timeStr.length - 4));
                            //var mins = Number(timeStr.substr(timeStr.length - 4, 2));
                            //return new Date(year, month, day, hours, mins)
                            return dateInt;
                        }), function (dateTime) {
                            //return the date unless it is greater than aDate, in which case return 0
                            if (dateTime < aDate) {
                                return dateTime;
                            } else {
                                return 0;
                            }
                        });
                        //console.log('maxDate = ' + maxDate + '-' + maxDate.toString() + '-' + maxDate.toString().slice(0, 8) + ':' + maxDate.toString().slice(-6));
                        //console.log(JSON.stringify(subItem));
                        var maxItem = subItem[maxDate.toString().slice(0, 8) + ':' + maxDate.toString().slice(-6)];
                        resultObj[aDate.toString().slice(0, 8)] = {
                            isAllGreen: (maxItem.failed === 0),
                            testsInProject: maxItem.failed + maxItem.passed
                        };
                    });
                    return resultObj;
                });
            });
            return results;
        })
        .then(function (results) {

            if (DEBUG) console.log('\n-write out results file..');
            //console.log(JSON.stringify(results, null, ' '));
            //write out the results file
            return Qfs.write("json/test-history-results.json", JSON.stringify(results, null, '\t'));
        })
        .then(function (result) {
            //console.log('--update status..');
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            //console.log('..repeat in 2 minutes..');
            /*repeat in 30 seconds*/
            setTimeout(doMainStuff, 120 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 5 minutes');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 300 * 1000); //try again in 5 minutes
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'status');
}