/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var http = require('http');
//var url = require('url');
//var qs = require('querystring');
//var path = require('path');
var fs = require('fs');
//var ECT = require('ect');
//var Q = require('q');
//var Qfs = require("q-io/fs");
//var _ = require('underscore');

//var fullpath = 'http://172.26.27.188:8084/json/testing-summary-deep.xml';
var fullpath = 'http://172.26.27.188:8084/json/testing-summary.xml';
//var path = '/json/testing-summary-deep.xml';
var path = '/json/testing-summary.xml';
var hostname = '172.26.27.188';
var port = 8084;
var outputFilename = 'output.xml';

var allOfBody = '';

var options = {
    hostname: hostname,
    port: port,
    path: path,
    method: 'POST',
    //auth: userAndToken,
    agent: false,
    timeout: 300 * 1000
};

var req = http.request(options, function (res) {
    console.log('STATUS: ' + res.statusCode);
    if (res.statusCode != 200) {
        throw 'return status was not 200';
    }
    res.setEncoding('utf8');
    res.on('data', function (chunk) {
        allOfBody = allOfBody + chunk;
    });
    res.on('end', function () {
        console.log('all body received');
        //console.log(allOfBody);
        fs.writeFileSync(outputFilename, allOfBody);
        //resolve({
        //    job: job,
        //    body: allOfBodyJson,
        //    isRunning: isRunningJson.isRunning
        //});
        console.log('complete - file written out as:' + outputFilename);
    });
    res.on('error', function (e) {
        console.log('result error.:' + e);
        throw e;
        //reject(e);
    });
});
req.on('error', function (e) {
    console.log('received an error:' + e);
    throw e;
    //reject(e);
});
req.setTimeout(300 * 1000); //node default is 2 minutes
// write data to request body
req.write(' ');
req.end();

console.log('request sent');