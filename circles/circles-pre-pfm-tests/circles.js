d3.queue().defer(d3.json, "hib_hotspot_proto.json").await(drawAll);

function drawAll(error, dataset) {
	var padding = 10,
		width = window.innerWidth - padding,
		height = window.innerHeight - padding;

	var centerX = width / 2, centerY = height / 2;

	var canvas = d3
		.select("#chart")
		.append("canvas")
		.attr("id", "canvas")
		.attr("width", width)
		.attr("height", height);

	var context = canvas.node().getContext("2d");
	context.clearRect(0, 0, width, height);

	var hiddenCanvas = d3
		.select("#chart")
		.append("canvas")
		.attr("id", "hiddenCanvas")
		.attr("width", width)
		.attr("height", height)
		.style("display", "none");

	var hiddenContext = hiddenCanvas.node().getContext("2d");
	hiddenContext.clearRect(0, 0, width, height);

	var detachedContainer = document.createElement("custom");
	var dataContainer = d3.select(detachedContainer);

	var colorCircle = d3
		.scaleLinear()
		.domain([-1, 5])
		.range(["hsl(185,60%,99%)", "hsl(187,40%,70%)"])
		.interpolate(d3.interpolateHcl);

	var diameter = Math.min(width * 0.9, height * 0.9), radius = diameter / 2;

	var colToCircle = {};

	var nextCol = 1;
	function genColor() {
		var ret = [];
		if (nextCol < 16777215) {
			ret.push(nextCol & 0xff); // R
			ret.push((nextCol & 0xff00) >> 8); // G
			ret.push((nextCol & 0xff0000) >> 16); // B
			nextCol += 1;
		}
		var col = "rgb(" + ret.join(",") + ")";
		return col;
	}

	var pack = d3.pack().padding(1).size([diameter, diameter]);

	var root = d3
		.hierarchy(dataset)
		.sum(function(d) {
			return d.size;
		})
		.sort(function(a, b) {
			return b.value - a.value;
		});

	var nodes = pack(root);
	var cWidth = canvas.attr("width");
	var cHeight = canvas.attr("height");

	var zoomInfo = {
		centerX: root.x,
		centerY: root.y,
		scale: diameter / (root.r * 2.05)
	};

	var zoom = {
		focus: root,
		target: [root.x, root.y, root.r * 2.05],
		timeElapsed: 0,
		interpolator: null,
		duration: 0
	};

	function drawCanvas(chosenContext, hidden) {
		chosenContext.fillStyle = "#fff";
		chosenContext.rect(0, 0, cWidth, cHeight);
		chosenContext.fill();

		nodes.each(function(node) {
			if (hidden) {
				if (node.color == null) {
					node.color = genColor();
					colToCircle[node.color] = node;
				}
				chosenContext.fillStyle = node.color;
			} else {
				chosenContext.fillStyle = node.data.weight > 0.0
					? "darkred"
					: node.children ? colorCircle(node.depth) : "WhiteSmoke";
			}

			chosenContext.beginPath();

			chosenContext.arc(
				(node.x - zoomInfo.centerX) * zoomInfo.scale + centerX,
				(node.y - zoomInfo.centerY) * zoomInfo.scale + centerY,
				node.r * zoomInfo.scale,
				0,
				2 * Math.PI,
				true
			);

			// Leaf node
			if (node.parent && !node.children) {
				chosenContext.lineWidth = 1;
				chosenContext.strokeStyle = "#777";
				chosenContext.stroke();
				// Root node
			} else if (!node.parent) {
				chosenContext.lineWidth = 2;
				chosenContext.strokeStyle = "#777";
				chosenContext.stroke();
			}

			if (hidden) {
				chosenContext.fill();
			} else {
				chosenContext.globalAlpha = node.data.weight;
				chosenContext.fill();
				chosenContext.globalAlpha = 1;
			}
		});

		if (!hidden) {
			var text = [];
			var focus = zoom.focus;
			while (focus && focus.data.name !== "root") {
				text.unshift(focus.data.name);
				focus = focus.parent;
			}

			chosenContext.font = "15px Arial";
			chosenContext.textAlign = "center";
			chosenContext.fillText(text.join(" / "), centerX, 20);
		}
	}

	function zoomToCanvas(newFocus) {
		var newTarget = [newFocus.x, newFocus.y, newFocus.r * 2.05];
		var interpolator = d3.interpolateZoom(zoom.target, newTarget);
		return {
			focus: newFocus,
			target: newTarget,
			timeElapsed: 0,
			interpolator: interpolator,
			duration: interpolator.duration
		};
	}

	function interpolateZoom(dt) {
		if (zoom.interpolator && zoom.duration > 0) {
			zoom.timeElapsed += dt;

			var t = d3.easeCubicInOut(zoom.timeElapsed / zoom.duration);

			zoomInfo.centerX = zoom.interpolator(t)[0];
			zoomInfo.centerY = zoom.interpolator(t)[1];
			zoomInfo.scale = diameter / zoom.interpolator(t)[2];

			if (zoom.timeElapsed >= zoom.duration) zoom.interpolator = null;
		}
	}

	document.getElementById("canvas").addEventListener("click", function(event) {
		drawCanvas(hiddenContext, true);

		var mouseX = event.layerX;
		var mouseY = event.layerY;

		var col = hiddenContext.getImageData(mouseX, mouseY, 1, 1).data;
		var colString = "rgb(" + col[0] + "," + col[1] + "," + col[2] + ")";
		var node = colToCircle[colString];

		if (node) {
			if (zoom.focus !== node) {
				zoom = zoomToCanvas(node);
			} else {
				zoom = zoomToCanvas(root);
			}
		}
	});

	zoom = zoomToCanvas(root);

	var last = 0;
	d3.timer(function(elapsed) {
		var delta = elapsed - last;
		interpolateZoom(delta);
		last = elapsed;
		drawCanvas(context);
	});
}
