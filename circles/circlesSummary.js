/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, windows: true, forin: true, vars: true, nomen: true, bitwise: true, regexp: true, indent: 4, maxerr: 50*/
/*jshint -W030*/
/*jshint expr:true*/
var w = 1280,
    h = 800,
    r = 720,
    x = d3.scale.linear().range([0, r]),
    y = d3.scale.linear().range([0, r]),
    node,
    root;

var pack = d3.layout.pack()
    .size([r, r])
    .value(function (d) {
        return d.size;
    })
    .sort(function (a, b) {
        return b.value - a.value;
    });

//first add my links to the sub items
var buttons = d3.select("body").insert("div", "h2");
d3.json("incidents-summary.json", function (data) {
    for (var x = 0; x < data.children.length; x++) {
        buttons
            .append("a")
            .text(data.children[x].name)
            .attr("href", "heat-map-" + data.children[x].name + ".html")
            .attr("target", "_blank")
            .attr("class", "button");
    }
});

//now get on with building the packed circle vis.
var vis = d3.select("body").insert("svg:svg", "h2")
    .attr("width", w)
    .attr("height", h)
    .append("svg:g")
    .attr("transform", "translate(" + (w - r) / 2 + "," + (h - r) / 2 + ")");

d3.json("incidents-summary.json", function (data) {
    node = root = data;

    //console.log("data=" + JSON.stringify(data));

    var nodes = pack.nodes(root);

    //quickly add a tooltip
    var tooltip = d3
        .select("body")
        .append('div')
        .attr('class', 'tooltip');
    tooltip.append('div')
        .attr('class', 'label');
    tooltip.append('div')
        .attr('class', 'count');
    tooltip.append('div')
        .attr('class', 'defects');

    vis.selectAll("circle")
        .data(nodes)
        .enter().append("svg:circle")
        .attr("class", function (d) {
            return d.children ? "parent" : "child";
        })
        .attr("fill", function (d) {
            if (!d.children) {
                if (d.weight) {
                    return 'firebrick';
                } else {
                    return '#adc'; //'#ccc'; //'#1f77b4';
                }
            }
        })
        .attr("opacity", function (d) {
            if (!d.children) {
                if (!d.weight) {
                    return "0.2";
                } else if (d.weight >= 1) {
                    return "1.0";
                } else if (d.weight <= 0.05) {
                    return "0.05";
                } else {
                    return "" + d.weight;
                }
            }
        })
        .attr("cx", function (d) {
            return d.x;
        })
        .attr("cy", function (d) {
            return d.y;
        })
        .attr("r", function (d) {
            return d.r;
        })
        .on("click", function (d) {
            return zoom(node == d ? root : d);
        })
        .on("mouseover", function (d) {
            var text = [];
            var focus = d;
            while (focus && focus.name !== "root") {
                text.unshift(focus.name);
                focus = focus.parent;
            }
            if (text.length === 0) {
                text = ['root'];
            }
            var weight = '';
            if (d.count) {
                weight += " (defects = " + d.count.toFixed(0) + ")";
            } else {
                weight += ' (total defects = ' + countRecursive(d).toFixed(0) + ')';
            }

            d3.select("#selectedNode").text(function (d) {
                return text.join(" : ") + weight;
            });

            //now add the tooltip
            tooltip.select('.label').html(text.join(" : "));
            if (d.count) {
                tooltip.select('.count').html('' + d.count.toFixed(0) + ' defects');
                tooltip.select('.defects').html(
                    d.defects
                    .reduce(function (memo, item) {
                        var itemYearMonth = item.dateRequested.substr(0, 6);
                        var matchingYearMonth = memo.find(function (item) {
                            return item.yearMonth === itemYearMonth;
                        });
                        if (matchingYearMonth) {
                            matchingYearMonth.count++;
                        } else {
                            memo.push({
                                yearMonth: itemYearMonth,
                                count: 1
                            });
                        }
                        return memo;
                    }, [])
                    .sort(function (a, b) {
                        return a.yearMonth - b.yearMonth;
                    })
                    .map(function (item) {
                        return item.yearMonth.substr(0, 4) + '-' + item.yearMonth.substr(4, 2) + ': ' + item.count;
                    }).join('\n<br>')

                    //d.defects.map(function (item) {
                    //    return 'job: ' + item.job + ' (incident:' + item.incident + ')';
                    //}).join('\n')
                );
            } else {
                tooltip.select('.count').html('' + countRecursive(d).toFixed(0) + ' total defects');
                if (text.length === 4) {
                    tooltip.select('.defects').html('No changes');
                } else {
                    tooltip.select('.defects').html('');
                }
            }
            tooltip.style('display', 'block');
        })
        .on('mouseout', function () {
            tooltip.style('display', 'none');
        })
        .on('mousemove', function (d) {
            tooltip
                .style('top', (d3.event.layerY + 10) + 'px')
                .style('left', (d3.event.layerX + 10) + 'px');
        });

    vis.selectAll("text")
        .data(nodes)
        .enter().append("svg:text")
        .attr("class", function (d) {
            return d.children ? "parent" : "child";
        })
        .attr("x", function (d) {
            return d.x;
        })
        .attr("y", function (d) {
            return d.y;
        })
        .attr("dy", ".35em")
        .attr("text-anchor", "middle")
        .style("opacity", function (d) {
            //return d.r > 20 ? 1 : 0; - AAG
            return d.r > 40 ? 1 : 0;
        })
        .text(function (d) {
            return d.name;
        });

    d3.select(window).on("click", function () {
        zoom(root);
    });
});

function countRecursive(node) {
    var count = 0;
    if (node.count) {
        count = node.count;
    } else if (node.children) {
        node.children.forEach(function (child) {
            count += countRecursive(child);
        });
    }
    return count;
}

function zoom(d, i) {
    var k = r / d.r / 2;
    x.domain([d.x - d.r, d.x + d.r]);
    y.domain([d.y - d.r, d.y + d.r]);

    var t = vis.transition()
        .duration(d3.event.altKey ? 7500 : 750);

    t.selectAll("circle")
        .attr("cx", function (d) {
            return x(d.x);
        })
        .attr("cy", function (d) {
            return y(d.y);
        })
        .attr("r", function (d) {
            return k * d.r;
        });

    t.selectAll("text")
        .attr("x", function (d) {
            return x(d.x);
        })
        .attr("y", function (d) {
            return y(d.y);
        })
        .style("opacity", function (d) {
            //return k * d.r > 20 ? 1 : 0; - AAG
            return k * d.r > 40 ? 1 : 0;
        });

    node = d;
    d3.event.stopPropagation();
}