/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, windows: true, forin: true, vars: true, nomen: true, bitwise: true, regexp: true, indent: 4, maxerr: 50*/
/*jshint -W030*/
/*jshint expr:true*/
var w = 1280,
    h = 800,
    r = 720,
    x = d3.scale.linear().range([0, r]),
    y = d3.scale.linear().range([0, r]),
    node,
    root;

var pack = d3.layout.pack()
    .size([r, r])
    .value(function (d) {
        return d.size;
    })
    .sort(function (a, b) {
        return b.value - a.value;
    });

var vis = d3.select("body").insert("svg:svg", "h2")
    .attr("width", w)
    .attr("height", h)
    .append("svg:g");
//.attr("transform", "translate(" + (w - r) / 2 + "," + (h - r) / 2 + ")");

d3.json("incidents-detailed.json", function (data) {
    node = root = data;

    console.log("data=" + JSON.stringify(data));

    var nodes = pack.nodes(root);

    vis.selectAll("circle")
        .data(nodes)
        .enter().append("svg:circle")
        .attr("class", function (d) {
            return d.children ? "parent" : "child";
        })
        .attr("fill", function (d) {
            if (!d.children) {
                if (d.weight) {
                    return 'firebrick';
                } else {
                    return '#adc'; //'#ccc'; //'#1f77b4';
                }
            }
        })
        .attr("opacity", function (d) {
            if (!d.children) {
                if (!d.weight) {
                    return "0.2";
                } else if (d.weight >= 1) {
                    return "1.0";
                } else if (d.weight <= 0.05) {
                    return "0.05";
                } else {
                    return "" + d.weight;
                }
            }
        })
        .attr("cx", function (d) {
            return d.x;
        })
        .attr("cy", function (d) {
            return d.y;
        })
        .attr("r", function (d) {
            return d.r;
        })
        .on("click", function (d) {
            return zoom(node == d ? root : d);
        })
        .on("mouseover", function (d) {
            //console.log("this.name=" + d.name);
            var text = [];
            var focus = d;
            while (focus && focus.name !== "root") {
                text.unshift(focus.name);
                focus = focus.parent;
            }
            if (text.length === 0) {
                text = ['root'];
            }
            //console.log(text);
            //console.log(text.join(" : "));
            var weight = '';
            //            if (d.weight) {
            //                weight += " (weight = " + d.weight.toFixed(3) + ")";
            //            }
            if (d.count) {
                weight += " (incidents = " + d.count.toFixed(0) + ")";
            }

            d3.select("#selectedNode").text(function (d) {
                return text.join(" : ") + weight;
            });
        });

    vis.selectAll("text")
        .data(nodes)
        .enter().append("svg:text")
        .attr("class", function (d) {
            return d.children ? "parent" : "child";
        })
        .attr("x", function (d) {
            return d.x;
        })
        .attr("y", function (d) {
            return d.y;
        })
        .attr("dy", ".35em")
        //        .attr("dx", function (d) {
        //            console.log("dx=" + d.r);
        //            return "" + d.r * .8 + "px";
        //        })
        //        .attr("dy", function (d) {
        //            return "" + d.r * .8 + "px";
        //        })
        .attr("text-anchor", "middle")
        .style("opacity", function (d) {
            //return d.r > 20 ? 1 : 0; - AAG
            return d.r > 40 ? 1 : 0;
        })
        .text(function (d) {
            return d.name;
        });

    d3.select(window).on("click", function () {
        zoom(root);
    });
});

function zoom(d, i) {
    var k = r / d.r / 2;
    x.domain([d.x - d.r, d.x + d.r]);
    y.domain([d.y - d.r, d.y + d.r]);

    var t = vis.transition()
        //.duration(d3.event.altKey ? 7500 : 750);
        .duration(0);

    t.selectAll("circle")
        .attr("cx", function (d) {
            return x(d.x);
        })
        .attr("cy", function (d) {
            return y(d.y);
        })
        .attr("r", function (d) {
            return k * d.r;
        });

    t.selectAll("text")
        .attr("x", function (d) {
            return x(d.x);
        })
        .attr("y", function (d) {
            return y(d.y);
        })
        .style("opacity", function (d) {
            //return k * d.r > 20 ? 1 : 0; - AAG
            return k * d.r > 40 ? 1 : 0;
        });

    node = d;
    d3.event.stopPropagation();
}