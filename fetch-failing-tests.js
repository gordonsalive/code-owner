/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

// This script:
// (1) trim the ROLREFTSTF table of aged data
// (2) Load all scenarios that have no fail_end datetime (i.e. still failing) (by view and sub-item)
// (3) Load failing scenarios from cuke-results file (by view and sub-item)
// (4) Compare the two:
//    (i) spin through already failing test, if they are not in cuke-resutls then give them a fail_end datetime
//    (ii) spin through cuke-results, if they are missing from already failing tests then add them with blank fail_end
// (5) spin through the updated failing tests list and: 
//    (i) update ROLREFTSTF if the failing test is now passing again
//    (ii) insert into ROLREFTSTF if a test has just started failing

//var fs = require('fs');
//var Qfs = require("q-io/fs");
//var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var pool = require('./scripts/shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');
var read_json = require('./scripts/read-json');
var promisify = require('./scripts/promisify');

var resultsJson = {};

writeOutStatus('failed');

(function doMainStuff() {
    var now = new Date();
    var nowInt = (now.getFullYear() * 10000000000) +
        ((now.getMonth() + 1) * 100000000) +
        (now.getDate() * 1000000) +
        (now.getHours() * 10000) +
        (now.getMinutes() * 100) +
        now.getSeconds();
    if (DEBUG) {
        console.log('--mainLoop:' + now.toTimeString());
    }

    //get 1 month ago cut off
    var oneMonthAgo = new Date();
    oneMonthAgo.setMonth(now.getMonth() - 1);
    //console.log(oneMonthAgo);
    var oneMonthAgoInt = (oneMonthAgo.getFullYear() * 10000) +
        ((oneMonthAgo.getMonth() + 1) * 100) +
        oneMonthAgo.getDate();
    oneMonthAgoInt *= 1000000; //multiple int date to make into datetime 
    //console.log(oneMonthAgoInt);
    //get 2 months ago cut off
    var twoMonthsAgo = new Date();
    twoMonthsAgo.setMonth(now.getMonth() - 2);
    //console.log(twoMonthsAgo);
    var twoMonthsAgoInt = (twoMonthsAgo.getFullYear() * 10000) +
        ((twoMonthsAgo.getMonth() + 1) * 100) +
        twoMonthsAgo.getDate();
    twoMonthsAgoInt *= 1000000; //multiple int date to make into datetime
    //console.log(twoMonthsAgoInt);

    pool
        .query(
            " SELECT fail_end FROM qgpl.rolreftstf " +
            " WHERE fail_end > 0 " +
            " AND fail_end < " + oneMonthAgoInt)
        .then(function (rows) {
            // (1) trim the ROLREFTSTF table of aged data
            if ((rows) && (rows.length > 0)) {
                if (DEBUG) {
                    console.log("..there are rows to delete!");
                }
                return pool.query(
                    " DELETE FROM qgpl.rolreftstf " +
                    " WHERE fail_end > 0 " +
                    " AND fail_end < " + oneMonthAgoInt);
            } else {
                return promisify(0);
            }
        })
        .then(function (deleteResult) {
            //flag up any records that have been failing for more than 2 months!
            if (DEBUG) {
                console.log("..deleted failing test info if test started working again more than 1 month ago (if any)");
            }
            if (DEBUG) {
                console.log("..now see if there are any records for items that have been failing for more than 2 months!");
            }
            return pool.query(
                    " SELECT view, item, type, fail_start, variable, value " +
                    " FROM qgpl.rolreftstf " +
                    " WHERE fail_start > 0 " +
                    " AND fail_end = 0 " +
                    " AND fail_start < " + twoMonthsAgoInt)
                .then(function (rows) {
                    var value = {};
                    //have data in the form
                    // [ { "VIEW": "-",
                    //     "ITEM": "flat",
                    //     "TYPE": "flat", 
                    //     "FAIL_START": 20160101, 
                    //     "VARIABLE": "scenario", 
                    //     "VALUE": "Some Feature:Some test scenario about something very important" } ]
                    if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                    if ((rows) && (rows.length > 0)) {
                        _.each(rows, function (row) {
                            //*****************
                            //*****************
                            //*****************
                            //console.log("Oh no! Test failing for more than 2 months! - " + row.VALUE);
                        });
                    }
                    return rows;
                });
        })
        .then(function (results) {
            // (2) Load all scenarios that have no fail_end datetime (i.e. still failing) (by view and sub-item)
            if (DEBUG) {
                console.log("..now load all failing tests without a fail_end datetime");
            }

            return pool.query(
                    " SELECT view, item, type, fail_start, variable, value " +
                    " FROM qgpl.rolreftstf " +
                    " WHERE variable = 'scenario' " +
                    " AND fail_start > 0 " +
                    " AND fail_end = 0 ")
                .then(function (rows) {
                    //have data in the form
                    // [ { "VIEW": "-",
                    //     "ITEM": "flat",
                    //     "TYPE": "flat", 
                    //     "FAIL_START": 20160101, 
                    //     "VARIABLE": "scenario", 
                    //     "VALUE": "Some Feature:Some test scenario about something very important" } ]
                    if (DEBUG) {
                        console.log('returned from main query - still failing tests');
                        //console.log('Result of Query=' + JSON.stringify(result, null, "  "));
                    }
                    return rows;
                })
                .then(function (results) {
                    //now group up the data by view and sub-item
                    var groupedByView = _.groupBy(results, 'VIEW');
                    //now I have {viewName: [ { "VIEW": "viewname", ... } ] }
                    var groupedByViewAndItem = _.mapObject(groupedByView, function (view) {
                        return _.groupBy(view, 'ITEM');
                    });
                    //_.each(groupedByView, function (view) {
                    //    groupedByView[view] = _.groupBy(view, 'ITEM');
                    //});
                    return groupedByViewAndItem;
                })
                .then(function (results) {
                    //so at this point we have already failing tests in the form:
                    // {
                    //     "viewName": {
                    //         "sub-item": [ {
                    //             "VIEW": "viewName",
                    //              "ITEM": "sub-item",
                    //              "TYPE": "flat", 
                    //              "FAIL_START": 20160101, 
                    //              "VARIABLE": "scenario", 
                    //              "VALUE": "Some Feature:Some test scenario about something very important"
                    //         } ]
                    //     }
                    // }
                    // I want to reformat this into style:
                    // {viewName: {sub-item: {failedScenarios: [], type: flat|tree}}
                    //
                    //console.log("alreadyFailing raw =" + JSON.stringify(results['quality-owners-flat-core-scripted-only']));
                    return _.mapObject(results, function (view) {
                        //view is { "sub-item": [ { "VIEW": "viewName", ... } ] }
                        return _.mapObject(view, function (subItemArray) {
                            return {
                                failedScenarios: _.map(subItemArray, function (row) {
                                    //console.log("row=" + JSON.stringify(row));
                                    return row.VALUE;
                                }),
                                type: subItemArray[0].TYPE
                            };
                        });
                    });
                });
        })
        .then(function (alreadyFailing) {
            // (3) Load failing scenarios from cuke-results file (by view and sub-item)
            //console.log("alreadyFailing=" + JSON.stringify(alreadyFailing));
            if (DEBUG) {
                console.log("..now load all the new failing tests from cuke-results");
            }
            //cuke-results are in the form:
            // {
            //      "flat": {
            //          "project": {failedScenarios: []}
            //      },
            //      "tree":  {
            //          "viewName": {
            //              "sub-item" : {
            //                  results: {
            //                      failedScenarios: []
            //                  }
            //              }
            //          }
            //      }
            // }
            // want to convert these into the straight viewName: { sub-item: [ { type: flat|tree, value: "some scenario" } ] }
            return read_json('json/cuke-results.json')
                .then(function (cukeResults) {
                    //(a)put flat in a view called flat and each project can be a sub-item, only add the data I want
                    var results = {
                        flat: _.mapObject(cukeResults.flat, function (project) {
                            return {
                                failedScenarios: project.failedScenarios,
                                type: 'flat'
                            };
                        })
                    };
                    //(b)spin through the views in the "tree" node and the "sub-items" adding the data I want
                    _.each(cukeResults.tree, function (view, viewName) {
                        results[viewName] = _.mapObject(view, function (subItem) {
                            return {
                                failedScenarios: subItem.results.failedScenarios,
                                type: 'tree'
                            };
                        });
                    });
                    return results;
                })
                .then(function (results) {
                    //console.log("newFailing = " + JSON.stringify(results['quality-owners-flat-core-scripted-only']));
                    //put alreadyFailing and newFailing together
                    return {
                        alreadyFailing: alreadyFailing,
                        newFailing: results
                    };
                });
        })
        .then(function (failingScenarios) {
            //console.log('failingScenarios.alreadyFailing.flat=' + JSON.stringify(failingScenarios.alreadyFailing.flat));
            //console.log('failingScenarios.alreadyFailing[quality-owners-flat-core-scripted-only]=' + JSON.stringify(failingScenarios.alreadyFailing['quality-owners-flat-core-scripted-only']));
            // (4) Compare the two:
            //    (i) spin through already failing test, if they are not in cuke-resutls then give them a fail_end datetime
            //          (this will include any tests that have been deleted);
            //    (ii) spin through cuke-results, if they are missing from already failing tests then add them with blank fail_end.
            if (DEBUG) {
                console.log("..now spin through already failing tests, if they are not in new failing tests then give them a fail_end datetime:" + nowInt);
            }
            //failingScenarios is of the form:
            // { alreadyFailing: {view: {sub-item: {failedScenarios: []}}},
            //   newFailing: {view: {sub-item: {failedScenarios: []}} }
            _.each(failingScenarios.alreadyFailing, function (view, viewName) {
                _.each(view, function (subItem, subItemName) {
                    _.each(subItem.failedScenarios, function (scenario) {
                        //if we can't find this scenario is the corresponding list of newly failing tests,
                        // then give this a fail_end datetime
                        if (!failingScenarios.newFailing[viewName] ||
                            !failingScenarios.newFailing[viewName][subItemName] ||
                            (!_.contains(failingScenarios.newFailing[viewName][subItemName].failedScenarios, scenario))) {
                            if (!failingScenarios.alreadyFailing[viewName][subItemName].nowPassing) {
                                failingScenarios.alreadyFailing[viewName][subItemName].nowPassing = [];
                            }
                            //console.log('found a test thats stopped failing:' + scenario + ':' + viewName + ':' + subItemName);
                            failingScenarios.alreadyFailing[viewName][subItemName].nowPassing.push(scenario);
                        }
                    });
                });
            });
            if (DEBUG) {
                console.log("..now spin through newly failing tests, if they are not in already failing tests then add them with blank fail_end datetime:" + nowInt);
            }
            _.each(failingScenarios.newFailing, function (view, viewName) {
                _.each(view, function (subItem, subItemName) {
                    _.each(subItem.failedScenarios, function (scenario) {
                        //if we can't find the scenario in the already failing list, then add it as newlyFailing
                        if (!failingScenarios.alreadyFailing[viewName]) {
                            failingScenarios.alreadyFailing[viewName] = {};
                        }
                        if (!failingScenarios.alreadyFailing[viewName][subItemName]) {
                            failingScenarios.alreadyFailing[viewName][subItemName] = {
                                type: failingScenarios.newFailing[viewName][subItemName].type
                            };
                        }
                        if (!failingScenarios.alreadyFailing[viewName][subItemName].failedScenarios ||
                            (!_.contains(failingScenarios.alreadyFailing[viewName][subItemName].failedScenarios, scenario))) {
                            //console.log('adding an item to newFailing:' +
                            //    failingScenarios.alreadyFailing[viewName][subItemName].failedScenarios +
                            //    ':' + scenario + ':' );
                            if (!failingScenarios.alreadyFailing[viewName][subItemName].newFailing) {
                                failingScenarios.alreadyFailing[viewName][subItemName].newFailing = [];
                            }
                            failingScenarios.alreadyFailing[viewName][subItemName].newFailing.push(scenario);
                        }
                    });
                });
            });
            return failingScenarios.alreadyFailing;
        })
        .then(function (updatedFailing) {
            //console.log("updatedFailing=" + JSON.stringify(updatedFailing['quality-owners-flat-core-scripted-only']));
            // (5) spin through the updated failing tests list and: 
            //    (i) update ROLREFTSTF if the failing test is now passing again
            //    (ii) insert into ROLREFTSTF if a test has just started failing
            if (DEBUG) {
                console.log("..now insert into ROLREFTSTF, with 0 fail_end datetime to those that are newly failing.");
            }
            //updatedFailing is of the form:
            //{view: {sub-item: {type: flat|tree, failedScenarios: [], newFailing: [], nowPassing: []}}}

            var inserts = _.flatten(
                _.map(updatedFailing, function (view, viewName) {
                    return _.flatten(
                        _.map(view, function (subItem, subItemName) {
                            //inserts
                            return _.map(subItem.newFailing, function (scenario) {
                                return [
                                    viewName,
                                    subItemName,
                                    subItem.type,
                                    nowInt,
                                    0,
                                    "scenario",
                                    scenario
                                ];
                            });
                        }), true);
                }), true);
            //console.log("inserts=" + JSON.stringify(_.first(inserts, 4)));

            var updates = _.flatten(
                _.map(updatedFailing, function (view, viewName) {
                    return _.flatten(
                        _.map(view, function (subItem, subItemName) {
                            //updates
                            return _.map(subItem.nowPassing, function (scenario) {
                                return [
                                    nowInt,
                                    viewName,
                                    subItemName,
                                    subItem.type,
                                    scenario
                                ];
                            });
                        }), true);
                }), true);
            //console.log("updates=" + JSON.stringify(_.first(updates, 4)));

            return pool.batchUpdate(
                    " INSERT INTO qgpl.rolreftstf " +
                    " (view, item, type, fail_start, fail_end, variable, value) " +
                    " VALUES " +
                    " ( ?, ?, ?, ?, ?, ?, ? ) ", inserts)
                .then(function (rows) {
                    if (DEBUG) {
                        console.log("..now update ROLREFTSTF, giving fail_end datetime to those that are now passing:" + nowInt);
                    }

                    return pool.batchUpdate(
                        " UPDATE qgpl.rolreftstf " +
                        " SET fail_end = ? " +
                        " WHERE view = ? " +
                        " AND item = ? " +
                        " AND type = ? " +
                        " AND variable = 'scenario' " +
                        " AND value = ? ", updates);
                });
        })
        .then(function (result) {
            var d = new Date();
            if (DEBUG) console.log('Completed: ' + d.toLocaleString() + '. Wait 60 minutes before repeating...');
            setTimeout(doMainStuff, 60 * 60 * 1000); /*try again in 60 minutes*/
        })
        .fail(function (err) {
            console.error(err);
            //update status (even if done already)
            writeOutStatus('failed');
            var d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 120 minutes');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 2 * 60 * 60 * 1000); //try again in 120 minutes
        });

})();

function writeOutStatus(connectionStatus) {
    update_status(connectionStatus, 'failed-tests');
}