/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 28/10/2016
// uses git to fetch figaro-features documentation, this can then be published by code-owners
//**********

var sys = require('sys');
var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var read_json = require("./scripts/read-json");
var promisify = require("./scripts/promisify");
var delete_folder_recursive_promise = require("./scripts/delete-folder-recursive-promise");
var exec_promise = require("./scripts/exec-promise");

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');

var resultsJson = {};


writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    //console.log('--parsefile..');
    read_json('json/features.json')
        .then(function (featuresFile) {
            var results = featuresFile.documentation;
            return results;
        })
        .then(function (documentation) {
            //delete the existing directory
            console.log('deleting existing figaro-features folder');
            return delete_folder_recursive_promise(documentation.name)
                .then(function (deleteResult) {
                    return documentation;
                });
        })
        .then(function (documentation) {
            return exec_promise("git clone --depth 1 " + documentation.repo);
        })
        .then(function (result) {
            console.log('--update status..');
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            console.log('..repeat in 1 day');
            /*repeat in 30 seconds*/
            setTimeout(doMainStuff, 24 * 60 * 60 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 1');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 60 * 60 * 1000); //try again in 1 hour
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'feature-docs');
}