/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 29/3/'16
// construct a tree containing the locations of the cuke.json files for all the code owners
// at specialistion, component and sub-component levels and also all the quality owners
// at projects level (for use in the Quality Owner View)
// missing nodes can be infered as the concatenation of all sub-tree nodes, if they are complete.
//
// results returned as a promise
//
// (cukes might be stored locally for testing/prototyping <- this comment to be moved to where cukes are read in)
//
//assume cukes tree in cukes.json is always a full tree made my copying the full tree from
//code-owners, and then imposing on top of it the cuke info that has been specified
//and then inferring any missing nodes where the cuke info in the tree beneath it is complete
var _ = require("underscore");
var Qfs = require("q-io/fs");
var Q = require("q");

var readJson = require("./read-json");
var promisify = require("./promisify");

module.exports = function (cukesJson) {

    function inferMissingCukeInfo(node) {
        function deriveCukeInfoFromSubNodes() {
            function allSubNodesHaveCukeInfo(thisNode) {
                var result = false;
                if ((thisNode.items) && (thisNode.items.length > 0)) {
                    result = _.every(thisNode.items, function (item) {
                        return thisNode[item].jenkinsJobs;
                    });
                }
                return result;
            }

            if (allSubNodesHaveCukeInfo(node)) {
                //update the 'jenkinsJobs' and tags properties on this node
                node.jenkinsJobs = _.reduce(node.items, function (memo, item) {
                    return memo.concat(node[item].jenkinsJobs);
                }, []);
                node.tags = _.reduce(node.items, function (memo, item) {
                    return memo.concat(node[item].tags);
                }, []);
            }
        }
        //do we have cuke info on this node?
        //..yes, good, don't need to do anything with this node, but still need to recurse
        //   into sub-nodes
        //..no, after we have recursed into sub-nodes, if all the sub-nodes have cuke info
        //   I can construct mine by concatenating the cuke info of the sub-nodes
        if (node.items) {
            _.each(node.items, function (item) {
                if (node[item]) {
                    inferMissingCukeInfo(node[item]);
                } else {
                    throw 'item ' + item + ' is missing from the object!';
                }
            });
        }
        if (!node.jenkinsJobs) {
            deriveCukeInfoFromSubNodes();
        }
    }

    function asynchInfer(cukesPromise) {
        return cukesPromise.then(function (cukes) {
            return Q.Promise(function (resolve, reject, notify) {
                try {
                    inferMissingCukeInfo(cukes['code-owners-tree']);
                    resolve(cukes);
                } catch (err) {
                    reject(err);
                }
            });
        });
    }

    function enrichTagsByConvention(node) {
        //ignoring to first layer of the tree (the views), spin through all nodes of tree
        //adding to the tags a tag based on convention: 
        //  @cap(specialism.component.sub-component)
    }

    function asynchEnrichByConvention(cukesPromise) {
        return cukesPromise.then(function (cukes) {
            return Q.Promise(function (resolve, reject, notify) {
                try {
                    //enrichTagsByConvention(cukes['code-owners-tree']);
                    resolve(cukes);
                } catch (err) {
                    reject(err);
                }
            });
        });
    }

    var cukesPromise;

    //(1) load up the cukes tree with data we have
    if (cukesJson) {
        //they've either passed in the location of the json, or the object itself
        if (typeof (cukesJson) === "string") {
            cukesPromise = readJson(cukesJson);
        } else {
            cukesPromise = promisify(cukesJson);
        }
    } else {
        cukesPromise = readJson("./cukes.json");
    }

    //(2) infer any missing nodes if they have sub-trees with complete cuke info
    cukesPromise = asynchInfer(cukesPromise);

    //(3) enrich tags by convention (e.g. assume cap(rpg.fees) for fees component in rpg spec.)
    return asynchEnrichByConvention(cukesPromise);
};