/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, esnext:true*/
/*jshint -W030*/
/*jshint expr:true*/
var Qfs = require("q-io/fs");
var Q = require("q");
var fs = require('fs');

//var spawn = require('child_process').spawn;
//var child_process = require('child_process');

var DEBUG = 0; //0=of

function puts(error, stdout, stderr) {
    console.log('in puts');
    console.log(stdout + stderr);
}

module.exports = function (command, silent, timeout) {
    var commandAndArgs = command.split(' ');
    //if (DEBUG) console.log(commandAndArgs);
    return Q.Promise(function (resolve, reject, notify) {
        var noisy = !silent;
        var child_process = require('child_process');
        var oldSpawn = child_process.spawn;

        function mySpawn() {
            if (DEBUG) console.log('spawn called');
            if (DEBUG) console.log(arguments);
            var result = oldSpawn.apply(this, arguments);
            return result;
        }
        child_process.spawn = mySpawn;
        var spawn = child_process.spawn;

        function onComplete(err, stdout, stderr) {
            if (err) {
                console.log('exec(' + command + '): failed.');
                console.error('err:' + err); // + '\nstderr:' + stderr);
                reject(err);
            } else {
                if (noisy) {
                    console.log('exec(' + command + '): succeeded.');
                    if (DEBUG) {
                        console.log(stdout);
                    }
                }
                resolve(stdout);
            }
        }
        //spawn the process and handle streams
        //console.log(process.env.PATH);
        //The shift() method removes the first element from an array and returns that element.
        if (DEBUG) console.log('spawn with:' + commandAndArgs);
        const proc = spawn(commandAndArgs.shift(), commandAndArgs)
            .on('error', function (err) {
                throw err;
            });
        //handle streams
        var output;
        proc.stdout.on('data', (data) => {
            output += data;
            if (noisy) {
                console.log('data=' + data);
            }
        });
        proc.stderr.on('data', (data) => {
            //proc.exit(1); // <<<< this works as expected and exits the process asap
            onComplete(data, '', data);
        });
        proc.on('close', (code) => {
            if (noisy) {
                console.log('child process exited with code' + code);
            }
            if (code) {
                onComplete(code, '', 'child process exited with non-zero code.');
            } else {
                onComplete(null, output);
            }
        });
        //prmise won't wait forever, it will timeout
        if (timeout) {
            setTimeout(function () {
                reject('timeout:' + timeout);
            }, timeout);
        } else {
            setTimeout(function () {
                reject('timeout');
            }, 5 * 60 * 1000); //wait 5 minutes max!
        }
    });
};