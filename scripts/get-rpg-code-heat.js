/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 17/5/2017
// Module to spin through incident data and update rpg code base data to produce code heat data
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
var read_json = require('./read-json');
//var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');

module.exports = function (periodStart, periodEnd, subDir, runName) {
    console.log('get-rpg-code-heat(' + runName + ')');
    return read_json('json/' + runName + '-incident-changes-results.json')
        .then(function (incidents) {
            //pull out all the RPG changes into array, capture object type, source type and object name
            var results = [];
            _.each(incidents, function (incident) {
                if (incident.rpgChanges) {
                    //for now just look at the F63 changes
                    var f63Changes = _.where(incident.rpgChanges, {
                        APPLICATION: 'F63'
                    });

                    var f63ChangesUniq = _.reduce(f63Changes, function (memo, item) {
                        var existingItem = _.find(memo, function (memoItem) {
                            return ((memoItem.OBJECT_TYPE === item.OBJECT_TYPE) &&
                                (memoItem.OBJECT_ATTRIBUTE === item.OBJECT_ATTRIBUTE.substring(0, 4)) &&
                                (memoItem.OBJECT_NAME === item.OBJECT_NAME));
                        });
                        //don't add if item already added, or if it's certain types of object
                        if ((!existingItem) &&
                            (!_.contains(['RPGLE', 'RPGLE_MOD', 'CRDTA', 'SRV_SRC', '*SRVPGM'], item.OBJECT_ATTRIBUTE)) &&
                            (parseInt(item.DATE_REQUESTED) > periodStart) &&
                            (parseInt(item.DATE_REQUESTED) < periodEnd)) {
                            //if (item.OBJECT_NAME === 'SCN993') {
                            //    console.log('new item:' + item.OBJECT_TYPE, item.OBJECT_ATTRIBUTE, item.OBJECT_NAME);
                            //    console.log('memo:' + JSON.stringify(memo));
                            //    console.log('existing item:' + existingItem);
                            //}
                            memo.push({
                                OBJECT_TYPE: item.OBJECT_TYPE,
                                OBJECT_ATTRIBUTE: item.OBJECT_ATTRIBUTE.substring(0, 4),
                                ////objectAttribute: objectAttribute.substring(0, 4),
                                OBJECT_NAME: item.OBJECT_NAME,
                                JOB_DESCRIPTION: item.JOB_DESCRIPTION,
                                FOUND_IN_VERSION: item.FOUND_IN_VERSION,
                                DATE_REQUESTED: item.DATE_REQUESTED
                            });
                        }
                        return memo;

                    }, []);
                    //add these into our results
                    results = results.concat(
                        _.map(f63ChangesUniq, function (change) {
                            return {
                                objectType: change.OBJECT_TYPE,
                                sourceType: change.OBJECT_ATTRIBUTE,
                                name: change.OBJECT_NAME,
                                job: incident.job,
                                incident: incident.incident,
                                description: change.JOB_DESCRIPTION,
                                foundInVersion: change.FOUND_IN_VERSION,
                                dateRequested: change.DATE_REQUESTED
                            };
                        })
                    );
                }
            });
            return results;
        })
        .then(function (rpgIncidentChanges) {
            //load up the rpg code changes (arranged in hierarchy: team, area, object type and source type)
            return read_json('json/rpg-code-base-unweighted.json')
                .then(function (rpgCodeBase) {
                    var allMatches = [];
                    // ! create 2 trees, a summary one just down to sourceType and a detailed on down to sourceItem
                    var results = {
                        summary: {
                            name: 'root',
                            children: _.map(rpgCodeBase.children, function (team) {
                                return {
                                    name: team.name,
                                    children: _.map(team.children, function (area) {
                                        return {
                                            name: area.name,
                                            children: _.map(area.children, function (objectType) {
                                                return {
                                                    name: objectType.name,
                                                    children: _.reduce(objectType.children, function (memo, sourceType) {
                                                        var sourceTypeGroup = getSourceTypeGroup(sourceType.name);
                                                        var sourceItemSummary = {
                                                            size: 0,
                                                            weight: 0,
                                                            defects: [] //defect instance details
                                                        };
                                                        _.each(sourceType.children, function (sourceItem) {
                                                            //console.log(".sourceItem=" + JSON.stringify(sourceItem));
                                                            sourceItemSummary.size += parseInt(sourceItem.size);
                                                            var matches = _.where(rpgIncidentChanges, {
                                                                objectType: objectType.name,
                                                                sourceType: sourceType.name.substring(0, 4),
                                                                name: sourceItem.name
                                                            });
                                                            if (matches.length > 0) {
                                                                allMatches = allMatches.concat(matches);
                                                                sourceItemSummary.weight += matches.length;
                                                                _.each(matches, function (match) {
                                                                    sourceItemSummary.defects.push({
                                                                        job: match.job,
                                                                        incident: match.incident,
                                                                        description: match.description,
                                                                        foundInVersion: match.foundInVersion,
                                                                        dateRequested: match.dateRequested
                                                                    });
                                                                });
                                                            }
                                                            //if (sourceItem.name === 'OR2.OR03') {
                                                            //    console.log(JSON.stringify(matches));
                                                            //}
                                                        });
                                                        //if it alrady exists in the memo we update it, else add it
                                                        if (_.where(memo, {
                                                                name: sourceTypeGroup
                                                            }).length) {
                                                            //console.log('..already in ' + sourceTypeGroup);
                                                            //already in list, so add to existing item
                                                            _.each(memo, function (item) {
                                                                if (item.name === sourceTypeGroup) {
                                                                    item.size += sourceItemSummary.size;
                                                                    item.weight += sourceItemSummary.weight;
                                                                    //item.defects.push(sourceItemSummary.defects);
                                                                    item.defects = item.defects.concat(sourceItemSummary.defects);
                                                                }
                                                            });
                                                        } else {
                                                            //console.log('..adding new item for ' + sourceTypeGroup);
                                                            //it's a new item so add it to the list
                                                            memo.push({
                                                                //name: sourceType.name,
                                                                name: sourceTypeGroup,
                                                                children: [],
                                                                size: sourceItemSummary.size,
                                                                weight: sourceItemSummary.weight,
                                                                defects: sourceItemSummary.defects
                                                            });
                                                        }
                                                        return memo;
                                                    }, [])
                                                };
                                            })
                                        };
                                    })
                                };
                            })
                        },
                        detailed: {
                            name: 'root',
                            children: _.map(rpgCodeBase.children, function (team) {
                                return {
                                    name: team.name,
                                    children: _.map(team.children, function (area) {
                                        return {
                                            name: area.name,
                                            children: _.map(area.children, function (objectType) {
                                                return {
                                                    name: objectType.name,
                                                    children: _.reduce(objectType.children, function (memo, sourceType) {
                                                            var sourceTypeGroup = getSourceTypeGroup(sourceType.name);
                                                            var sourceTypeChildren = _.map(sourceType.children, function (sourceItem) {
                                                                var matches = _.where(rpgIncidentChanges, {
                                                                    objectType: objectType.name,
                                                                    sourceType: sourceType.name.substring(0, 4),
                                                                    name: sourceItem.name
                                                                });
                                                                return {
                                                                    name: sourceItem.name,
                                                                    children: [],
                                                                    size: sourceItem.size,
                                                                    weight: matches.length,
                                                                    defects: _.map(matches, function (match) {
                                                                        return {
                                                                            job: match.job,
                                                                            incident: match.incident,
                                                                            description: match.description,
                                                                            foundInVersion: match.foundInVersion,
                                                                            dateRequested: match.dateRequested
                                                                        };
                                                                    })
                                                                };
                                                            });
                                                            //if it alrady exists in the memo we update it, else add it
                                                            if (_.where(memo, {
                                                                    name: sourceTypeGroup
                                                                }).length) {
                                                                //console.log('already in ' + team.name + ':' + area.name + ':' + objectType.name + ':' + sourceType.name + '.' + sourceTypeGroup);
                                                                //already in list, so add to existing item
                                                                _.each(memo, function (item) {
                                                                    if (item.name === sourceTypeGroup) {
                                                                        //.log('about to concat...');
                                                                        item.children = item.children.concat(sourceTypeChildren);
                                                                    }
                                                                });
                                                            } else {
                                                                //console.log('adding new item for ' + team.name + ':' + 
                                                                //area.name + ':' + objectType.name + ':' + 
                                                                //sourceType.name + '.' + sourceTypeGroup);
                                                                //it's a new item so add it to the list
                                                                memo.push({ //name: sourceType.name,
                                                                    name: sourceTypeGroup,
                                                                    children: sourceTypeChildren
                                                                });
                                                            }
                                                            return memo;
                                                        }, [])
                                                        //_.map(objectType.children, function (sourceType) {
                                                        //    return {
                                                        //        name: sourceType.name,
                                                        //        children: _.map(sourceType.children, function (sourceItem) {
                                                        //            var matches = _.where(rpgIncidentChanges, {
                                                        //                objectType: objectType.name,
                                                        //                sourceType: sourceType.name.substring(0, 4),
                                                        //                name: sourceItem.name
                                                        //            });
                                                        //            //if (matches.length > 0) {
                                                        //            //    allMatches = allMatches.concat(matches);
                                                        //            //}
                                                        //            return {
                                                        //                name: sourceItem.name,
                                                        //                children: [],
                                                        //                size: sourceItem.size,
                                                        //                weight: matches.length
                                                        //            };
                                                        //        })
                                                        //    };
                                                        //})
                                                };
                                            })
                                        };
                                    })
                                };
                            })
                        }
                    };
                    //console.log(JSON.stringify(results.summary));
                    //console.log(JSON.stringify(allMatches));
                    //now add in all the results that didn't match:
                    //(i) get list of items we didn't match
                    //var unmatched = _.without(rpgIncidentChanges, allMatches);
                    var unmatched = _.reject(rpgIncidentChanges, function (item) {
                        return _.findWhere(allMatches, {
                            objectType: item.objectType,
                            sourceType: item.sourceType,
                            name: item.name
                        });
                    });
                    //(ii) group this list to get some structure to display
                    var groupedByObjectSourceItem = _.mapObject(
                        _.groupBy(unmatched, 'objectType'),
                        function (objectType) {
                            return _.mapObject(
                                _.groupBy(objectType, 'sourceType'),
                                function (sourceType) {
                                    return _.groupBy(sourceType, 'name');
                                }
                            );
                        }
                    );
                    //console.log("\n\ngroupedByObjectSourceItem=" + JSON.stringify(groupedByObjectSourceItem));
                    var unmatchedChildren = _.map(groupedByObjectSourceItem, function (objectType, objectTypeName) {
                        return {
                            name: objectTypeName,
                            children: _.map(objectType, function (sourceType, sourceTypeName) {
                                return {
                                    name: sourceTypeName,
                                    children: _.map(sourceType, function (sourceItem, sourceItemName) {
                                        //console.log("sourceItem =" + JSON.stringify(sourceItem));
                                        return {
                                            name: sourceItemName,
                                            children: [],
                                            size: 500,
                                            weight: 0.25 * sourceItem.length
                                        };
                                    })
                                };
                            })
                        };
                    });
                    //(iii) add these in as an unmatched node into results
                    var unmatchedNode = {
                        name: "unmatched",
                        children: unmatchedChildren
                    };
                    results.summary.children.push(unmatchedNode);
                    results.detailed.children.push(unmatchedNode);
                    return results;
                });
        })
        .then(function (rpgCodeBaseWithCounts) {
            //console.log("---rpgCodeBaseWithCounts.unmatched=" + JSON.stringify(rpgCodeBaseWithCounts.unmatched));
            //now convert the counts in to weights (find the max then devide all counts by that, simples...)
            function findMaxRecursive(node, max) {
                //console.log(node.name + ':' + max);
                if ((node.weight) && (node.weight > max)) {
                    max = node.weight;
                }
                if ((node.children) && (node.children.length > 0)) {
                    _.each(node.children, function (item) {
                        max = findMaxRecursive(item, max);
                    });
                }
                return max;
            }
            var maxes = {
                summary: findMaxRecursive(rpgCodeBaseWithCounts.summary, 0),
                detailed: findMaxRecursive(rpgCodeBaseWithCounts.detailed, 0)
            };
            //console.log("===rpgCodeBaseWithCounts=" + JSON.stringify(rpgCodeBaseWithCounts));
            //console.log("maxes = " + JSON.stringify(maxes));
            //now we have a max we need to update the weightings.  We can do this in a similar way
            function updateCountsToWeightsRecursiveAndSort(node, max) {
                if (node.weight) {
                    //console.log("weight=" + node.weight);
                    node.count = node.weight;
                    node.weight = node.weight / max;
                }
                if ((node.children) && (node.children.length > 0)) {
                    _.each(node.children, function (item) {
                        updateCountsToWeightsRecursiveAndSort(item, max);
                    });
                }
            }
            updateCountsToWeightsRecursiveAndSort(rpgCodeBaseWithCounts.summary, maxes.summary);
            updateCountsToWeightsRecursiveAndSort(rpgCodeBaseWithCounts.detailed, maxes.detailed);
            //console.log("+++rpgCodeBaseWithCounts=" + JSON.stringify(rpgCodeBaseWithCounts));
            return rpgCodeBaseWithCounts;
        })
        .then(function (weightedResults) {
            //find the largest
            return Qfs.write("json/" + runName + "-rpg-code-base-summary.json", JSON.stringify(weightedResults.summary, null, '\t'))
                .then(function (writeResult) {
                    return Qfs.write("json/" + runName + "-rpg-code-base-detailed.json", JSON.stringify(weightedResults.detailed, null, '\t'));
                })
                .then(function (writeResult) {
                    return Qfs.write("circles/" + subDir + "/incidents-summary.json", JSON.stringify(weightedResults.summary, null, '\t'));
                })
                .then(function (writeResult) {
                    return Qfs.write("circles/" + subDir + "/incidents-detailed.json", JSON.stringify(weightedResults.detailed, null, '\t'));
                })
                .then(function (writeResult) {
                    return weightedResults;
                });
        });
};

function getSourceTypeGroup(sourceType) {
    if (_.contains(['SQLRPGLE_S', 'SQLR', 'RPGLE_SRC', 'RPGL', 'RPG'], sourceType)) {
        return 'RPG';
    } else if (_.contains(['PF', 'LF', 'SQL'], sourceType)) {
        return 'Database';
    } else if (_.contains(['CLP', 'CLLE'], sourceType)) {
        return 'CLP/CLLE';
    } else {
        return sourceType;
    }
}