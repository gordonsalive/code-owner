/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require('underscore');
var fs = require('fs');
//var read_json = require('./read-json');


// AAG 30/8/'16
// method to add a helper to the data, to make it easier to display with the ECT templates
module.exports = function (data) {
    //console.log('add-test-history-helper');
    //add a helper to data
    data.helper = {};
    data.helper.passesFilter = function (viewName) {
        if (data.helper.params.viewfilter.length > 0) {
            //console.log(data.helper.params.viewfilter);
            //console.log(viewName);
            //console.log('result=' + data.helper.params.viewfilter.indexOf(viewName));
            //console.log('hiya');
            return (data.helper.params.viewfilter.indexOf(viewName) >= 0);
        } else return true;
    };
    data.helper.getDisplayName = function (viewName) {
        var cukes = JSON.parse(fs.readFileSync("./json/cukes.json"));
        return cukes['code-owners-tree'][viewName].info.displayName;
    };
    data.helper.uniqueNo = 0;
    data.helper.getViewChartData = function (viewObj) {
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        //return data as an array, for example:
        //          ['Year', 'Sales', 'Expenses'],
        //          ['2004',  1000,      400],
        //          ['2005',  1170,      460],
        //          ['2006',  660,       1120],
        //          ['2007',  1030,      540]
        //console.log("getDailyChartData");
        var headings = "[" + ["'date'", "'failed'", "'passed'", "'missing'", "'total'"].join(',') + "]";
        //view is in the form:
        //{"20160831":{"passed":24135, "failed": 2906, "missing":408}, "20160901":{...}, ...}
        var rows = _.map(view, function (stats, date) {
            return "[" + ["'" + helper.reformatDateTime(date) + "'", stats.failed, stats.passed, stats.missing, (stats.passed + stats.failed + stats.missing)].join(',') + "]";
        }).join(',');
        if (rows.length > 0) {
            return [headings, rows].join(',');
        } else {
            return headings;
        }
    };
    data.helper.getViewChartPassFailData = function (viewObj) {
        //console.log('passfail');
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        var headings = "[" + ["'date'", "'failed'", "'passed'", "'total'"].join(',') + "]";
        //view is in the form:
        //{"20160831":{"passed":24135, "failed": 2906, "missing":408}, "20160901":{...}, ...}
        var rows = _.map(view, function (stats, date) {
            return "[" + ["'" + helper.reformatDateTime(date) + "'", stats.failed, stats.passed, (stats.passed + stats.failed)].join(',') + "]";
        }).join(',');
        if (rows.length > 0) {
            return [headings, rows].join(',');
        } else {
            return headings;
        }
    };
    data.helper.getViewChartPassFailColumns = function (viewObj) {
        //console.log('passfail - COLUMNS');
        var result = "data.addColumn('date', 'date');\n";
        result += "data.addColumn('number', 'failed');\n";
        result += "data.addColumn('number', 'passed');\n";
        result += "data.addColumn('number', 'total');\n";
        return result;
    };
    data.helper.getViewChartPassFailRows = function (viewObj) {
        //console.log('passfail - ROWS');
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        //view is in the form:
        //{"20160831:103545":{"passed":24135, "failed": 2906, "missing":408}, "20160901:103545":{...}, ...}
        var rows = _.map(view, function (stats, date) {
            return "[" + helper.reformatDateTimeToNewDate(date) + ", " + [stats.failed, stats.passed, (stats.passed + stats.failed)].join(', ') + "]";
        }).join(',\n');
        return "data.addRows([\n" + rows + "\n]);";
    };
    data.helper.getViewChartFailMissingData = function (viewObj) {
        //console.log('failmissing');
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        var headings = "[" + ["'date'", "'failed'", "'missing'", "'total'"].join(',') + "]";
        //view is in the form:
        //{"20160831":{"passed":24135, "failed": 2906, "missing":408}, "20160901":{...}, ...}
        var rows = _.map(view, function (stats, date) {
            return "[" + ["'" + helper.reformatDateTime(date) + "'", stats.failed, stats.missing, (stats.failed + stats.missing)].join(',') + "]";
        }).join(',');
        if (rows.length > 0) {
            return [headings, rows].join(',');
        } else {
            return headings;
        }
    };
    data.helper.getViewChartTotalData = function (viewObj) {
        //console.log('total');
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        var headings = "[" + ["'date'", "'total'", "'total'"].join(',') + "]";
        //view is in the form:
        //{"20160831":{"passed":24135, "failed": 2906, "missing":408}, "20160901":{...}, ...}
        var rows = _.map(view, function (stats, date) {
            return "[" + ["'" + helper.reformatDateTime(date) + "'", (stats.failed + stats.passed), (stats.failed + stats.passed)].join(',') + "]";
        }).join(',');
        if (rows.length > 0) {
            return [headings, rows].join(',');
        } else {
            return headings;
        }
    };
    data.helper.getViewChartSubItemsColumns = function (viewObj) {
        //console.log('subitems - COLUMNS');
        var result = "data.addColumn('string', 'Project');\n";
        result += "data.addColumn('number', 'total');\n";
        return result;
    };
    data.helper.getViewChartSubItemsRows = function (viewObj) {
        //for each item, grab it's name and the total for the most recent number of tests.
        //{history.views.someView.subItemName.date:time.results.{passed,failed,mising}
        //console.log('subitems - ROWS');
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        //view is in the form:
        //{"Carbon UI-Chrome - F63": {"20160831:103545":{results:{"passed":24135, "failed": 2906, "missing":408}}, "20160901:103545":{...}, ...}}
        //for each sub item create a row by taking the sub item name and the total of the results for the most recent item
        var rows = _.map(view, function (stats, subItem) {
            //console.log(subItem);
            return "['" + subItem + "', " +
                _.max(
                    _.map(stats, function (results, key) {
                        //console.log(key);
                        //console.log(Number(key.slice(0, 8)) * 1000000 + Number(key.slice(-6)));
                        //console.log("(J):" + JSON.stringify(results));
                        return {
                            dateTime: Number(key.slice(0, 8)) * 1000000 + Number(key.slice(-6)),
                            total: results.passed + results.failed
                        };
                    }),
                    function (result) {
                        //console.log("~" + JSON.stringify(result));
                        return result.dateTime;
                    }).total + "]";
        }).join(',\n');
        return "data.addRows([\n" + rows + "\n]);";
    };
    data.helper.getViewChartStabilityMonthColumns = function (viewObj) {
        //console.log('teststability - COLUMNS');
        var result = "data.addColumn('string', 'day');\n";
        result += "data.addColumn('number', 'notAllGreen');\n";
        result += "data.addColumn('number', 'allGreen');\n";
        return result;
    };
    data.helper.getViewChartStabilityMonthRows = function (viewObj) {
        //console.log('teststability - ROWS');
        /**
         ** OK, for each of the last 30 days, spin through projects and group by all green or not
         ** then add allGreen together into a single result and not allGreen into another (and stack)
         **/
        //result set looks like {history.stability.viewName.subItem.dateIntStr.{isAllGreen, testsInProject}}
        var view = viewObj.views[viewObj.viewName];
        var helper = viewObj.helper;
        //var today = new Date();
        var daysRange = _.range(0, 30 + 1);
        //console.log('.' + daysRange);
        var results = _.map(daysRange, function (delta) {
            //console.log('testsability - map');
            var day = '-' + delta.toString();
            //console.log('.' + day);
            //what's the matching dateIntStr
            var matchDate = new Date();
            matchDate.setDate(matchDate.getDate() - delta);
            var matchStr = (matchDate.getFullYear() * 10000 +
                (matchDate.getMonth() + 1) * 100 +
                matchDate.getDate()).toString();
            //console.log('.' + matchStr);
            //spin through the sub-items
            var result = _.reduce(view, function (memo, subItem, subItemKey) {
                //console.log('teststability-' + subItemKey);
                //console.log('.reduce:' + memo.day + ':' + JSON.stringify(subItem));
                if (subItem[matchStr]) {
                    if (subItem[matchStr].isAllGreen) {
                        memo.allGreen = memo.allGreen + subItem[matchStr].testsInProject;
                    } else {
                        memo.notAllGreen = memo.notAllGreen + subItem[matchStr].testsInProject;
                    }
                }
                //console.log("teststability memo:" + JSON.stringify(memo));
                return memo;
            }, {
                day: day,
                notAllGreen: 0,
                allGreen: 0
            });
            return "[" + ["'" + day + "'", result.notAllGreen, result.allGreen].join(",") + "]";
        }).reverse().join(",\n");

        return "data.addRows([\n" + results + "\n]);";
    };
    data.helper.setupViewsItems = function (viewObj) {
        var views = viewObj.views;
        var helper = viewObj.helper;
        //console.log("setupViewsItems(" + /*JSON.stringify(viewObj) +*/ ")");
        views.items = _.map(views, function (viewItem, viewName) {
            //console.log("viewname = " + viewName);
            return viewName;
        });
        //console.log(JSON.stringify(views.items));
    };
    data.helper.getUniqueNo = function (increment) {
        if (increment) {
            this.uniqueNo += 1;
        }
        return this.uniqueNo;
    };
    data.helper.getTimeString = function (item, threshold) {
        //console.log("getTimeString:" + JSON.stringify(item));
        try {
            var times = _.mapObject(item, function (total, name) {
                var unit;
                var time;
                var myFormat = new Intl.NumberFormat('en-IN', {
                    maximumFractionDigits: 2
                });
                if (total < 1000) {
                    unit = 'milliseconds';
                    time = myFormat.format(total);
                } else if (total < 60 * 1000) {
                    unit = 'seconds';
                    time = myFormat.format(total / 1000);
                } else if (total < 60 * 60 * 1000) {
                    unit = 'minutes';
                    time = myFormat.format(total / (60 * 1000));
                } else {
                    unit = 'hours';
                    time = myFormat.format(total / (60 * 60 * 1000));
                }
                var returnString = name + ':' + time + " " + unit;
                if (total > threshold)
                    return '<span style="color: Crimson">' + returnString + '</span>';
                else
                    return returnString;
            });
            return _.reduce(times, function (memo, item) {
                return memo + item + "<br>";
            }, "");
        } catch (error) {
            console.error(error);
        }
    };
    data.helper.reformatDateTime = function (str) {
        //takes a string of the form 20160930:100537 
        //and converts it to the form 2016/09/30 10:05:37
        var dateStr = str.split(':')[0];
        var timeStr = str.split(':')[1];
        var date = str.substr(0, 4) + '/' + str.substr(4, 2) + '/' + str.substr(6, 2);
        var time = timeStr.substr(0, timeStr.length - 4) + ':' +
            timeStr.substr(timeStr.length - 4, 2); // + ':' +
        //timeStr.substr(timeStr.length - 2, 2);
        return date + ' ' + time;
    };
    data.helper.reformatDateTimeToNewDate = function (str) {
        //takes a string of the form 20160930:100537 
        //and converts it to the form 2016/09/30 10:05:37
        var dateStr = str.split(':')[0];
        var timeStr = str.split(':')[1];
        var year = str.substr(0, 4);
        var month = (Number(str.substr(4, 2)) - 1).toString();
        var day = str.substr(6, 2);
        var hours = timeStr.substr(0, timeStr.length - 4);
        var mins = timeStr.substr(timeStr.length - 4, 2);
        return "new Date(" + year + ", " + month + ", " + day + ", " + hours + ", " + mins + ")";
    };

    return data;
};