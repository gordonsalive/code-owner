/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Qfs = require("q-io/fs");
var Q = require("q");
var fs = require('fs');


module.exports = function (path) {
    function deleteFolderRecursive(path) {
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function (file, index) {
                var curPath = path + "/" + file;
                if (fs.lstatSync(curPath).isDirectory()) { // recurse
                    deleteFolderRecursive(curPath);
                } else { // delete file
                    fs.unlinkSync(curPath);
                }
            });
            fs.rmdirSync(path);
        }
    }
    return Q.Promise(function (resolve, reject, notify) {
        try {
            deleteFolderRecursive("./" + path);
            resolve(path);
        } catch (err) {
            console.log("ERROR:" + err);
            reject(err);
        }
    });
    //        .then(function (result) {
    //            console.log("removed folders: " + result);
    //            return result;
    //        });
};