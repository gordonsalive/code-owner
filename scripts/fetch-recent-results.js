/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 9/8/2016
// Module to fetch test history from QGPL.ROLREFTST
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var pool = require('./shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var DEBUG = 0; //off

var update_status = require('./update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');

// AAG 9/8/'16
// method to asynchronously fetch the recent results stats from the database
// TODO: need some tests just for this module
module.exports = function (cukesFile) {
    if (DEBUG) console.log('in fetch-recent-results.js');
    // --------------
    // latest results
    // --------------
    // (i) load the cukes file
    return read_json(cukesFile)
        // (ii) for each view (with flat as a special case) find the latest run from ROLREFTST
        .then(function (cukes) {
            if (DEBUG) console.log('read in cukes.json, about to fetch most recent results...');
            //get the latest run date and time and add this to the result
            return pool.query("SELECT MAX(time) as time, MAX(date) as date FROM qgpl.rolreftst WHERE date = (SELECT MAX(date) FROM qgpl.rolreftst)")
                .then(function (rows) {
                    var result = {};
                    //have data in the form
                    // [ { "TIME": "-",
                    //     "DATE": "-" ]
                    if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                    if ((rows) && (rows.length > 0) && (rows[0].DATE)) {
                        result = {
                            date: rows[0].DATE,
                            time: rows[0].TIME
                        };
                    }
                    if (DEBUG) console.log('fetched date and time of most recent stored result=' + JSON.stringify(result));
                    return result;
                })
                .then(function (latestResult) {
                    if (DEBUG) console.log('...about to fetch most recent flat results');
                    //add to the latest date and time the most recent flat run
                    latestResult.runs = {};
                    latestResult.runs.flat = {};

                    return pool.query("SELECT value FROM qgpl.rolreftst WHERE type='flat' AND variable='run' AND id = (SELECT MAX(id) FROM qgpl.rolreftst WHERE type='flat' AND variable='run')")
                        .then(function (rows) {
                            var result = '{"results":{}, "items":{}}';
                            //have data in the form
                            // [ { "VALUE": "{results:{passed:12, failed:0, missing:0}, items:{itemName1:123, itemName2:124, itemName3:125}}" ]
                            if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                            if ((rows) && (rows.length > 0)) {
                                result = rows[0].VALUE;
                            }
                            return result;
                        })
                        .then(function (valueJson) {
                            if (DEBUG) console.log('got flat results');
                            latestResult.runs.flat = JSON.parse(valueJson);
                            return latestResult;
                        });
                })
                .then(function (latestResult) {
                    if (DEBUG) console.log('...about to fetch most recent tree results');
                    //add the most recent tree runs corresponding to the views in cukes.tree
                    latestResult.runs.tree = {};
                    //use Q.all() to fetch the viewJson for all views in parallel
                    return Q.all(_.map(cukes['code-owners-tree'], function (view, viewName) {
                            var sql = "SELECT value FROM qgpl.rolreftst WHERE type='tree' AND variable='run' AND view='" + viewName + "' ";
                            sql += " AND id = (SELECT MAX(id) FROM qgpl.rolreftst WHERE type='tree' AND variable='run' AND view='" + viewName + "')";
                            return pool.query(sql)
                                .then(function (rows) {
                                    var result = '{"results":{}, "items":[]}';
                                    //have data in the form
                                    // [ { "VALUE": "{results:{passed:12, failed:0, missing:0}, items:{itemName1:123, itemName2:124, itemName3:125}" ]
                                    if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                    if ((rows) && (rows.length > 0)) {
                                        result = rows[0].VALUE;
                                    }
                                    return result;
                                })
                                .then(function (viewJson) {
                                    if (DEBUG) console.log('got tree results (' + viewName + ')');
                                    latestResult.runs.tree[viewName] = JSON.parse(viewJson);
                                    return viewJson;
                                });
                        }))
                        .then(function (allValues) {
                            return latestResult;
                        });
                })
                .then(function (latestResult) {
                    if (DEBUG) console.log('\n-wrap up in latestResults node');
                    var result = {};
                    result.latestResult = latestResult;
                    return result;
                });
        })
        // (iii) for each view latest run find the results from ROLREFTST
        .then(function (latestResult) {
            latestResult.latestResult.views = {};
            //reconstruct the results json for each view - flat first then tree items in a map
            //var flatIDsStr = latestResult.latestResult.runs.flat.items.join(",");
            var flatIDsStr = _.map(latestResult.latestResult.runs.flat.items, function (item, key) {
                return item; //id of row for each item in flat view
            }).join(",");
            var treeIDsStrings = _.mapObject(latestResult.latestResult.runs.tree, function (runJson) {
                //return runJson.items.join(",");
                return _.map(runJson.items, function (item) {
                    return item;
                }).join(",");
            });
            if (DEBUG) console.log('\n-id strings:' + flatIDsStr + ':' + JSON.stringify(treeIDsStrings));

            var flatResultsPromise;
            if (flatIDsStr === '') {
                if (DEBUG) console.log('\nempty flatIDsStr');
                flatResultsPromise = promisify([{}]);
            } else {
                flatResultsPromise = pool.query("SELECT item, value FROM qgpl.rolreftst WHERE type='flat' AND variable='results' AND id IN (" + flatIDsStr + ")")
                    .then(function (rows) {
                        var result = null;
                        //have data in the form
                        // [ { "VALUE": "{results:{passed:12, failed:0, missing:0}, items:[123, 124, 125]}" ]
                        if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                        if ((rows) && (rows.length > 0)) {
                            result = _.map(rows, function (row) {
                                return {
                                    item: row.ITEM,
                                    value: JSON.parse(row.VALUE)
                                };
                            });
                        }
                        //console.log('returning the results for flat:' + JSON.stringify(result));
                        return result;
                    });
            }

            return flatResultsPromise
                .then(function (itemArray) {
                    latestResult.latestResult.views.flat = _.reduce(itemArray, function (memo, item) {
                        //each item get added to the object as a property...
                        //...we return the completed object at the end
                        memo[item.item] = item.value;
                        return memo;
                    }, {});
                    if (DEBUG) console.log('\n-reformatted flat results as object properties.');
                    return latestResult;
                })
                .then(function (latestResult) {
                    if (DEBUG) {
                        console.log('\n-now moving onto the tree view items');
                        //console.log(JSON.stringify(latestResult, null, ' '));
                    }
                    var wholeTreeResultsPromise;
                    //console.log('treeIDsStrings = ' + JSON.stringify(treeIDsStrings));
                    if (treeIDsStrings.length === 0) {
                        latestResult.views.tree = {};
                        wholeTreeResultsPromise = promisify(latestResult);
                    } else {
                        //use Q.all() to fetch the viewJson for all views in parallel
                        wholeTreeResultsPromise = Q.all(_.map(treeIDsStrings, function (treeIDsStr, viewName) {
                            var treeResultsPromise;
                            if (treeIDsStr === '') {
                                if (DEBUG) console.log('treeIDsStr is empty');
                                treeResultsPromise = promisify([{}]);
                            } else {
                                treeResultsPromise = pool.query("SELECT item, value FROM qgpl.rolreftst " +
                                        " WHERE type='tree' AND variable='results' " +
                                        " AND view='" + viewName + "' AND id IN (" + treeIDsStr + ")")
                                    .then(function (rows) {
                                        var result = null;
                                        //have data in the form
                                        // [ { "VALUE": "{results:{passed:12, failed:0, missing:0}, items:[123, 124, 125]}" ]
                                        if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                        if ((rows) && (rows.length > 0)) {
                                            result = _.map(rows, function (row) {
                                                return {
                                                    view: viewName,
                                                    item: row.ITEM,
                                                    value: JSON.parse(row.VALUE)
                                                };
                                            });
                                        }
                                        return result;
                                    });
                            }
                            return treeResultsPromise;
                        }));
                    }
                    return wholeTreeResultsPromise
                        .then(function (viewsItemsArray) {
                            //console.log('\nviewItemsArray(tree):' + JSON.stringify(viewsItemsArray, null, '  '));
                            //have an array of {view: "Name", item: value:}
                            //group this to get {Name: [{view: "Name", item: value:}]}
                            latestResult.latestResult.views.tree = _.groupBy(viewsItemsArray.flatten(), 'view');
                            //console.log('\nlatest results after grouping:' + JSON.stringify(latestResult, null, '  '));
                            //now condence each of the view nodes from an array of objects to object properties
                            latestResult.latestResult.views.tree = _.mapObject(latestResult.latestResult.views.tree, function (itemArray) {
                                return _.reduce(itemArray, function (memo, item) {
                                    //each item gets added to the object as a property...
                                    //...we return the completed object at the end
                                    memo[item.item] = item.value;
                                    return memo;
                                }, {});
                            });
                            if (latestResult.latestResult.views.tree.undefined) {
                                delete latestResult.latestResult.views.tree.undefined;
                            }
                            return latestResult;
                        })
                        .then(function (latestResult) {
                            //console.log('\nlatestResult=' + JSON.stringify(latestResult, null, '  '));
                            return latestResult;
                        });
                });
        })
        // (iv) return the recent results (we already put date and time on latest results at very start)
        .then(function (latestResult) {
            //save the results to recent-results.json and return this back
            return Qfs.write("json/recent-results.json", JSON.stringify(latestResult, null, '\t'))
                .then(function (result) {
                    return latestResult;
                });
        });


    //**TODO:
    //move these into a result highlights module called from update-test-result-history
    // store these on the database as new kinds of rows
    // store them in a new local file ready for reporting.
    // --------------
    // best results
    // --------------
    // for each view and item (and all), what was the best result in the previous:
    // - clock hour
    // - calendar day
    // - calendar week
    // - calendar month

    // --------------
    // days since results
    // --------------
    // for each view and item (and all), when did it most recently pass and fail:
    // - achieved by parsing the best results above

    // --------------
    // failure results
    // --------------
    // for each view item, which scenarios failed (up to a maximum of 50)
    // fetch both last and previous runs for each view so we can highlight the scenarios which are new

};

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'error');
}