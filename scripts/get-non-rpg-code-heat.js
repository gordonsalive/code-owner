/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 25/7/2017
// Module to spin through incident data and update all (delphi & java) code base data to produce code heat data
//**********
var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 1; //off

//var update_status = require('./update-status');
var read_json = require('./read-json');
//var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');
var fetch_job_descriptions = require('./fetch-job-descriptions');

const C_DELPHI_CHANGES_IDX = 0;
const C_JAVA_CHANGES_IDX = 1;
const C_DELPHI_SVN_CHANGES_IDX = 2;
var codeTypes = ['delphiChanges', 'javaChanges', 'delphiSvnChanges'];//these are the tech names from the repos,json file
var repoGroupTech = {
    'delphi': codeTypes[C_DELPHI_CHANGES_IDX],
    'java-figaroweb': codeTypes[C_JAVA_CHANGES_IDX],
    'java-api': codeTypes[C_JAVA_CHANGES_IDX],
    'java-other': codeTypes[C_JAVA_CHANGES_IDX],
    'delphiSvn': codeTypes[C_DELPHI_SVN_CHANGES_IDX]
};


module.exports = function (periodStart, periodEnd, subDir, runName) {
    var maxCounts = {}; //will hold the maximum counts for each tech group for this runName
    var jobs = []; //will hold all the job numbers so I can look up the descriptions and then enrich my results for this runName

    if (DEBUG) console.log('get-non-rpg-code-heat:' + periodStart + ':' + periodEnd + ':' + subDir + ':' + runName);

    //first we need to turn the incident changes on its head, so we have code items and a count of items
    return read_json('json/' + runName + '-incident-changes-results.json')
        .then(function (incidents) {
            //console.log('get-non-rpg-code-heat, incidents=' + JSON.stringify(incidents, null, ' '))
            //incidents is an array of items, with optional chnages nodes:
            //[{incident: 1000280, job: "248048", rpgChanges: [], 
            //    delphiChanges: {
            //        dates: ["2015-11-30"], 
            //        versions: ["trunk"], 
            //        paths: ["/thenon/F63/delphi/trunk/Figaro63/Control/Extlists.pas"]},
            //    javaChanges: {
            //        "dates": ["2016-05-03","2016-05-04","2016-05-06"],
            //        "versions": ["branches/153","trunk"],
            //        "paths": [
            //        "/thenon/F63/ marketdata/JhcStreamingService/branches/153/src/main/java/jhc/streaming/server/feed/digitallook/DigitalLookMessageHelper.java",
            //        "/thenon/F63/ marketdata/JhcStreamingService/trunk/src/main/java/jhc/streaming/server/feed/digitallook/DigitalLookMessageHelper.java"]}}]
            //        
            var results = {}; //array of code items against count
            results[codeTypes[C_DELPHI_CHANGES_IDX]] = {};
            results[codeTypes[C_JAVA_CHANGES_IDX]] = {};
            results[codeTypes[C_DELPHI_SVN_CHANGES_IDX]] = {};

            _.each(incidents, function (incident) {
                if (!jobs[runName]) {
                    jobs[runName] = [];
                }
                jobs[runName].push(incident.job);
                _.each(codeTypes, function (codeType) {
                    if (incident[codeType] &&
                        incident[codeType].paths &&
                        incident[codeType].versions.includes("trunk")) {
                        _.each(incident[codeType].paths, function (path) {
                            var incidentDate = incident[codeType].dates[0].replace('-', '').replace('-', '');
                            //as long as it is a change in trunk (for now), so not 'branch'
                            //console.log('periodEnd=' + periodEnd + ', periodStart=' + periodStart + 'incidentDate=' + incidentDate + ':' + parseInt(incidentDate));
                            if (!path.includes("/branch") &&
                                (parseInt(incidentDate) < periodEnd) &&
                                (parseInt(incidentDate) > periodStart)) {
                                //if it exists in restuls already then increment, else add
                                if (results[codeType][path]) {
                                    results[codeType][path].count++;
                                    results[codeType][path].defects.push({
                                        job: incident.job,
                                        incident: incident.incident,
                                        dateRequested: incidentDate
                                    });
                                } else {
                                    results[codeType][path] = {
                                        count: 1,
                                        defects: [{
                                            job: incident.job,
                                            incident: incident.incident,
                                            dateRequested: incidentDate
                                        }]
                                    };
                                }
                            }
                        });
                    }
                });
            });
            return results;
        })
        .then(function (incidents) {
            //console.log(JSON.stringify(incidents, null, ' '));
            //at this point incidents has the structure:
            //{delphiChanges: {"/thenon/F63/delphi/trunk/Figaro63/Control/Extlists.pas": {firstDate: "2016-05-06", branch: "trunk", count: 1}, {}},
            // javaChanges: {...}}
            //Now load up the repos and start working on the code changes
            return read_json('json/repos.json')
                .then(function (repos) {
                    //now spin through the code heat repos groups
                    return Q.all(
                        //_.map(repos["repos-heat"], function (techRepos, techName) {
                        _.map(repos["repos-heat"], function (techRepos, techName) {
                            //console.log('techName=' + techName);
                            if (!maxCounts[runName]) {
                                maxCounts[runName] = {};
                            }
                            maxCounts[runName][techName] = 0;
                            console.log("maxCounts[" + runName + "][" + techName + "]=" + maxCounts[runName][techName]);
                            //load the code hierarchy for this tech group
                            return read_json('repos/' + techName + '-code-hierarchy.json')
                                .then(function (techCodeHierarchy) {
                                    //spin recursively through the code hierarchy finding code items
                                    // then looking for incidents for that code item in the incident-changes-results
                                    return countIncidentsForEachCodeItem(incidents, techCodeHierarchy, repoGroupTech[techName], techName, techRepos);
                                })
                                .then(function (techCodeHierarchy) {
                                    console.log("-maxCounts[" + runName + "][" + techName + "]=" + maxCounts[runName][techName]);
                                    //now use the counts and maxCoutns to assign the weights to each code node
                                    return updateWeightsForEachCodeItem(techCodeHierarchy, techName);
                                })
                                .then(function (techCodeHierarchy) {
                                    console.log('about to fetch job descriptions');
                                    //filter out crud like "No Job"
                                    jobs[runName] = _.filter(jobs[runName], function (job) {
                                        return /^\d\d\d\d\d\d$/.test(job);
                                    });
                                    return fetch_job_descriptions(jobs[runName])
                                        .then(function (jobDescriptions) {
                                            jobs[runName] = jobDescriptions;
                                            return techCodeHierarchy;
                                        });
                                })
                                .then(function (techCodeHierarchy) {
                                    console.log('now enrich my code hierarchy with job descriptions for defects');
                                    return updateJobDescriptions(techCodeHierarchy);
                                })
                                .then(function (techCodeHierarchy) {
                                    return Qfs.write('json/' + runName + '-' + techName + '-incidents.json',
                                        JSON.stringify(techCodeHierarchy, null, '\t'))
                                        .then(function (writeResults) {
                                            return Qfs.write('circles/' + subDir + '/' + techName + '-incidents.json',
                                                JSON.stringify(techCodeHierarchy, null, '\t'));
                                        });
                                });
                        }));
                });
        });

    function updateJobDescriptions(techCodeHierarchy) {
        function updateJobDescriptionsRecursive(hierarchyNode) {
            if (hierarchyNode.code && hierarchyNode.defects) {
                _.each(hierarchyNode.defects, function (defect) {
                    var defectDescriptionObj = _.findWhere(jobs[runName], {
                        job: defect.job
                    });
                    if (defectDescriptionObj) {
                        defect.description = defectDescriptionObj.description;
                    }
                });
            } else if (hierarchyNode.children) {
                hierarchyNode.children = _.map(hierarchyNode.children, function (childNode) {
                    return updateJobDescriptionsRecursive(childNode);
                });
            }
            return hierarchyNode;
        }
        return updateJobDescriptionsRecursive(techCodeHierarchy);
    }

    function updateWeightsForEachCodeItem(techCodeHierarchy, techName) {
        function findAndUpdateCodeItemsRecursively(hierarchyNode, techName) {
            if (hierarchyNode.code) {
                //update weight
                //console.log("techName=" + techName + ':maxCounts=' + maxCounts);
                //console.log("hierarchyNode.defectCount=" + hierarchyNode.defectCount + ':maxCounts[techName]=' + maxCounts[techName]);
                if (hierarchyNode.defectCount) {
                    hierarchyNode.weight = hierarchyNode.defectCount / maxCounts[runName][techName];
                    hierarchyNode.count = hierarchyNode.defectCount;
                } else {
                    hierarchyNode.weight = 0;
                }
            } else if (hierarchyNode.children) {
                hierarchyNode.children = _.map(hierarchyNode.children, function (childNode) {
                    return findAndUpdateCodeItemsRecursively(childNode, techName);
                });
            }
            return hierarchyNode;
        }
        return findAndUpdateCodeItemsRecursively(techCodeHierarchy, techName);
    }

    function countIncidentsForEachCodeItem(incidents, techCodeHierarchy, techChanges, techName, techRepos) {
        //incidents has the structure:
        //{delphiChanges: {"/thenon/F63/delphi/trunk/Figaro63/Control/Extlists.pas": {firstDate: "2016-05-06", branch: "trunk", count: 1}, {}},
        // javaChanges: {...}}
        //    
        //techCodeHierarchy has the structure:
        // {name: "root", children: [{name: "delphi", children: [{name: "Figaro63", children: [{name:SharedDefinigions, children: [
        //       {"name": "MSHTML_TLB.pas", "blank": 1789, "comment": 4705, "code": 33339, "language": "Pascal", "size": 33339, "weight": 0 }, {..}
        //]}]}]}]}
        function findMatchingIncident(matchingPaths) {
            //drop the top of the match, it's the techGroup name and nore really part of the path beneath that
            //match = match.substring(techName.length + 2, match.length);
            //if (matchingPaths.filePath.includes('IfaAccountOwnerRequest.java')) {
            //    console.log('matchingPaths=' + JSON.stringify(matchingPaths));
            //}
            //we need to try to match tech and REPO in patch of change so we don't match a file change to a different repo.
            //we added the tech and REPO to the patch of the changes, so now we should be able to find them as we try to match
            //specific bits of source to spceific changes
            return _.find(incidents[techChanges], function (item, fullPath) {
                return fullPath.includes(matchingPaths.repoPath) &&
                    fullPath.includes(matchingPaths.filePath);
            });
        }

        function getMatchingPaths(path, techRepos) {
            //spin through techRepos and if name exists in path, replace it with techRepos.path
            //console.log('searching for match in:' + path);
            var matchingTechRepo = _.find(techRepos, function (techRepo) {
                //console.log('techRepo.name=' + techRepo.name);
                return path.includes(techRepo.name);
            });
            if (matchingTechRepo) {
                var filePath = path.replace('/' + matchingTechRepo.name + '/', '');
                var repoPath;
                if (matchingTechRepo['scm-type'] && matchingTechRepo['scm-type'] === 'git') {
                    repoPath = matchingTechRepo.name;
                } else {
                    repoPath = matchingTechRepo.path.replace('jhc/', '').replace('trunk/', '');
                }
                //console.log('(repoPath,filePath)=(' + repoPath + ',' + filePath + ')');
                return {
                    //repoPath: matchingTechRepo.path.replace('jhc/', ''),--AAG:3/11/2017
                    repoPath: repoPath,
                    filePath: filePath
                };
            } else {
                throw 'failed to find a matching repo';
            }
        }

        function findAndCountCodeItemsRecursively(hierarchyNode, path, techName, techRepos) {
            //console.log('path + hierarchyNode.name=' + path + '/' + hierarchyNode.name);
            //is this a code item?  else if has children then recurse deeper
            if (hierarchyNode.code) {
                // first get the matching repo path, and the consequent full filepath within that repo
                var matchingPaths = getMatchingPaths(path, techRepos);
                matchingPaths.filePath = matchingPaths.filePath + '/' + hierarchyNode.name;
                //if (hierarchyNode.name === 'pom.xml') {
                //    console.log('matchingPaths=' + JSON.stringify(matchingPaths));
                //}
                var match = findMatchingIncident(matchingPaths);
                var thisCount = match ? match.count : 0;
                if (thisCount > maxCounts[runName][techName]) {
                    //console.log('found a match');
                    maxCounts[runName][techName] = thisCount;
                }
                hierarchyNode.defectCount = thisCount;
                hierarchyNode.defects = match ? match.defects : [];
            } else if (hierarchyNode.children) {
                hierarchyNode.children = _.map(hierarchyNode.children, function (childNode) {
                    return findAndCountCodeItemsRecursively(childNode, path + '/' + hierarchyNode.name, techName, techRepos);
                });
            }
            return hierarchyNode;
        }
        // ** Under root thre may be 1 or more individual repos, need to cucle through all of them
        // ** When looking for a match, first I need to convert my repo grouings back into the original repo path stub
        //    to do this, within my tech group, I look for name in my path and replace with path, then match that to path in incidents.
        //console.log('techRepos=' + JSON.stringify(techRepos, null, ' '));

        //skip passed the root node, but leave it in to hold the list of individual repos
        return {
            name: "root",
            children: _.map(techCodeHierarchy.children, function (childNode) {
                return findAndCountCodeItemsRecursively(childNode, '', techName, techRepos);
            })
        };
    }

};