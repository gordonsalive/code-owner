/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

var fs = require('fs');
//var Qfs = require("q-io/fs");
//var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var DEBUG = 0; //0=off

var promisify = require('./promisify');

module.exports = function (path, topJob) {
    function summariseCucumberFile(path) {
        //console.log('summarise cucumber file:' + path);
        var result = [];
        var cukeJson = JSON.parse(fs.readFileSync(path));
        //console.log(JSON.stringify(cukeJson));
        // cucumber json is of the form:
        // [ { keyword: Feature, name: something, tags: [ { name: @tag } ], elements: [ {
        //         type: scenario, keyword: Scenario, steps: [ { 
        //             status: passed, ...
        //         } ]
        //     } ] 
        // } ]
        //**I've got to copy across the filtering for funny data, missing data etc.*****
        if (cukeJson) {
            result = _.where(cukeJson, {
                keyword: 'Feature'
            }).map(function (feature) {
                //console.log('mapping features');
                return {
                    name: (feature.description) ? feature.name + ': ' + feature.description : feature.name,
                    file: path,
                    tags: _.map(feature.tags, function (tagObj) {
                        return tagObj.name;
                    }),
                    scenarios: _.filter(feature.elements, function (element) {
                            return (element.type === 'scenario') || (element.keyword.includes('Scenario'));
                        })
                        .map(function (scenario) {
                            //console.log('mapping scenarios');
                            return {
                                name: scenario.name,
                                tags: _.map(scenario.tags, function (tagObj) {
                                    return tagObj.name;
                                }),
                                result: scenarioResult(scenario),
                                steps: _.map(scenario.steps, function (step) {
                                    //need to handle tables as well as simple step definitions
                                    if (step.rows) {
                                        return {
                                            step: step.keyword + step.name + '\n' + _.map(step.rows, function (row) {
                                                return _.map(row.cells, function (cellValue) {
                                                    return cellValue;
                                                }).join('\t| '); //use an actual tab character
                                            }).join('\n'),
                                            result: step.result.status
                                        };
                                    } else {
                                        return {
                                            step: step.keyword + step.name,
                                            result: step.result.status
                                        };
                                    }
                                })
                            };
                        })
                };
            });
        }
        //console.log('result=' + JSON.stringify(result, null, '  '));
        return result;
    }

    function scenarioResult(scenario) {
        //is it tagged as a missing test?
        if (scenario.tags && scenario.tags.length && _.some(scenario.tags, function (tagObj) {
                return tagObj.name === '@missing';
            })) {
            return "missing";
        } else {
            //if not tagged as missing:
            if (scenario.steps && scenario.steps.length) {
                //if any of the steps are marked with result.status === "" then the scenario is missing
                if (_.some(scenario.steps, function (step) {
                        return (step.result && (step.result.status === ''));
                    })) {
                    return "missing";
                } else if (_.some(scenario.steps, function (step) {
                        return (step.result && (step.result.status === 'failed'));
                    })) {
                    return "failed";
                } else {
                    return "passed";
                }
            } else {
                if (scenario.name.includes('PENDING') || (scenario.tags && scenario.tags.length) && _.some(scenario.tags, function (tagObj) {
                        return tagObj.name.toUpperCase() === 'PENDING';
                    })) {
                    //it's pending, but with no steps, this counts as passing!
                    return "pending";
                } else {
                    //it's got no steps, and isn't pending, so it must be missing
                    return "missing";
                }
            }
        }
    }

    function summariseCucumberTestsRecursive(path) {
        var result = [];
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function (file, index) {
                var curPath = path + "/" + file;
                if (fs.lstatSync(curPath).isDirectory()) { // recurse
                    if (!curPath.includes('.git')) {
                        result.push(summariseCucumberTestsRecursive(curPath));
                    }
                } else {
                    // Summarise cucumber file
                    if (curPath.includes('.json')) {
                        result.push(summariseCucumberFile(curPath));
                    }
                }
            });
        }
        return _.flatten(result);
    }

    return promisify.method(
            function () {
                //console.log('summarise cucumber tests recursively for ' + path);
                return summariseCucumberTestsRecursive(path);
            }
        )
        .then(function (results) {
            return {
                job: topJob,
                features: _.flatten(results)
            };
        });
};