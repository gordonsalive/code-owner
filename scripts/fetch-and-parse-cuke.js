/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: July 2016
// Module to parse cucumber json or JUnit xml files and return pass/fail counts
//**********

var http = require('http');
var fs = require('fs');
var path = require('path');
//var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
var DOMParser = require('xmldom').DOMParser;
//Q.longStackSupport = true;

var DEBUG = 0; //0=off

var update_status = require('./update-status');
var exec_promise = require('./exec-promise');

var user = "chapmand";
var token = "7b94c59e8f4ea17721122a1f6b0de568";
var userAndToken = user + ':' + token;

function reformatTagsObjectArray(tags) {
    //viewTags is in the form ["@tag1", "@tag2"]
    //itemTags is in the form:
    //    "tags": [ { 
    //          "line": 1,
    //          "name": "@fees" }, {
    //          "line": 1,
    //          "name": "@cap(fees_FEE091)" } ]
    return _.map(tags, function (tagObject) {
        return tagObject.name;
    });
}

function writeOutStatus(connectionStatus) {
    update_status(connectionStatus, 'error');
}

// AAG 11/7/'16
// method to asynchronously read update the status of the latest fetch attempts
// TODO: need some tests just for this module
module.exports = function (job, hostname, filterArray) {
    function checkTagsForFeature(itemTags, viewTags) {
        //tagsArray looks something like this:
        // [ { "tags": [ "@qualityOwnerDocuments" ],
        //     "displayName": "Documents" },
        //   {...} ]

        //if there are no viewTags then include all features and scenarios in "other"
        //if the feature contains a matching tag then all scenarios can go in the displayName of that item
        //if the scenario contains a matching tag then that scenario will go in the displayName of that item instead

        //TODO: should handle excluding features (e.g. ~@notF154)

        //return object of the form: {excludeFeature: false; featureTagsMatch: "other"}
        if (!viewTags) {
            return {
                excludeFeature: false,
                displayName: "Other",
                featureTagsMatch: "Other"
            };
        } else {
            //do itemTags and viewTags overlap?
            var itemTagsArray = reformatTagsObjectArray(itemTags);
            var itemTagsLower = _.map(itemTagsArray, function (tagStr) {
                return tagStr.toLowerCase();
            });
            //find the first matching tag in the tagsArray, if there is one
            var matchingFilterItem = _.find(viewTags, function (filterItem) {
                var viewTagsLower = _.map(filterItem.tags, function (tagStr) {
                    return tagStr.toLowerCase();
                });
                return (_.intersection(viewTagsLower, itemTagsLower).length !== 0);
            });
            var displayName = '';
            if (matchingFilterItem) {
                displayName = matchingFilterItem.displayName;
            } else if ((viewTags.length > 0) && (viewTags[0].tags.length === 0)) {
                //No tags were even passed in - i.e. all features and scenarios match, 
                //..so use the display name for this item:
                displayName = viewTags[0].displayName;
            } else displayName = "Other";

            //if either itemTagsArray was empty or we did infact find a matching item, 
            //..then use the supplied display name, else use "Other"
            return {
                excludeFeature: false,
                featureTagsMatch: displayName,
                displayName: displayName
            };
        }
    }

    function directoryExists(path) {
        try {
            return fs.statSync(path).isDirectory();
        } catch (err) {
            return false;
        }
    }

    function ensureDirectoryExistence(filePath) {
        //e.g. for this/that/it, check this/that/it, if not check this/that, if not check this...
        //when we arrive at a directory that exists then recurse back creating them all the way down
        var dirname = path.dirname(filePath);
        if (directoryExists(dirname)) {
            return true;
        }
        ensureDirectoryExistence(dirname);
        fs.mkdirSync(dirname);
        return false;//if we had to create any directories return false
    }

    // If the passed in job has a cuke file, process it as normal, else if it's junit do it slightly differently
    if (job.resultsStyle === 'cucumber') {

        // (i) fetch the cucumber json at this path
        // (ii) parse cucumber and return results
        return fetch_job_colour_promise(hostname, job)
            .then(function (isRunningJson) {
                return Q.Promise(function (resolve, reject, notify) {
                    var job = isRunningJson.job;
                    // Have we got this file already?
                    var cukeJson = "";
                    var cukeFilename = "cukes/" + job.name + "/" + job.path;
                    try {
                        var cukeStats = fs.statSync(cukeFilename);
                        if (cukeStats.isFile()) {
                            cukeJson = JSON.parse(fs.readFileSync(cukeFilename));
                        }
                    } catch (err) {
                        //handle this by continuing to try and get it from Jenkins instead
                    }
                    if (cukeJson) {
                        resolve({
                            job: job,
                            body: cukeJson,
                            isRunning: isRunningJson.isRunning
                        });
                    } else {
                        // (i) fetch the cucumber json at this path
                        var allOfBody = '';

                        var path = '/job/' + encodeURI(job.name) + '/ws/' + job.path;

                        var options = {
                            hostname: hostname,
                            port: 80,
                            path: path,
                            //method: 'POST',
                            method: 'GET',
                            auth: userAndToken,
                            agent: false,
                            timeout: 300 * 1000
                        };
                        if (job.hostname) {
                            //console.log("job overrides hostname (" + job.name + "):" + job.hostname);
                            options.hostname = job.hostname;
                        }

                        if (DEBUG) {
                            console.log("fetching cuke from Jenkins:" + path);
                            //console.log(options);
                        }

                        var req = http.request(options, function (res) {
                            if (DEBUG && res.isFailed) console.log('STATUS(' + path + '): ' + res.statusCode);
                            if (res.statusCode != 200) {
                                writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?
                                //some requests may end in error (404) if files are missing, need to tolerate this
                                //reject(new Error("Status code was " + res.statusCode));
                            }
                            res.setEncoding('utf8');
                            res.on('data', function (chunk) {
                                allOfBody = allOfBody + chunk;
                            });
                            res.on('end', function () {
                                //if (DEBUG) console.log('--fetch views from jenkins..complete(' + options.path + ').');
                                var allOfBodyJson;
                                try {
                                    allOfBodyJson = JSON.parse(allOfBody);
                                    //cache it so I don't have to fetch it again
                                    ensureDirectoryExistence(cukeFilename);
                                    fs.writeFileSync(cukeFilename, JSON.stringify(allOfBodyJson, null, "\t"));
                                } catch (e) {
                                    //if (DEBUG) console.error('error reading json for ' + job.name + ':' + job.path + "(error:" + e + ")");
                                }
                                //if there was a 404 or some error in the JSON then we need to resolve with {}
                                resolve({
                                    job: job,
                                    body: allOfBodyJson,
                                    isRunning: isRunningJson.isRunning
                                });
                            });
                            res.on('error', function (e) {
                                console.log('result error.:' + e);
                                reject(e);
                            });
                        });
                        req.on('error', function (e) {
                            console.log('received an error fetching (job:' + job.name + ', path:' + path + '):' + e);
                            reject(e);
                        });
                        req.setTimeout(300 * 1000); //node default is 2 minutes
                        // write data to request body
                        req.write(' ');
                        req.end();
                    }
                });
            })
            .then(function (cukeJson) {
                //if (DEBUG) console.log('in parse cucumber: ' + job.name + ':' + job.path);
                var result;
                if (job.stickyGroups) {
                    if ((filterArray.length > 0) && (filterArray[0].tags.length === 0)) {
                        //No tags were even passed in - i.e. all features and scenarios match, 
                        //..so use the display name for this item:
                        result = {};
                        result[filterArray[0].displayName] = {
                            scenariosPassed: 0,
                            scenariosFailed: 0,
                            scenariosMissing: 0,
                            isRunning: cukeJson.isRunning,
                            failedScenarios: []
                        };
                    }
                }
                //        else {
                //                result = {
                //                    Other: {
                //                        scenariosPassed: 0,
                //                        scenariosFailed: 0,
                //                        scenariosMissing: 0
                //                    }
                //                };
                //            }
                //TODO - tags: need to be able to exclude features/scenarios based on tag?
                try {
                    if (cukeJson) {
                        if (cukeJson.body && cukeJson.body.length) {
                            //TODO: pull this section out into separate method
                            //for each body item of keyword=Feature
                            result =
                                _.where(cukeJson.body, {
                                    keyword: 'Feature'
                                })
                                .reduce(function (scenarioTotals, feature) {
                                    //if a tagsArray has been passed in then we need to group totals out by different tags
                                    //the remainder get put in "other" (or if no tagsArray passed in)
                                    //tagsArray looks something like this:
                                    // [ { "tags": [ "@qualityOwnerDocuments" ],
                                    //     "displayName": "Documents" },
                                    //   {...} ]

                                    //check for matching tags at the feature level, if viewTags has been passed in
                                    //returns the displayName to log totals against or "other"
                                    var featureTagCheck = checkTagsForFeature(feature.tags, filterArray);
                                    if ((!featureTagCheck.excludeFeature)) {
                                        //if it contains elements and elements has a length
                                        if (feature.elements && feature.elements.length) {
                                            //for each element with type=scenario
                                            var stepsTotals = parseScenarioTotals(feature, filterArray, featureTagCheck.featureTagsMatch);
                                            //console.log('\nstepsTotals=' + JSON.stringify(stepsTotals));

                                            //stepTotals will be a total for "other" and then optionally
                                            //totals for different categories based on the tag matching (and displayName supplied therein)
                                            //(i) spin through scenario totals, if there is matching node in stepTotals then sum them
                                            scenarioTotals = _.mapObject(scenarioTotals, function (item, key) {
                                                if (stepsTotals[key]) {
                                                    var failedScenarios = stepsTotals[key].failedScenarios;
                                                    if (item.failedScenarios) {
                                                        //console.log('item.failedScenarios=' + JSON.stringify(item.failedScenarios));
                                                        //console.log('stepsTotals[key].failedScenarios=' + JSON.stringify(stepsTotals[key].failedScenarios));
                                                        failedScenarios = item.failedScenarios.concat(stepsTotals[key].failedScenarios);
                                                    } else {
                                                        console.log('o-oh!');
                                                    }
                                                    return {
                                                        scenariosPassed: item.scenariosPassed + stepsTotals[key].stepsPassed,
                                                        scenariosFailed: item.scenariosFailed + stepsTotals[key].stepsFailed,
                                                        scenariosMissing: item.scenariosMissing + stepsTotals[key].stepsMissing,
                                                        isRunning: cukeJson.isRunning,
                                                        failedScenarios: failedScenarios
                                                    };
                                                } else {
                                                    return item;
                                                }
                                            });
                                            //console.log('scenarioTotals=' + JSON.stringify(scenarioTotals));

                                            //(ii) spin through step totals, if there is a node that is missing from scenarioTotals then add it in
                                            _.each(stepsTotals, function (item, key) {
                                                if (!scenarioTotals[key]) {
                                                    scenarioTotals[key] = {
                                                        scenariosPassed: item.stepsPassed,
                                                        scenariosFailed: item.stepsFailed,
                                                        scenariosMissing: item.stepsMissing,
                                                        isRunning: cukeJson.isRunning,
                                                        failedScenarios: item.failedScenarios
                                                    };
                                                }
                                            });
                                        } else {
                                            //if (DEBUG) console.error('no scenarios for ' + job.name + ' for feature ' + feature.name);
                                        }
                                    }
                                    return scenarioTotals;
                                }, {
                                    // Other: {
                                    //   scenariosPassed: 0,
                                    //   scenariosFailed: 0,
                                    //   scenariosMissing: 0
                                    // }
                                });
                        } else {
                            //if (DEBUG) console.error('cuke json contains no valid body for ' + job.name);
                        }
                    } else {
                        //if (DEBUG) console.error('no cuke json returned for ' + job.name);
                    }
                } catch (e) {
                    console.error('error reading cuke json - skip: ' + e + ':' + job.name);
                }
                // (ii) parse cucumber and return results
                //TODO - additional info like jenkins job name should be passed in within an info sub-node
                //       and then that can simply be passed back out again.
                var resultObj = {
                    name: job.name,
                    path: job.path,
                    jenkinsViewName: job.jenkinsViewName,
                    jenkinsJobName: job.jenkinsJobName,
                    stickyGroups: job.stickyGroups,
                    totals: result,
                    isRunning: cukeJson.isRunning
                };
                if (job.displayName) {
                    resultObj.name = job.displayName;
                }

                //console.log("Result Object = " + JSON.stringify(resultObj));
                return resultObj;
            });
    } else {
        //it's a set of junit stlye xml files so I need to process them differently
        // (i) fetch the workspace (which I seem only to be able to get at HTML) and pull from this the list of xml files
        // (ii) fetch each of the xml files counting all the tests without and with fails (store them locally to avoid refetch)
        return fetch_job_colour_promise(hostname, job)
            .then(function (isRunningJson) {
                return Q.Promise(function (resolve, reject, notify) {
                    var allOfBody = '';
                    var job = isRunningJson.job;
                    var path = '/job/' + encodeURI(job.name) + '/ws/' + job.path;

                    var options = {
                        hostname: hostname,
                        port: 80,
                        path: path,
                        //method: 'POST',
                        method: 'GET',
                        auth: userAndToken,
                        agent: false,
                        timeout: 300 * 1000
                    };
                    if (job.hostname) {
                        //console.log("job overrides hostname (" + job.name + "):" + job.hostname);
                        options.hostname = job.hostname;
                    }


                    if (DEBUG) {
                        console.log("fetching ws from Jenkins:" + path);
                        console.log(options);
                    }

                    var req = http.request(options, function (res) {
                        if (DEBUG) console.log("fetching ws from Jenkins:" + path + 'STATUS: ' + res.statusCode);
                        if (res.statusCode != 200) {
                            writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?
                            //some requests may end in error (404) if files are missing, need to tolerate this
                            //reject(new Error("Status code was " + res.statusCode));
                        }
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            allOfBody = allOfBody + chunk;
                        });
                        res.on('end', function () {
                            if (DEBUG) console.log('--fetch ws from jenkins..complete(' + options.path + ').');
                            //pull out of the page all the files and then we can fetch them in the next stage
                            var regex = /\.xml">[\w\._]*.xml</gmi;
                            var myArray = allOfBody.match(regex);
                            //spin through array trimming the excess
                            myArray = _.map(myArray, function (item) {
                                return item.substring(6, item.length - 1);
                            });
                            //console.log(myArray);

                            //if there was a 404 or some error in the JSON then we need to resolve with {}
                            resolve({
                                job: job,
                                files: myArray,
                                isRunning: isRunningJson.isRunning
                            });
                        });
                        res.on('error', function (e) {
                            console.log('error..:' + e);
                            reject(e);
                        });
                    });
                    req.on('error', function (e) {
                        console.log('--error on fetch ws from jenkins (' + options.path + ').');
                        reject(e);
                    });
                    req.setTimeout(300 * 1000); //node default is 2 minutes
                    // write data to request body
                    req.write(' ');
                    req.end();
                });
            })
            .then(function (jobFiles) {
                var delay = 0;
                //console.log('jobFiles:' + JSON.stringify(jobFiles, null, ' '));
                //console.log('if(jobFiles.job["git-repo-branch"])=' + (jobFiles.job["git-repo-branch"] ? true : false));
                //for each file, fetch it from Jenkins, unless we have it already

                //**it's possible for this to flood the jenkins server with requests, which it cannot handle,
                //**or which then floods this client with reponses which it cannot handle - causing ETIMEDOUT
                //**TODO: need to make these calls in sequence.
                return Q(true)
                    .then(function(result) {
                        //console.log('jobFiles.job["git-repo-branch"]=' + jobFiles.job["git-repo-branch"]);
                        //console.log('\n*****\n*****\n*****if(jobFiles.job["git-repo-branch"])=' + (jobFiles.job["git-repo-branch"] ? true : false));
                        if (jobFiles.job["git-repo-branch"]) {
                            //if I know how to fetch the code from a repo instead of each file by http from Jenkins, then do it
                            //get the specific branch into the specific branch directory...
                            //...then read in all the xml files into an array of strings
                            //var junitFilename = "junit/" + jobFilesjob.name + "/" + jobFilesjob.path + "/" + file;
                            return fetch_junit_xml_from_branch_prom(jobFiles.job);
                        } else {
                            return true;
                        }
                    })
                    .then(function(result) {
                        //console.log('\n*****\n*****\n*****about to process the xml files for: ' + jobFiles.job.name);
                        return Q.all(
                            fetch_and_accumulate_junit_xml_pooled(hostname, jobFiles, job)
                        );
                    });
            })
            .then(function (xmlResults) {
                //every <testcase...></testcase> is equivalent to a scenario, and it has either passed or failed.
                //the test suite (e.g. <testsuite failures="1" tests="9" time="3.704">)..
                //..tells me how many tests and how many failures
                //console.log("xmlResults=" + JSON.stringify(xmlResults, null, '  '));
                //console.log('(JUNIT) about to call checkTagsForFeature, filterArray=' + JSON.stringify(filterArray));
                var featureTagCheck = checkTagsForFeature([],
                    filterArray);
                var memoStart = {};
                var itemDisplayName = featureTagCheck.featureTagsMatch;
                memoStart[itemDisplayName] = {
                    scenariosPassed: 0,
                    scenariosFailed: 0,
                    scenariosMissing: 0,
                    isRunning: false,
                    failedScenarios: []
                };
                var result = _.reduce(xmlResults, function (memo, item) {
                        var parser = new DOMParser();
                        //console.log("item.xml = ");
                        //console.log(item.xml);
                        try {
                            var doc = parser.parseFromString(item.xml, "text/xml");
                            var failures = 0;
                            var passes = 0;
                            var tests = 0;

                            var attributes;
                            if (doc.getElementsByTagName("testsuite")[0]) {
                                attributes = doc.getElementsByTagName("testsuite")[0].attributes;
                                //find tests node
                                var testsAttrib = _.find(attributes, function (attrib) {
                                    //console.log("attrib=" + attrib);
                                    //console.log(attrib);
                                    return (attrib.name === 'tests');
                                });
                                //console.log("testsAttrib=" + testsAttrib);
                                //console.log(testsAttrib);
                                tests = parseInt(testsAttrib.value); //need to convert this to number
                                //find failures node, if it is there
                                //console.log("some debugging" + doc + ":" + attributes);
                                //console.log(attributes);
                                var failuresAttrib = _.find(attributes, function (attrib) {
                                    //console.log("attrib=" + attrib);
                                    //console.log(attrib);
                                    return (attrib.name === 'failures');
                                });
                                if (failuresAttrib) {
                                    failures = parseInt(failuresAttrib.value); //need to convert this to number
                                }
                            }
                            passes = tests - failures;
                            memo[itemDisplayName] = {
                                scenariosPassed: memo[itemDisplayName].scenariosPassed + passes,
                                scenariosFailed: memo[itemDisplayName].scenariosFailed + failures,
                                scenariosMissing: 0,
                                isRunning: memo[itemDisplayName].isRunning || item.isRunning,
                                failedScenarios: [] //memo[itemDisplayName].failedScenarios.concat()
                            };
                        } catch (error) {
                            //incorrect XML should not cause the whole process to fail, but just be skipped.
                            console.error(error);
                        }
                        return memo;
                    },
                    memoStart
                );

                var resultObj = {
                    name: job.name,
                    path: job.path,
                    jenkinsViewName: job.jenkinsViewName,
                    jenkinsJobName: job.jenkinsJobName,
                    stickyGroups: job.stickyGroups,
                    totals: result
                };
                if (job.displayName) {
                    resultObj.name = job.displayName;
                }

                //console.log("Result Object = " + JSON.stringify(resultObj));
                return resultObj;
            });
    }

    function parseScenarioTotals(feature, filterArray, featureTagsMatch) {
        function checkTagsForScenario(itemTags, viewTags, featureTagsMatch) {
            //tagsArray looks something like this:
            // [ { "tags": [ "@qualityOwnerDocuments" ],
            //     "displayName": "Documents" },
            //   {...} ]

            //if there are no viewTags then include all features and scenarios in "other"
            //if the feature contains a matching tag then all scenarios can go in the displayName of that item
            //if the scenario contains a matching tag then that scenario will go in the displayName of that item instead

            //TODO: should handle excluding scenarios (e.g. ~@notF154)

            //return object of the form: {excludeFeature: false; featureTagsMatch: "other"}
            if (!viewTags) {
                return "Other";
            } else {
                //do itemTags and viewTags overlap?
                var itemTagsArray = reformatTagsObjectArray(itemTags);
                var itemTagsLower = _.map(itemTagsArray, function (tagStr) {
                    return tagStr.toLowerCase();
                });
                //find the first matching tag in the tagsArray, if there is one
                var matchingFilterItem = _.find(viewTags, function (filterItem) {
                    var viewTagsLower = _.map(filterItem.tags, function (tagStr) {
                        return tagStr.toLowerCase();
                    });
                    return (_.intersection(viewTagsLower, itemTagsLower).length !== 0);
                });

                return matchingFilterItem ? matchingFilterItem.displayName : featureTagsMatch;
            }
        }
        //can't simply where type === 'scenario' because the scalatestcuke omits this (easy to do, so best to avoid dependency here)
        //_.where(feature.elements, {
        //    type: 'scenario'
        //})
        return _.filter(feature.elements, function (element) {
                return (element.type === 'scenario') || (element.keyword.includes('Scenario'));
            })
            .reduce(function (stepTotals, scenario) {
                var displayName = checkTagsForScenario(scenario.tags, filterArray, featureTagsMatch);
                //is it tagged as a missing scenario?
                if (scenario.tags && scenario.tags.length && _.some(scenario.tags, function (tagObj) {
                        return tagObj.name === '@missing';
                    })) {
                    stepTotals[displayName] = {
                        stepsPassed: stepTotals[displayName] ? stepTotals[displayName].stepsPassed : 0,
                        stepsFailed: stepTotals[displayName] ? stepTotals[displayName].stepsFailed : 0,
                        stepsMissing: stepTotals[displayName] ? stepTotals[displayName].stepsMissing + 1 : 1,
                        failedScenarios: stepTotals[displayName] ? stepTotals[displayName].failedScenarios : []
                    };
                } else {
                    //if not missing:
                    //if contains steps and steps has a length
                    if (scenario.steps && scenario.steps.length) {
                        //if any of the steps are marked with result.status === "" then the scenario is missing
                        var isMissing = _.some(scenario.steps, function (step) {
                            return (step.result && (step.result.status === ''));
                        });
                        //if all steps are not marked as 'failed' then scenario has passed, else it has failed
                        var isFailed = !isMissing && _.some(scenario.steps, function (step) {
                            return (step.result && (step.result.status === 'failed'));
                        });
                        var isPassed = !isMissing && !isFailed;

                        //if (stepTotals[displayName]) {
                        //    console.log('(stepTotals[displayName]=' + JSON.stringify(stepTotals[displayName]));
                        //}

                        stepTotals[displayName] = {
                            stepsPassed: stepTotals[displayName] ? stepTotals[displayName].stepsPassed + (isPassed ? 1 : 0) : (isPassed ? 1 : 0),
                            stepsFailed: stepTotals[displayName] ? stepTotals[displayName].stepsFailed + (isFailed ? 1 : 0) : (isFailed ? 1 : 0),
                            stepsMissing: stepTotals[displayName] ? stepTotals[displayName].stepsMissing + (isMissing ? 1 : 0) : (isMissing ? 1 : 0),
                            failedScenarios: stepTotals[displayName] ? stepTotals[displayName].failedScenarios : []
                        };
                        if (stepTotals[displayName] && isFailed) {
                            stepTotals[displayName].failedScenarios.push((feature.name + ':' + scenario.name).substring(0, 256).trim());
                        }
                        //console.log("stepTotals(" + displayName + "):" + JSON.stringify(stepTotals[displayName]));
                    } else {
                        if (scenario.name.includes('PENDING') || (scenario.tags && scenario.tags.length) && _.some(scenario.tags, function (tagObj) {
                                return tagObj.name.toUpperCase() === 'PENDING';
                            })) {
                            //it's pending, but with no steps, this counts as passing!
                            stepTotals[displayName] = {
                                stepsPassed: stepTotals[displayName] ? stepTotals[displayName].stepsPassed + 1 : 1,
                                stepsFailed: stepTotals[displayName] ? stepTotals[displayName].stepsFailed : 0,
                                stepsMissing: stepTotals[displayName] ? stepTotals[displayName].stepsMissing : 0,
                                failedScenarios: stepTotals[displayName] ? stepTotals[displayName].failedScenarios : []
                            };
                        } else {
                            if (DEBUG) {
                                //console.error('scenario has no steps and is not PENDING: ' + scenario.name);
                                //console.log(scenario);
                            }
                            //it's got no steps, and isn't pending, so it must be missing
                            stepTotals[displayName] = {
                                stepsPassed: stepTotals[displayName] ? stepTotals[displayName].stepsPassed : 0,
                                stepsFailed: stepTotals[displayName] ? stepTotals[displayName].stepsFailed : 0,
                                stepsMissing: stepTotals[displayName] ? stepTotals[displayName].stepsMissing + 1 : 1,
                                failedScenarios: stepTotals[displayName] ? stepTotals[displayName].failedScenarios : []
                            };
                        }
                    }
                }
                return stepTotals;
            }, {
                // Other: {
                //   stepsPassed: 0,
                //   stepsFailed: 0,
                //   stepsMissing: 0
                // }        
            });

    }

    function fetch_and_accumulate_job_colour_pooled(hostname, jobs) {
        //console.log('fetch_and_accumulate_reviews_pooled');
        //split the id's out into n different batches, or just 1 if there are less than n items
        var n = 20;
        if (jobs.length <= n) {
            if (DEBUG) console.log('small number so proceed as a single batch');
            return fetch_and_accumulate_job_colour(hostname, jobs);
        } else {
            if (DEBUG) {
                console.log('items = ' + jobs.length);
                console.log('splitting into batches, n = ' + n);
                console.log('batch = ' + n);
            }
            var batch = Math.floor(jobs.length / n);
            var batches = [];
            var tail = jobs;
            var head = [];
            //allocate n-1 batches and the remainder goes in the last batch
            for (var x = 0; x < n - 1; x++) {
                head = _.first(tail, batch);
                tail = _.difference(tail, head);
                batches.push(head);
            }
            batches.push(tail);
            //console.log(batches);

            return Q.all(_.map(batches, function (batch) {
                    return fetch_and_accumulate_job_colour(hostname, batch);
                }))
                .then(function (results) {
                    //console.log(JSON.stringify(results));
                    if (DEBUG) console.log('flatten the results');
                    return _.flatten(results);
                });
        }
    }

    function fetch_and_accumulate_job_colour(hostname, jobs) {
        //console.log('fetch_and_accumulate_job_colour');
        //console.log('creating array of promises');
        var dots = '.';
        var arrayOfPromises = _.map(jobs, function (job) {
            dots += '.';
            return fetch_job_colour_accumulate_function_promise(hostname, job);
        });
        //console.log(dots);

        //convert the array of promises into a sequence of thens...
        return arrayOfPromises.reduce(function (soFar, f) {
            return soFar.then(f);
        }, Q([]));
    }

    function fetch_job_colour_accumulate_function_promise(hostname, job) {
        //console.log('fetch_junit_xml_function_promise');
        return function (resultsSoFar) {
            return fetch_job_colour_promise(resultsSoFar, hostname, job);
        };
    }

    function fetch_job_colour_accumulate_promise(resultsSoFar, hostname, job) {
        return fetch_job_colour_promise(hostname, job)
            .then(function (results) {
                //console.log('push the results onto results so far');
                resultsSoFar.push(results);
                return resultsSoFar;
            });
    }

    function fetch_job_colour_promise(hostname, job) {
        return Q.Promise(function (resolve, reject, notify) {
            // (0) is this job currently running?  If so we'll need to mark it as such
            var jobBody = '';
            var jobPath = '/job/' + encodeURI(job.name) + '/api/json?tree=color';
            var options = {
                hostname: hostname,
                port: 80,
                path: jobPath,
                //method: 'POST',
                method: 'GET',
                auth: userAndToken,
                agent: false,
                timeout: 300 * 1000
            };
            if (job.hostname) {
                options.hostname = job.hostname;
            }
            if (DEBUG) {
                console.log("fetching job color:" + jobPath);
            }
            var req = http.request(options, function (res) {
                if (DEBUG && res.isFailed) console.log('STATUS(' + jobPath + '): ' + res.statusCode);
                if (res.statusCode != 200) {
                    writeOutStatus('failed');
                    //all jobs should exist, so if they're missing, this is fatal
                    reject(new Error("Status code was (fetching job color - " + job.name + "): " + res.statusCode));
                } else {
                    res.setEncoding('utf8');
                    res.on('data', function (chunk) {
                        jobBody = jobBody + chunk;
                    });
                    res.on('end', function () {
                        //if (DEBUG) console.log('--fetch job color..complete(' + options.path + ').');
                        var allOfBodyJson;
                        try {
                            allOfBodyJson = JSON.parse(jobBody);
                        } catch (e) {
                            if (DEBUG) console.error('error reading (color) json for ' + job.name + ':' + job.path + "(error:" + e + ")");
                        }
                        //if there was a 404 or some error in the JSON then we need to resolve with {}
                        var isRunning = allOfBodyJson && allOfBodyJson.color && allOfBodyJson.color.includes('anime');
                        resolve({
                            job: job,
                            isRunning: isRunning
                        });
                    });
                    res.on('error', function (e) {
                        console.log('error:' + e);
                        reject(e);
                    });
                }
            });
            req.on('error', function (e) {
                console.error('error fetching (color) json for ' + job.name + ':' + job.path + "(error:" + e + ")");
                reject(e);
            });
            req.setTimeout(300 * 1000); //node default is 2 minutes
            req.write(' '); // write data to request body
            req.end();
        });
    }



    function fetch_and_accumulate_junit_xml_pooled(hostname, jobFiles, job) {
        //console.log('fetch_and_accumulate_reviews_pooled');
        //split the id's out into n different batches, or just 1 if there are less than n items
        var n = 20;
        if (jobFiles.files.length <= n) {
            if (DEBUG) console.log('small number so proceed as a single batch');
            return fetch_and_accumulate_junit_xml(hostname, jobFiles.job, jobFiles.isRunning, jobFiles.files, job);
        } else {
            //console.log('items = ' + jobFiles.files.length);
            //console.log('splitting into batches, n = ' + n);
            var batch = Math.floor(jobFiles.files.length / n);
            //console.log('batch = ' + batch);
            var batches = [];
            var tail = jobFiles.files;
            var head = [];
            //allocate n-1 batches and the remainder goes in the last batch
            for (var x = 0; x < n - 1; x++) {
                head = _.first(tail, batch);
                tail = _.difference(tail, head);
                batches.push(head);
            }
            batches.push(tail);
            //console.log(batches);

            return Q.all(_.map(batches, function (batch) {
                    return fetch_and_accumulate_junit_xml(hostname, jobFiles.job, jobFiles.isRunning, batch, job);
                }))
                .then(function (results) {
                    //console.log(JSON.stringify(results));
                    //console.log('flatten the results');
                    return _.flatten(results);
                });
        }
    }

    function fetch_and_accumulate_junit_xml(hostname, jobFilesjob, isRunning, files, job) {
        //console.log('fetch_and_accumulate_junit_xml');
        //console.log('creating array of promises');
        var dots = '.';
        var arrayOfPromises = _.map(files, function (file) {
            dots += '.';
            return fetch_junit_xml_function_promise(hostname, jobFilesjob, isRunning, file, job);
        });
        //console.log(dots);

        //convert the array of promises into a sequence of thens...
        return arrayOfPromises.reduce(function (soFar, f) {
            return soFar.then(f);
        }, Q([]));
    }

    function fetch_junit_xml_function_promise(hostname, jobFilesjob, isRunning, file, job) {
        //console.log('fetch_junit_xml_function_promise');
        return function (resultsSoFar) {
            return fetch_junit_xml_promise(resultsSoFar, hostname, jobFilesjob, isRunning, file, job);
        };
    }

    function fetch_junit_xml_promise(resultsSoFar, hostname, jobFilesjob, isRunning, file, job) {
        //console.log('fetch_junit_xml_promise:' + job + ':' + file);
        return Q.Promise(function (resolve, reject, notify) {
                // Have we got this file already?
                var junitXml = "";
                var junitFilename = "junit/" + jobFilesjob.name + "/" + jobFilesjob.path + "/" + file;
                try {
                    var junitStats = fs.statSync(junitFilename);
                    if (junitStats.isFile()) {
                        junitXml = fs.readFileSync(junitFilename, "utf-8");
                    }
                } catch (err) {
                    //handle this by continuing to try and get it from Jenkins instead
                }
                if (junitXml) {
                    //console.log("xml for " + junitFilename);
                    //console.log(junitXml);
                    resolve({
                        job: jobFilesjob,
                        xml: junitXml,
                        isRunning: isRunning
                    });
                } else {
                    // (i) fetch the cucumber json at this path
                    var allOfBody = '';

                    var path = '/job/' + encodeURI(job.name) + '/ws/' + job.path + "/" + file + "/*view*/";

                    var options = {
                        hostname: hostname,
                        port: 80,
                        path: path,
                        //method: 'POST',
                        method: 'GET',
                        auth: userAndToken,
                        agent: false,
                        timeout: 300 * 1000
                    };

                    if (job.hostname) {
                        //console.log("job overrides hostname (" + job.name + "):" + job.hostname);
                        options.hostname = job.hostname;
                    }


                    if (DEBUG) {
                        console.log("fetching junit xml file from Jenkins:" + path);
                        //console.log(options);
                    }

                    //spread out the calls a bit more, so we don't flood jenkins/get flooded ourselves
                    var req = http.request(options, function (res) {
                        if (DEBUG && res.isFailed) console.log('STATUS(' + path + '): ' + res.statusCode);
                        if (res.statusCode != 200) {
                            writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?
                            //some requests may end in error (404) if files are missing, need to tolerate this
                            //reject(new Error("Status code was " + res.statusCode));
                        }
                        res.setEncoding('utf8');
                        res.on('data', function (chunk) {
                            allOfBody = allOfBody + chunk;
                        });
                        res.on('end', function () {
                            if (DEBUG) console.log('--fetch junit xml file from jenkins..complete(' + options.path + ').');
                            try {
                                //cache it so I don't have to fetch it again
                                ensureDirectoryExistence(junitFilename);
                                fs.writeFileSync(junitFilename, allOfBody);
                            } catch (e) {
                                if (DEBUG) console.error('error reading xml for ' + options.path + "(error:" + e + ")");
                            }
                            //if there was a 404 or some error in the JSON then we need to resolve with {}
                            resolve({
                                job: job,
                                xml: allOfBody,
                                isRunning: isRunning
                            });
                        });
                        res.on('error', function (e) {
                            console.log('result error....:' + e);
                            reject(e);
                        });
                    });
                    req.on('error', function (e) {
                        console.error('error fetching xml for ' + options.path + "(error:" + e + ")");
                        reject(e);
                    });
                    req.setTimeout(300 * 1000); //node default is 2 minutes
                    // write data to request body
                    req.write(' ');
                    req.end();
                }
            })
            .then(function (results) {
                //console.log('push the results onto results so far');
                resultsSoFar.push(results);
                return resultsSoFar;
            });
    }

    function fetch_junit_xml_from_branch_prom(jobFilesJob) {
        //get the specific branch into the specific branch directory (//junit/gitRepoBranch/..)...
        //...then read in all the xml files into an array of strings
        //var junitFilename = "junit/" + jobFilesjob.name + "/" + jobFilesjob.path + "/" + file;
        var repoDir = 'junit/' + jobFilesJob.name + ((jobFilesJob.path !== './') ? '/' + jobFilesJob.path : '');
        //console.log('repoPath=' + repoDir);
        if (ensureDirectoryExistence(repoDir)) {
            return Q(true);//we delete all folders at start of process, if it's there already we've fetched it for a previous view and there's no need to fetch it again!
        } else { 
            var repoBranch = jobFilesJob["git-repo-branch"];
            //console.log("repoBranch=" + repoBranch);
            //git clone --depth 1 --no-single-branch -b rpgunit https://gordonsalive@bitbucket.org/jhc-systems/silk-jenkins-test.git "junit/RPGUnit - Core"
            var exec = 'git clone --depth 1 -b ' + repoBranch + " https://gordonsalive@bitbucket.org/jhc-systems/silk-jenkins-test.git" + ' "' + repoDir + '"';
            //console.log('about to clone repo: ' + exec);
            return exec_promise(exec, true, 4 * 60 * 1000)//wait upto 4 minutes
                .fail(function(err) {
                    //console.error(err);//error is already printed out, before this point.
                    return true;//kill the failure here, we don't care, we can always try to fetch the code the old way.
                });    
        }
    };

};