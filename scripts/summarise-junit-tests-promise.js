/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

var fs = require('fs');
//var Qfs = require("q-io/fs");
//var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var DEBUG = 0; //0=off

var promisify = require('./promisify');
var DOMParser = require('xmldom').DOMParser;



module.exports = function (path, topJob) {
    function summariseJunitFile(path) {
        //console.log('summarise junit file:' + path);

        var junitXml = fs.readFileSync(path, "utf-8");
        var result = [];
        // junit xml is of the form:
        //<?xml version="1.0"?>
        //<testsuites>
        //<testsuite tests="5" time="21.181">
        //<testcase classname="TSB.ACC002" name="ISUSERAUTHORISEDTOACCOUNT_FSVEQUALSN" time="16.040">
        //</testcase>
        //...
        //</testsuite>
        //</testsuites>

        //TODO: this code should cycle through testsuites and into their test cases, 
        //   currently it only copes with a single test suite
        //TODO: need to handle errors as well as failures

        var parser = new DOMParser();
        //console.log("xml = ");
        //console.log(junitXml);
        try {
            var doc = parser.parseFromString(junitXml, "text/xml");
            //*** testsuite element ***
            var suiteNameAttrib;
            var suiteName;
            var suiteTests = 0;
            if (doc.getElementsByTagName("testsuite")[0]) {
                var suiteAttributes = doc.getElementsByTagName("testsuite")[0].attributes;
                //find tests node
                var testsAttrib = _.find(suiteAttributes, function (attrib) {
                    return (attrib.name === 'tests');
                });
                suiteTests = parseInt(testsAttrib.value); //need to convert this to number
                var suiteFailures = 0;
                var suiteFailuresAttrib = _.find(suiteAttributes, function (attrib) {
                    return (attrib.name === 'failures');
                });
                if (suiteFailuresAttrib) {
                    suiteFailures = parseInt(suiteFailuresAttrib.value); //need to convert this to number
                }
                var suitePasses = suiteTests - suiteFailures;
                suiteNameAttrib = _.find(suiteAttributes, function (attrib) {
                    return (attrib.name === 'name');
                });
            } else {
                console.log('failed to find doc.getElementsByTagName("testsuite")[0].attributes for ' + path);
            }

            if (suiteNameAttrib) {
                suiteName = suiteNameAttrib.value;
            }

            //*** cycle through each testcase element ***
            var testcaseAttributes;
            var testcaseArray = [];
            for (var x = 0; x < suiteTests; x++) {
                testcaseAttributes = doc.getElementsByTagName("testcase")[x].attributes;
                var testcaseName;
                var testcaseNameAttrib = _.find(testcaseAttributes, function (attrib) {
                    return (attrib.name === 'name');
                });
                if (testcaseNameAttrib) {
                    testcaseName = testcaseNameAttrib.value;
                }
                var testcaseClassName;
                var testcaseClassNameAttrib = _.find(testcaseAttributes, function (attrib) {
                    return (attrib.name === 'classname');
                });
                if (testcaseClassNameAttrib) {
                    testcaseClassName = testcaseClassNameAttrib.value;
                    if (!suiteName) {
                        suiteName = testcaseClassName;
                    }
                }
                //Any fails or errors?
                var testcaseElement = doc.getElementsByTagName("testcase")[x];
                var testcaseFailures = testcaseElement.getElementsByTagName("failure").length;

                testcaseArray.push({
                    name: testcaseName,
                    classname: testcaseClassName,
                    failures: testcaseFailures
                });
            }

            result = [{
                name: suiteName,
                file: path,
                scenarios: _.map(testcaseArray, function (testcase) {
                    return {
                        name: testcase.name,
                        result: (testcase.failures < 1) ? 'passed' : 'failed'
                    };
                })
            }];
        } catch (error) {
            //incorrect XML should not cause the whole process to fail, but just be skipped.
            console.error(error);
        }

        //console.log('result=' + JSON.stringify(result, null, '  '));
        return result;
    }

    function summariseJunitTestsRecursive(path) {
        //each directory is a job, and needs it's own level in the hierarchy
        //console.log(path);
        var result = [];
        if (fs.existsSync(path)) {
            fs.readdirSync(path).forEach(function (file, index) {
                var curPath = path + "/" + file;
                if (fs.lstatSync(curPath).isDirectory()) { // recurse
                    if (DEBUG) console.log('is a directory');
                    if (!curPath.includes('.git')) {
                        result.push(summariseJunitTestsRecursive(curPath));
                    }
                    if (DEBUG) console.log(JSON.stringify(result, null, '  '));
                } else {
                    // Summarise junit file
                    if (curPath.includes('.xml')) {
                        result.push(summariseJunitFile(curPath));
                    }
                }
            });
        }
        return _.flatten(result);
    }

    return promisify.method(
            function () {
                //console.log('summarise junit tests recursively for ' + path);
                return summariseJunitTestsRecursive(path);
            }
        )
        .then(function (results) {
            return {
                job: topJob,
                features: _.flatten(results)
            };
        });
};
