/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 28/3/'17
// This script fetches all Crucible code:
// (1) Fetch all code reviews (just closed ones for now)
// (2) filter out all results more than 
// (3) parse code reviews to pull out the SVN revisions that have been reviewed
// (4) 
// (5) 

// TODO:
// * fetch code reviews at other states so I can high light changes with no code reviews!


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;
var Client = require('node-rest-client').Client;
var client = new Client();

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 0; //0=off

var read_json = require('./read-json');
var promisify = require('./promisify');
//var exec_promise = require('./scripts/exec-promise');

var resultsJson = {};

module.exports = function (reviews) {
    console.log('..fetching code reviews');

    return promisify.spread(
        [
            fetch_all_reviews(reviews.host, 'Closed'),
            fetch_all_reviews(reviews.host, 'Review')
        ],
            function (filteredReviewsClosed, filteredReviewsOpen) {
                //console.log(JSON.stringify(filteredReviews));
                console.log("..pulling out review ids");
                return {
                    closed: _.reduce(filteredReviewsClosed, function (memo, review) {
                        memo.push(review.permaId.id);
                        return memo;
                    }, []),
                    open: _.reduce(filteredReviewsOpen, function (memo, review) {
                        memo.push(review.permaId.id);
                        return memo;
                    }, [])
                };
            })
        .then(function (reviewIds) {
            //reviewIds is split by Closed and Open, need to handle this all the way through to the results
            //console.log("fisrt review = " + _.first(reviewIds) + ', last review = ' + _.last(reviewIds));
            console.log("..now the fun starts, for each review item pull back the SVN revisions");
            return promisify.spread(
                [
                    fetch_and_accumulate_reviews_pooled(reviews.host, reviewIds.closed),
                    fetch_and_accumulate_reviews_pooled(reviews.host, reviewIds.open)
                ],
                function (resultsClosed, resultsOpen) {
                    return {
                        revisions: {
                            closed: resultsClosed,
                            open: resultsOpen
                        },
                        reviews: reviewIds //split by Closed and Open
                    };
                });
        })
        .then(function (revisions) {
            //console.log("revisions = " + revisions);
            var revisionsObj = {
                lastYearsReviewedRevisions: revisions.revisions,
                lastYearsReviews: revisions.reviews
            };
            return Qfs.write("json/review-results.json", JSON.stringify(revisionsObj, null, '\t'));
        });

};

function fetch_all_reviews(host, state) {
    var today = new Date();
    var yearAgo = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
                
    return Q.Promise(function (resolve, reject, notify) {

            //console.log(JSON.stringify(reviews));
            //get ALL closed reviews
            //host = http://fisheye.jhc.co.uk

            //var reqStr;
            //if (state === 'Closed') {
            //    reqStr = "/rest-service/reviews-v1/filter/closed";
            //} else if (state === 'Review') {
            //    reqStr = "/rest-service/reviews-v1/?state=Review";
            //}

            var req = client.get(host + "/rest-service/reviews-v1/?state=" + state, {
                        //var req = client.get(host + "/rest-service/reviews-v1/filter/" + state, {
                        //var req = client.get(host + reqStr, {
                        requestConfig: {
                            //timeout: 300 * 1000, //request timeout in milliseconds 
                            timeout: 60 * 60 * 1000, //request timeout in milliseconds 
                            noDelay: true, //Enable/disable the Nagle algorithm 
                            keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
                            keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
                        },
                        responseConfig: {
                            timeout: 60 * 60 * 1000 //response timeout 
                        }
                    },
                    function (data, response) {
                        //data is the parsed response body
                        //response is the raw response data
                        resolve(data);
                    })
                .on('requestTimeout', function (req) {
                    console.log('request has expired');
                    req.abort();
                    reject('request has expired');
                })
                .on('responseTimeout', function (res) {
                    console.log('response has expired');
                    reject('request has expired');
                })
                .on('error', function (err) {
                    //it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
                    console.log('request error', err);
                    reject(err);
                });

            client.on('error', function (err) {
                console.error('Something went wrong on the client', err);
                reject(err);
            });
        })
        //        .then(function (codeReviews) {
        //            //fetch all code reviews takes a while, so save the results for use when testing
        //            return Qfs.write("json/all-code-reviews.json", JSON.stringify(codeReviews));
        //        })
        //    return read_json("json/all-code-reviews.json")
        .then(function (codeReviews) {
            console.log('..got code reviews(' + state + '), now filter out ones more than a year old from ' + codeReviews.reviews.length);
            //console.log(JSON.stringify(codeReviews));
            return _.filter(codeReviews.reviews.reviewData, function (review) {
                //console.log(JSON.stringify(review));
                var createDate = new Date(review.createDate);
                //console.log("reviewData.createDate=" + review.createDate);
                //console.log("createDate=" + createDate);
                //console.log("yearAgo=" + yearAgo.toLocaleString());
                //console.log("review ID = " + review.permaId.id);
                //console.log("recent = " + (createDate > yearAgo));
                return createDate > yearAgo;
            });
        });
}

function fetch_and_accumulate_reviews_pooled(host, reviewIds) {
    //console.log('fetch_and_accumulate_reviews_pooled');
    //split the id's out into n different batches, or just 1 if there are less than n items
    var n = 10;
    if (reviewIds.length <= n) {
        console.log('small number so proceed as a single batch');
        return fetch_and_accumulate_reviews(host, reviewIds);
    } else {
        console.log('splitting into batches, n =' + n);
        var batch = Math.floor(reviewIds.length / n);
        var batches = [];
        var tail = reviewIds;
        var head = [];
        //allocate n-1 batches and the remainder goes in the last batch
        for (var x = 0; x < n - 1; x++) {
            head = _.first(tail, batch);
            tail = _.difference(tail, head);
            batches.push(head);
        }
        batches.push(tail);
        //console.log(batches);

        return Q.all(_.map(batches, function (batch) {
                return fetch_and_accumulate_reviews(host, batch);
            }))
            .then(function (results) {
                console.log('flatten the results');
                //return _.flatten(results);
                return _.reduce(results, function (memo, result) {
                    //console.log(JSON.stringify(memo));
                    //console.log(JSON.stringify(result));
                    return {
                        allRevisions: _.uniq(memo.allRevisions.concat(result.allRevisions)),
                        reviewsObj: memo.reviewsObj.concat(result.reviewsObj)
                    };
                }, {
                    allRevisions: [],
                    reviewsObj: []
                });
            });
    }
}

function fetch_and_accumulate_reviews(host, reviewIds) {
    //console.log('fetch_and_accumulate_reviews');
    //TODO: **** make these do a dozen at a time ****
    //console.log('creating array of promises');
    var dots = '.';
    var arrayOfPromises = _.map(reviewIds, function (reviewId) {
        dots += '.';
        return fetch_revisions_for_review_function_promise(host, reviewId);
    });
    //console.log(dots);

    return arrayOfPromises.reduce(function (soFar, f) {
        return soFar.then(f);
    }, Q({
        allRevisions: [],
        reviewsObj: []
    }));
}

//function fetch_and_accumulate_reviews(reviewIds) {
//    console.log('fetch_and_accumulate_reviews');
//    //TODO: **** make these do a dozen at a time ****
//    console.log('creating array of promises');
//    var arrayOfPromises = _.map(reviewIds, function (reviewId) {
//        return fetch_revisions_for_review_function_promise(reviewId);
//    });
//    var result = Q([]);
//    console.log('constructing sequence of promise-thens');
//    var dots = '.';
//    arrayOfPromises.forEach(function (f) {
//        result = result.then(f);
//        dots += '.';
//    });
//    console.log(dots);
//    return result;
//}

function fetch_revisions_for_review_function_promise(host, reviewId) {
    return function (resultsSoFar) {
        return fetch_revisions_for_review_promise(resultsSoFar, host, reviewId);
    };
}

function fetch_revisions_for_review_promise(resultsSoFar, host, reviewId) {
    //console.log(JSON.stringify(resultsSoFar));
    //space out the request to avoid flooding the crucible server with requests.
    return Q.Promise(function (resolve, reject, notify) {
            //get the review items for this review
            //console.log('getting review items for ' + reviewId);
            var req = client.get(host + "/rest-service/reviews-v1/" + reviewId + "/reviewitems", {
                        requestConfig: {
                            timeout: 60 * 60 * 1000, //request timeout in milliseconds 
                            noDelay: true, //Enable/disable the Nagle algorithm 
                            keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
                            keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
                        },
                        responseConfig: {
                            timeout: 60 * 60 * 1000 //response timeout 
                        }
                    },
                    function (data, response) {
                        //console.log('got review items for ' + reviewId);
                        //data is the parsed response body
                        //response is the raw response data
                        resolve(data);
                    })
                .on('requestTimeout', function (req) {
                    console.log('request has expired');
                    req.abort();
                    reject('request has expired');
                })
                .on('responseTimeout', function (res) {
                    console.log('response has expired');
                    reject('request has expired');
                })
                .on('error', function (err) {
                    //it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
                    console.log('request error', err);
                    reject(err);
                });

            //                client.on('error', function (err) {
            //                    console.error('Something went wrong on the client', err);
            //                    reject(err);
            //                });            
        })
        .then(function (reviewItems) {
            //console.log('..pulling out revision numbers');
            //console.log(JSON.stringify(reviewItems));
            //console.log(JSON.stringify(reviewItems.reviewItems));
            if (!reviewItems.reviewItems.reviewItem) {
                console.log(host + ':' + reviewId + ':' + JSON.stringify(reviewItems));
                return [];
            } else {
                if (Array.isArray(reviewItems.reviewItems.reviewItem)) {
                    return {
                        review: reviewId,
                        revisions: _.uniq(_.map(reviewItems.reviewItems.reviewItem, function (review) {
                            //console.log(JSON.stringify(review));
                            //files can be changes or added, for change look at toFRevision, for added look at revision
                            return review.toRevision ? review.toRevision : review.revision;
                        })),
                        repo: _.uniq(_.map(reviewItems.reviewItems.reviewItem, function (review) {
                            return review.repositoryName;
                        })),
                        paths: _.map(reviewItems.reviewItems.reviewItem, function (review) {
                            return review.toPath;
                        })
                    };
                } else {
                    var aReview = reviewItems.reviewItems.reviewItem;
                    //console.log(JSON.stringify(aReview));
                    //files can be changes or added, for change look at toFRevision, for added look at revision
                    return {
                        revisions: aReview.toRevision ? aReview.toRevision : aReview.revision,
                        review: reviewId
                    };
                }
            }
        })
        .then(function (revisions) {
            //I also want the description of the code review.  (Interim code reviews happen against revisions outside svn, so we cannot use revision to tie these to a job - need to parse description.)
            return Q.Promise(function (resolve, reject, notify) {
                    var req = client.get(host + "/rest-service/reviews-v1/" + reviewId, {
                                requestConfig: {
                                    timeout: 60 * 60 * 1000, //request timeout in milliseconds 
                                    noDelay: true, //Enable/disable the Nagle algorithm 
                                    keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
                                    keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
                                },
                                responseConfig: {
                                    timeout: 60 * 60 * 1000 //response timeout 
                                }
                            },
                            function (data, response) {
                                //console.log('got review items for ' + reviewId);
                                //data is the parsed response body
                                //response is the raw response data
                                resolve(data);
                            })
                        .on('requestTimeout', function (req) {
                            console.log('request has expired');
                            req.abort();
                            reject('request has expired');
                        })
                        .on('responseTimeout', function (res) {
                            console.log('response has expired');
                            reject('request has expired');
                        })
                        .on('error', function (err) {
                            //it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
                            console.log('request error', err);
                            reject(err);
                        });
                })
                .then(function (review) {
                    if (!review.reviewData.description) {
                        //console.log(host + ':' + reviewId + ':no description');
                        return '';
                    } else {
                        return review.reviewData.description;
                    }
                })
                .then(function (reviewDescription) {
                    var jobPattern = /2[456789]\d{4}/g;
                    var incidentPattern = /1\d{6}/g;
                    var jobMatch = jobPattern.exec(reviewDescription);
                    var incidentMatch = incidentPattern.exec(reviewDescription);
                    if (jobMatch) {
                        revisions.job = jobMatch[0];
                    }
                    if (incidentMatch) {
                        revisions.incident = incidentMatch[0];
                    }
                    revisions.description = reviewDescription;
                    return revisions;
                });
        })
        .then(function (revisions) {
            //.log('..adding revisions to list to pass into next call.');
            //console.log(_.uniq(resultsSoFar.allRevisions.concat(revisions.revisions)));
            //console.log(resultsSoFar);
            //console.log(JSON.stringify(revisions));
            return {
                allRevisions: _.uniq(resultsSoFar.allRevisions.concat(revisions.revisions)),
                reviewsObj: resultsSoFar.reviewsObj.concat(revisions)
            };
        });
}