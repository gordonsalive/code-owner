/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 16/3/'17
// This script fetches all code changes for this calendar month from SVN by:
// (1) load config (where is SVN, which SVN repos, allocate repos to 'technology' for reporting)
// (2) for the current month, for each 'technology', for each repo, fetch the code changes and comments
// (3) store these for later use
// (4) for the current month, for each 'technology', for each repo, parse the comments 
//     and create a unique list of job/jira/issue numbers
// (5) write this out for later use.

// TODO:
// * fetch code changes from git (bitbucket)
// * fetch changes for each of the last 12 calendar months (including so far this month)


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 0; //0=off

//var update_status = require('./scripts/update-status');
//var read_json = require('./scripts/read-json');
var promisify = require('./promisify');
var spawn_promise = require('./spawn-promise');
var fetch_job_descriptions = require('./fetch-job-descriptions');

var resultsJson = {};

module.exports = function (repos) {
    var yearMonths = {};
    _.each(_.range(6), function (delta) {
        var yearMonth = new Date();
        yearMonth.setDate(1); //every month has a first of the month, so safe to take away months.
        yearMonth.setMonth(yearMonth.getMonth() - delta);
        yearMonths[yearMonth.getFullYear() + '-' + ('0' + (yearMonth.getMonth() + 1)).slice(-2)] = {};
    });
    //console.log('yearMonths=' + JSON.stringify(yearMonths));

    return promisify(repos)
        .then(function (repos) {
            //first sort out our array of calls
            //console.log('repos=' + JSON.stringify(repos));
            return Q.all(_.map(yearMonths, function (empty, yearMonth) {
                    return fetch_svn_logs_for_yearmonth_promise(repos, yearMonth);
                }))
                .then(function (results) {
                    //wait for the results and return them
                    return _.flatten(results);
                });
        })
        //        .then(function (execs) {
        //            //console.log('execs=' + JSON.stringify(execs, null, '  '));
        //            return Q.all(
        //                _.map(execs, function (exec) {})
        //            );
        //        })        
        .then(function (comments) {
            //console.log('comments=' + JSON.stringify(comments));
            var groupByYearMonth = _.groupBy(comments, 'yearMonth');
            return _.mapObject(groupByYearMonth, function (yearMonth) {
                return _.groupBy(yearMonth, 'tech');
            });
        })
        .then(function (groupedComments) {
            //console.log('comments=' + JSON.stringify(groupedComments));
            return _.mapObject(groupedComments, function (yearMonth) {
                return _.mapObject(yearMonth, function (tech) {
                    var authorsAndTechByRepo = _.map(tech, function (commentsObj) {
                        var result = {
                            revisions: [], //unique list of revisions
                            authors: [], //unique list of authors
                            jobs: [], //unique list of job/jira/issue no.s (or comment if no no.)
                            jobItems: [] //list of objects for each unique job with authors and revisions
                        };
                        //var pattern = new RegExp(/r\d+ \| (\w+) \| \d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d.* \| .+ line.*(\r\r|\n\n|\r\n\r\n).*(\d\d\d\d\d\d|\b\w\w\w-\d\d\d\b|1\d\d\d\d\d\d)[^.].*/g);
                        var pattern = new RegExp(
                            /r(\d+) \| (\w+) \| \d{4}-\d\d-\d\d \d\d:\d\d:\d\d.* \| .+ lines?(?:\r\r|\n\n|\r\n\r\n)(?!\[.+\]|Jenkins:|.*JENKINS_APPROVE|[Cc]hange parent pom|[Uu]pdate version|[Uu]pdate pom|[Rr]eplacing pom|[Cc]hange pom|[Cc]reating branch|Update domain version objects|.*AUTH-NO-REVIEW|.*AUTH_NO_REVIEW)(.*(\b\d{6}\b)[^/]?.*|.*(\b\w?\w?\w\w\w-\d+\b)[^.]?.*|.*(\b1\d{6}\b).*|.+)/g
                        );
                        var match = pattern.exec(commentsObj.comments);
                        while (match) {
                            //console.log('match=' + match[1] + ':' + match[3]);
                            var revision = match[1];
                            var author = match[2];
                            var job = match[4] ? match[4] :
                                (match[5] ? match[5] :
                                    (match[6] ? match[6] :
                                        match[3]));
                            //if (!match[3] && !match[4] && !match[5]) {
                            //    console.log('no job/issue/jira found for:' + match[2]);
                            //}
                            //console.log("revision=" + revision + ", author=" + author + ", job=" + job);
                            if ((revision) && (author != "cibuildsrw")) {
                                if (result.revisions.indexOf(revision) == -1) {
                                    result.revisions.push(revision);
                                }
                                if (result.authors.indexOf(author) == -1) {
                                    result.authors.push(author);
                                }
                                if (result.jobs.indexOf(job) == -1) {
                                    result.jobs.push(job);
                                    //add new item to the jobItems list
                                    result.jobItems.push({
                                        job: job,
                                        authors: [author],
                                        revisions: [revision]
                                    });
                                } else {
                                    //add to the existing jobItems object
                                    var jobItem = _.findWhere(result.jobItems, {
                                        job: job
                                    });
                                    if (jobItem.authors.indexOf(author) == -1) {
                                        jobItem.authors.push(author);
                                    }
                                    if (jobItem.revisions.indexOf(revision) == -1) {
                                        jobItem.revisions.push(revision);
                                    }
                                }
                            }
                            match = pattern.exec(commentsObj.comments);
                        }
                        //console.log('result=' + JSON.stringify(result));
                        return result;
                    });
                    //flatten the arrays (where multiple repos to a tech) and remove any empty sets for repos that failed.
                    return {
                        //                        authors: _.uniq(_.flatten(_.map(authorsAndTechByRepo, function (item) {
                        //                            return item.authors;
                        //                        }))),
                        authors: _.uniq(_.flatten(_.map(authorsAndTechByRepo, function (item) {
                            return item.authors;
                        }))),
                        jobs: _.uniq(_.flatten(_.map(authorsAndTechByRepo, function (item) {
                            return item.jobs;
                        }))),
                        revisions: _.uniq(_.flatten(_.map(authorsAndTechByRepo, function (item) {
                            return item.revisions;
                        }))),
                        jobItems: _.uniq(_.flatten(_.map(authorsAndTechByRepo, function (item) {
                            return item.jobItems;
                        })))
                    };
                });
            });
        })
        .then(function (results) {
            if (results.undefined) {
                delete results.undefined;
            }
            //console.log("results=" + JSON.stringify(results, null, " "));
            //enrich the results with descriptions for the job numbers:
            //(1) get a unique list of all job numbers and promisify it
            var jobs = _.flatten(_.map(results, function (yearMonth) {
                return _.map(yearMonth, function (tech) {
                    return tech.jobs;
                });
            }));
            jobs = _.filter(jobs, function (job) {
                return /^\d\d\d\d\d\d$/.test(job);
            });
            //console.log("jobs=" + jobs);
            //(2) get a job-description map (uses pooled promise to fetch descriptions in n pools)
            return fetch_job_descriptions(jobs)
                .then(function (jobDescriptions) {
                    //(3) spin through our results data adding jobDescription to jobItems
                    return _.mapObject(results, function (yearMonth) {
                        return _.mapObject(yearMonth, function (tech) {
                            return {
                                authors: tech.authors,
                                jobs: tech.jobs,
                                revisions: tech.revisions,
                                jobItems: _.map(tech.jobItems, function (jobItem) {
                                    var jobDescriptionItem = _.findWhere(jobDescriptions, {
                                        job: jobItem.job
                                    });
                                    if (jobDescriptionItem) {
                                        return _.extend(jobItem, {
                                            description: jobDescriptionItem.description
                                        });
                                    } else {
                                        return jobItem;
                                    }
                                })
                            };
                        });
                    });
                });
        })
        .then(function (results) {
            //console.log('results=' + JSON.stringify(results, null, '  '));
            return results;
        })
        .then(function (resultObject) {
            //console.log('--write out results file..');
            //console.log('results=' + JSON.stringify(results, null, '  '));
            return Qfs.write("json/changes-results.json", JSON.stringify(resultObject, null, '\t'));
        });

};

function fetch_svn_logs_for_yearmonth_promise(repos, yearMonth) {
    var svnHost = repos.svnHost;

    //console.log('yearMonth=' + yearMonth);
    return Q.all(_.map(repos['repos-reviews'], function (tech, techName) {
        return fetch_svn_logs_for_yearmonth_tech_promise(svnHost, yearMonth, tech, techName);
    })).then(function (results) {
        //console.log('got svn logs for yearmonth');
        //wait for the results and return them
        return results;
    });
}


function fetch_svn_logs_for_yearmonth_tech_promise(svnHost, yearMonth, tech, techName) {
    //console.log('techName=' + techName);
    //delphi, java, ..
    return Q.all(
            _.flatten(
                _.map(tech, function (repo) {
                    //expand the repo path - for most cases we want /trunk/ and last 5 quarterly branches: /171/, /164/, etc./
                    // for some repos there are too many sub projects and they get added to over time, so just get all changes
                    // even from branches we might not be intereted in.
                    var now = new Date();
                    var yearNow = now.getFullYear(); // e.g. 2017
                    var QNow = Math.ceil((now.getMonth() + 1) / 3); // e.g. 1
                    //console.log('yearq = ' + yearNow + QNow);
                    var Q = yearNow.toString().slice(-2) + QNow; //e.g. 171
                    var Q_4 = (yearNow - 1).toString().slice(-2) + '4'; //e.g. 164
                    var Q_3 = QNow <= 3 ? (yearNow - 1).toString().slice(-2) + '3' : yearNow.toString().slice(-2) + '3'; //e.g. 163
                    var Q_2 = QNow <= 2 ? (yearNow - 1).toString().slice(-2) + '2' : yearNow.toString().slice(-2) + '2'; //e.g. 162
                    var Q_1 = QNow === 1 ? (yearNow - 1).toString().slice(-2) + '1' : yearNow.toString().slice(-2) + '1'; //e.g. 161
                    if (repo.branches === '*') {
                        return fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path);
                    } else if (repo.branches === 'trunk') {
                        return fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "trunk/");
                    } else if (repo.branches === 'trunk+') {
                        return [
                        fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "trunk/"),
                        //fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "branches/" + Q + "/"),
                        fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "branches/" + Q_4 + "/"),
                        fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "branches/" + Q_3 + "/"),
                        fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "branches/" + Q_2 + "/"),
                        fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo.path + "branches/" + Q_1 + "/")
                    ];
                    } else {
                        throw "Invalid value for branches in repo.json";
                    }
                })
            )
        )
        .then(function (results) {
            //console.log('...got svn logs for yearmonth and tech' + yearMonth + ', ' + techName);
            //wait for the results and return them
            return results;
        });
}

function fetch_svn_log_for_yearmonth_tech_repo_promise(svnHost, yearMonth, techName, repo) {
    var dateRange = dateRangeForMonth(yearMonth);
    //return promisify.method(function () {
    //var exec = 'svn log -v ' + svnHost + repo + ' -r ' + dateRange;
    var exec = 'svn log ' + svnHost + repo + ' -r ' + dateRange;
    //console.log('exec=' + exec);
    //return {
    //    yearMonth: yearMonth,
    //    tech: techName,
    //    exec: exec
    //};

    return spawn_promise(exec, true, 4 * 60 * 1000)
        .then(function (comments) {
            //console.log('comments=' + comments);
            return {
                yearMonth: yearMonth,
                tech: techName,
                comments: comments
            };
        })
        .fail(function (err) {
            //if any of these calls fails, just skip over them
            return {
                yearMonth: yearMonth,
                tech: techName,
                comments: ''
            };
        });
}

function dateRangeForMonth(yearMonth) {
    function daysInMonth(month, year) {
        //month in javascript dates is 0 based, year and date (day of month) are 1 based, 
        //going out of bounds automatically wraps,
        //so this says for next month, what is the last day of the previous month (i.e. last day of this month, see?)
        return new Date(year, month, 0).getDate();
    }

    var month = yearMonth.slice(-2);
    var year = yearMonth.slice(0, 4);
    return '{' + yearMonth + '-01}:{' + yearMonth + '-' + ('0' + daysInMonth(month, year)).slice(-2) + '}';
}