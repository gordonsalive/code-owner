/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, esversion: 6*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 20/4/'17
// This script sends out the weekly testing summary email
// (1) 
// (2) 

// TODO:
// * ..
var _ = require("underscore");
var Qfs = require("q-io/fs");

var DEBUG = 0; //0=of

var read_json = require('./read-json');
var promisify = require('./promisify');
var emailer = require('./emailer');

module.exports = function () {
    var now = new Date();
    var sched_testing_summary_json = 'json/sched-testing-summary.json';
    var schedule_json = 'json/schedule.json';
    var testing_summary_json = 'json/testing-summary.json';
    //return read_json('json/sched_review_emails.json')*************************
    return promisify.spread(
        [read_json(sched_testing_summary_json), read_json(schedule_json)],
        function (sched_testing_summary, schedule) {

            return promisify.method(function () {
                    if (!schedule.testingSummaryEmails) {
                        schedule.testingSummaryEmails = {};
                    }
                    if (!schedule.testingSummaryEmails.weekly) {
                        schedule.testingSummaryEmails.weekly = {};
                    }

                    //do we need to do a weekly email?
                    var nextSend = schedule.testingSummaryEmails.weekly.nextSend;
                    var nextSendDate;
                    if ((nextSend) && (nextSend !== "")) {
                        nextSendDate = new Date(nextSend);
                    }
                    console.log('weekly.nextSend:' + nextSend);
                    console.log('weekly.nextSendDate:' + nextSendDate);
                    //if we've never sent a mail or we have passed the next send date, then send email
                    return ((!nextSend) || (nextSend === "") || (nextSendDate < now));
                })
                .then(function (isSendWeekly) {
                    if (isSendWeekly) {
                        console.log('is send weekly');
                        //lets parse the testing-summary.json file
                        return read_json(testing_summary_json)
                            .then(function (testing_summary) {
                                //convert the testing summary into a simple summary by jobs and files
                                var jobsSummary = _.flatten(_.map(testing_summary, function (job) {
                                    return _.map(
                                        _.groupBy(job.features, function (feature) {
                                            if ((feature.file.includes('junit')) &&
                                                (feature.file.slice(-3) === 'xml')) {
                                                return 'junit xml files';
                                            } else {
                                                return feature.file;
                                            }
                                        }),
                                        function (features, file) {
                                            return {
                                                job: job.job,
                                                file: file,
                                                features: features.length,
                                                scenarios: _.reduce(features, function (memo, feature) {
                                                    return memo += feature.scenarios.length;
                                                }, 0)
                                            };
                                        }
                                    );
                                }));
                                //console.log("jobsSummary:" + JSON.stringify(jobsSummary, null, ' '));
                                //find the next send date 
                                //(if dow == 1 then add 7 days to today, if dow = 0 then add 1, if dow > 1 then add (7-dow) )
                                var whenDow = sched_testing_summary.weekly.when.dow;
                                var dow = now.getDay();
                                var diff = (dow < whenDow) ? whenDow - dow : 8 - dow;
                                //var diff = (dow === 0) ? 1 : 8 - dow;
                                var nextSendDate = new Date();
                                nextSendDate.setDate(nextSendDate.getDate() + diff); //should wrap around months if it has to
                                //now setup the options for the emailer and construct email.
                                var weekly = sched_testing_summary.weekly;

                                console.log('set up email:');
                                var mailOptions = {
                                    from: weekly.who.from, // sender address
                                    to: weekly.who.to, // list of receivers
                                    cc: weekly.who.cc, // list of receivers
                                    subject: weekly.subject, // Subject line
                                    text: constructEmailBody(weekly, jobsSummary, false), // plain text body
                                    html: constructEmailBody(weekly, jobsSummary, true), // html body
                                    attachments: [{
                                        filename: 'testing-summary.xml',
                                        path: 'json/testing-summary.xml'
                                    }]
                                };
                                return emailer(mailOptions)
                                    .then(function (result) {
                                        //need to reload schedule at this point, 
                                        //as it might have been updated since this promise chain was created and we don't want to undo those changes when we save it
                                        return read_json(schedule_json);
                                    })
                                    .then(function (schedule) {
                                        //update the nextSend on this node and write it out
                                        schedule.testingSummaryEmails.weekly.nextSend = nextSendDate.toDateString();
                                        schedule.testingSummaryEmails.weekly.lastSent = now.toDateString();
                                        return Qfs.write(schedule_json, JSON.stringify(schedule, null, '\t'));
                                    });
                            });
                    }
                });
        });

    function constructEmailBody(sched, jobsSummary, isHtml) {
        console.log('constructEmailBody');
        //console.log('weekly = ' + JSON.stringify(weekly));
        //console.log('reviewResults = ' + JSON.stringify(reviewResults));
        //console.log('changes = ' + JSON.stringify(changes));
        var body = isHtml ? sched.html.join('<br>\n') : sched.html.join('\n');

        var fullBody = isHtml ? getBodyStyle() : '';
        //take the body and replace the tags with the values from the review results
        fullBody += body
            .replace(/<group>/g, sched.who.group)
            .replace(/<test_job_table>/g, constructJobsSummaryTable(jobsSummary, isHtml))
            .replace(/<who_from>/g, sched.who.signoff);

        fullBody += isHtml ? '\n</BODY>\n' : '';

        return fullBody;
    }

    function getBodyStyle() {
        console.log('getBodyStyle');
        return [
            '<BODY>',
            '<STYLE type="text/css">',
            'body {font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; font-size: 14px; text-align: left; }',
            'h1 {font-size: 20px}',
            'h2 {font-size: 16px}',
            'table {font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; font-size: 12px; background: #fff; margin: 10px; width: 800px; border-collapse: collapse; text-align: left;}',
            'th {font-size: 14px; font-weight: normal; color: #039; padding: 10px 8px; border-bottom: 2px solid #6678b1;}',
            'td {border-bottom: 1px solid #ccc; color: #669; padding: 6px 8px;}',
            '.unauth td {color: #F00;}',
            'p.summary {font-size: 12px;}',
            '</STYLE>\n'
            ].join('\n');
    }

    function constructJobsSummaryTable(jobsSummary, isHtml) {
        console.log('constructChangesTable');
        var tableBody = '';
        if (isHtml) {
            tableBody = [
                '<TABLE>',
                '  <TR>',
                '    <TH>Job</TH>',
                '    <TH>Results File</TH>',
                '    <TH>Features</TH>',
                '    <TH>Scenarios</TH>',
                '  </TR>\n'
            ].join('\n');

            tableBody += _.map(jobsSummary, function (jobSummary) {
                return '  <TR>\n<TD>' + jobSummary.job +
                    '    </TD>\n<TD>' + jobSummary.file +
                    '    </TD>\n<TD>' + jobSummary.features +
                    '    </TD>\n<TD>' + jobSummary.scenarios +
                    '    </TD>\n  </TR>\n';
            }).join('\n');

            tableBody += '</TABLE>\n';
        } else {
            tableBody = '\n\n\tJOB\t\tRESULTS FILE\t\tFEATURES\t\tSCENARIOS\n';
            tableBody += _.map(jobsSummary, function (jobSummary) {
                return '\t' + jobSummary.job + '\t\t' + jobSummary.file + '\t\t' + jobSummary.features + '\t\t' + jobSummary.scenarios;
            }).join('\n');
        }
        //console.log(tableBody);
        return tableBody;
    }
};