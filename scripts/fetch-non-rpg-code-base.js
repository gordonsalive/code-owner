/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 25/7/2017
// Module to fetch shape and lines of code for the non-rpg code base
// Fetch code base of all repos, count up lines of code, create value hierarchy
//   top levels of the hierarchy are technologies (delphi & java) come from repos.json
//   Currently limited to just SVN.
//   Uses cloc (which uses perl) to count lines of code:
//     cloc . --vcs=git --by-file --json --out=line-counts.json
//**********
var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");
var fs = require('fs');
var parseString = require('xml2js').parseString;

var DEBUG = 1; //0=off

var read_json = require('./read-json');
var delete_folder_recursive_promise = require('./delete-folder-recursive-promise');
//var spawn_promise = require('./spawn-promise');
var exec_promise = require('./exec-promise');
var promisify = require('./promisify');


module.exports = function () {
    if (DEBUG) console.log('fetch-non-rpg-code-base.js');

    var nonRpgCodeBasesAsJsonObj = {
        //delphi: {
        //    name1: {name1_1: {}, name1_2: {}}, name2: {}
        //}
    };
    var nonRpgCodeBasesHierarchy = {
        //delphi: {
        //    name: "delphi",
        //    children: [{}]
        //},
        //java: {}
    };

    return read_json('json/repos.json')
        .then(function (repos) {
            if (DEBUG) console.log('just read in repos = '); // + JSON.stringify(repos));
            //for each technology:
            //  fetch down each repo trunk branch (trunk & trunk+ = path/trunk, * = (*)/trunk)
            //  run cloc to count lines (per file) and produce json file
            //  delete repo leaving only cloc files behind
            return Q.all(
                //_.map(repos['repos-heat'], function (tech, techName) {
                _.map(repos['repos-heat'], function (tech, techName) {
                    //return delete_folder_recursive_promise('repos/' + techName)
                    return promisify({})
                        .then(function (deleteResult) {
                            //if (DEBUG) console.log('(not deleted folder) ' + 'repos/' + techName + ' now recreating');
                            //now create the tech folder ready to start checking out repos into it
                            if (!fs.existsSync('./repos/' + techName)) {
                                console.log('directory missing, creating it:' + './repos/' + techName);
                                fs.mkdirSync('./repos/' + techName);
                            } else {
                                console.log('directory already exists:' + './repos/' + techName);
                            }
                            return true;
                        })
                        .then(function (createDirResult) {
                            if (DEBUG) console.log('now about to checkout repos');
                            //for each repo fetch code into its own directory (with sub path, if required)
                            return Q.all(
                                _.map(tech, function (repo) {
                                    if (repo['scm-type'] && repo['scm-type'] === 'git') {
                                        //we've fetched the code already, so don't do it again
                                        if (fs.existsSync('./repos/' + techName + '/' + repo.name)) {
                                            return {
                                                path:'repos/' + techName + '/' + repo.name,
                                                scmType: 'git'
                                            };
                                        } else {
                                            console.log("FAILED: failed to find code for git repo" + repo.path + " (" + './repos/' + techName + '/' + repo.name + ")! ");
                                            return {path:''};
                                        }
                                    } else {
                                        var thisRepo = '';
                                        var exec = '';
                                        if ((repo.branches === 'trunk') || (repo.branches === 'trunk+')) {
                                            if (DEBUG) console.log('it`s a trunk or trunk+ repo');
                                            //if working folder is there already, just update it
                                            var repoToCheckout = repos.svnHost + repo.path + 'trunk/ ';
                                            var checkoutToFolder = 'repos/' + techName + '/' + repo.name;
                                            var isItThereAlready = 'ls ' + checkoutToFolder + '/.svn';
                                            var cleanItUp = 'svn cleanup ' + checkoutToFolder + ' --non-interactive';
                                            var updateIt = 'svn update ' + checkoutToFolder;
                                            var checkItOut = 'svn checkout -q ' + repoToCheckout + checkoutToFolder;
                                            exec = '(' + isItThereAlready + ' && ' + cleanItUp + ' && ' + updateIt + ')' +
                                                ' || ' + checkItOut;
                                            //console.log(exec);

                                            return exec_promise(exec, !DEBUG, 10 * 60 * 1000)
                                                .then(function (log) {
                                                    if (DEBUG) 'return repo name:' + repo.name;
                                                    return {
                                                        path: 'repos/' + techName + '/' + repo.name,
                                                        scmType: 'svn'
                                                    };
                                                })
                                                .fail(function (err) {
                                                    //if any of these calls fails, just skip over them
                                                    console.log("FAILED: svn checkout for " + repo.path + "trunk/ failed!  " + err);
                                                    //return err;
                                                    return {path:''};
                                                });
                                            //return promisify('repos/' + techName + '/' + repo.name);


                                        } else if (repo.branches === '*') {
                                            if (DEBUG) console.log(".. " + repo.path + " is a * repo - getting list of sub items");
                                            //going to need to iterate through each sub-item
                                            // dragging down item/trunk
                                            var tidyName = 'repos/' + repo.path.replace(/[\\/]/g, '~') + '.xml';
                                            exec = 'svn list ' + repos.svnHost + repo.path + ' --xml > ' + tidyName; // -out:sub-repos.xml'
                                            return exec_promise(exec, !DEBUG, 10 * 60 * 1000)
                                                .then(function (result) {
                                                    if (DEBUG) console.log('parsing list of sub items.');
                                                    //return parseString(subItemsXML);
                                                    return Qfs.read(tidyName)
                                                        .then(function (subItemsXML) {
                                                            //console.log('subItemsXML:' + subItemsXML);
                                                            return parseStringPromise(subItemsXML);
                                                        });
                                                })
                                                .then(function (subItems) {
                                                    //if (DEBUG) console.log('subItems:' + JSON.stringify(subItems, null, ' '));
                                                    //pull out the sub item directories from the xml list
                                                    //console.log(JSON.stringify(subItems.lists.list[0]));
                                                    var subItemDirs = _.map(subItems.lists.list[0].entry, function (item) {
                                                        //console.log(JSON.stringify(item));
                                                        return item.name[0];
                                                    });
                                                    if (DEBUG) console.log('---subItemDirs(' + repo.path + '):' + subItemDirs);
                                                    //************* GET THIS! *************
                                                    //** It turns out that within a repo tree like non-thenon/api
                                                    //** Some sub-repos will have trunk and branches
                                                    //** AND OTHERS WON'T!
                                                    //** Need to do a second list to determine this!
                                                    //*************************************
                                                    return Q.all(
                                                        _.map(subItemDirs,
                                                            function (subItem) {
                                                                //Does this node in SVN have a trunk sub-node, 
                                                                //  if so, fetch that
                                                                //  if not, fetch this node as is.
                                                                var listTidyName = 'repos/' + (repo.path + subItem).replace(/[\\/]/g, '~') + '.xml';
                                                                //console.log('listTidyName=' + listTidyName);
                                                                var listExec = 'svn list ' + repos.svnHost + repo.path + subItem + ' --xml > ' + listTidyName; // -out:sub-repos.xml'
                                                                return exec_promise(listExec, !DEBUG, 10 * 60 * 1000)
                                                                    .then(function (listResult) {
                                                                        if (DEBUG) console.log('parsing list of sub-sub items.');
                                                                        return Qfs.read(listTidyName)
                                                                            .then(function (subSubItemsXML) {
                                                                                //console.log('subItemsXML:' + subItemsXML);
                                                                                return parseStringPromise(subSubItemsXML);
                                                                            })
                                                                            .then(function (subSubItems) {
                                                                                var subSubItemDirs = _.map(subSubItems.lists.list[0].entry, function (item) {
                                                                                    //console.log('.entry = ' + JSON.stringify(item));
                                                                                    return item.name[0];
                                                                                });
                                                                                //console.log('entries=' + subSubItemDirs);
                                                                                //console.log('includesTrunk=' + subSubItemDirs.includes('trunk'));
                                                                                return subSubItemDirs.includes('trunk');
                                                                            });
                                                                    })
                                                                    .then(function (containsTrunkDir) {
                                                                        //Does this node in SVN have a trunk sub-node, 
                                                                        //  if so, fetch that
                                                                        //  if not, fetch this node as is.
                                                                        var repoToCheckout = '';
                                                                        if (containsTrunkDir) {
                                                                            repoToCheckout = repos.svnHost + repo.path + subItem + '/trunk/ ';
                                                                        } else {
                                                                            repoToCheckout = repos.svnHost + repo.path + subItem + '/ ';
                                                                        }
                                                                        var checkoutToFolder = 'repos/' + techName + '/' + repo.name + '/' + subItem;
                                                                        var isItThereAlready = 'ls ' + checkoutToFolder + '/.svn';
                                                                        var cleanItUp = 'svn cleanup ' + checkoutToFolder + ' --non-interactive';
                                                                        var updateIt = 'svn update ' + checkoutToFolder;
                                                                        var checkItOut = 'svn checkout -q ' + repoToCheckout + checkoutToFolder;
                                                                        var subSubExec = '(' + isItThereAlready + ' && ' +
                                                                            cleanItUp + ' && ' +
                                                                            updateIt + ')' +
                                                                            ' || ' + checkItOut;

                                                                        //var subSubExec;
                                                                        //if (containsTrunkDir) {
                                                                        //    subSubExec = 'svn checkout -q ' +
                                                                        //        repos.svnHost + repo.path + subItem + '/trunk/ ' + 'repos/' + techName + '/' + repo.name + '/' + subItem;
                                                                        //} else {
                                                                        //    subSubExec = 'svn checkout -q ' +
                                                                        //        repos.svnHost + repo.path + subItem + '/ ' + 'repos/' + techName + '/' + repo.name + '/' + subItem;
                                                                        //}


                                                                        return exec_promise(subSubExec, !DEBUG, 10 * 60 * 1000)
                                                                            .then(function (log) {
                                                                                return {
                                                                                    path: 'repos/' + techName + '/' + repo.name + '/' + subItem,
                                                                                    scmType: 'svn'
                                                                                };
                                                                            })
                                                                            .fail(function (err) {
                                                                                //if any of these calls fails, just skip over them
                                                                                console.log("FAILED: svn checkout for (" + subSubExec + ") failed!  " + err);
                                                                                //return err;
                                                                                return {path:''};
                                                                            });
                                                                        //return 'repos/' + techName + '/' + subItem;


                                                                    })
                                                                    .fail(function (err) {
                                                                        //if any of these calls fails, just skip over them
                                                                        console.log("FAILED: svn list for " + repos.svnHost + repo.path + subItem + " failed!  " + err);
                                                                        //return err;
                                                                        return {path:''};
                                                                    });
                                                            })
                                                    );
                                                })
                                                .then(function (subItems) {
                                                    return subItems;
                                                })
                                                .fail(function (err) {
                                                    //if any of these calls fails, just skip over them
                                                    console.log("FAILED: svn list for " + repo.path + " failed!  " + err);
                                                    //return err;
                                                    return {path:''};
                                                });
                                        }
                                    }
                                })
                            );
                        })
                        .then(function (results) {
                            //if (DEBUG) console.log('results = ' + JSON.stringify(results, null, ' ') + ', results.length = ' + results.length);
                            //now count the lines of code, output results for every file in output file
                            return Q.all(
                                _.map(_.flatten(results), function (result) {
                                    var item = result.path;
                                    if (item && (item !== '')) {
                                        //if (DEBUG) console.log('item =' + item);
                                        var folders = item.split(/[/\\]/); //split on any / or \
                                        folders.shift(); //removes "repos" from front
                                        var outputFilename = folders.join('~') + '.json';
                                        var dotdot = _.map(folders, function (item) {
                                            return '..';
                                        }).join('/');
                                        //drop down to folder of item, count lines and copy output back up to repos
                                        var exec = 'cd ' + item + ' && ' +
                                            'pwd ' + ' && ' +
                                            'echo {} > ' + outputFilename + ' && ' +
                                            'cloc . --vcs='+result.scmType+' --by-file --json --out=' + outputFilename + ' && ' +
                                            'cp ' + outputFilename + ' ' + dotdot;// + '/repos';
                                        console.log(exec);
                                        return exec_promise(exec, !DEBUG, 15 * 60 * 1000)
                                            .then(function (execResult) {
                                                return outputFilename;
                                            })
                                            .fail(function (err) {
                                                //if any of these calls fails, just skip over them
                                                console.log("FAILED: cloc for " + item + ':' + outputFilename + " failed!  " + err);
                                                //return err;
                                                return '';
                                            });

                                    } else {
                                        return null;
                                    }
                                })
                            );
                        })
                        .then(function (results) {
                            var filenames = _.flatten(results);
                            console.log('about to read in line counts and convert to map hierarchy (json obj), filenames = ' + filenames);
                            //now transform these results into a hiearchy of maps (json object)
                            nonRpgCodeBasesAsJsonObj[techName] = {};
                            return Q.all(
                                _.map(filenames, function (filename) {
                                    console.log('filename=' + filename);
                                    if (!filename) {
                                        return null;
                                    }
                                    var groupings = filename.substring(0, filename.length - 5).split('~');
                                    //find or create a place to put this file's line counts
                                    var point = nonRpgCodeBasesAsJsonObj[techName];
                                    var head;
                                    var tail = groupings;
                                    tail.shift(); //lose the tech name
                                    //tail.shift(); //lose the name of the repo
                                    while (tail.length) {
                                        head = tail.shift();
                                        if (!point[head]) {
                                            point[head] = {};
                                        }
                                        point = point[head];
                                    }
                                    //read in the file
                                    return read_json('./repos/' + filename)
                                        .then(function (repoFilesLineCounts) {
                                            //read in the line counts and put them into right place in the structyre
                                            _.each(repoFilesLineCounts, function (fileLineCounts, filePath) {
                                                if (filePath === "SUM") {
                                                    point.sumBlank = fileLineCounts.blank;
                                                    point.sumComment = fileLineCounts.comment;
                                                    point.sumCode = fileLineCounts.code;
                                                    point.sumNoFiles = fileLineCounts.nFiles;
                                                } else if ((filePath === "header") && (fileLineCounts.n_files)) {
                                                    //ignore the header record
                                                } else {
                                                    var fileGroupings = filePath.split('/');
                                                    var filePoint = point;
                                                    var fileHead;
                                                    var fileTail = fileGroupings;
                                                    while (fileTail.length) {
                                                        fileHead = fileTail.shift();
                                                        if (!filePoint[fileHead]) {
                                                            filePoint[fileHead] = {};
                                                        }
                                                        filePoint = filePoint[fileHead];
                                                    }
                                                    filePoint.blank = fileLineCounts.blank;
                                                    filePoint.comment = fileLineCounts.comment;
                                                    filePoint.code = fileLineCounts.code;
                                                    filePoint.language = fileLineCounts.language;
                                                }
                                            });
                                            console.log('return points');
                                            return point;
                                        })
                                        .then(function (points) {
                                            //now convert the map hierarchy (json obj) into a node hierarchy
                                            //console.log('nonRpgCodeBasesAsJsonObj=' +
                                            //    JSON.stringify(nonRpgCodeBasesAsJsonObj, null, '  '));
                                            return points;
                                        });
                                })
                            );
                        })
                        .then(function (points) {
                            //now, for this tech, 
                            //spin recursively through map hierarchy converting it to node hierarchy
                            function childrenRecursive(point, pointName) {
                                //console.log("recursing on - " + pointName);
                                if (point && (typeof point === 'object')) {
                                    //console.log("pointMap=" + _.map(point, function (item) {
                                    //    return item;
                                    //}));
                                    //console.log("pointMap.length=" + _.map(point, function (item) {
                                    //    return item;
                                    //}).length);
                                    //console.log("point == {}:" + (point == {}));
                                    return _.map(point, function (subItemPoint, subItemPointName) {
                                        if (subItemPoint.code) {
                                            //this is a set of results, so stop recursing
                                            return {
                                                name: subItemPointName,
                                                blank: subItemPoint.blank,
                                                comment: subItemPoint.comment,
                                                code: subItemPoint.code,
                                                language: subItemPoint.language,
                                                size: subItemPoint.code, //size of bubbles in bubble chart
                                                weight: 0 //shade of bubbles from 0 - 1
                                            };
                                        } else {
                                            //recurse
                                            //console.log("subItemPointName=" + subItemPointName);
                                            return {
                                                name: subItemPointName,
                                                children: childrenRecursive(subItemPoint, subItemPointName)
                                            };
                                        }
                                    });

                                } else {
                                    //console.log("returning null");
                                    return null;
                                }
                            }
                            console.log('create RPG code base hierarchy.');

                            nonRpgCodeBasesHierarchy[techName] = {
                                name: "root", //techName,
                                children: childrenRecursive(nonRpgCodeBasesAsJsonObj[techName], techName)
                            };
                            return nonRpgCodeBasesHierarchy;
                        })
                        .then(function (results) {
                            //write out results
                            console.log('writing out ' + 'repos/' + techName + '-code-base.json' + 'and ' + 'repos/' + techName + '-code-hierarchy.json');
                            return Qfs.write('repos/' + techName + '-code-base.json', JSON.stringify(nonRpgCodeBasesAsJsonObj[techName], null, '\t'))
                                .then(function (results) {
                                    return Qfs.write('repos/' + techName + '-code-hierarchy.json', JSON.stringify(nonRpgCodeBasesHierarchy[techName], null, '\t'));
                                });
                        })
                        .then(function (results) {
                            console.log("(not about to delete tech folder), tech=" + techName); // + " results = " + JSON.stringify(results, null, ' '));
                            return null;
                            //return delete_folder_recursive_promise('repos/' + techName);
                        });
                })
            )
                .then(function (results) {
                    //console.log("results=" + JSON.stringify(results));
                    return results;
                });
        })
        .then(function (results) {
            return results;
        });

};

function parseStringPromise(xml) {
    return Q.Promise(function (resolve, reject, notify) {
        parseString(xml, function (err, result) {
            if (err) {
                reject(err);
            } else {
                //console.log(JSON.stringify(result, null, ' '));
                resolve(result);
            }
        });
    });
}