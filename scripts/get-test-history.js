/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");

var readJson = require("./read-json");
var promisify = require("./promisify");

// AAG 22/3/'16
// method to return a promise to get db populate timings data
module.exports = function (historyJson) {
    //console.log('get-test-history(' + historyJson + ')');
    var testHistory;

    if (historyJson) {
        //they've either passed in the location of the json, or the object itself
        if (typeof (historyJson) === "string") {
            //console.log('its a string');
            testHistory = readJson(historyJson);
        } else {
            //console.log('it must be a result');
            testHistory = promisify(historyJson);
        }
    } else {
        testHistory = readJson("./test-history-results.json");
    }

    return testHistory;
};