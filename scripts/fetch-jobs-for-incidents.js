/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 7/4/'17
// This script queries Henry's smartCore REST intf. to get job numbers for incidents
// (1) 
// (2) 
// (3) 
// (4) 
// (5) 

// TODO:
// * fetch code reviews at other states so I can high light changes with no code reviews!


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;
var Client = require('node-rest-client').Client;
var client = new Client();
client.on('error', function (err) {
    console.error('Something went wrong on the client', err);
    throw err;
});
//var http = require("http");

var DEBUG = 0; //0=off

var read_json = require('./read-json');
//var promisify = require('./promisify');
//var exec_promise = require('./scripts/exec-promise');

var resultsJson = {};

module.exports = function (incidents) {
    //reviews is an array of incident numbers, e.g. [1000280. ...]
    //console.log('..fetching job numbers.');
    var incidentsWithJobsGlobal = [];

    return read_json('json/incidentJobMap.json')
        .then(function (incidentJobMap) {
            //remove from my list of incidents those that I already have
            var existingIncidents = _.pluck(incidentJobMap, 'incident');
            var remainingIncidents = _.difference(incidents, existingIncidents);
            //now fetch these from smartcode, but add them to the cache whether we succeed or fail!
            return get_job_for_incident_pooled(remainingIncidents)
                .then(function (incidentsWithJobs) {
                    //we now have a list of {incident:} or {incident: , job: }
                    // to combine to cache, resave cache and return
                    // (must ensure no duplicate incidents when doing this)
                    //take global list and cache, 
                    //for each item in global list, if it is in cache replace it, if not add it
                    incidentJobMap = _.map(incidentJobMap, function (item) {
                        var incidentWithJob = _.findWhere(incidentsWithJobs, {
                            incident: item.incident
                        });
                        if (incidentWithJob) {
                            return incidentWithJob;
                        } else {
                            return item;
                        }
                    });
                    _.each(incidentsWithJobs, function (incidentWithJob) {
                        var item = _.findWhere(incidentJobMap, {
                            incident: incidentWithJob.incident
                        });
                        if (item) {
                            //do nothing, it's in there already
                        } else {
                            incidentJobMap.push(incidentWithJob);
                        }
                    });
                    return Qfs.write('json/incidentJobMap.json', JSON.stringify(_.reject(incidentJobMap, function (item) {
                            return (item.job === "Not Found");
                        }), null, '\t'))
                        .then(function (writeResult) {
                            //return incidentsWithJobs;
                            return incidentJobMap;
                        });
                })
                .fail(function (err) {
                    //use the global list of incidents with jobs to combine with cache and resave
                    // then reraise the issue to stop main process
                    // (must ensure no duplicate incidents when resaving to cache)

                    //take global list and cache, 
                    //for each item in global list, if it is in cache replace it, if not add it
                    incidentJobMap = _.map(incidentJobMap, function (item) {
                        var incidentWithJob = _.findWhere(incidentsWithJobsGlobal, {
                            incident: item.incident
                        });
                        if (incidentWithJob) {
                            return incidentWithJob;
                        } else {
                            return item;
                        }
                    });
                    _.each(incidentsWithJobsGlobal, function (incidentWithJob) {
                        var item = _.findWhere(incidentJobMap, {
                            incident: incidentWithJob.incident
                        });
                        if (item) {
                            //do nothing, it's in there already
                        } else {
                            incidentJobMap.push(incidentWithJob);
                        }
                    });
                    return Qfs.write('json/incidentJobMap.json', JSON.stringify(incidentJobMap, null, '\t'))
                        .then(function (writeResult) {
                            throw err;
                        });
                });
        });

    function get_job_for_incident_pooled(incidents) {
        //console.log('get_job_for_incident_pooled');
        //split the id's out into n different batches, or just 1 if there are less than n items
        var n = 2; //10;
        var items = _.map(incidents);
        if (items.length <= n) {
            //console.log('small number so proceed as a single batch');
            return get_job_for_incident_acc(items, 0);
        } else {
            //console.log('items = ' + items.length);
            //console.log('splitting into batches, n = ' + n);
            var batch = Math.floor(items.length / n);
            //console.log('batch = ' + batch);
            var batches = [];
            var tail = items;
            var head = [];
            //allocate n-1 batches and the remainder goes in the last batch
            for (var x = 0; x < n - 1; x++) {
                head = _.first(tail, batch);
                tail = _.difference(tail, head);
                batches.push(head);
            }
            batches.push(tail);
            //console.log(batches);

            return Q.all(_.map(batches, function (batch, batchId) {
                    return get_job_for_incident_acc(batch, batchId);
                }))
                .then(function (results) {
                    //console.log("\n\nall jobs for incidents=" + JSON.stringify(results));
                    //console.log('flatten the results');
                    return _.flatten(results);
                });
        }
    }

    function get_job_for_incident_acc(items, batchId) {
        //console.log('get_job_for_incident_acc');
        //console.log('creating array of promises');
        var dots = '.';
        var arrayOfPromises = _.map(items, function (item) {
            dots += '.';
            return get_job_for_incident_acc_function_promise(item);
        });
        //console.log(dots);

        //convert the array of promises into a sequence of thens...
        var x = 0;
        return arrayOfPromises.reduce(function (soFar, f) {
            return soFar.then(function (f) {
                //output something on progress!
                x++;
                if (((items.length > 10) && (x % Math.floor(items.length / 10) === 0)) || (x === items.length)) {
                    //console.log("batch(" + batchId + ") progress: " + x + " out of " + items.length + " (" + ((x * 100) / items.length).toFixed(0) + "%)");
                }
                return f;
            }).then(f);
        }, Q([]));
    }

    function get_job_for_incident_acc_function_promise(incident) {
        //console.log('get_job_for_incident_acc_function_promise');
        return function (resultsSoFar) {
            return get_job_for_incident_acc_promise(resultsSoFar, incident);
        };
    }

    function get_job_for_incident_acc_promise(resultsSoFar, incident) {
        return get_job_no_for_incident_promise(incident)
            .then(function (results) {
                //console.log('push the results onto results so far');
                resultsSoFar.push(results);
                return resultsSoFar;
            });
    }

    function get_job_no_for_incident_promise(incident) {
        return Q.Promise(function (resolve, reject, notify) {

                //                var options = {
                //                    //protocol: 'https',
                //                    hostname: 'smartcore.jhc.co.uk',
                //                    //hostname: 'www.showmemyip.com',
                //                    port: 443, //80,
                //                    path: "/REST/JHCLive/GetJobForINC?INC=" + incident,
                //                    //path: '',
                //                    method: 'POST',
                //                    //auth: userAndToken,
                //                    agent: false,
                //                    timeout: 300 * 1000
                //                };
                //                var allOfBody = '';
                //                console.log("about to send request:" + options.hostname + ":" + options.port + options.path);
                //                var req = http.request(options, function (res) {
                //                    console.log("handle response");
                //                    if (DEBUG) console.log('STATUS: ' + res.statusCode);
                //                    if (res.statusCode != 200) {
                //                        reject(new Error("Status code was " + res.statusCode));
                //                    }
                //                    res.setEncoding('utf8');
                //                    res.on('data', function (chunk) {
                //                        allOfBody = allOfBody + chunk;
                //                    });
                //                    res.on('end', function () {
                //                        if (DEBUG) console.log('--fetch for incident..complete(' + options.path + ').');
                //                        console.log("all of body=" + allOfBody);
                //                        var allOfBodyJson;
                //                        try {
                //                            allOfBodyJson = JSON.parse(allOfBody);
                //                        } catch (e) {
                //                            if (DEBUG) console.error('error reading json for ' + incident + "(error:" + e + ")");
                //                        }
                //                        //if there was a 404 or some error in the JSON then we need to resolve with {}
                //                        resolve({});
                //                    });
                //                    res.on('error', function (e) {
                //                        console.log('result error.:' + e);
                //                        reject(e);
                //                    });
                //                });
                //                req.on('error', function (e) {
                //                    console.log('received an error fetching (incident:' + incident + '):' + e);
                //                    reject(e);
                //                });
                //                req.setTimeout(300 * 1000); //node default is 2 minutes
                //                // write data to request body
                //                req.write(' ');
                //                req.end();

                //get ALL closed reviews
                //var req = 
                client.get("https://smartcore.jhc.co.uk/REST/JHCLive/GetJobForINC?INC=" + incident, {
                            requestConfig: {
                                timeout: 300 * 1000, //request timeout in milliseconds 
                                noDelay: true, //Enable/disable the Nagle algorithm 
                                keepAlive: true, //Enable/disable keep-alive functionalityidle socket. 
                                keepAliveDelay: 1000 //and optionally set the initial delay before the first keepalive probe is sent 
                            },
                            responseConfig: {
                                timeout: 300 * 1000 //response timeout 
                            }
                        },
                        function (data, response) {
                            //data is the parsed response body
                            //response is the raw response data
                            setTimeout(function () {
                                //console.log("data=" + JSON.stringify(data));
                                //console.log("response=" + JSON.stringify(response));
                                //give the rest server a slight breather before allowing it to move on to the call
                                resolve(data);
                            }, 30);
                        })
                    .on('requestTimeout', function (req) {
                        console.log('request has expired');
                        req.abort();
                        reject('request has expired');
                    })
                    .on('responseTimeout', function (res) {
                        console.log('response has expired');
                        reject('request has expired');
                    })
                    .on('error', function (err) {
                        //it's usefull to handle request errors to avoid, for example, socket hang up errors on request timeouts 
                        console.log('request error', err);
                        reject(err);
                    });

            })
            .then(function (result) {
                //console.log("result = " + result);
                //pull out the job number (or Not Found or No Job, which are both useful to know)
                var pattern = new RegExp(/r.*<Job>(.*)<\/Job>/g);
                var match = pattern.exec(result);
                //console.log("match = " + match);
                var results = {};
                if (match) {
                    results = {
                        incident: incident,
                        job: match[1]
                    };
                } else {
                    results = {
                        incident: incident
                    };
                }
                incidentsWithJobsGlobal.push(result);
                return results;
            });
    }
};