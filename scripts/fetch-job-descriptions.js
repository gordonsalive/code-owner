/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 27/4/2017
// Module to fetch the job description for a given job number (from iSeries)
//**********

var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");

var pool = require('./shared-pool')();

var DEBUG = 0; //off

//var update_status = require('./update-status');
//var read_json = require('./read-json');
//var promisify = require('./promisify');
var pool_prommise = require('./pool-promise');

module.exports = function (jobs) {
    if (DEBUG) console.log('in fetch-job-description.js');

    var fetch_description_for_job = function (job) {
        var sql =
            " SELECT descrq AS description " +
            " FROM jhcjutil.jobs3 " +
            " WHERE codex = " + job;

        return pool.query(sql)
            .then(function (rows) {
                //have data in the form
                // [ { "DESCRIPTION": "Testing:Client Reporting-Valuation Creation" } ]
                if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));

                if ((rows) && (rows.length > 0) && (rows[0].DESCRIPTION)) {
                    return {
                        job: job,
                        description: rows[0].DESCRIPTION
                    };
                } else {
                    return {
                        job: job,
                        description: ""
                    };
                }

            });
    };

    return pool_prommise(jobs, fetch_description_for_job, 10);
};