/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 9/8/2016
// Module to update test history in QGPL.ROLREFTST
//**********

//**********
// TODO: The latest results need to 'cleaned': where results are missing the most recent results should be put in their place
//       as long as there are results that a newer than a day old for that item
//       ...This might be as easy as removing empty results from the updates list, if a result exists for item in list and that item is not aged...
//          ...but in this case, how do I determine if an item is aged?  Need to attache date of item when selecting and keep it for this comparison
// How about this:
// where results are 0 and I want to update them, strip them out and add a marker in a local file with view, sub-item and date/time...
//...then next run if I find the same results are still zero I can see if date & time stamp is more than a day old, and if  
// so then don't remove them but leave them in to be updated (and remove the marker from my local file)
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});
var pool = require('./shared-pool')();

var DEBUG = 0; //0=off

var fetch_recent_results = require('./fetch-recent-results');
var update_status = require('./update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');

// AAG 11/7/'16
// method to asynchronously update the test history on the database
// TODO: need some tests just for this module
module.exports = function (cukeResults, cukesFile) {
    //console.log('does recent-results.json exist?');
    // (i) load the most recent results - if they are missing fetch them from the database and then load them
    return Qfs.exists('recent-results.json')
        .then(function (recentResultsFileExists) {
            if (DEBUG) console.log('load recent-results.json');
            // does the recent results file exist, if so load it
            var result = null;
            if (recentResultsFileExists) {
                //load it
                result = read_json('recent-results.json');
            }
        })
        .then(function (recentResults) {
            if (DEBUG) console.log('get the most recent result date and time');
            return pool.query("SELECT MAX(time) as time, MAX(date) as date " +
                    " FROM qgpl.rolreftst " +
                    " WHERE date = (SELECT MAX(date) FROM qgpl.rolreftst)")
                .then(function (rows) {
                    var result = null;
                    //have data in the form 
                    // [ { "TIME": "-", 
                    //     "DATE": "-" ]
                    if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                    if ((rows) && (rows.length > 0) && (rows[0].DATE)) {
                        result = {
                            date: rows[0].DATE,
                            time: rows[0].TIME
                        };
                        //console.log('most recent result date and time = ' + JSON.stringify(result));
                    }
                    return result;
                })
                .then(function (latestResult) {
                    //combine the results with the date & time of the most recent result on the database
                    return {
                        recentResults: recentResults,
                        latestResult: latestResult
                    };
                });
        })
        .then(function (recentResultsObj) {
            if (DEBUG) console.log('about to load the recent results, obj = ' + JSON.stringify(recentResultsObj));
            var recentResults = recentResultsObj.recentResults;
            var latestDate = recentResultsObj.date;
            var latestTime = recentResultsObj.time;
            if ((recentResults) && (recentResults.latestResult.date === latestDate) && (recentResults.latestResult.time)) {
                // we have results and they are up-to-date! So load them!
                if (DEBUG) console.log('load recentResults');
                return recentResults;
            } else {
                // we don't have results or they are out of date, so refetch them...
                if (DEBUG) console.log('fetch the recent results');
                return fetch_recent_results(cukesFile);
            }
        })
        // (ii) load the latest test results (pass/fail counts)
        .then(function (recentResults) {
            if (DEBUG) {
                console.log('\n-back from fetch_recent_results');
                //console.log(JSON.stringify(recentResults, null, '  '));
            }
            return read_json(cukeResults)
                .then(function (cukeResultsJson) {
                    return {
                        recentResult: recentResults.latestResult,
                        cukeResults: cukeResultsJson
                    };
                });
        })
        // (iii) for each view (with flat as a special kind of view)
        .then(function (recentAndLatestResults) {
            //grab now for date and time
            var d = new Date();
            var datestring = d.getFullYear() +
                ("0" + (d.getMonth() + 1)).slice(-2) +
                ("0" + d.getDate()).slice(-2);
            var timestring = ("0" + d.getHours()).slice(-2) +
                ("0" + d.getMinutes()).slice(-2) +
                ("0" + d.getSeconds()).slice(-2);

            var recentResult = recentAndLatestResults.recentResult.views;
            var recentRuns = recentAndLatestResults.recentResult.runs;
            var cukeResults = recentAndLatestResults.cukeResults;
            //before we write out the view 'run' rows, calculate totals for the whole view to write out with the row
            //** TODO - this is what's going wrong! The totals are being calculated using only the cuke results,
            //**but if a job is running, it has no results (0's), and these aren't being counted in creating the total,
            //**whereas, it should be falling back to the result from the previous non-zero run
            //** so just need to pass in the other one and do the substituion in calculateFlatTotals() and calculateTreeTotals()
            var totals = {};
            //console.log('\n--cukeResults.flat=' + JSON.stringify(cukeResults.flat, null, '') + '\n');
            //console.log('\n--recentResult.flat=' + JSON.stringify(recentResult.flat, null, '') + '\n');
            totals.flat = calculateFlatTotals(cukeResults.flat, recentResult.flat);
            totals.tree = {};
            _.each(cukeResults.tree, function (cukeTreeViewItem, cukeTreeViewName) {
                //console.log('\n--cukeResults.tree(' + cukeTreeViewName + ')=\n' + JSON.stringify(cukeTreeViewItem, null, '') + '\n');
                //console.log('\n--recentResult.tree(' + cukeTreeViewName + ')=\n' + JSON.stringify(recentResult.tree[cukeTreeViewName], null, '') + '\n');
                totals.tree[cukeTreeViewName] = calculateTreeTotals(cukeTreeViewItem, recentResult.tree[cukeTreeViewName]);
            });
            //determine what our updates are going to be
            var updates = {};
            var changes = haveAnyItemsChanged(recentResult.flat, cukeResults.flat);
            if (changes !== {}) {
                updates.flat = changes;
            }
            updates.tree = {};
            _.each(cukeResults.tree, function (cukeTreeViewItem, cukeTreeViewName) {
                // (iv) have any of the items for this view changed?
                changes = haveAnyItemsChangedTreeStyle(recentResult.tree[cukeTreeViewName], cukeTreeViewItem);
                if ((changes !== {}) && (updates.tree)) {
                    updates.tree[cukeTreeViewName] = changes;
                }
            });
            if (DEBUG) console.log('\nupdates=' + JSON.stringify(updates, null, '  '));
            return promisify(updates)
                .then(function (updates) {
                    // (iv,a) save the changed items as new results rows, with flat as a special case
                    return Q.all(_.map(updates.flat, function (updateValue, updateItem) {
                            if (!(updateValue.delete)) {
                                return pool.query(
                                        " SELECT id FROM NEW TABLE ( " +
                                        " INSERT INTO qgpl.rolreftst ( " +
                                        " view, date, time, type, variable, item, value " +
                                        " ) VALUES ( " +
                                        " 'flat', " + datestring + ", " + timestring + ", 'flat', 'results', " +
                                        " '" + updateItem + "', '" + JSON.stringify(updateValue) + "' " +
                                        " ) " +
                                        " ) ")
                                    .then(function (rows) {
                                        var result = null;
                                        //have data in the form 
                                        // [ { "ID": "123" } ]
                                        if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                        if ((rows) && (rows.length > 0) && (rows[0].ID)) {
                                            result = {
                                                view: updateItem,
                                                id: parseInt(rows[0].ID.toString())
                                            };
                                        }
                                        return result;
                                    });
                            } else {
                                //console.log('skipping item (' + updateItem + '), because it has been deleted.');
                                return {};
                            }
                        }))
                        .then(function (newIDs) {
                            // (iv,b) save a new run row linking the new changed items with the unchanged items for the rest of the run
                            //newIDs is in the form [{view:~, id:123}, ...]
                            //we're aiming to insert a row with value=[ { "VALUE": "{results:{passed:12, failed:0, missing:0}, items:{itemName1:123, itemName2:124, itemName3:125}}" ]
                            //don't forget to pick up any items that need to be removed
                            //(a) remove any items that are in my newIDs list
                            var newItems = _.reduce(recentRuns.flat.items, function (memo, item, key) {
                                var found = _.find(newIDs, function (newID) {
                                    return (newID.view === key);
                                });
                                if (!found) {
                                    //add it to the new object
                                    memo[key] = item;
                                }
                                return memo;
                            }, {});
                            //(b) add in all the items from newIDs
                            _.each(newIDs, function (newID) {
                                newItems[newID.view] = newID.id;
                            });
                            //console.log('updates = ' + JSON.stringify(updates, null, ''));
                            //(c) finally remove any items that are marked for deletion in the updates
                            _.each(updates.flat, function (updateItem, updateKey) {
                                if ((updateItem.delete) && (newItems[updateKey])) {
                                    //console.log('deleting item');
                                    delete newItems[updateKey];
                                }
                            });
                            newItems = {
                                results: {},
                                items: newItems,
                            };
                            return newItems;
                        })
                        .then(function (newItems) {
                            if (JSON.stringify(updates.flat) !== '{}') {
                                newItems.results = totals.flat;
                                if (DEBUG) console.log('newItems:' + JSON.stringify(newItems));
                                return pool.insertList(
                                        'qgpl.rolreftst',
                                        'ID', [{
                                            VIEW: 'flat',
                                            DATE: parseInt(datestring),
                                            TIME: parseInt(timestring),
                                            TYPE: 'flat',
                                            VARIABLE: 'run',
                                            ITEM: 'flat',
                                            VALUE: JSON.stringify(newItems)
                                    }])
                                    .then(function (listOfGeneratedIds) {
                                        if (DEBUG) console.log('generated run id = ' + listOfGeneratedIds);
                                        return listOfGeneratedIds;
                                    });
                                //                            return pool.query(
                                //                                " SELECT id FROM NEW TABLE ( " +
                                //                                " INSERT INTO qgpl.rolreftst ( " +
                                //                                " view, date, time, type, variable, item, value " +
                                //                                " ) VALUES ( " +
                                //                                " 'flat', " + datestring + ", " + timestring + ", 'flat', 'run', " +
                                //                                " 'flat', '" + JSON.stringify(newItems) + "' " +
                                //                                " ) " +
                                //                                " ) ");
                            } else {
                                if (DEBUG) console.log('no changes for view (flat)');
                                return -1;
                            }
                        })
                        .then(function (flatResult) {
                            //NOW REPEAT THE ADDITION OF result AND run ROWS FOR EACH TREE VIEW
                            if (DEBUG) console.log('flatResult=' + flatResult);

                            //for each view
                            return Q.all(_.map(updates.tree, function (updateViewValue, updateViewKey) {
                                if (DEBUG) {
                                    console.log('update View Value(' + updateViewKey + ')');
                                    //console.log(JSON.stringify(updateViewValue));
                                }
                                //for each item in the view
                                return Q.all(
                                        _.map(updateViewValue, function (updateValue, updateKey) {
                                            if (DEBUG) {
                                                console.log('updateValue (' + updateKey + ')');
                                                console.log(JSON.stringify(updateValue));
                                            }
                                            if (!(updateValue.delete)) {
                                                return pool.query(
                                                        " SELECT id FROM NEW TABLE ( " +
                                                        " INSERT INTO qgpl.rolreftst ( " +
                                                        " view, date, time, type, variable, item, value " +
                                                        " ) VALUES ( " +
                                                        " '" + updateViewKey + "', " + datestring + ", " + timestring + ", " +
                                                        " 'tree', 'results', " +
                                                        " '" + updateKey + "', '" + JSON.stringify(updateValue) + "' " +
                                                        " ) " +
                                                        " ) ")
                                                    .then(function (rows) {
                                                        var result = null;
                                                        //have data in the form 
                                                        // [ { "ID": "123" ]
                                                        if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));
                                                        if ((rows) && (rows.length > 0) && (rows[0].ID)) {
                                                            result = {
                                                                view: updateKey,
                                                                id: parseInt(rows[0].ID.toString())
                                                            };
                                                        }
                                                        return result;
                                                    });
                                            } else {
                                                return promisify({});
                                            }
                                        }))
                                    .then(function (viewIds) {
                                        return {
                                            view: updateViewKey,
                                            ids: viewIds
                                        };
                                    });
                            }));
                        })
                        .then(function (newViewIDs) {
                            if (DEBUG) console.log('\n-new View IDs:' + JSON.stringify(newViewIDs));
                            //again we need to loop through each view
                            return Q.all(_.map(newViewIDs, function (viewIds) {
                                promisify(viewIds)
                                    .then(function (viewIds) {
                                        //(a) remove any items that are in my newIDs list
                                        var newItems = _.reduce(recentRuns.tree[viewIds.view].items, function (memo, item, key) {
                                            var found = _.find(viewIds, function (newID) {
                                                return (newID.view === key);
                                            });
                                            if (!found) {
                                                //add it to the new object
                                                memo[key] = item;
                                            }
                                            return memo;
                                        }, {});
                                        //(b) add in all the items from newIDs
                                        _.each(viewIds.ids, function (newID) {
                                            newItems[newID.view] = newID.id;
                                        });
                                        //(c) finally remove any items that are marked for deletion in the updates
                                        _.each(updates.tree[viewIds.view], function (updateItem, updateKey) {
                                            if ((updateItem.delete) && (newItems[updateKey])) {
                                                if (DEBUG) console.log('deleting item');
                                                delete newItems[updateKey];
                                            }
                                        });
                                        newItems = {
                                            results: {},
                                            items: newItems,
                                        };
                                        return newItems;
                                    })
                                    .then(function (newItems) {
                                        if (JSON.stringify(updates.tree[viewIds.view]) !== '{}') {
                                            newItems.results = totals.tree[viewIds.view];
                                            if (DEBUG) console.log('newItems:' + JSON.stringify(newItems));
                                            //**********
                                            //** TODO **
                                            //**********
                                            //** If nothing has changed, then we should only add a new run row each hour 
                                            //** (or we need to add it at all?)
                                            //**********
                                            return pool.insertList(
                                                    'qgpl.rolreftst',
                                                    'ID', [{
                                                        VIEW: viewIds.view,
                                                        DATE: parseInt(datestring),
                                                        TIME: parseInt(timestring),
                                                        TYPE: 'tree',
                                                        VARIABLE: 'run',
                                                        ITEM: viewIds.view,
                                                        VALUE: JSON.stringify(newItems)
                                                }])
                                                .then(function (listOfGeneratedIds) {
                                                    if (DEBUG) console.log('generated view run id = ' + listOfGeneratedIds);
                                                    return listOfGeneratedIds;
                                                });
                                        } else {
                                            if (DEBUG) console.log('no changes for view (' + viewIds.view + ')');
                                            return -1;
                                        }
                                    });
                            }));
                        })
                        .then(function (result) {
                            return updates;
                        });
                });
        })

    // (v) clean up: write out status and write out an updated recent-results.json
    .then(function (byModelLevelStartVariable) {
        return byModelLevelStartVariable;
        //write it out to the results file
        //return Qfs.write("recent-results.json", JSON.stringify(___, null, '\t'));
    });
    //.fail(function (err) {});--don't handle failure here, let it buble up to the calling promise handler
};

function haveAnyItemsChanged(recentResult, latestResult) {
    function stripDownResults(resultItem) {
        //console.log('strip down results');
        var result = {};
        result.passed = resultItem.passed;
        result.failed = resultItem.failed;
        result.missing = resultItem.missing;
        return result;
    }

    if (DEBUG) console.log('\n-have any items changed (or added/removed)');
    //spin through the latest results, if an item differs from the most recent stored results 
    //then return it in the list
    var result = _.reduce(latestResult, function (memo, item, key) {
        //don't capture changes for jobs that were running as results may be incomplete (just wait for that job to be not running)
        if (!item.isRunning) {
            //can we find the item, and does it have the different values for passed, failed and missing?
            //Note: don't capture a change from having results to having no results (as this is a sign of
            //a missing results file which occurs if the test job was running or a framework error occurred).
            if (!recentResult ||
                !(recentResult[key]) ||
                (((recentResult[key].passed !== item.passed) ||
                        (recentResult[key].failed !== item.failed) ||
                        (recentResult[key].missing !== item.missing)) &&
                    ((item.passed) ||
                        (item.failed) ||
                        (item.missing)))) {

                memo[key] = stripDownResults(item);

                memo[key] = item;
                if (memo[key].failedScenarios) {
                    memo[key].failedScenarios = [];
                }
            }
        }
        return memo;
    }, {});
    if (DEBUG) {
        //console.log('result:' + JSON.stringify(result));
        //console.log('latest result = ' + JSON.stringify(latestResult));
        //console.log('recent result = ' + JSON.stringify(recentResult));
    }
    //check to see if any items have been removed from the view!
    _.each(recentResult, function (findItem, findKey) {
        var found = _.find(latestResult, function (latestItem, latestKey) {
            return (findKey === latestKey);
        });
        if ((!found) && findKey) {
            //need to add this item as an item not to write back!
            //console.log('need to remove this key item:' + findKey);
            result[findKey] = {
                delete: true
            };
        }
    });
    return result;
}

function haveAnyItemsChangedTreeStyle(recentResult, latestResult) {
    function stripDownResults(resultItem) {
        //console.log('strip down results');
        var result = {};
        result.results = {};
        result.results.passed = resultItem.results.passed;
        result.results.failed = resultItem.results.failed;
        result.results.missing = resultItem.results.missing;
        if (resultItem["sub-items"]) {
            result["sub-items"] = _.mapObject(resultItem["sub-items"], function (subItem) {
                var subResult = {};
                subResult.results = {};
                subResult.results.passed = subItem.results.passed;
                subResult.results.failed = subItem.results.failed;
                subResult.results.missing = subItem.results.missing;
                return subResult;
            });
        }
        return result;
    }
    if (DEBUG) console.log('\n-have any items changed (or added/removed) (tree)');
    //spin through the latest results, if an item differs from the most recent stored results 
    //then return it in the list
    var result = _.reduce(latestResult, function (memo, item, key) {
        var hasItemChanged = false;
        //if job is still running, then ignore results as they may be partial (just have to wait until job is not running)
        if (!item.results.isRunning) {
            //can we find the item, and does it have the same values for passed, failed and missing?
            hasItemChanged = (!recentResult ||
                !(recentResult[key]) ||
                (((recentResult[key].results.passed !== item.results.passed) ||
                        (recentResult[key].results.failed !== item.results.failed) ||
                        (recentResult[key].results.missing !== item.results.missing)) &&
                    ((item.results.passed) ||
                        (item.results.failed) ||
                        (item.results.missing))));
        }
        var haveSubItemsChanged = hasItemChanged;
        //for tree style we need to look down through the sub-nodes too!
        if (item["sub-items"]) {
            haveSubItemsChanged = false;
            _.each(item["sub-items"], function (subItem, subItemName) {
                //if job is still running, then ignore results as they may be partial (just have to wait until job is not running)
                if (!subItem.results.isRunning) {
                    haveSubItemsChanged =
                        haveSubItemsChanged ||
                        (!recentResult) ||
                        (!recentResult[key]) ||
                        (!recentResult[key]["sub-items"]) ||
                        (!recentResult[key]["sub-items"][subItemName]) ||
                        (((recentResult[key]["sub-items"][subItemName].results.passed !== subItem.results.passed) ||
                                (recentResult[key]["sub-items"][subItemName].results.failed !== subItem.results.failed) ||
                                (recentResult[key]["sub-items"][subItemName].results.missing !== subItem.results.missing)) &&
                            ((subItem.results.passed) ||
                                (subItem.results.failed) ||
                                (subItem.results.missing)));
                }
            });
            hasItemChanged = hasItemChanged || haveSubItemsChanged;
        }
        if (hasItemChanged && haveSubItemsChanged) {
            memo[key] = stripDownResults(item);
        }
        return memo;
    }, {});
    //check to see if any items have been removed from the view!
    _.each(recentResult, function (findItem, findKey) {
        var found = _.find(latestResult, function (latestItem, latestKey) {
            return (findKey === latestKey);
        });
        if (!found) {
            //need to add this item as an item not to write back!
            //console.log('need to remove this key item:' + findKey);
            result[findKey] = {
                delete: true
            };
        }
    });
    return result;
}

function calculateFlatTotals(latestResult, recentResult) {
    if (DEBUG) {
        console.log('calculateFlatTotals.');
        //console.log(JSON.stringify(latestResult));
    }
    return _.reduce(latestResult, function (memo, item, key) {
        //console.log('key:' + JSON.stringify(key));
        if ((!item.isRunning) && ((item.passed) || (item.failed) || (item.missing))) {
            memo.passed += item.passed;
            memo.failed += item.failed;
            memo.missing += item.missing;
        } else if (recentResult[key]) {
            //if the job was running so the result might be partial, or if they are all zeros, then get the results from the last ones stored in the DB
            //console.log('recent vals for missing cuke vals:' + recentResult[key].passed + ', ' + recentResult[key].failed + ', ' + recentResult[key].missing);
            memo.passed += recentResult[key].passed;
            memo.failed += recentResult[key].failed;
            memo.missing += recentResult[key].missing;
        }
        return memo;
    }, {
        passed: 0,
        failed: 0,
        missing: 0
    });
}

function calculateTreeTotals(latestResult, recentResult) {
    if (DEBUG) {
        console.log('calculateTreeTotals.');
        //console.log(JSON.stringify(latestResult));
    }
    return _.reduce(latestResult, function (memo, item, key) {
        //console.log('key:' + JSON.stringify(key));
        if ((!item.results.isRunning) && ((item.results.passed) || (item.results.failed) || (item.results.missing))) {
            memo.passed += item.results.passed;
            memo.failed += item.results.failed;
            memo.missing += item.results.missing;
        } else if ((recentResult) && (recentResult[key]) && (recentResult[key].results)) {
            //if the job was running (and the results might be partial) or if they are all zeros then get the results from the last ones stored in the DB
            //console.log('\n\n\ngetting recent values for missing cuke values:' + 
            //recentResult[key].results.passed + ', ' + recentResult[key].results.failed + ', ' + recentResult[key].results.missing);
            memo.passed += recentResult[key].results.passed;
            memo.failed += recentResult[key].results.failed;
            memo.missing += recentResult[key].results.missing;
        }
        return memo;
    }, {
        passed: 0,
        failed: 0,
        missing: 0
    });
}

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'error');
}