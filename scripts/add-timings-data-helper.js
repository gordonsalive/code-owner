/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require('underscore');

// AAG 6/5/'16
// method to add a helpder to the code owner data, to make it easier to display with the ECT templates
module.exports = function (data) {
    //console.log('add-code-owner-data-helper');
    //add a helper to data
    data.helper = {};
    data.helper.getDailyAverageChartData = function (paramObejct) {
        //console.log('getDailyAverageChartData');

        function splitPopulateAndRefresh(dailyAverage) {
            //console.log('splitPopulateAndRefresh');
            return _.reduce(dailyAverage.layers, function (memo, layer, key) {
                if (key.includes('DatabaseRefresh')) {
                    memo.refresh.layers.push(key);
                    memo.refresh.total += layer / (60 * 1000);
                } else {
                    memo.populate.layers.push(key);
                    memo.populate.total += layer / (60 * 1000);
                }
                return memo;
            }, {
                refresh: {
                    layers: [],
                    total: 0
                },
                populate: {
                    layers: [],
                    total: 0
                }
            });
        }
        var dailyAverages = paramObejct.dailyAverages;
        var helper = paramObejct.helper;
        var layers = ['date', 'runTotal', 'populateAndRefresh', 'populateOnly', 'refreshOnly'];
        var chartData = _.map(dailyAverages, function (dailyAverage, key) {
            //console.log('each dailyAverage');
            result = {};
            result.date = key;
            result.runTotal = dailyAverage.run / (60 * 1000);
            result.populateAndRefresh = dailyAverage.population / (60 * 1000);

            var populateAndRefreshTotals = splitPopulateAndRefresh(dailyAverage);
            result.refreshOnly = populateAndRefreshTotals.refresh.total;
            result.populateOnly = populateAndRefreshTotals.populate.total;

            _.each(dailyAverage.layers, function (layer, key) {
                //console.log('each layer');
                //exclude refresh layers from the output - just the populate layers in detail
                if (_.contains(populateAndRefreshTotals.populate.layers, key)) {
                    result[key] = layer / (60 * 1000);
                    if (!(_.contains(layers, key))) {
                        layers.push(key);
                    }
                }
            });

            return result;
        }); //.reverse();

        //filter out outliers - i.e. if the number is less the half or more than twice the points to either side
        var removeOutliers = true;
        if (removeOutliers) {
            //console.log('remove outliers');
            //identify the outliers
            for (var x = 0;
                (x < chartData.length); x++) {
                //we allow the first points into the dataset regardless
                if ((x === 0) || (x === chartData.length - 1)) {
                    chartData[x].isOutlier = false;
                } else {
                    var thisRun = chartData[x].runTotal;
                    var nextRun = chartData[x + 1].runTotal;
                    var lastRun = chartData[x - 1].runTotal;
                    if ((thisRun > lastRun * 1.8) && (thisRun > nextRun * 1.8)) {
                        console.log('is an outlier ' + thisRun + ':' + lastRun + ':' + nextRun);
                        chartData[x].isOutlier = true;
                    } else if ((thisRun < lastRun / 1.8) && (thisRun < nextRun / 1.8)) {
                        chartData[x].isOutlier = true;
                    } else {
                        chartData[x].isOutlier = false;
                    }
                }
            }
            //filter out the outliers
            chartData = _.filter(chartData, function (date) {
                return !(date.isOutlier);
            });
            //remove the isOUtlier property before I start processing this dataset
            _.each(chartData, function (item) {
                delete item.isOutlier;
            });
        }

        //We want to return a string of the form:
        //          ['Year', 'Sales', 'Expenses'],
        //          ['2004',  1000,      400],
        //          ['2005',  1170,      460],
        //          ['2006',  660,       1120],
        //          ['2007',  1030,      540]
        //get column names
        var headings = layers;
        var result = "['" + headings.join("','") + "'],\n";
        result += _.map(chartData, function (row) {
            //each row is in fact an object, so map it back to an array for this:
            return "[" + _.map(headings, function (heading) {
                var value = row[heading];
                if (!value) {
                    value = 0;
                }
                if (isNaN(value)) {
                    value = "'" + value + "'";
                }
                return value;
            }).join(",") + "]";
        }).join(",\n");
        return result;
    };
    data.helper.getDailyAverageData = function (paramObejct) {
        var dailyAverages = paramObejct.dailyAverages;
        var helper = paramObejct.helper;
        //console.log("getDailyAverageData: " + JSON.stringify(dailyAverages, null, "  "));
        this.dailyAverages = _.map(dailyAverages, function (dailyAverage, key) {
            //console.log("map:" + key + ":" + JSON.stringify(dailyAverage, null, "  "));
            return {
                date: key,
                runs: dailyAverage.totalRuns,
                runTime: helper.getTimeString({
                    runTime: dailyAverage.run
                }, 3 * 60 * 60 * 1000),
                layers: helper.getTimeString(dailyAverage.layers, 5 * 60 * 1000),
                slowEntities: helper.getTimeString(dailyAverage.slowEntities, 30 * 1000)
            };
        }).reverse();
        //console.log("RETURN:" + JSON.stringify(this.dailyAverages, null, "  "));
        return this.dailyAverages;
    };
    data.helper.getTimeString = function (item, threshold) {
        //console.log("getTimeString:" + JSON.stringify(item));
        try {
            var times = _.mapObject(item, function (total, name) {
                var unit;
                var time;
                var myFormat = new Intl.NumberFormat('en-IN', {
                    maximumFractionDigits: 2
                });
                if (total < 1000) {
                    unit = 'milliseconds';
                    time = myFormat.format(total);
                } else if (total < 60 * 1000) {
                    unit = 'seconds';
                    time = myFormat.format(total / 1000);
                } else if (total < 60 * 60 * 1000) {
                    unit = 'minutes';
                    time = myFormat.format(total / (60 * 1000));
                } else {
                    unit = 'hours';
                    time = myFormat.format(total / (60 * 60 * 1000));
                }
                var returnString = name + ':' + time + " " + unit;
                if (total > threshold)
                    return '<span style="color: Crimson">' + returnString + '</span>';
                else
                    return returnString;
            });
            return _.reduce(times, function (memo, item) {
                return memo + item + "<br>";
            }, "");
        } catch (error) {
            console.error(error);
        }
    };

    return data;
};