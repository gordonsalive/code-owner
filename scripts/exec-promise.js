/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Qfs = require("q-io/fs");
var Q = require("q");
var fs = require('fs');

var exec = require('child_process').exec;

var DEBUG = 0; //0=of

function puts(error, stdout, stderr) {
    console.log('in puts');
    console.log(stdout + stderr);
}

module.exports = function (command, silent, timeout) {
    var noisy = !silent;
    return Q.Promise(function (resolve, reject, notify) {
        function onComplete(err, stdout, stderr) {
            if (err) {
                console.log('FAILED:exec(' + command + '): failed.');
                console.error('err:' + err); // + '\nstderr:' + stderr);
                reject(err);
            } else {
                if (noisy) {
                    console.log('exec(' + command + '): succeeded.');
                    if (DEBUG) {
                        console.log(stdout);
                    }
                }
                resolve(stdout);
            }
        }
        if (DEBUG) {
            console.log('exec:' + command);
        }
        exec(command, onComplete);
        if (timeout) {
            setTimeout(function () {
                reject('timeout:' + timeout + '\ncommand:' + command);
            }, timeout);
        } else {
            setTimeout(function () {
                reject('timeout');
            }, 5 * 60 * 1000); //wait 5 minutes max!
        }
    });
};