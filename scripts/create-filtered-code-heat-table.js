/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 19/12/2017
// Module to spin through code heat hierarchy data and filter it to just the ones we are interested in,
//   and in the process conert it to a flat structure and write it out as xml
// .defect-tracking.json looks like:
//     { "delphiOrdersTransactions": [ { "module": "FCI#CLIENT", "team": "WEA", "area": "FOE", "objectType": "*PGM", "sourceType": "RPG" }, ...
// .hierarchical code heat looks like:
//     { "name": "root", "children": [
//		  { "name": "CON", "children": [
//			 { "name": "CON", "children": [
//				{ "name": "*PGM", "children": [
//				   { "name": "RPG", "children": [
// 					  { "name": "ERNIE.01",
//						"children": [],
//						"size": "19637",
//						"weight": 0.18181818181818182,
//						"defects": [ { "job": "248872",
//										"incident": 1000987,
//										"description": "Contract portfolio currency to base currency exchange rate incorrect",
//										"foundInVersion": "",
//										"dateRequested": "20160429" }, ...
//**********

var fs = require('fs');
//var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");
var js2xmlparser = require("js2xmlparser");

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');

module.exports = function (runName) {
    console.log('create-filtered-code-heat-table(' + runName + ')');

    return promisify.spread(
        [read_json("json/" + runName + "-rpg-code-base-detailed.json"),
         read_json("json/defect-tracking.json")],
            function (rpg_code_base_detailed, defect_tracking) {
                //console.log("defect_tracking=" + JSON.stringify(defect_tracking));
                //..for each test project
                return _.mapObject(defect_tracking, function (testProjectModules) {
                    //console.log("testProjectModules=" + JSON.stringify(testProjectModules));
                    //..spin through code heat hierarchy, flattening it and filtering it
                    var filteredFiles = [];
                    _.each(rpg_code_base_detailed.children, function (team) {
                        //console.log(team.name);
                        _.each(team.children, function (area) {
                            //console.log(team.name + '-' + area.name);
                            _.each(area.children, function (objectType) {
                                //console.log(objectType.name);
                                _.each(objectType.children, function (sourceType) {
                                    //console.log(team.name + '-' + area.name + '-' + sourceType.name);
                                    _.each(sourceType.children, function (module) {
                                        //console.log(team.name + '-' + area.name + '-' + sourceType.name + '-' + module.name);
                                        //console.log(module.name + ',' + team.name + ',' + area.name + ',' + objectType.name + ',' + sourceType.name); // + '=' + JSON.stringify(testProjectModules));
                                        var isMatchingModule = _.findWhere(
                                            testProjectModules, {
                                                module: module.name,
                                                team: team.name,
                                                area: area.name,
                                                objectType: objectType.name,
                                                sourceType: sourceType.name
                                            }
                                        );
                                        if (isMatchingModule) {
                                            console.log("match=" + module.name + ',' + team.name + ',' + area.name + ',' + objectType.name + ',' + sourceType.name); // + '=' + JSON.stringify(testProjectModules));
                                            console.log('module.defects=' + module.defects);
                                            filteredFiles.push({
                                                module: module.name,
                                                team: team.name,
                                                area: area.name,
                                                objectType: objectType.name,
                                                sourceType: sourceType.name,
                                                defects: module.defects,//!!this might not be safe, might need to copy array properly??
                                                count: module.count
                                            });
                                        }
                                    });
                                });
                            });
                        });
                    });
                    return filteredFiles;
                });
            })
        .then(function (filteredDefectsByProject) {
            //for each of the projects write out a json file with the filtered list (and defects)
            _.each(filteredDefectsByProject, function (testProjectModules, testProjectName) {
                console.log(testProjectName);
                fs.writeFileSync("json/" + testProjectName + "-filtered-defects.json", JSON.stringify(testProjectModules, null, '\t'));
            });
            return filteredDefectsByProject;
        })
        .then(function (filteredDefectsByProject) {
            //for each of the projects, convert the results into xml and write them out
            _.each(filteredDefectsByProject, function (testProjectModules, testProjectName) {
                console.log(testProjectName);
                fs.writeFileSync("json/" + testProjectName + "-filtered-defects.xml", js2xmlparser.parse("filteredDefects", testProjectModules));
            });
            return filteredDefectsByProject;
        });
};