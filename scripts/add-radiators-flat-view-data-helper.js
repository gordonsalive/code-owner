/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require('underscore');

//var get_status = require('./scripts/get-status');
var get_status = require('./get-status');

// AAG 6/5/'16
// method to add a helpder to the flat radiator view data, to make it easier to display with the ECT templates
module.exports = function (data, params) {
    //console.log('addRadiatorsFlatViewDataHelper');
    //console.log(data);
    //console.log(params);
    //add a helper to data
    data.helper = {};
    data.helper.params = params;

    data.helper.status = get_status();
    //console.log(data.helper.status);

    data.helper.getStatusLine = function (view, helper) {
        function isStale(helper, now) {
            //return true;
            var lastSuccessfulFetchDate = Date.parse(helper.status.lastSuccessfullConnect);
            var lastAttemptedFetchDate = Date.parse(helper.status.lastConnectionAttempt);
            var lastTotalsUpdateDate = Date.parse(helper.status.lastTotalsUpdate);
            //are any of these more than 30 minutes out of date?
            return ((now - lastSuccessfulFetchDate) > (1000 * 60 * 30)) ||
                ((now - lastAttemptedFetchDate) > (1000 * 60 * 30)) ||
                ((now - lastTotalsUpdateDate) > (1000 * 60 * 30));
        }
        var d = new Date();
        var stale = isStale(this, d);
        var result = '<span';
        if (stale) result += ' style="color:red"';
        result += '">';
        result += d.toLocaleString() + '&nbsp;&nbsp;-&nbsp;&nbsp;';
        //TODO: format these dates nicely.
        var lastSuccessfullConnect = new Date(Date.parse(this.status.lastSuccessfullConnect));
        var lastConnectionAttempt = new Date(Date.parse(this.status.lastConnectionAttempt));
        var lastSuccessfulTotals = new Date(Date.parse(this.status.lastTotalsUpdate));
        result += '(View: ' + this.status.connectionStatus +
            ' @ ' + lastSuccessfullConnect.toLocaleString() +
            ', last attempt: ' + lastConnectionAttempt.toLocaleString() +
            '; Totals:' + lastSuccessfulTotals.toLocaleString() + ')';
        result += '</span>';
        return result;
    };
    data.helper.setTypeFilter = function (filterObj) {
        var passedOrFailed = filterObj.passedOrFailed;
        var jobs = filterObj.jobs;
        var helper = filterObj.helper;
        //console.log('setTypeFilter: ' + passedOrFailed);
        //console.log(jobs);
        if (passedOrFailed) {
            this.filteredList = jobs.filter(function (job) {
                return (helper.passedFailedFromColour(job.color) === passedOrFailed);
            });
        } else {
            this.filteredList = jobs;
        }
    };
    data.helper.passedFailedFromColour = function (colour) {
        //console.log('passedFailedFromColour..');
        //starts with blue...or not.
        if (colour.lastIndexOf('blue') === 0) {
            //console.log('..passed');
            return 'passed';
        } else {
            //console.log('..failed');
            return 'failed';
        }
    };
    data.helper.getCircleHtml = function (itemObj) {
        //console.log('getCircleHtml');
        var job = itemObj.job;
        var helper = itemObj.helper;

        var radius = helper.getCircleRadius();

        var result = '<td width="' + radius + '" height="' + radius + '" background="' + helper.getCircleFile(job) + '" valign="middle" align="center"';
        if ((helper.params.xmas) && (helper.params.type == 'failed')) {
            //result += ' style="color:Teal; font-weight:bold; background-size: ' + radius + 'px ' + radius + 'px;
            //            background-repeat:no-repeat; vertical-align:middle; text-align:center; line-spacing: 1; line-height:1.0">';
            result += ' class="circle" style="color:Teal; background-size: ' + radius + 'px ' + radius + 'px">';
        } else {
            result += ' class="circle" style="background-size: ' + radius + 'px ' + radius + 'px">';
        }
        //if (helper.total) {
        if (job.results && job.results.passed) {
            if (helper.jobKind(job) === 'passed') {
                result += job.results.passed;
            } else {
                result += '<u>' + job.results.failed + '</u><br>' + (job.results.failed + job.results.passed);
            }
        }
        //draw job name
        result += '</td>';
        return result;
    };
    data.helper.jobKind = function (job) {
        return this.passedFailedFromColour(job.color);
    };
    data.helper.getJobHtml = function (itemObj) {
        //console.log('getJobHtml');
        var job = itemObj.job;
        var helper = itemObj.helper;

        var fontSize = helper.getFontSize();
        var smallFontSize = helper.getSmallFontSize(helper.orientation, fontSize);

        //var result = '<td valign="middle" style="vertical-align:middle; font-size:' + fontSize + 'px; line-spacing: 1; line-height:1.1">';
        var result = '<td valign="middle" class="job-line" style="font-size:' + fontSize + 'px">';
        result += job.name;
        if ((helper.jobKind(job) === 'failed') && (helper.params.orientation === 'landscape')) {
            result += '<br><div style="font-size:' + smallFontSize + 'px">';
            result += 'Last successful build: ' + (job.lastSuccessfulBuild ? job.lastSuccessfulBuild.id : 'None!') + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            result += helper.claimedBy(job);
            result += '</div>';
        }
        result += '</td>';
        return result;
    };
    data.helper.noOfJobs = function (item) {
        //console.log('noOfJobs');
        if (item) {
            return item.jobs.length;
        } else {
            return this.filteredList.length;
        }
    };
    data.helper.getFontSize = function () {
        //console.log('getFontSize');
        if (this.params.orientation == 'landscape') {
            switch (this.scaling(this.noOfJobs())) {
            case 'large':
                return 48;
            case 'medium':
                return 32;
            case 'small':
                return 24;
                //assume very small
            default:
                return 20;
            }
        } else {
            return 32;
        }
    };
    data.helper.getSmallFontSize = function (orientation, fontSize) {
        //console.log('getSmallFontSize');
        if (orientation == 'landscape') {
            return fontSize / 4;
        } else {
            return fontSize / 5;
        }
    };
    data.helper.getCircleRadius = function () {
        //console.log('getCircleRadius');
        if (this.params.orientation == 'landscape') {
            switch (this.scaling(this.noOfJobs())) {
            case 'large':
                return 60;
            case 'medium':
                return 40;
            case 'small':
                return 30;
                //assume very small
            default:
                return 24;
            }
        } else {
            return 40;
        }
    };
    data.helper.getCircleFile = function (job) {
        //console.log('getCircleFile');
        var circleFile = '';
        if (this.params.xmas) {
            circleFile = 'img/' + this.circleColour(job) + '-xmas';
        } else {
            circleFile = 'img/' + this.circleColour(job) + '-circle';
        }
        if ((job.color.indexOf('anime') > -1)) {
            circleFile = circleFile + '-anime.gif';
        } else {
            circleFile = circleFile + '.png';
        }
        //console.log('..' + circleFile);
        return circleFile;
    };
    data.helper.circleColour = function (job) {
        //console.log('circleColour');
        if (this.passedFailedFromColour(job.color) === 'passed') {
            return 'green';
        } else {
            return 'red';
        }
    };
    data.helper.claimedBy = function (job) {
        //console.log('claimedBy');
        var result = '';
        if ((job) && (job.lastUnsuccessfulBuild) && (job.lastUnsuccessfulBuild.actions)) {
            var claimedByList = job.lastUnsuccessfulBuild.actions.filter(function (action) {
                return (action.hasOwnProperty('claimedBy'));
            });
            var claimedBy = null;
            if (claimedByList.length > 0) {
                claimedBy = claimedByList[0].claimedBy;
            }
            result = 'Claimed by: ';
            if (claimedBy === null) {
                result += 'Not claimed yet!';
            } else {
                result += claimedBy;
            }
        }
        return result;
    };
    data.helper.scaling = function (count) {
        //console.log('scaling');
        if (count <= 8) {
            return 'large';
        } else if (count <= 12) {
            return 'medium';
        } else if (count <= 16) {
            return 'small';
        } else {
            return 'very-small';
        }
    };
    //console.log('..finished adding helpers');

    return data;
};