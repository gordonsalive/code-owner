/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 16/6/2017
// Module to fetch all svn changes for a range of incidents, broken out by tech and repo
// (1) load repos.json which holds repos by tech - there's special handling for certain repos
// (2) Use svn log comments to associate job/incident numbers with specific revisions
// (3) use svn log to get changes for those revisions
// (4) add these changes into incident-changes-results.json, according to technology
// Notes, use --xml to get something easier to graple?
//**********

//var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
//var read_json = require('./read-json');
//var pool_prommise = require('./pool-promise');
var read_json = require('./read-json');
//var promisify = require('./promisify');
var spawn_promise = require('./spawn-promise');

module.exports = function (incidents) {


};