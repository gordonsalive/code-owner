/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");

var readJson = require("./read-json");
var promisify = require("./promisify");

// AAG 22/3/'16
// method to return a promise to get db populate timings data
module.exports = function (timingsJson) {

    var timings;

    if (timingsJson) {
        //they've either passed in the location of the json, or the object itself
        if (typeof (timingsJson) === "string") {
            timings = readJson(timingsJson);
        } else {
            timings = promisify(timingsJson);
        }
    } else {
        timings = readJson("./timings-results.json");
    }

    return timings;
};