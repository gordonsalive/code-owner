/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, esversion: 6*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 7/4/'17
// This script sends emails via to local (localhost) SMTP server
// (1) 
// (2) 

// TODO:
// * ..
var nodemailer = require('nodemailer');
var _ = require("underscore");

var DEBUG = 0; //0=of

var promisify = require('./promisify');

module.exports = function (options) {
    return promisify.method(function () {
        console.log('emailer');
        if (!options) {
            throw "emailer: must pass in options";
        }

        console.log('set up poolConfig');
        //gmail again:
        //        var poolConfig = {
        //            pool: true,
        //            host: 'smtp.gmail.com',
        //            port: 465,
        //            secure: true, // use TLS
        //            auth: {
        //                user: 'gordonsalive@gmail.com',
        //                pass: 'password'
        //            }
        //        };
        var poolConfig = {
            pool: true,
            port: 25 //3000
        };

        console.log('create transporter');
        // create reusable transporter object using the default SMTP transport
        //        var transporter = nodemailer.createTransport({
        //            service: 'gmail',
        //            auth: {
        //                user: 'gmail.user@gmail.com',
        //                pass: 'yourpass'
        //            }
        //        });
        var transporter = nodemailer.createTransport(poolConfig);

        console.log('verify the transporter');
        // verify connection configuration
        transporter.verify(function (error, success) {
            if (error) {
                console.log(error);
            } else {
                console.log('Server is ready to take our messages');
            }
        });

        console.log('set up mail options');
        // setup email data with unicode symbols
        var mailOptions = {
            from: '"Test Radiators 👻" <testradiators@jhc.financial>', // sender address
            to: 'alan.gordon@jhc.financial', // list of receivers
            subject: 'Hello ✔', // Subject line
            text: 'Hello world ?', // plain text body
            html: '<b>Hello world ?</b>' // html body
        };

        console.log('extend the options');
        mailOptions = _.extend(mailOptions, options);

        //console.log('about to send email, options=' + JSON.stringify(mailOptions));
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message %s sent: %s', info.messageId, info.response);
        });

        return true;
    });
};