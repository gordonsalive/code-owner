/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 17/5/2017
// Module to fetch shape and lines of code for the rpg code base
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var pool = require('./shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
//var read_json = require('./read-json');
//var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');

module.exports = function () {
    if (DEBUG) console.log('in fetch-rpg-code-base.js');

    var sql =
        " SELECT " +
        " ffobj AS OBJECT, ffobjd AS DESCRIPTION, ffteam AS TEAM, ffarea AS AREA, " +
        " ffobjt AS OBJECT_TYPE, mlseu AS SOURCE_TYPE, mlnrcd AS RECORD_COUNT " +
        " FROM fsvlib.figobj AS obj " +
        " INNER JOIN fsvlib.figfun AS fun " +
        "     ON obj.fffunc = fun.fffunc " +
        " INNER JOIN jctest.SOURCEMBRL AS s " +
        "     ON mlname = ffobj " +
        "     AND (ffobjt = '*CMD' AND mlseu = 'CMD' " +
        "     OR   ffobjt = '*FILE' AND mlseu IN ('DSPF', 'PRTF', 'PF', 'LF', 'ICFF', 'SQL') " +
        "     OR   ffobjt = '*PGM' AND mlseu IN ('CLE', 'CLLE', 'CLP', 'CPP', 'RPG', 'RPGL', 'SQLR')) " +
        //" WHERE ffmdlt <> 'Y' " +//fig func reg has a bug with lots of items marked as deleted incorrectly
        " ORDER BY mlnrcd DESC ";

    //console.log(sql);
    return pool.query(sql)
        .then(function (rows) {
            //have data in the form
            // [ { "OBJECT": "ERNIE.01",
            //     "DESCRIPTION": "CONT. Contract Entry  (first module)",
            //     "TEAM": "CON", ... } ]
            if (DEBUG); //console.log('Result of Query=' + JSON.stringify(rows, null, "  "));

            return rows;
        })
        .then(function (rows) {
            //group up the rows, first by team, then area, then object type, then source type
            var groupedByTeam = _.groupBy(rows, 'TEAM');
            var groupedByTeamAndArea = _.mapObject(groupedByTeam, function (team) {
                return _.groupBy(team, 'AREA');
            });
            var groupedByTeamAreaAndObjectType = _.mapObject(groupedByTeamAndArea, function (team) {
                return _.mapObject(team, function (area) {
                    return _.groupBy(area, 'OBJECT_TYPE');
                });
            });
            return _.mapObject(groupedByTeamAreaAndObjectType, function (team) {
                return _.mapObject(team, function (area) {
                    return _.mapObject(area, function (objectType) {
                        return _.groupBy(objectType, 'SOURCE_TYPE');
                    });
                });
            });
        })
        .then(function (groupedRows) {
            //now tidy the records into the format I actually want:
            return {
                name: 'root',
                children: _.map(groupedRows, function (team, teamName) {
                    return {
                        name: teamName,
                        children: _.map(team, function (area, areaName) {
                            return {
                                name: areaName,
                                children: _.map(area, function (objectType, objectTypeName) {
                                    return {
                                        name: objectTypeName,
                                        children: _.map(objectType, function (sourceType, sourceTypeName) {
                                            return {
                                                name: sourceTypeName,
                                                children: _.map(sourceType, function (sourceItem) {
                                                    return {
                                                        name: sourceItem.OBJECT,
                                                        children: [],
                                                        size: sourceItem.RECORD_COUNT,
                                                        weight: Math.random() * Math.random()
                                                    };
                                                })
                                            };
                                            // return {
                                            //     name: sourceTypeName,
                                            //     children: [],
                                            //     size: _.reduce(sourceType, function (memo, source) {
                                            //         return memo += parseInt(source.RECORD_COUNT);
                                            //     }, 0),
                                            //     weight: Math.random(),
                                            //     count: sourceType.length
                                            // };
                                        })
                                    };
                                })
                            };
                        })
                    };
                })
            };
        })
        .then(function (unweightedResults) {
            //find the largest
            return Qfs.write("json/rpg-code-base-unweighted.json", JSON.stringify(unweightedResults, null, '\t'))
                .then(function (writeResult) {
                    return unweightedResults;
                });
        });
};