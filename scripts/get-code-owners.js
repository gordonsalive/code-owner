/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");

var readJson = require("./read-json");
var promisify = require("./promisify");

// AAG 22/3/'16
// method to return a promise to get a tree of code owners
// (either the full tree or a particular branch)
module.exports = function (codeOwnersJson, item, type) {

    function findItem(item, treeNode, type) {
        var i;
        if ((treeNode[item]) && (treeNode[item].type === type)) {
            //if the item is here at this level then return it
            return treeNode[item];
        } else {
            //do we have any more sub nodes?
            if ((treeNode.items) && (treeNode.items.length > 0)) {
                var result = null;
                //cycle though the nodes at this level calling findItem
                for (i = 0; i < treeNode.items.length; i++) {
                    var nextItem = treeNode[treeNode.items[i]];
                    if (nextItem) {
                        result = findItem(item, nextItem, type);
                        if (result) {
                            return result;
                        }
                    } else {
                        throw 'item ' + treeNode.items[i] + ' is in items list in owners json, but property is missing from this node.';
                    }
                }
            } else {
                //did find anything down this branch
                return null;
            }
        }
    }

    function asynchFind(item, codeOwnersPromise, type) {
        //when the codeOwnersPromise resolves giving us the code owners object, 
        //then, find the given sub-node and return as a promise so it can be chained.
        return codeOwnersPromise.then(function (codeOwnersObj) {
            return Q.Promise(function (resolve, reject, notify) {
                try {
                    resolve(findItem(item, codeOwnersObj, type));
                } catch (err) {
                    reject(err);
                }
            });
        });
    }

    var codeOwners;

    if (codeOwnersJson) {
        //they've either passed in the location of the json, or the object itself
        if (typeof (codeOwnersJson) === "string") {
            codeOwners = readJson(codeOwnersJson);
        } else {
            codeOwners = promisify(codeOwnersJson);
        }
    } else {
        codeOwners = readJson("./code-owners.json");
    }

    if (item) {
        //then we need to find this item in the tree and return this item
        return asynchFind(item, codeOwners, type);
    } else {
        return codeOwners;
    }

};