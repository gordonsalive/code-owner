/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");
var Qfs = require("q-io/fs");

// AAG 8/4/'16
// method to return a promise to get a flat list of jenkins view test results
// list is for all views configured in cukes.json
// and already fetched into view-results.json
// all we have to do it ready it in and add the up to date status info
module.exports = function (viewName) {
    //console.log('get-radiators-flat-view-data: ' + viewName);
    return Qfs.read('json/view-results.json')
        .then(function (viewResultFile) {
            //console.log('about to return JSON from view-results.json');
            //console.log(viewResultFile);
            //console.log(JSON.parse(viewResultFile)[viewName]);
            return JSON.parse(viewResultFile)[viewName];
        });
    //.fail(); - don't handle failure here, let calling module handle it
};