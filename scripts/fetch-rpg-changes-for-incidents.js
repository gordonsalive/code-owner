/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 7/4/2017
// Module to fetch all rpg changes for a range of incidents
//**********

var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");

var pool = require('./shared-pool')();

//var pool = require('node-jt400').pool({
//    host: '172.26.27.15', //TRACEY//172.26.27.15
//    user: 'GOLDSTAR',
//    password: 'JHCJHC'
//});

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
//var read_json = require('./read-json');
var promisify = require('./promisify');
var pool_prommise = require('./pool-promise');

module.exports = function (incidents) {
    //incidents is an array of incident and job numbers, e.g [{incident: 1000280, job: 123456}, ...]
    //not all the incident record will have job numbers, so will need to skip these.
    if (DEBUG) console.log('in fetch-rpg-changes-for-incidents.js');

    var fetch_rpg_changes_for_incident = function (item) {
        //console.log("item=" + JSON.stringify(item));
        var job = item.job;
        var incident = item.incident;
        if (job === "No Job") {
            //console.log("incident=" + incident + ', no job');
            return promisify(item); //return it untouched
        } else {
            //console.log("incident=" + incident + ", job=" + job);
            var sql =
                " SELECT CAST(o.obirno AS CHAR(6) CCSID 37) AS ir_number, " +
                " CAST(o.obseqn AS CHAR(2) CCSID 37) AS cr_number, " +
                " CAST(i.iriapp AS CHAR(3) CCSID 37) AS application, " +
                " CAST(o.obobjn AS CHAR(10) CCSID 37) AS object_name, " +
                " CAST(o.obobjt AS CHAR(10) CCSID 37) AS object_type, " +
                " CAST(o.obobja AS CHAR(10) CCSID 37) AS object_attribute, " +
                " jobs3.descrq AS JOB_DESCRIPTION, " +
                " jobs3.rlsver AS FOUND_IN_VERSION, " +
                " jobs3.reques AS DATE_REQUESTED " +
                " FROM omsdta.xob AS o " +
                " INNER JOIN omsdta.xir AS i " +
                " ON i.irsysm = o.obsysm AND " +
                " i.irirno = o.obirno, " +
                " jhcjutil.jobs3 " +
                " WHERE i.irjnbr = '" + job + "' " +
                " AND o.obstat = '09' " +
                " AND jobs3.codex = i.irjnbr ";
            //console.log(sql);
            return pool.query(sql)
                .then(function (rows) {
                    var result = {};
                    //have data in the form
                    // [ { "IR_NUMBER": 064366,
                    //     "CR_NUMBER": 01,
                    //     "APPLICATION": "ECM", ... } ]
                    if (DEBUG) console.log('Result of Query=' + JSON.stringify(rows, null, "  "));

                    //return _.extend(item, {
                    //    rpgChanges: rows
                    //});
                    item.rpgChanges = rows;
                    return item;

                    //if ((rows) && (rows.length > 0) && (rows[0].DATE)) {
                    //    result = {
                    //        date: rows[0].DATE,
                    //        time: rows[0].TIME
                    //    };
                    //}
                });
        }
    };

    return pool_prommise(incidents, fetch_rpg_changes_for_incident, 10);
};