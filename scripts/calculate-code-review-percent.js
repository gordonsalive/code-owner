/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 28/3/'17
// This script calculates code review percents:
// (1) read in code changes
// (2) read in reviewed revisions
// (3) mark changes are reviewed
// (4) write out review-percent-results.json
// (5) 

// TODO:
// * 


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;
var Client = require('node-rest-client').Client;
var client = new Client();

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 0; //0=off

var read_json = require('./read-json');
var promisify = require('./promisify');
//var exec_promise = require('./scripts/exec-promise');

var resultsJson = {};

module.exports = function (repos) {
    console.log('..calculating review percentages');
    console.log('..read in changes, read in review results');

    return promisify.spread([
        read_json('json/changes-results.json'),
        read_json('json/review-results.json')
        ], function (changes, reviews) {
            //console.log(JSON.stringify(reviews));
            return {
                changes: changes,
                reviews: reviews //(split out my closed and open)
            };
        })
        .then(function (results) {
            console.log('..identifying which jobs have been reviewed (reviewed revisions)');
            //changes is broken out by year-month, then tech, 
            //  then jobItems has an array of objects with job, authors and revisions
            //within year-month, within tech, for each jobItems,
            //  are any of the revisions in the reviewed revisions list?
            //  if so write out object with job and isReviewed
            return _.mapObject(results.changes, function (tech) {
                return _.mapObject(tech, function (changes) {
                    return _.map(changes.jobItems, function (item) {
                        var isReviewedClosed = (_.intersection(item.revisions, results.reviews.lastYearsReviewedRevisions.closed.allRevisions).length > 0);
                        var isReviewedOpen = (_.intersection(item.revisions, results.reviews.lastYearsReviewedRevisions.open.allRevisions).length > 0);
                        var isReviewInterim = !isReviewedClosed && !isReviewedOpen && _.some(
                            results.reviews.lastYearsReviewedRevisions.closed.reviewsObj.concat(
                                results.reviews.lastYearsReviewedRevisions.open.reviewsObj
                            ),
                            function (reviewObj) {
                                return item.job === reviewObj.job;
                            }
                        );
                        var isReviewed = isReviewedClosed || isReviewedOpen || isReviewInterim;
                        return {
                            job: item.job,
                            description: item.description,
                            isReviewed: isReviewed,
                            isReviewedClosed: isReviewedClosed,
                            isReviewedOpen: isReviewedOpen,
                            isReviewInterim: isReviewInterim,
                            revisions: item.revisions,
                            authors: item.authors
                        };
                    });
                });
            });
        })
        .then(function (results) {
            //console.log(JSON.stringify(results));
            //now stick results in changes node and add another node with each tech and the percentage of jobs that are reviewed!
            return {
                changes: results,
                percentages: _.mapObject(results, function (tech) {
                    return _.mapObject(tech, function (jobItems) {
                        //console.log('jobItems=' + JSON.stringify(jobItems));
                        //console.log("where=" + JSON.stringify(_.where(jobItems, {
                        //    isReviewed: true
                        //})));
                        //console.log("jobsItems.length=" + jobItems.length);
                        var reviews = _.where(jobItems, {
                            isReviewed: true
                        });
                        //var reviews = _.filter(jobItems, function (jobItem) {
                        //    return jobItem.isReviewedClosed || jobItem.isReviewedOpen;
                        //});
                        return {
                            percent: ((100 * reviews.length) / jobItems.length),
                            noOfChanges: jobItems.length,
                            noOfReviews: reviews.length
                        };
                    });
                })
            };
        })
        .then(function (results) {
            //write out the results
            //console.log(JSON.stringify(results, null, '  '));
            return Qfs.write("json/review-percent-results.json", JSON.stringify(results, null, '\t'));
        });
};