/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Qfs = require("q-io/fs");
var Q = require("q");

// AAG 11/7/'16
// method to asynchronously read update the status of the latest fetch attempts
// TODO: need some tests just for this module
var myStatus = {
    connectionStatus: 'not connected',
    lastSuccessfullConnect: 'N/A',
    lastConnectionAttempt: 'N/A'
};
var error = {
    lastConnectionAttempt: 'N/A'
};
var timingsStatus = {
    lastConnectionAttemp: 'N/A',
    connectionStatus: 'N/A'
};
var failingTests = {
    lastConnectionAttempt: 'N/A'
};
var codeChanges = {
    lastConnectionAttempt: 'N/A'
};
var incidentChanges = {
    lastConnectionAttempt: 'N/A'
};
var schedule = {
    lastConnectionAttempt: 'N/A'
};


module.exports = function (connectionStatus, type) {
    var d = new Date();
    var localeDate = d.toISOString(); //toLocaleString();
    if (type === 'status') {
        //var localeDate = d.toLocaleString();
        if (connectionStatus == 'succeeded') {
            myStatus.lastSuccessfullConnect = localeDate; // localeDate;
            myStatus.connectionStatus = 'connected';
        } else {
            myStatus.connectionStatus = 'not connected';
        }
        //write out the data to status.json
        myStatus.lastConnectionAttempt = localeDate; // toLocaleString(); //localeDate;

        console.log(myStatus);

        return Qfs.write("json/status.json", JSON.stringify(myStatus, null, '\t'))
            .fail(console.error);
    } else if (type === 'error') {
        error.lastConnectionAttempt = localeDate;
        if (connectionStatus === 'succeeded') {
            error.connectionStatus = 'connected';
            console.log(error);
        } else {
            error.connectionStatus = 'not connected';
        }

        return Qfs.write("json/error.json", JSON.stringify(error))
            .fail(console.error);
    } else if (type === 'timings') {
        timingsStatus.connectionStatus = connectionStatus;
        timingsStatus.lastConnectionAttemp = localeDate;
        console.log(timingsStatus);
        return Qfs.write("json/timings-status.json", JSON.stringify(timingsStatus))
            .fail(console.error);
    } else if (type === 'failed-tests') {
        failingTests.lastConnectionAttemp = localeDate;
        if (connectionStatus === 'succeeded') {
            failingTests.connectionStatus = 'connected';
        } else {
            failingTests.connectionStatus = 'not connected';
        }
        console.log(failingTests);
        return Qfs.write("json/failing-tests-status.json", JSON.stringify(failingTests))
            .fail(console.error);
    } else if (type === 'code-changes') {
        codeChanges.lastConnectionAttemp = localeDate;
        if (connectionStatus === 'succeeded') {
            codeChanges.connectionStatus = 'connected';
        } else {
            codeChanges.connectionStatus = 'not connected';
        }
        console.log(codeChanges);
        return Qfs.write("json/code-changes-status.json", JSON.stringify(codeChanges))
            .fail(console.error);
    } else if (type === 'incident-changes') {
        incidentChanges.lastConnectionAttemp = localeDate;
        if (connectionStatus === 'succeeded') {
            incidentChanges.connectionStatus = 'connected';
        } else {
            incidentChanges.connectionStatus = 'not connected';
        }
        console.log(incidentChanges);
        return Qfs.write("json/incident-changes-status.json", JSON.stringify(incidentChanges))
            .fail(console.error);
    } else if (type === 'schedule') {
        schedule.lastConnectionAttemp = localeDate;
        if (connectionStatus === 'succeeded') {
            schedule.connectionStatus = 'connected';
        } else {
            schedule.connectionStatus = 'not connected';
        }
        console.log(schedule);
        return Qfs.write("json/schedule-status.json", JSON.stringify(schedule))
            .fail(console.error);
    }
};