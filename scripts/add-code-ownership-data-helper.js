/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require('underscore');

// AAG 6/5/'16
// method to add a helpder to the code owner data, to make it easier to display with the ECT templates
module.exports = function (data) {
    //console.log('add-code-owner-data-helper');
    //add a helper to data
    data.helper = {};
    data.helper.joinOwnersHelper = function (owners) {
        return _.map(owners, function (owner) {
            //console.log(owner);
            return owner.name;
        }).join(', ');
    };
    //the ones that take an item, coould return an object with all the bits and bobs I need instead of separate methods
    data.helper.ownerTypeHelper = function (item) {
        //console.log('ownerTypeHelper');
        //console.log(item);
        switch (item.type) {
        case 'specialisation':
            return 'Technical Specialist';
        case 'component':
            return 'Guardian';
        case 'sub-component':
            return 'Expert';
        case 'project':
            return 'Quality Owner';
        case 'project-area':
            return 'Deputy Quality Owner';
        default:
            return 'Item';
        }
    };
    data.helper.listTypeHelper = function (item) {
        //console.log('listTypeHelper');
        //console.log(item);
        switch (item.type) {
        case 'specialisation':
            return 'Specialisation';
        case 'component':
            return 'Components';
        case 'sub-component':
            return 'Sub-Component';
        case 'project':
            return 'Projects';
        case 'project-area':
            return 'Project Area';
        default:
            return 'View';
        }
    };
    return data;
};