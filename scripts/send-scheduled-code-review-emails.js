/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200, esversion: 6*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 7/4/'17
// This script sends out the weekly and monthly summary of code review stats
// (1) load sched_review_emails.json, with details of who to send to and when
// (2) do we need to send a weekly email@
//     ..if so, construct the body for a weekly email and then send it
// (3) do we need to send a month email@
//     ..if so, construct the body for a weekly email and then send it

// TODO:
// * ..
var _ = require("underscore");
var Qfs = require("q-io/fs");

var DEBUG = 0; //0=of

var read_json = require('./read-json');
var promisify = require('./promisify');
var emailer = require('./emailer');

module.exports = function () {
    var now = new Date();
    var sched_review_emails_json = 'json/sched-review-emails.json';
    var schedule_json = 'json/schedule.json';
    //return read_json('json/sched_review_emails.json')*************************
    return promisify.spread(
        [read_json(sched_review_emails_json), read_json(schedule_json)],
        function (sched_review_emails, schedule) {
            var monthStart = new Date(now.getFullYear(), now.getMonth(), 1);
            var lastMonthEnd = new Date(now.getFullYear(), now.getMonth(), 1);
            lastMonthEnd.setDate(0);
            var lastMonthStart = new Date(lastMonthEnd.getFullYear(), lastMonthEnd.getMonth(), 1);
            //allow month start date to be adjusted.
            var monthStartDelta = sched_review_emails.monthly.when.date - 1;
            monthStart.setDate(monthStart.getDate() + monthStartDelta);
            lastMonthEnd.setDate(lastMonthEnd.getDate() + monthStartDelta);
            lastMonthStart.setDate(lastMonthStart.getDate() + monthStartDelta);

            if (!schedule.codeReviewEmails) {
                schedule.codeReviewEmails = {};
            }
            if (!schedule.codeReviewEmails.weekly) {
                schedule.codeReviewEmails.weekly = {};
            }
            if (!schedule.codeReviewEmails.monthly) {
                schedule.codeReviewEmails.monthly = {};
            }

            return promisify.method(function () {
                    //do we need to do a weekly email?
                    var nextSend = schedule.codeReviewEmails.weekly.nextSend;
                    var nextSendDate;
                    if ((nextSend) && (nextSend !== "")) {
                        nextSendDate = new Date(nextSend);
                    }
                    //don't send emails around mid-night when things tend to fail, wait till 8am
                    var now = new Date();
                    var isDaytime = (now.getHours() >= 8);

                    console.log('weekly.nextSend:' + nextSend);
                    console.log('weekly.nextSendDate:' + nextSendDate);
                    //if we've never sent a mail or we have passed the next send date, then send email
                    return (isDaytime) && ((!nextSend) || (nextSend === "") || (nextSendDate < now));
                })
                .then(function (isSendWeekly) {
                    if (isSendWeekly) {
                        console.log('is send weekly');
                        //find the next send date 
                        //(if dow == 1 then add 7 days to today, if dow = 0 then add 1, if dow > 1 then add (7-dow) )
                        var whenDow = sched_review_emails.weekly.when.dow;
                        var dow = now.getDay();
                        var diff = (dow < whenDow) ? whenDow - dow : 8 - dow;
                        //var diff = (dow === 0) ? 1 : 8 - dow;
                        var nextSendDate = new Date();
                        nextSendDate.setDate(nextSendDate.getDate() + diff); //should wrap around months if it has to
                        //now setup the options for the emailer and construct email.
                        var weekly = sched_review_emails.weekly;
                        weekly.periodStart = monthStart.toDateString();
                        weekly.periodEnd = now.toDateString();

                        return promisify.spread(
                            [read_in_review_values(0), read_in_changes(0)],
                                function (reviewResults, changes) {
                                    console.log('set up email:');
                                    //console.log('----reviewResults:');
                                    //console.log(JSON.stringify(reviewResults));
                                    //console.log('----changes:');
                                    //console.log(JSON.stringify(changes));
                                    var mailOptions = {
                                        from: weekly.who.from, // sender address
                                        to: weekly.who.to, // list of receivers
                                        cc: weekly.who.cc, // list of receivers
                                        subject: weekly.subject, // Subject line
                                        text: constructEmailBody(weekly, reviewResults, changes, false), // plain text body
                                        html: constructEmailBody(weekly, reviewResults, changes, true) // html body
                                    };
                                    return emailer(mailOptions);
                                })
                            .then(function (result) {
                                //need to reload schedule at this point, as it might have been updated since this promise chain was created and we don't want to undo those changes when we save it
                                return read_json(schedule_json);
                            })
                            .then(function (schedule) {
                                //update the nextSend on this node and write it out
                                schedule.codeReviewEmails.weekly.nextSend = nextSendDate.toDateString();
                                schedule.codeReviewEmails.weekly.lastSent = now.toDateString();
                                schedule.codeReviewEmails.weekly.periodStart = weekly.periodStart;
                                schedule.codeReviewEmails.weekly.periodEnd = weekly.periodEnd;
                                return Qfs.write(schedule_json, JSON.stringify(schedule, null, '\t'));
                            });
                    }
                })
                .then(function (result) {
                    //do we need to do a weekly email?
                    var nextSend = schedule.codeReviewEmails.monthly.nextSend;
                    var nextSendDate;
                    if ((nextSend) && (nextSend !== "")) {
                        nextSendDate = new Date(nextSend);
                    }
                    //don't send emails around mid-night when things tend to fail, wait till 8am
                    var now = new Date();
                    var isDaytime = (now.getHours() >= 8);

                    console.log('monthly.nextSend:' + nextSend);
                    console.log('monthly.nextSendDate:' + nextSendDate);
                    //if we've never sent a mail or we have passed the next send date, then send email
                    return (isDaytime) && ((!nextSend) || (nextSend === "") || (nextSendDate < now));
                })
                .then(function (isSendMonthly) {
                    if (isSendMonthly) {
                        console.log('is send monthly');
                        //next send date is whatever last send date was plus 1 month
                        var nextSendDate = new Date();
                        nextSendDate.setDate(1);
                        nextSendDate.setMonth(now.getMonth() + 1); //should wrap around years if it has to
                        //now setup the options for the emailer and construct email.
                        var monthly = sched_review_emails.monthly;
                        monthly.periodStart = lastMonthStart.toDateString();
                        monthly.periodEnd = lastMonthEnd.toDateString();

                        return promisify.spread(
                            [read_in_review_values(-1), read_in_changes(-1)],
                                function (reviewResults, changes) {
                                    console.log('set up email:');
                                    //console.log('----reviewResults:');
                                    //console.log(JSON.stringify(reviewResults));
                                    //console.log('----changes:');
                                    //console.log(JSON.stringify(changes));
                                    var mailOptions = {
                                        from: monthly.who.from, // sender address
                                        to: monthly.who.to, // list of receivers
                                        cc: monthly.who.cc, // list of receivers
                                        subject: monthly.subject, // Subject line
                                        text: constructEmailBody(monthly, reviewResults, changes, false), // plain text body
                                        html: constructEmailBody(monthly, reviewResults, changes, true) // html body
                                    };
                                    return emailer(mailOptions);
                                })
                            .then(function (result) {
                                //need to reload schedule at this point, as it might have been updated since this promise chain was created and we don't want to undo those changes when we save it
                                return read_json(schedule_json);
                            })
                            .then(function (schedule) {
                                //update the nextSend on this node and write it out
                                schedule.codeReviewEmails.monthly.nextSend = nextSendDate.toDateString();
                                schedule.codeReviewEmails.monthly.lastSent = now.toDateString();
                                schedule.codeReviewEmails.monthly.periodStart = monthly.periodStart;
                                schedule.codeReviewEmails.monthly.periodEnd = monthly.periodEnd;

                                return Qfs.write(schedule_json, JSON.stringify(schedule, null, '\t'));
                            });
                    }
                });
        });

    function read_in_review_values(monthAdjust) {
        console.log('read_in_review_values');
        var currentYear = now.getFullYear();
        var currentMonth = now.getMonth() + 1 + monthAdjust;
        if (currentMonth === 0) {
            currentMonth = 12;
        }
        var currentYearMonth = currentYear.toString() + '-' + ('0' + currentMonth).slice(-2);
        return read_json('json/review-percent-results.json')
            .then(function (review_percent_results) {
                console.log('read_in_review_values..return percentages:' + currentYearMonth);
                //console.log(JSON.stringify(review_percent_results.percentages[currentYearMonth]));
                return review_percent_results.percentages[currentYearMonth]; //PERCENTAGES!
            });
    }

    function read_in_changes(monthAdjust) {
        console.log('read_in_changes');
        var currentYear = now.getFullYear();
        var currentMonth = now.getMonth() + 1 + monthAdjust;
        if (currentMonth === 0) {
            currentMonth = 12;
        }
        var currentYearMonth = currentYear.toString() + '-' + ('0' + currentMonth).slice(-2);
        return read_json('json/review-percent-results.json')
            .then(function (review_percent_results) {
                console.log('read_in_changes..return percentages:' + currentYearMonth);
                //console.log(JSON.stringify(review_percent_results.changes[currentYearMonth]));
                return review_percent_results.changes[currentYearMonth]; //CHANGES!
            });
    }

    function constructEmailBody(sched, reviewResults, changes, isHtml) {
        console.log('constructEmailBody');
        //console.log('weekly = ' + JSON.stringify(weekly));
        //console.log('reviewResults = ' + JSON.stringify(reviewResults));
        //console.log('changes = ' + JSON.stringify(changes));
        var body = isHtml ? sched.html.join('<br>\n') : sched.html.join('\n');

        var fullBody = isHtml ? getBodyStyle() : '';
        //take the body and replace the tags with the values from the review results
        fullBody += body
            .replace(/<group_name>/g, sched.who.group)
            .replace(/<period_start>/g, sched.periodStart)
            .replace(/<period_end>/g, sched.periodEnd)
            .replace(/<java_real_changes>/g, reviewResults.java ? reviewResults.java.noOfChanges : 'none')
            .replace(/<java_admin_changes>/g, '(TBA)')
            .replace(/<java_code_reviews>/g, reviewResults.java ? reviewResults.java.noOfReviews : 'none')
            .replace(/<java_code_review_percent>/g, (reviewResults.java && reviewResults.java.percent) ? reviewResults.java.percent.toFixed(2) : '0')
            //.replace(/<delphi_real_changes>/g, reviewResults.delphi ? reviewResults.delphi.noOfChanges : 'none')
            //.replace(/<delphi_admin_changes>/g, '(TBA)')
            //.replace(/<delphi_code_reviews>/g, reviewResults.delphi ? reviewResults.delphi.noOfReviews : 'none')
            //.replace(/<delphi_code_review_percent>/g, (reviewResults.delphi && reviewResults.delphi.percent) ? reviewResults.delphi.percent.toFixed(2) : '0')
            .replace(/<java_changes_wo_review_table>/g, changes.java ? constructChangesTable(_.sortBy(_.where(changes.java, {
                "isReviewed": false
            }), function (item) {
                return item.authors.join();
            }), isHtml) : '')
            //.replace('<delphi_changes_wo_review_table>', changes.delphi ? constructChangesTable(_.sortBy(_.where(changes.delphi, {
            //    "isReviewed": false
            //}), function (item) {
            //    return item.authors.join();
            //}), isHtml) : '')
            .replace('<open_reviews_table>', constructOpenReviewsTable(
                _.where(changes.java, {
                    "isReviewedOpen": true
                })));
                //.concat(
                //    _.where(changes.delphi, {
                //        "isReviewedOpen": true
                //    })), isHtml));

        fullBody += isHtml ? '\n</BODY>\n' : '';

        return fullBody;
    }

    function getBodyStyle() {
        console.log('getBodyStyle');
        return [
            '<BODY>',
            '<STYLE type="text/css">',
            'body {font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; font-size: 14px; text-align: left; }',
            'h1 {font-size: 20px}',
            'h2 {font-size: 16px}',
            'table {font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif; font-size: 12px; background: #fff; margin: 10px; width: 800px; border-collapse: collapse; text-align: left;}',
            'th {font-size: 14px; font-weight: normal; color: #039; padding: 10px 8px; border-bottom: 2px solid #6678b1;}',
            'td {border-bottom: 1px solid #ccc; color: #669; padding: 6px 8px;}',
            '.unauth td {color: #F00;}',
            'p.summary {font-size: 12px;}',
            '</STYLE>\n'
            ].join('\n');
    }

    function constructChangesTable(unreviewedChanges, isHtml) {
        console.log('constructChangesTable');
        var tableBody = '';
        if (isHtml) {
            tableBody = [
                '<TABLE>',
                '  <TR>',
                '    <TH>Change (job/jira/instance else comment)</TH>',
                '    <TH>Author</TH>',
                '    <TH>Revisions</TH>',
                '  </TR>\n'
            ].join('\n');

            tableBody += _.map(unreviewedChanges, function (change) {
                return '  <TR>\n<TD>' + change.job + (change.description ? ' - ' + change.description : '') +
                    '    </TD>\n<TD>' + change.authors.join(', ') +
                    '    </TD>\n<TD>' + change.revisions.join(', ') +
                    '    </TD>\n  </TR>\n';
            }).join('\n');

            tableBody += '</TABLE>\n';
        } else {
            tableBody = '\n\n\tCHANGE\t\tAUTHOR\t\tREVISIONS\n';
            tableBody += _.map(unreviewedChanges, function (change) {
                return '\t' + change.job + (change.description ? ' - ' + change.description : '') +
                    '\t\t' + change.authors.join(', ') +
                    '\t\t' + change.revisions.join(', ');
            }).join('\n');
        }
        return tableBody;
    }

    function constructOpenReviewsTable(openReviews, isHtml) {
        console.log('constructOpenReviewsTable');
        var tableBody = '';
        if (isHtml) {
            tableBody = [
                '<TABLE>',
                '  <TR>',
                '    <TH>Change (job/jira/instance else comment)</TH>',
                '    <TH>Author</TH>',
                '    <TH>Revisions</TH>',
                '  </TR>\n'
            ].join('\n');

            tableBody += _.map(openReviews, function (change) {
                return '  <TR>\n<TD>' + change.job +
                    '    </TD>\n<TD>' + change.authors.join(', ') +
                    '    </TD>\n<TD>' + change.revisions.join(', ') +
                    '    </TD>\n  </TR>\n';
            }).join('\n');

            tableBody += '</TABLE>\n';
        } else {
            tableBody = '\n\n\tCHANGE\t\tAUTHOR\t\tREVISIONS\n';
            tableBody += _.map(openReviews, function (change) {
                return '\t' + change.job +
                    '\t\t' + change.authors.join(', ') +
                    '\t\t' + change.revisions.join(', ');
            }).join('\n');
        }
        return tableBody;
    }

};