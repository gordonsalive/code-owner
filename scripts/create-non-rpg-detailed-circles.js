/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 31/5/2017
// Module to split RPG detailed code heat up into each 'team' and generate pages for them
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

//var update_status = require('./update-status');
var read_json = require('./read-json');
//var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');

module.exports = function () {
    return read_json('json/rpg-code-base-detailed.json')
        .then(function (detailedHeat) {
            //for each child of the root node I want a json output
            return Q.all(
                    _.each(detailedHeat.children, function (team) {
                        var teamJson = "circles/rpg-incidents-detailed-" + team.name + ".json";
                        return Qfs.write(teamJson, JSON.stringify(team, null, '\t'));
                    })
                )
                .then(function (writeResults) {
                    return detailedHeat;
                });
        })
        .then(function (detailedHeat) {
            //for each child of the root node I want an HTML page
            return Qfs.read('circles/heat-map-template.html')
                .then(function (template) {
                    return Q.all(
                        _.each(detailedHeat.children, function (team) {
                            //console.log('about to load html template and process for team:' + team.name);
                            var teamTemplate = template;
                            //console.log("processing template");
                            var teamJson = "rpg-incidents-detailed-" + team.name + ".json";
                            var teamHtml = template
                                .replace(/<TAG_ROOT><\/TAG_ROOT>/g, "'" + team.name + "'")
                                .replace(/_TAG_ROOT_/g, team.name)
                                .replace(/<TAG_SUB_ITEM_JSON><\TAG_SUB_ITEM_JSON>/g, teamJson)
                                .replace(/_TAG_SUB_ITEM_JSON_/g, "'" + teamJson + "'");
                            //console.log('about to write out HTML');
                            return Qfs.write("circles/heat-map-" + team.name + ".html", teamHtml);
                        })
                    );
                })
                .then(function (writeResults) {
                    //console.log('returning detailedHeat');
                    return detailedHeat;
                });
        });
};