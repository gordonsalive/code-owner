/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: Alan Gordon
// Date: 7/6/2017
// Module to convert cucumber.json files into junit.xml
// Assumptions: 'testsuite' = 'feature', 'testcase'='scenario'
//              all steps passed = passed, any step failed = failed, anything else = test skipped
//**********
// Notes:
// ------- Junit has this basic structure
//<?xml version="1.0" encoding="UTF-8"?>
//<testsuites>
//   <testsuite name="JUnitXmlReporter" errors="0" tests="0" failures="0" time="0" timestamp="2013-05-24T10:23:58" />
//   <testsuite name="JUnitXmlReporter.constructor" errors="0" skipped="1" tests="3" failures="1" time="0.006" timestamp="2013-05-24T10:23:58">
//      <properties>
//         <property name="java.vendor" value="Sun Microsystems Inc." />
//         <property name="compiler.debug" value="on" />
//         <property name="project.jdk.classpath" value="jdk.classpath.1.6" />
//      </properties>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default path to an empty string" time="0.006">
//         <failure message="test failure">Assertion failed</failure>
//      </testcase>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default consolidate to true" time="0">
//         <skipped />
//      </testcase>
//      <testcase classname="JUnitXmlReporter.constructor" name="should default useDotNotation to true" time="0" />
//   </testsuite>
//</testsuites>
// ------- cucumber has this basic structure:
//[
//  {
//    "uri": "features/one_passing_one_failing.feature",
//    "keyword": "Feature",
//    "id": "one-passing-scenario,-one-failing-scenario",
//    "name": "One passing scenario, one failing scenario",
//    "line": 2,
//    "description": "",
//    "tags": [
//      {
//        "name": "@a",
//        "line": 1
//      }
//    ],
//    "elements": [
//      {
//        "keyword": "Scenario",
//        "id": "one-passing-scenario,-one-failing-scenario;passing",
//        "name": "Passing",
//        "line": 5,
//        "description": "",
//        "tags": [
//          {
//            "name": "@b",
//            "line": 4
//          }
//        ],
//        "type": "scenario",
//        "steps": [
//          {
//            "keyword": "Given ",
//            "name": "this step passes",
//            "line": 6,
//            "match": {
//              "location": "features/step_definitions/steps.rb:1"
//            },
//            "result": {
//              "status": "passed",
//              "duration": 1
//            }
//          }
//        ]
//      },
//      {
//        "keyword": "Scenario",
//        "id": "one-passing-scenario,-one-failing-scenario;failing",
//        "name": "Failing",
//        "line": 9,
//        "description": "",
//        "tags": [
//          {
//            "name": "@c",
//            "line": 8
//          }
//        ],
//        "type": "scenario",
//        "steps": [
//          {
//            "keyword": "Given ",
//            "name": "this step fails",
//            "line": 10,
//            "match": {
//              "location": "features/step_definitions/steps.rb:4"
//            },
//            "result": {
//              "status": "failed",
//              "error_message": " (RuntimeError)\n./features/step_definitions/steps.rb:4:in /^this step fails$/'\nfeatures/one_passing_one_failing.feature:10:in Given this step fails'",
//              "duration": 1
//            }
//          }
//        ]
//      }
//    ]
//  }
//]


//var Q = require("q");
//var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

var read_json = require('./read-json');
var get_junit_from_cucumber_json = require('./get-junit-from-cucumber-json');
//var promisify = require('./scripts/promisify');

module.exports = function (inputFile) {
    if (DEBUG) console.log('get-junit-from-cucumber(' + inputFile + ')');
    return read_json(inputFile)
        .then(function (cucumberJson) {
            return get_junit_from_cucumber_json(cucumberJson);
        });
};
