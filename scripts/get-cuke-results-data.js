/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");
var Qfs = require("q-io/fs");

// AAG 5/5/'16
// method to return the cuke results as a JSON object
// results (passed, failed, missing totals) have already been fetched asynchronously
// and saved in cuke-results.json - all we have to do it load it, parse it and save it.
module.exports = function () {
    //console.log('get-cuke-results-data');
    return Qfs.read('json/cuke-results.json')
        .then(function (cukeResultFile) {
            //console.log('about to return JSON from cuke-results.json');
            //console.log(cukeResultFile);
            //console.log(JSON.parse(cukeResultFile, null, " "));
            return JSON.parse(cukeResultFile);
        });
    //.fail(); - don't handle failure here, let calling module handle it
};