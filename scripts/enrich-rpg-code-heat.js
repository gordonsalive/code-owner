/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 17/5/2017
// Module to spin through and rpg-code-heat files and enrich the defect lists with job description and year-month of fix
//**********

//var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //off

//var update_status = require('./update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');
//var pool_prommise = require('./pool-promise');

module.exports = function (codeHeatFile) {
    return promisify.spread([
        read_json(codeHeatFile),
        read_json('json/changes-results.json')
        ], function (codeHeat, changes) {
            return enrichNodeRecursive(codeHeat, changes);
        })
        .then(results) {
            return Qfs.write(codeHeatFile, JSON.stringify(results, null, '\t'));
        };
};

function enrichNodeRecursive(node, changes)) {
    if (node.defects) {
        node.defects = _.map(node.defects, function (defect) {
            var jobDescAndFixedDate = findDescriptionAndFixedDate(defect.job, changes);
            if (jobDescAndFixedDate) {
                defect.descrpition = jobDescAndFixedDate.description;
                defect.fixedDate = jobDescAndFixedDate.fixedDate;
            }
            return defect;
        });
    }
    if (node.children) {
        node.children = _.map(node.children, function (child) {
            return enrichNodeRecursive(child, changes);
        })
    }
    return node;
}

function findDescriptionAndFixedDate(job, changes) {
    return
}