/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 7/4/'17
// utility method that takes a promise and executes it for an array in a number of batches across a pool

//var fs = require('fs');
//var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var DEBUG = 0; //0=off

module.exports = function (items, promise, poolSize) {
    console.log('pool size=', poolSize); // + ',' + items);

    return fulfil_promise_pooled();

    function fulfil_promise_pooled() {
        console.log('fulfil_promise_pooled');
        //split the id's out into n different batches, or just 1 if there are less than n items
        var n;
        if (poolSize) {
            n = poolSize;
        } else {
            n = 4;
        }
        var itemsArray = _.map(items);
        //console.log("itemsArray=" + itemsArray);
        var noOfItems = itemsArray.length;
        if (noOfItems <= n) {
            //console.log('small number so proceed as a single batch');
            return fulfil_promise_acc(itemsArray, 0);
        } else {
            console.log('items = ' + noOfItems);
            console.log('splitting into batches, n = ' + n);
            var batch = Math.floor(noOfItems / n);
            console.log('batch = ' + batch);
            var batches = [];
            var tail = itemsArray;
            var head = [];
            //allocate n-1 batches and the remainder goes in the last batch
            for (var x = 0; x < n - 1; x++) {
                head = _.first(tail, batch);
                tail = _.difference(tail, head);
                batches.push(head);
            }
            batches.push(tail);
            //console.log(batches);

            return Q.all(_.map(batches, function (batch, batchId) {
                    return fulfil_promise_acc(batch, batchId);
                }))
                .then(function (results) {
                    //console.log(JSON.stringify(results));
                    //console.log('flatten the results');
                    return _.flatten(results);
                });
        }
    }

    function fulfil_promise_acc(items, batchId) {
        //console.log('get_job_for_incident_acc');
        if (DEBUG) console.log('creating array of promises');
        var dots = '.';
        var arrayOfPromises = _.map(items, function (item) {
            dots += '.';
            return fulfil_promise_acc_function_promise(item);
        });
        console.log(dots);

        //convert the array of promises into a sequence of thens...
        var x = 0;
        return arrayOfPromises.reduce(function (soFar, f) {
            return soFar.then(function (f) {
                //output something on progress!
                x++;
                if (((items.length > 10) && (x % Math.floor(items.length / 10) === 0)) || (x === items.length)) {
                    console.log("batch(" + batchId + ") progress: " + x + " out of " + items.length + " (" + ((x * 100) / items.length).toFixed(0) + "%)");
                }
                return f;
            }).then(f);
        }, Q([]));
    }

    function fulfil_promise_acc_function_promise(item) {
        //console.log('get_job_for_incident_acc_function_promise');
        return function (resultsSoFar) {
            return fulfil_promise_acc_promise(resultsSoFar, item);
        };
    }

    function fulfil_promise_acc_promise(resultsSoFar, item) {
        return fulfil_promise(item)
            .then(function (results) {
                //console.log('push the results onto results so far');
                resultsSoFar.push(results);
                return resultsSoFar;
            });
    }

    function fulfil_promise(incident) {
        return promise(incident);
    }
};