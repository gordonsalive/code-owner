/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var _ = require('underscore');

//var get_status = require('./scripts/get-status');
var get_status = require('./get-status');

// AAG 6/5/'16
// method to add a helpder to the flat radiator view data, to make it easier to display with the ECT templates
module.exports = function (data, params) {
    //console.log('addRadiatorsFlatViewDataHelper');
    //console.log(data);
    //console.log(params);
    //add a helper to data
    data.helper = {};
    data.helper.params = params;

    data.helper.status = get_status();
    //console.log(data.helper.status);

    data.helper.getStatusLine = function (view, helper) {
        function isStale(helper, now) {
            //return true;
            var lastSuccessfulFetchDate = Date.parse(helper.status.lastSuccessfullConnect);
            var lastAttemptedFetchDate = Date.parse(helper.status.lastConnectionAttempt);
            var lastTotalsUpdateDate = Date.parse(helper.status.lastTotalsUpdate);
            //are any of these more than 30 minutes out of date?
            return ((now - lastSuccessfulFetchDate) > (1000 * 60 * 30)) ||
                ((now - lastAttemptedFetchDate) > (1000 * 60 * 30)) ||
                ((now - lastTotalsUpdateDate) > (1000 * 60 * 30));
        }
        var d = new Date();
        var stale = isStale(this, d);
        var result = '<span';
        if (stale) result += ' style="color:red"';
        result += '">';
        result += d.toLocaleString() + '&nbsp;&nbsp;-&nbsp;&nbsp;';
        //TODO: format these dates nicely.
        var lastSuccessfullConnect = new Date(Date.parse(this.status.lastSuccessfullConnect));
        var lastConnectionAttempt = new Date(Date.parse(this.status.lastConnectionAttempt));
        var lastSuccessfulTotals = new Date(Date.parse(this.status.lastTotalsUpdate));
        result += '(View: ' + this.status.connectionStatus +
            ' @ ' + lastSuccessfullConnect.toLocaleString() +
            ', last attempt: ' + lastConnectionAttempt.toLocaleString() +
            '; Totals:' + lastSuccessfulTotals.toLocaleString() + ')';
        result += '</span>';
        return result;
    };
    data.helper.setTypeFilter = function (filterObj) {
        //console.log("setTypeFilter" + JSON.stringify(filterObj, null, "  "));
        var passedOrFailed = filterObj.passedOrFailed;
        var view = filterObj.view;
        var helper = filterObj.helper;
        helper.maxNoOfSubItems = 0;
        var viewList = _.map(view, function (item, key) {
            var result = {
                name: key,
                results: item.results,
                info: item.results.info
            };
            if (item['sub-items']) {
                //console.log("sub-items:" + JSON.stringify(item["sub-items"], null, "  "));
                var noOfSubItems = Object.keys(item['sub-items']).length;
                if (noOfSubItems > helper.maxNoOfSubItems) {
                    helper.maxNoOfSubItems = noOfSubItems;
                }
                result["sub-items"] = _.map(item['sub-items'], function (subItem, subItemKey) {
                    return {
                        name: subItemKey,
                        results: subItem.results,
                        info: subItem.results.info
                    };
                });
            }
            //console.log("result:" + JSON.stringify(result, null, "  "));
            return result;
        });
        if (passedOrFailed) {
            this.filteredList = viewList.filter(function (viewItem) {
                return ((((viewItem.results.failed === 0) && (viewItem.results.passed > 0)) ? 'passed' : 'failed') === passedOrFailed);
            });
            this.filteredList = _.sortBy(this.filteredList, 'name');
        } else {
            //sort it by no of tests passing first (desc)...
            viewList = _.sortBy(viewList, function (item) {
                return item.results.passed;
            }); //.reverse();
            //..then sort it by no of tests failing (desc)
            viewList = _.sortBy(viewList, function (item) {
                return item.results.failed;
            }).reverse();
            this.filteredList = viewList;
        }
        //console.log("filteredList:" + JSON.stringify(this.filteredList, null, "  "));
        //give the list a consistent sort
        //console.log("setTypeFilter - exit" + JSON.stringify(filterObj, null, "  "));
    };
    data.helper.getCircleHtml = function (itemObj) {
        //console.log('getCircleHtml');
        var viewItem = itemObj.viewItem;
        var helper = itemObj.helper;
        var result = '';

        var radius = helper.getCircleRadius();

        result += '\n<td class="container" valign="middle" align="center" ';
        var colsAndRows = helper.subItemColsAndRows(helper.maxNoOfSubItems);
        if (colsAndRows.rows > 1) {
            result += ' rowspan="' + colsAndRows.rows + '" ';
        }
        if ((helper.params.xmas) && (helper.params.type == 'failed')) {
            result += ' style="color:Teal; " >';
        } else {
            result += ' >';
        }
        var whiteSpaceRegx = /\s/g;
        var jobName = '';
        if ((viewItem.info) && (viewItem.info.jenkinsJobName)) {
            jobName = 'job/' + viewItem.info.jenkinsJobName;
        } else if ((viewItem.info) && (viewItem.info.jenkinsViewName)) {
            jobName = 'view/' + viewItem.info.jenkinsViewName;
        } else {
            jobName = 'job/' + viewItem.name;
        }
        //http://jenkins.jhcllp.local/job/R-and-P%20AS%20-%20F154/
        result += '<a href=http://jenkins3.jhcllp.local/' + jobName.replace(whiteSpaceRegx, '%20') + ' valign="middle" align="center" />';

        result += '<img src="' + helper.getCircleFile(viewItem, helper) + '" alt="circle" width="' + radius + '" height="' + radius + '" style="width:' + radius + 'px height:' + radius + 'px; ">';
        result += '<div valign="middle" align="center" class="totals center">';
        if (viewItem.results.passed) {
            if (helper.viewItemKind(viewItem) === 'passed') {
                result += viewItem.results.passed;
            } else {
                result += '<u>' + viewItem.results.failed + '</u><br>' + (viewItem.results.failed + viewItem.results.passed);
            }
        }
        result += '</div>';
        //draw job name
        result += '</a></td>';
        return result;
    };
    data.helper.getSubItemCirclesAndText = function (itemObj) {
        //console.log("getSubItemCirclesAndText");
        //itemObj is of the form: {viewItem: data.helper.filteredList[i], helper: data.helper}
        var result = '';
        var viewItem = itemObj.viewItem;
        var helper = itemObj.helper;
        var whiteSpaceRegx = /\s/g;
        //if (viewItem['sub-items']) {
        //size text and circle according to how many rows we're showing (min 2)
        var colsAndRows = helper.subItemColsAndRows(helper.maxNoOfSubItems);
        //var fontSize = Math.floor(helper.getFontSize() / colsAndRows.rows);
        //var circleSize = Math.floor((helper.getCircleRadius() * 1.5) / colsAndRows.rows);
        var fontSize = Math.floor(helper.getFontSize() / 2.5);
        var circleSize = Math.floor((helper.getCircleRadius() * 1.5) / 2.5);

        var i = 0;
        for (var y = 0; y < colsAndRows.rows; y++) {
            if (y > 0) {
                result += '\n<tr>';
            }
            for (var x = 0;
                (x < colsAndRows.cols); x++) {

                var jobName = '';
                //add text for the sub item
                result += '\n<td valign="middle" class="job-line" style="font-size:' + fontSize + 'px">';
                if ((viewItem['sub-items']) && (viewItem['sub-items'][i])) {
                    jobName = '';
                    if ((viewItem['sub-items'][i].info) && (viewItem['sub-items'][i].info.jenkinsJobName)) {
                        jobName = viewItem['sub-items'][i].info.jenkinsJobName;
                    } else {
                        jobName = viewItem['sub-items'][i].name;
                    }
                    //http://jenkins.jhcllp.local/job/R-and-P%20AS%20-%20F154/
                    result += '<a href=http://jenkins3.jhcllp.local/job/' + jobName.replace(whiteSpaceRegx, '%20') + '/>';
                    result += viewItem['sub-items'][i].name;
                    result += '</a>';
                }
                result += '</td>';


                //add a circle, size it according to how many sub rows we're showing
                result += '\n<td class="container" valign="middle" align="center" width="' + circleSize + '" height="' + circleSize + '" >';
                if ((viewItem['sub-items']) && (viewItem['sub-items'][i])) {
                    jobName = '';
                    if ((viewItem['sub-items'][i].info) && (viewItem['sub-items'][i].info.jenkinsJobName)) {
                        jobName = viewItem['sub-items'][i].info.jenkinsJobName;
                    } else {
                        jobName = viewItem['sub-items'][i].name;
                    }
                    //http://jenkins.jhcllp.local/job/R-and-P%20AS%20-%20F154/
                    result += '<a href=http://jenkins3.jhcllp.local/job/' + jobName.replace(whiteSpaceRegx, '%20') + '  valign="middle" align="center" />';
                    result += '<img src="' + helper.getCircleFile(viewItem['sub-items'][i], helper) + '" alt="circle" width="' + circleSize + '" height="' + circleSize + '" ';
                    result += ' style="width:' + circleSize + 'px height:' + circleSize + 'px; ">';
                    result += '<div valign="middle" align="center" class="center totals" style="font-size:' + Math.ceil(fontSize / 1.8) + 'px">';

                    if (viewItem['sub-items'][i].results.passed) {
                        if (helper.viewItemKind(viewItem['sub-items'][i]) === 'passed') {
                            result += viewItem['sub-items'][i].results.passed;
                        } else {
                            result += '<u>' + viewItem['sub-items'][i].results.failed + '</u><br>' + (viewItem['sub-items'][i].results.failed + viewItem['sub-items'][i].results.passed);
                        }
                    }
                    result += '</a>';
                    result += '</div>';
                }
                result += '</td>';

                i++;
            }
            if (y < colsAndRows - 1) {
                result += '</tr>';
            }
        }
        //}
        return result;
    };
    data.helper.subItemColsAndRows = function (maxNoOfSubItems) {
        switch (maxNoOfSubItems) {
        case 0:
            return {
                cols: 0,
                rows: 0
            };
        case 1:
            return {
                cols: 1,
                rows: 1
            };
        case 2:
            return {
                cols: 2,
                rows: 1
            };
        case 3:
        case 4:
            return {
                cols: 2,
                rows: 2
            };
        case 5:
        case 6:
            return {
                cols: 2,
                rows: 3
            };
        case 7:
        case 8:
        case 9:
            return {
                cols: 3,
                rows: 3
            };
        case 10:
        case 11:
        case 12:
            return {
                cols: 3,
                rows: 4
            };
            //case 13:
        default:
            return {
                cols: 4,
                rows: Math.ceil(maxNoOfSubItems / 4)
            };
        }
    };
    data.helper.viewItemKind = function (viewItem) {
        return ((viewItem.results.failed === 0) && (viewItem.results.passed > 0)) ? 'passed' : 'failed';
    };
    data.helper.getViewHtml = function (itemObj) {
        //console.log('getJobHtml');
        var viewItem = itemObj.viewItem;
        var helper = itemObj.helper;
        var result = '';

        var fontSize = helper.getFontSize();
        var smallFontSize = helper.getSmallFontSize(helper.orientation, fontSize);

        result += '\n<td valign="middle" class="job-line" ';
        var colsAndRows = helper.subItemColsAndRows(helper.maxNoOfSubItems);
        if (colsAndRows.rows > 1) {
            result += ' rowspan="' + colsAndRows.rows + '" ';
        }
        result += ' style="font-size:' + fontSize + 'px">';

        var whiteSpaceRegx = /\s/g;
        var jobName = '';
        if ((viewItem.info) && (viewItem.info.jenkinsJobName)) {
            jobName = 'job/' + viewItem.info.jenkinsJobName;
        } else if ((viewItem.info) && (viewItem.info.jenkinsViewName)) {
            jobName = 'view/' + viewItem.info.jenkinsViewName;
        } else {
            jobName = 'job/' + viewItem.name;
        }
        //http://jenkins.jhcllp.local/job/R-and-P%20AS%20-%20F154/
        result += '<a href=http://jenkins3.jhcllp.local/job/' + jobName.replace(whiteSpaceRegx, '%20') + '/>';

        result += viewItem.name;
        if ((helper.viewItemKind(viewItem) === 'failed') && (helper.params.orientation === 'landscape')) {
            result += '<br><div style="font-size:' + smallFontSize + 'px">';
            result += '</div>';
        }
        result += '</a></td>';
        return result;
    };
    data.helper.noOfViewItems = function (item) {
        //console.log('noOfJobs');
        if (item) {
            return item.view.length;
        } else {
            return this.filteredList.length;
        }
    };
    data.helper.getFontSize = function () {
        //console.log('getFontSize');
        if (this.params.orientation == 'landscape') {
            switch (this.scaling(this.noOfViewItems(), this.maxNoOfSubItems)) {
            case 'very-large':
                return 56;
            case 'large':
                return 48;
            case 'medium':
                return 32;
            case 'small':
                return 24;
                //assume very small
            default:
                return 20;
            }
        } else {
            if (this.noOfViewItems() >= 7) {
                return 40;
            } else {
                return 64;
            }
        }
    };
    data.helper.getSmallFontSize = function (orientation, fontSize) {
        //console.log('getSmallFontSize');
        if (orientation == 'landscape') {
            return fontSize / 4;
        } else {
            return fontSize / 5;
        }
    };
    data.helper.getCircleRadius = function () {
        //console.log('getCircleRadius');
        if (this.params.orientation == 'landscape') {
            switch (this.scaling(this.noOfViewItems(), this.maxNoOfSubItems)) {
            case 'very-large':
                return 80;
            case 'large':
                return 60;
            case 'medium':
                return 40;
            case 'small':
                return 30;
                //assume very small
            default:
                return 24;
            }
        } else {
            if (this.noOfViewItems() >= 7) {
                return 50;
            } else {
                return 90;
            }
        }
    };
    data.helper.getCircleFile = function (viewItem, helper) {
        //console.log('getCircleFile');
        // If the item has passing tests and no failing tests then it's green
        //   - check if it is running to see if it needs to be animated
        // If the item has failing tests then it is red
        //   - check if it is running to see if it needs to be animated
        // If it has no passing or failing tests, then
        //   - if it is running it should get it's status from the Jenkins job
        //   - if it is not running, it should be grey (no cuke file - so some kinf of error)
        //     unless the job is marked as not having cuke results, in which case is should get it status from the job
        var circleFile = 'img/';
        var animeExt = '-anime.gif';
        var staticExt = '.png';
        if (this.params.xmas) {
            animeExt = '-xmas' + animeExt;
            staticExt = '-xmas' + staticExt;
        } else {
            animeExt = '-circle' + animeExt;
            staticExt = '-circle' + staticExt;
        }
        if ((viewItem.results.failed === 0) && (viewItem.results.passed > 0)) {
            if (helper.isRunning(viewItem)) {
                circleFile += 'green' + animeExt;
            } else {
                circleFile += 'green' + staticExt;
            }
        } else if ((viewItem.results.failed > 0)) {
            if (helper.isRunning(viewItem)) {
                circleFile += 'red' + animeExt;
            } else {
                circleFile += 'red' + staticExt;
            }
        } else {
            var jobName;
            if ((viewItem.info) && viewItem.info.jenkinsJobName) {
                //this item has a linked job name
                jobName = viewItem.info.jenkinsJobName;
            } else if ((viewItem['sub-items']) && (viewItem['sub-items'][0]) && (viewItem['sub-items'][0].info)) {
                //this item has a sub item that has a linked job name
                jobName = viewItem['sub-items'][0].info.jenkinsJobName;
            } else {
                //assume the view item name itself is the name of a jenkins job
                jobName = viewItem.name;
            }
            var matchingJobData = _.find(data.jobs, function (job) {
                return (job.name === jobName);
            });
            var ext;
            if (helper.isRunning(viewItem)) {
                ext = animeExt;
            } else {
                ext = staticExt;
            }
            if (matchingJobData) {
                if (matchingJobData.color.includes('red')) {
                    circleFile += 'red' + ext;
                } else if (matchingJobData.color.includes('blue')) {
                    circleFile += 'green' + ext;
                } else {
                    circleFile += 'grey-circle.png';
                }
            } else {
                circleFile += 'grey-circle.png';
            }
        }
        //console.log('..' + circleFile);
        return circleFile;
    };
    data.helper.isRunning = function (viewItem) {
        var jobName;
        if ((viewItem.info) && viewItem.info.jenkinsJobName) {
            jobName = viewItem.info.jenkinsJobName;
        } else {
            jobName = viewItem.name;
        }

        var matchingJobData = _.find(data.jobs, function (job) {
            return (job.name === jobName);
        });
        if (matchingJobData) {
            return matchingJobData.color.includes('anime');
        } else {
            return false;
        }
    };
    data.helper.scaling = function (count, maxNumberOfSubItems) {
        //console.log('scaling:' + count + ':' + maxNumberOfSubItems);
        if (maxNumberOfSubItems && (maxNumberOfSubItems > 1)) {
            //scaling is about the number of sub-group circles we'll need to fit in
            //we can fit in 14 sub-group circles at large-size (60 * .6 * 15 = 540)
            // (504 / (40 *.6) = 22 at the medium size and 30 at the small size)
            var maxCircles = count * this.subItemColsAndRows(maxNumberOfSubItems).rows;
            //console.log('maxCircles:' + maxCircles);
            if (maxCircles <= 9) {
                return 'very-large';
            } else if (maxCircles <= 15) {
                return 'large';
            } else if (maxCircles <= 22) {
                return 'medium';
            } else if (maxCircles <= 30) {
                return 'small';
            } else {
                return 'very-small';
            }
        } else {
            if (count <= 8) {
                return 'large';
            } else if (count <= 12) {
                return 'medium';
            } else if (count <= 16) {
                return 'small';
            } else {
                return 'very-small';
            }
        }
    };
    //console.log('..finished adding helpers');

    return data;
};