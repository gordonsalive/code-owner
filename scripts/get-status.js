/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var fs = require('fs');
var _ = require('underscore');

// AAG 6/5/'16
// method to get and parse the Jenkins connection status from status.json
module.exports = function (data) {
    //console.log('getStatus');
    //return JSON.parse(fs.readFileSync("json/status.json"));
    var status = JSON.parse(fs.readFileSync("json/status.json"));
    var error = JSON.parse(fs.readFileSync("json/error.json"));
    if (error && error.lastConnectionAttempt && (error.connectionStatus === "connected")) {
        status.lastTotalsUpdate = error.lastConnectionAttempt;
    }
    return status;
};