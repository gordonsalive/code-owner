/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");

var get_code_owners = require('./get-code-owners');

// AAG 7/4/'16
// method to return a promise to get a tree of quality owner test results
// tree is from 'Quality Ownwer View' down, with pass/fail statistics at each level
// (either the full tree or a particular branch)
module.exports = function (codeOwnersJson, item, type) {

    //(1) get the code owners tree for the 'Quality Owners View'
    return get_code_owners('json/code-owners.json', 'quality-owners', 'view')
        .then(function (data) {
            //add in the test result location information
            return addCukeInfo(data);
        })
        .then(function (data) {
            //
        });
};

function addCukeInfo(data) {
    return Q.Promise(function (resolve, reject, notify) {
        try {
            try {} finally {}
            resolve();
        } catch (err) {
            reject(err);
        }
    });
}