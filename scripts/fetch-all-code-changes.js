/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 4/5/'17
// This script fetches all code changes from SVN by:
// (1) load config (where is SVN, which SVN repos, allocate repos to 'technology' for reporting)
// (2) for each 'technology', for each repo, from given start date, 
//   (a) fetch the code changes: comment, file paths and when change was made
//   (b) filter out any changes that aren't to branches we care about and apply a version number?
// (3) add these to the incident-changes-results.json
//   (a) any changes that don't fit against existing incidents, for into a new pot called other
// () write this out for later use.

// TODO:
// * fetch code changes from git (bitbucket)


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 0; //0=off

//var update_status = require('./scripts/update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');
var spawn_promise = require('./spawn-promise');

var resultsJson = {};

module.exports = function () {

    //assume F153 as the earliest point we are interested in
    var versions = ['non-thenon', 'trunk', 'branches/153', 'branches/154', 'branches/161', 'branches/162',
                    'branches/163', 'branches/164', 'branches/171', 'branches/172', 'branches/173', 'branches/174',
                    'branches/181', 'branches/182', 'branches/183', 'branches/184', 'branches/191', 'branches/192',
                    'branches/193', 'branches/194'];

    var startDate = '2015-07-1'; //F153

    var now = new Date();
    var endDate = now.getFullYear().toString() + '-' + (now.getMonth() + 1) + '-' + now.getDate();

    return read_json('json/repos.json')
        .then(function (repos) {
            //first sort out our array of calls
            //console.log('repos=' + JSON.stringify(repos));
            //for each technology get an array of calls for each repo

            var dateRange = '{' + startDate + '}:{' + endDate + '}';

            return Q.all(
                    _.flatten(_.map(repos.repos, function (tech, techName) {
                        return _.map(tech, function (repo) {
                            var exec = 'svn log -v ' + repos.svnHost + repo.path + ' -r ' + dateRange;
                            //console.log(exec);
                            return spawn_promise(exec, true, 4 * 60 * 1000)
                                .then(function (log) {
                                    //console.log('log=' + log);
                                    //console.log('add log');
                                    return {
                                        tech: techName,
                                        log: log
                                    };
                                })
                                .fail(function (err) {
                                    //if any of these calls fails, just skip over them
                                    console.log("failed!");
                                    return {
                                        tech: 'NONE'
                                    };
                                });
                        });
                    }))
                )
                .then(function (results) {
                    //console.log("results=" + JSON.stringify(results));
                    return _.filter(results, function (item) {
                        return item.tech !== 'NONE';
                    });
                });
        })
        .then(function (logsWithTech) {
            //console.log("logsWithTech=" + JSON.stringify(logsWithTech));
            // currently have [{tech: 'delphi', log: '...'}, ...]
            //now group up by tech and split the logs out into individual changes with a regex
            var resultsByTech = _.groupBy(logsWithTech, 'tech');
            // now have {'delphi': [{tech: 'delphi', log: '...'}], 'java': [{...}, ...]}
            return _.mapObject(resultsByTech, function (tech) {
                return _.flatten(_.map(tech, function (item) {
                    //console.log("item.log=" + JSON.stringify(item.log));
                    var pattern = /----------(\r|\n|\r\n)r((?!----------).*(\r|\n|\r\n))+----------/g;
                    var match = pattern.exec(item.log);
                    if (!match) {
                        console.log("there's no match!");
                    }
                    var results = [];
                    while (match) {
                        results.push({
                            tech: item.tech,
                            change: match[0]
                        });
                        match = pattern.exec(item.log);
                    }
                    return results;
                }));
            });
        })
        .then(function (changesByTech) {
            //console.log("\n\nchangesByTech=" + JSON.stringify(changesByTech));
            // currently have {'delphi': [{tech: 'delphi', change: '...'}, {...}], 'java': [{...}, ...]}
            //now parse changes with regex to get job-no/incident-no/other, file paths and change date

            return _.mapObject(changesByTech, function (tech) {
                return _.map(tech, function (change) {
                    //return _.map(changes, function (change) {
                    //console.log("change=" + JSON.stringify(change));
                    //var pattern = /r\d+ \| \w+ \| (\d\d\d\d-\d\d-\d\d).*lines?(\r|\n|\r\n)Changed paths:(\r|\n|\r\n)((   \w .*(\r|\n|\r\n))+)(\r|\n|\r\n)(.*(\b1\d{6}\b|\b2\d{5}\b).*|.*)/g;
                    var pattern = /r\d+ \| \w+ \| (\d\d\d\d-\d\d-\d\d).*lines?\r\nChanged paths:\r\n(?:   \w .*\r\n)+\r\n(.*\b1\d{6}\b|.*\b2\d{5}\b|.+?)/g;
                    var match = pattern.exec(change.change);
                    if (match) {
                        //console.log("match found!--->" + JSON.stringify(match));
                        //We need to do another match to allow us to iterate through path matches to pick them all up
                        //  I'm going to ignore file deletions at this point, just new or modified files.
                        var paths = [];
                        var pathPattern = /   [MA] (.*)/g;
                        var changeCopy = change.change;
                        var pathMatch = pathPattern.exec(changeCopy);
                        while (pathMatch) {
                            paths.push(pathMatch[1]);
                            pathMatch = pathPattern.exec(changeCopy);
                        }

                        //console.log("paths2=" + JSON.stringify(paths));
                        //filter out changes to branches we don't care about
                        var matchingVersions = [];
                        paths = _.filter(paths, function (path) {
                            var version = _.find(versions, function (version) {
                                return path.includes(version);
                            });
                            if (version) {
                                matchingVersions.push(version);
                                return true;
                            } else {
                                return false;
                            }
                        });
                        //console.log("paths3=" + JSON.stringify(paths));
                        matchingVersions = _.uniq(matchingVersions);

                        //console.log('match[2]=' + match[2]);

                        return {
                            incidentJobOther: (match[2].length > 1) ? match[2] : 'other',
                            date: match[1],
                            paths: paths,
                            versions: matchingVersions
                        };
                    } else {
                        //console.log("oh...no match!");
                        return {};
                    }
                    //});
                });
            });

        })
        .then(function (changesByTechWithPaths) {
            //console.log("\n\nchangesByTechWithPaths=" + JSON.stringify(changesByTechWithPaths));
            //group up all the changes that have the same incidentJobOther number 
            //ready to marry results up with the RPG results we have so far
            return _.mapObject(changesByTechWithPaths, function (tech) {
                var merged = [];
                var grouped = _.groupBy(tech, 'incidentJobOther');
                //--for now, lets just drop the other node--
                if (grouped.other) {
                    delete grouped.other;
                }
                //console.log("\n\ngrouped=" + JSON.stringify(grouped, null, ' '));
                //now walk through the grouped structure and create my new merged structure
                return _.map(grouped, function (group) {
                    return {
                        incidentJobOther: group[0].incidentJobOther,
                        dates: _.uniq(_.pluck(group, 'date')),
                        paths: _.uniq(_.flatten(_.pluck(group, 'paths'))),
                        versions: _.uniq(_.flatten(_.pluck(group, 'versions')))
                    };
                });
            });
        })
        .then(function (changesByTechTidied) {
            //console.log("\n\nchangesByTechTidied=" + JSON.stringify(changesByTechTidied));
            //load up the current results, and then add these results to those
            return read_json("json/incident-changes-results.json")
                .then(function (incident_changes_results) {
                    //console.log("\n\njson/incident-changes-results.json=" + JSON.stringify(incident_changes_results));
                    //this is of the form:
                    //[{"incident": 1000291, "job": "245565", "rpgChanges": []}, {...}, ...]
                    //and my SVN results are in the form:
                    //{'delphi': [{incident: "1234567", job: "251346", dates: [], paths: [], versions: []}, ...], ...}

                    //(1) add an other node to my incidents
                    incident_changes_results.other = [];
                    //(2) spin through my incidents looking for matching changes items (by incident or job number)
                    return _.map(incident_changes_results, function (incident) {
                        //look for delphi changes first
                        var match = _.find(changesByTechTidied.delphi, function (change) {
                            return ((change.incidentJobOther == incident.incident) || (change.incidentJobOther == incident.job));
                        });
                        if (match) {
                            incident.delphiChanges = {
                                dates: match.dates,
                                versions: match.versions,
                                paths: match.paths
                            };
                        }
                        //look for java changes next
                        match = _.find(changesByTechTidied.java, function (change) {
                            return ((change.incidentJobOther == incident.incident) || (change.incidentJobOther == incident.job));
                        });
                        if (match) {
                            incident.javaChanges = {
                                dates: match.dates,
                                versions: match.versions,
                                paths: match.paths
                            };
                        }
                        return incident;
                    });
                })
                .then(function (results) {
                    //console.log('about to write out results:' + JSON.stringify(results));
                    //return Qfs.write("json/incident-changes-results.json", JSON.stringify(results, null, '\t'))
                    //    .then(function (result) {
                    return results;
                    //    });
                });
        });
};