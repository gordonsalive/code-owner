/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

// AAG 4/5/'17
// This script fetches all code changes from SVN by:
// (1) load config (where is SVN, which SVN repos, allocate repos to 'technology' for reporting)
// (2) for each 'technology', for each repo, from given start date, 
//   (a) fetch the code changes: comment, file paths and when change was made
//   (b) filter out any changes that aren't to branches we care about and apply a version number?
// (3) add these to the incident-changes-results.json
//   (a) any changes that don't fit against existing incidents, for into a new pot called other
// () write this out for later use.

// TODO:
// * fetch code changes from git (bitbucket)


//var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
var fs = require('fs');

//Q.longStackSupport = true;

//var Client = require('node-rest-client').Client;
//var client = new Client();

var DEBUG = 1; //0=off

//var update_status = require('./scripts/update-status');
var read_json = require('./read-json');
var promisify = require('./promisify');
//var spawn_promise = require('./spawn-promise');
var exec_promise = require('./exec-promise');
var delete_folder_recursive_promise = require("./delete-folder-recursive-promise");


var resultsJson = {};

module.exports = function (incidents, startDate) {

    //assume F153 as the earliest point we are interested in
    var versions = ['non-thenon', 'trunk', 'branches/153', 'branches/154', 'branches/161', 'branches/162',
        'branches/163', 'branches/164', 'branches/171', 'branches/172', 'branches/173', 'branches/174',
        'branches/181', 'branches/182', 'branches/183', 'branches/184', 'branches/191', 'branches/192',
        'branches/193', 'branches/194'];

    //var startDate = '2015-07-01'; //F153

    var now = new Date();
    var endDate = now.getFullYear().toString() + '-' + (now.getMonth() + 1) + '-' + now.getDate();

    return read_json('json/repos.json')
        .then(function (repos) {
            if (DEBUG) console.log('fetch-non-rpg-code-changes, just loaded repos.json');
            //first sort out our array of calls
            //console.log('repos=' + JSON.stringify(repos));
            //for each technology get an array of calls for each repo

            var dateRange = '{' + startDate + '}:{' + endDate + '}';

            console.log(JSON.stringify(repos, null, '\t'));

            return Q.all(
                //_.flatten(_.map(repos['repos-heat'], function (tech, techName) {
                _.flatten(_.map(repos['repos-heat'], function (tech, techName) {
                    return _.map(tech, function (repo) {
                        if (repo['scm-type'] && (repo['scm-type'] == 'git')) {
                            console.log(' - - its a git repo');
                            //for GIT we need to clone or fetch and pull latest changes and then get log
                            return get_git_changes_and_log(repo, techName, repos.gitHost, startDate, endDate);
                        } else {
                            //otherwise it's old SVN
                            console.log(' - - its an old SVN repo');
                            return get_svn_log(repo, techName, repos.svnHost, dateRange);
                        }
                    });
                }))
            )
                .then(function (results) {
                    //console.log("results=" + JSON.stringify(results));
                    return _.filter(results, function (item) {
                        return item.tech !== 'NONE';
                    });
                })
                //})
                .then(function (logsWithTech) {
                    //console.log("logsWithTech=" + JSON.stringify(logsWithTech));
                    // currently have [{tech: 'delphi', log: '...'}, ...]
                    //now group up by tech and split the logs out into individual changes with a regex
                    var resultsByTech = _.groupBy(logsWithTech, 'tech');
                    // now have {'delphi': [{tech: 'delphi', log: '...'}], 'java': [{...}, ...]}
                    return _.mapObject(resultsByTech, function (tech, techName) {
                        if (DEBUG) console.log('getting changes by tech for ' + techName);
                        return _.flatten(_.map(tech, function (item) {
                            var results = [];
                            if (item.isJson) {
                                console.log('it`s json format');

                                results = _.map(item.log, function (change) {
                                    return {
                                        tech: item.tech,
                                        repo: item.repo,
                                        change: change,
                                        isJson: true
                                    }
                                });
                            } else {
                                //if (techName != 'delphi') {
                                //    console.log("item.log(" + techName + ")=" + JSON.stringify(item.log));
                                //}
                                var pattern = /----------(\r|\n|\r\n)r((?!----------).*(\r|\n|\r\n))+----------/g;
                                var match = pattern.exec(item.log);
                                //if (!match) {
                                //    console.log("there's no match to an SVN log entry!");
                                //}
                                while (match) {
                                    //if (techName != 'delphi') {
                                    //    console.log("there was a match(" + techName + ")=" + JSON.stringify(match));
                                    //}
                                    results.push({
                                        tech: item.tech,
                                        repo: item.repo,
                                        change: match[0]
                                    });
                                    match = pattern.exec(item.log);
                                }
                            }
                            return results;
                        }));
                    });
                })
                .then(function (changesByTech) {
                    //console.log("\n\nchangesByTech=" + JSON.stringify(changesByTech));
                    // currently have {'delphi': [{tech: 'delphi', change: '...'}, {...}], 'java': [{...}, ...]}
                    //now parse changes with regex to get job-no/incident-no/other, file paths and change date

                    return _.mapObject(changesByTech, function (tech, techName) {
                        return _.map(tech, function (change) {
                            if (change.isJson) {
                                //console.log(change)
                                //console.log(change.change);
                                //console.log(change.change.message.replace('\n', '-N-').replace('\r', '-R-').replace('\n', '-N-').replace('\r', '-R-'));
                                //the commit message might look like this:
                                //260874 - updated AOAPI extract.
                                //
                                //M       Transactions/AOAPI/aoAPI.pas
                                var pattern = /(.*(\b1\d{6}\b|\b2\d{5}\b).*|.*)\n\n(\w\s+.*)/g;
                                var match = pattern.exec(change.change.message);
                                if (match) {
                                    //console.log("GIT:match on change!" + match[0])
                                    var paths = [];
                                    var pathPattern = /[MA]\s+(.*)/g;
                                    var changeCopy = change.change.message;
                                    var pathMatch = pathPattern.exec(changeCopy);
                                    while (pathMatch) {
                                        //console.log("GIT:match on path!")
                                        //we need to tech and repo so that we can correlate a change to a specific repo (and no match POM.XML to every repo!)
                                        //console.log('path='+change.tech + '/' + change.repo + '/' + pathMatch[1]);
                                        paths.push(change.tech + '/' + change.repo + '/' + pathMatch[1]);
                                        pathMatch = pathPattern.exec(changeCopy);
                                    }
                                    //for GIT we only get the branch we care about (trunk), so no need to filter other changes out
                                    var incidentDateStr = change.change.date;
                                    if (isDate(incidentDateStr)) {
                                        var incidentDate = new Date(incidentDateStr);
                                        var incidentDateStr = incidentDate.getFullYear() + '-' + ("00" + (incidentDate.getMonth() + 1)).slice(-2) + '-' + ("00" + incidentDate.getDate()).slice(-2);
                                    }
                                    return {
                                        incidentJobOther: ((match[2]) && (match[2].length > 1)) ? match[2] : 'other',
                                        date: incidentDateStr,
                                        paths: paths,
                                        versions: ['trunk']
                                    };
                                } else {
                                    //console.log("oh...no match!");
                                    return {};
                                }
                            } else {
                                //return _.map(changes, function (change) {
                                //console.log("change=" + JSON.stringify(change));
                                //var pattern = /r\d+ \| \w+ \| (\d\d\d\d-\d\d-\d\d).*lines?(\r|\n|\r\n)Changed paths:(\r|\n|\r\n)((   \w .*(\r|\n|\r\n))+)(\r|\n|\r\n)(.*(\b1\d{6}\b|\b2\d{5}\b).*|.*)/g;
                                //var pattern = /r\d+ \| \w+ \| (\d\d\d\d-\d\d-\d\d).*lines?\r\nChanged paths:\r\n(?:   \w .*\r\n)+\r\n(.*\b1\d{6}\b|.*\b2\d{5}\b|.+?)/g;
                                var pattern = /r\d+ \| \w+ \| (\d\d\d\d-\d\d-\d\d).*lines?\r\nChanged paths:\r\n(?:   \w .*\r\n)+\r\n(.*(\b1\d{6}\b|\b2\d{5}\b).*|.*)/g;
                                var match = pattern.exec(change.change);
                                if (match) {
                                    //console.log("SVN:match on change!")
                                    //if (techName != 'delphi') {
                                    //    console.log('pattern match(' + techName + ')=' + JSON.stringify(match));
                                    //}

                                    //console.log("match found!--->" + JSON.stringify(match));
                                    //We need to do another match to allow us to iterate through path matches to pick them all up
                                    //  I'm going to ignore file deletions at this point, just new or modified files.
                                    var paths = [];
                                    var pathPattern = /   [MA] (.*)/g;
                                    var changeCopy = change.change;
                                    var pathMatch = pathPattern.exec(changeCopy);
                                    while (pathMatch) {
                                        //console.log("SVN:match on path!")
                                        //we need to tech and repo so that we can correlate a change to a specific repo (and no match POM.XML to every repo!)
                                        paths.push(change.tech + '/' + change.repo + '/' + pathMatch[1]);
                                        pathMatch = pathPattern.exec(changeCopy);
                                    }

                                    //console.log("paths2=" + JSON.stringify(paths));
                                    //filter out changes to branches we don't care about
                                    var matchingVersions = [];
                                    paths = _.filter(paths, function (path) {
                                        var version = _.find(versions, function (version) {
                                            return path.includes(version);
                                        });
                                        if (version) {
                                            matchingVersions.push(version);
                                            return true;
                                        } else {
                                            return false;
                                        }
                                    });
                                    //console.log("paths3=" + JSON.stringify(paths));
                                    matchingVersions = _.uniq(matchingVersions);

                                    //console.log('match[3]=' + match[3]);

                                    return {
                                        incidentJobOther: ((match[3]) && (match[3].length > 1)) ? match[3] : 'other',
                                        date: match[1],
                                        paths: paths,
                                        versions: matchingVersions
                                    };
                                } else {
                                    //console.log("oh...no match!");
                                    return {};
                                }
                                //});
                            }
                        });
                    });

                })
                .then(function (changesByTechWithPaths) {
                    //console.log("\n\nchangesByTechWithPaths=" + JSON.stringify(changesByTechWithPaths));
                    //group up all the changes that have the same incidentJobOther number 
                    //ready to marry results up with the RPG results we have so far
                    return _.mapObject(changesByTechWithPaths, function (tech, techName) {
                        var merged = [];
                        var grouped = _.groupBy(tech, 'incidentJobOther');
                        //--for now, lets just drop the other node--
                        if (grouped.other) {
                            delete grouped.other;
                        }
                        //if (techName != 'delphi') {
                        //    console.log("\n\ngrouped(" + techName + ")=" + JSON.stringify(grouped, null, ' '));
                        //}
                        //now walk through the grouped structure and create my new merged structure
                        return _.map(grouped, function (group) {
                            //if (techName != 'delphi') {
                            //    ****console.log('group(' + techName + ')=' + JSON.stringify(group));
                            //}
                            return {
                                incidentJobOther: group[0].incidentJobOther,
                                dates: _.uniq(_.pluck(group, 'date')),
                                paths: _.uniq(_.flatten(_.pluck(group, 'paths'))),
                                versions: _.uniq(_.flatten(_.pluck(group, 'versions')))
                            };
                        });
                    });
                })
                .then(function (changesByTechTidied) {
                    //console.log("\n\nchangesByTechTidied=" + JSON.stringify(changesByTechTidied, null, ' '));
                    //load up the current results, and then add these results to those
                    //return read_json("json/" + runName + "-incident-changes-results.json")//if incidents is incorrect, just incident config file, then read in manually from there, for debugging/partial running
                    return promisify(incidents)
                        .then(function (incident_changes_results) {
                            //console.log("\n\njson/incident-changes-results.json=" + JSON.stringify(incident_changes_results, null, ' '));
                            //this is of the form:
                            //[{"incident": 1000291, "job": "245565", "rpgChanges": []}, {...}, ...]
                            //and my SVN results are in the form:
                            //{'delphi': [{incident: "1234567", job: "251346", dates: [], paths: [], versions: []}, ...], ...}

                            //(1) add an other node to my incidents
                            incident_changes_results.other = [];
                            //(2) spin through my incidents looking for matching changes items (by incident or job number)
                            return _.map(incident_changes_results, function (incident) {
                                //for each repo technology group
                                _.each(repos['repos-heat'], function (tech, techName) {
                                    //_.each(repos['repos-heat'], function (tech, techName) {
                                    var match = _.find(changesByTechTidied[techName],
                                        function (change) {
                                            //console.log('searching through changes:' + JSON.stringify(change));
                                            return ((change.incidentJobOther) && ((change.incidentJobOther == incident.incident) || (change.incidentJobOther == incident.job)));
                                        });
                                    if (match) {
                                        //if (techName != 'delphi') {
                                        //    console.log(techName + 'Changes=' + JSON.stringify(match));
                                        //}
                                        //console.log('found a delphi match=' + JSON.stringify(match, null, ' '));
                                        incident[techName + 'Changes'] = {
                                            dates: match.dates,
                                            versions: match.versions,
                                            paths: match.paths
                                        };
                                    }
                                });


                                //                        //look for delphi changes first
                                //                        var match = _.find(changesByTechTidied.delphi, function (change) {
                                //                            return ((change.incidentJobOther == incident.incident) || (change.incidentJobOther == incident.job));
                                //                        });
                                //                        if (match) {
                                //                            //console.log('found a delphi match=' + JSON.stringify(match, null, ' '));
                                //                            incident.delphiChanges = {
                                //                                dates: match.dates,
                                //                                versions: match.versions,
                                //                                paths: match.paths
                                //                            };
                                //                        }
                                //                        //**** THIS IS MY PROBLEM *****
                                //                        //**  I've got tech by each tech group, but I've got hard coded handling for 'java' which isn't there
                                //                        //**  Ineed to handle this with the technologies groupings.
                                //                        
                                //                        //look for java changes next
                                //                        match = _.find(changesByTechTidied.java, function (change) {
                                //                            return ((change.incidentJobOther == incident.incident) || (change.incidentJobOther == incident.job));
                                //                        });
                                //                        if (match) {
                                //                            console.log('found a java match=' + JSON.stringify(match, null, ' '));
                                //                            incident.javaChanges = {
                                //                                dates: match.dates,
                                //                                versions: match.versions,
                                //                                paths: match.paths
                                //                            };
                                //                        }
                                return incident;
                            });
                        })
                        .then(function (results) {
                            //console.log('about to write out results:' + JSON.stringify(results));
                            //return Qfs.write("json/"+runName+"-incident-changes-results.json", JSON.stringify(results, null, '\t'))
                            //    .then(function (result) {
                            return results;
                            //    });
                        });
                });
        });
};

function get_svn_log(repo, techName, svnHost, dateRange) {
    var sanitisedName = sanitiseName(techName + '~' + repo.path + '.log');
    var exec = 'svn log -v ' + svnHost + repo.path + ' -r ' + dateRange + ' > ' + sanitisedName;
    //console.log(exec);
    //return spawn_promise(exec, true, 240 * 1000)
    return exec_promise(exec, true, 6 * 60 * 1000)
        .then(function (log) {
            return Qfs.read(sanitisedName);
        })
        .then(function (log) {
            //console.log('log=' + log);
            //console.log('add log');
            return {
                tech: techName,
                repo: repo.name,
                log: log
            };
        })
        .fail(function (err) {
            //if any of these calls fails, just skip over them
            console.log("failed! exec(" + exec + "): " + err);
            return {
                tech: 'NONE'
            };
        });
}

function get_git_changes_and_log(repo, techName, gitHost, startDate, endDate) {
    var sanitisedName = sanitiseName(techName + '~' + repo.name + '.log');
    var repoDir = 'repos/' + techName + '/' + repo.name;
    return Q(true)
        .then(function (result) {
            //is the repo there already?  If so, delete it.
            console.log('about to delete: ' + repoDir)
            if (fs.existsSync(repoDir)) {
                return delete_folder_recursive_promise(repoDir);
            } else {
                return true;
            }
        })
        .then(function (result) {
            //now fetch it
            var exec = 'git clone --depth 1 ' + gitHost + repo.path + ' ' + repoDir;
            console.log('about to clone repo: ' + exec);
            return exec_promise(exec, true, 6 * 60 * 1000);//wait upto 6 minutes
        })
        .then(function (result) {
            //var cmd = 'git log -n5 --branches=* --pretty=format:\'{%n^@^hash^@^:^@^%h^@^,%n^@^author^@^:^@^%an^@^,%n^@^date^@^:^@^%ad^@^,%n^@^email^@^:^@^%aE^@^,%n^@^message^@^:^@^%s^@^,%n^@^commitDate^@^:^@^%ai^@^,%n^@^age^@^:^@^%cr^@^},\'';
            //now get the log in pretty format
            var exec = 'cd ' + repoDir +
                ' && git log ' +
                " --since='" + startDate + "' --until='" + endDate + "' " +
                //' --pretty=format:\'{%n^@^hash^@^:^@^%h^@^,%n^@^author^@^:^@^%an^@^,%n^@^date^@^:^@^%ad^@^,%n^@^email^@^:^@^%aE^@^,%n^@^message^@^:^@^%s^@^,%n^@^commitDate^@^:^@^%ai^@^,%n^@^age^@^:^@^%cr^@^},\' ' +
                ' --pretty="\x1e%H\x1f%an\x1f%ad\x1f%cn\x1f%cd\x1f%s" --name-status ' + // \x1f is ASCII field separator, \x1e is record separator
                ' > ' + sanitisedName;
            //console.log('about to get log in pretty format: ' + exec);
            return exec_promise(exec, true, 6 * 60 * 1000)//wait upto 6 minutes
                .then(function (execResult) {
                    return Qfs.read(repoDir + '/' + sanitisedName)
                        .then(function (gitlog) {
                            return _.map(gitlog.substr(1).split('\x1e'), function (record) {
                                fields = record.split('\x1f');
                                return {
                                    hash: fields[0],
                                    author: fields[1],
                                    date: fields[2],
                                    commitAuthor: fields[3],
                                    commitDate: fields[4],
                                    message: fields[5]
                                }
                            })
                        })
                })
        })
        .then(function (log) {
            //console.log('log=');
            //console.log(log);
            //console.log('add log');
            return {
                tech: techName,
                repo: repo.name,
                log: log,
                isJson: true
            };
        })
        .fail(function (err) {
            //if any of these calls fails, just skip over them
            console.log("failed to get and log git changes! exec(" +
                repo + ":" + techName + ":" + repo.name + ":" + gitHost + "): " + err);
            return {
                tech: 'NONE'
            };
        });

}

function sanitiseName(name) {
    while (name.includes('/')) {
        name = name.replace('/', '~');
    }
    return name;
}

function isDate(dateString) {
    return (new Date(dateString) !== "Invalid Date") && !isNaN(new Date(dateString));
}