/*jslint devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var Q = require("q");
// AAG 31/3/'16
// method to take a value an return it back as a promise (so it can be chained with other promises)
// TODO: need some tests just for this module
module.exports = function (obj) {
    return Q(obj);
};

module.exports.method = function (func) {
    return Q.fcall(func);
};

module.exports.spread = function (promiseArray, func) {
    return Q.spread(promiseArray, func);
};