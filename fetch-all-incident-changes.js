/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/

//**********
// Author: ALan Gordon
// Date: 7/4/2017
// Module to fetch changes in all technologies for all incidents
//**********

var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");

var DEBUG = 0; //0=off

var update_status = require('./scripts/update-status');
var read_json = require('./scripts/read-json');
var promisify = require('./scripts/promisify');
var fetch_rpg_code_base = require('./scripts/fetch-rpg-code-base');
var fetch_non_rpg_code_base = require('./scripts/fetch-non-rpg-code-base');
var get_rpg_code_heat = require('./scripts/get-rpg-code-heat');
var get_non_rpg_code_heat = require('./scripts/get-non-rpg-code-heat');
var fetch_rpg_changes_for_incidents = require('./scripts/fetch-rpg-changes-for-incidents');
var fetch_jobs_for_incidents = require('./scripts/fetch-jobs-for-incidents');
var fetch_non_rpg_code_changes = require('./scripts/fetch-non-rpg-code-changes');
var create_rpg_detailed_circles = require('./scripts/create-rpg-detailed-circles');
var create_filtered_code_heat_table = require('./scripts/create-filtered-code-heat-table');

var dateF153 = 20150700; //F153
var dateOrdersTestsComplete = 20161200; //All orders tests in place
var dateOrdersTestsStart = 20160200; //orders tests project starts
var datePFMTestsComplete = 20171000; //80% PFM reballancing tests in place
var dateFarFuture = 20500000; //far future, i.e. up to tooday
var dirFullResults = '.';
var dirPreOrdersTests = 'circles-pre-orders-tests';
var dirPostOrdersTests = 'circles-post-orders-tests';
var dirPrePFMTests = 'circles-pre-pfm-tests';
var dirPostPFMTests = 'circles-post-pfm-tests';
var startDate = '2015-07-01'; //F153
var runs = {
    full: {
        periodStart: dateF153,
        periodEnd: dateFarFuture,
        subDir: dirFullResults
    }//,
    // preOrdersTests: {
    //     periodStart: dateF153,
    //     periodEnd: dateOrdersTestsComplete,
    //     subDir: dirPreOrdersTests
    // },
    // postOrdersTests: {
    //     periodStart: dateOrdersTestsComplete,
    //     periodEnd: dateFarFuture,
    //     subDir: dirPostOrdersTests
    // },
    // prePFMTests: {
    //     periodStart: dateF153,
    //     periodEnd: datePFMTestsComplete,
    //     subDir: dirPrePFMTests
    // },
    // postPFMTests: {
    //     periodStart: datePFMTestsComplete,
    //     periodEnd: dateFarFuture,
    //     subDir: dirPostPFMTests
    // }
};
var runNames = _.map(runs, function (run, runName) {
    return runName;
});
console.log('runNames=' + runNames.join(','));

writeOutStatus('failed');

(function doMainStuff() {
    var now = new Date();
    if (DEBUG) {
        console.log('--mainLoop:' + now.toTimeString());
    }

    read_json('json/incidents.json')
        .then(function (incidentsJson) {
            return promisify(incidentsJson)
                ////////
                //                    //this is the section I can move around when commenting out sections of code to do a partial run
                //                    .then(function (results) {
                //                        return read_json('json/incident-changes-results.json');
                //                    })
                ////////
                .then(function (incidentsJson) {
                    //fetch all the incident details afresh
                    return fetch_jobs_for_incidents(_.range(incidentsJson.incidentNoLow, incidentsJson.incidentNoHigh));
                })
                .then(function (incidentsWithJobNos) {
                    //use the results that came back from Henry's REST intf to filter out invalid incident numbers
                    return _.filter(incidentsWithJobNos, function (incidentWithJob) {
                        return (incidentWithJob.job !== "Not Found");
                    });
                })
                //.then(write_out_results_so_far)//all incidents + their job nos over all time
                .then(function (results) {
                    return write_out_results_so_far(results, runNames);
                })
                //use SQL to find the RPG changes for these incidents
                .then(fetch_rpg_changes_for_incidents)
                //.then(write_out_results_so_far)
                .then(function (results) {
                    return write_out_results_so_far(results, runNames);
                })
                .then(function (incidentsWithJobsAndRPG) {
                    //console.log("incidentsWithJobsAndRPG=" + JSON.stringify(incidentsWithJobsAndRPG));
                    return incidentsWithJobsAndRPG;
                 })
                //////////
                //use SVN log to find all the delphi and java code changes for these incidents
                .then(function (incidents) {
                    return fetch_non_rpg_code_changes(incidents, startDate);
                })
                //.then(write_out_results_so_far)
                .then(function (results) {
                    return write_out_results_so_far(results, runNames);
                })
                //use SQL to fetch the RPG code base
                .then(fetch_rpg_code_base)
                //fetch the Delphi & Java code base - no need to repeat this for each run!
                .then(fetch_non_rpg_code_base)
                .then(function (results) {
                    return Q.all(_.map(runs, function (run, runName) {
                        //lay rpg incident counts onto the code base to create rpg code heat
                        //.then(get_rpg_code_heat)
                        return get_rpg_code_heat(run.periodStart, run.periodEnd, run.subDir, runName)
                            //create our site pages for rpg code heat - summary view and details of each team
                            //.then(create_rpg_detailed_circles)
                            .then(function (results) {
                                return create_rpg_detailed_circles(run.subDir, runName);
                            })
                            //lay incident counts onto the Delphi & Java code bases to create code heat
                            //.then(get_non_rpg_code_heat)
                            .then(function (results) {
                                return get_non_rpg_code_heat(run.periodStart, run.periodEnd, run.subDir, runName);
                            })
                            //created a filtered list of just RPG modules we care about, in xml so we can import into Excel
                            .then(function (resultws) {
                                return create_filtered_code_heat_table(runName);
                            });
                    }));
                });
        })
        .then(function (result) {
            var d = new Date();
            console.log('Completed: ' + d.toLocaleString() + '. Wait 3 hours before repeating...');
            setTimeout(doMainStuff, 3 + 60 * 60 * 1000); /*try again in 60 minutes*/
        })
        .fail(function (err) {
            console.error(err);
            //update status (even if done already)
            writeOutStatus('failed');
            var d = new Date();
            console.log(d.toLocaleString() + ': error, try again in 4 hours');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 4 * 60 * 60 * 1000); //try again in 120 minutes
        });

})();

function write_out_results_so_far(results, runNames) {
    return Q.all(_.map(runNames, function (runName) {
            return Qfs.write("json/" + runName + "-incident-changes-results.json", JSON.stringify(results, null, '\t'));
        }))
        .then(function (result) {
            return Qfs.write("json/incident-changes-results.json", JSON.stringify(results, null, '\t'));
        })
        .then(function (result) {
            return results;
        });
}

function writeOutStatus(connectionStatus) {
    update_status(connectionStatus, 'incident-changes');
}