call forever stopall
call forever start code-owners.js
call forever start fetch-view-results.js
call forever start fetch-cuke-results.js
call forever start fetch-timings-results.js
call forever start fetch-test-history.js
call forever start fetch-failing-tests.js
call forever start fetch-and-update-test-results-history.js
call forever start fetch-code-changes-and-reviews.js
call forever start fetch-and-run-schedule.js
:: call forever start fetch-all-incident-changes.js