{| class="wikitable collapsible collapsed"
|-
! Specialism !! Component !! Sub-Component !! Tag
|-
|  Delphi || App Launcher || Application ||@cap(delphi.app-launcher.application)
|-
|  Delphi || Bargain List || Application ||@cap(delphi.bargain-list.application)
|-
|  Delphi || Bootstrap || Application ||@cap(delphi.bootstrap.application)
|-
|  Delphi || CRM || Application ||@cap(delphi.crm.application)
|-
|  Delphi || CRM || Secure messaging ||@cap(delphi.crm.secure-messaging)
|-
|  Delphi || CCM || Delphi ||@cap(delphi.ccm.delphi)
|-
|  Delphi || CCM || Template Editor ||@cap(delphi.ccm.template-editor)
|-
|  Delphi || CCM || RPG Transactions ||@cap(delphi.ccm.rpg-transactions)
|-
|  Delphi || Dealing interfaces || DealServers ||@cap(delphi.dealing-interfaces.dealservers)
|-
|  Delphi || Dealing interfaces || BestEx DealServer ||@cap(delphi.dealing-interfaces.bestex-dealserver)
|-
|  Delphi || Dealing interfaces || TD FX DealServer ||@cap(delphi.dealing-interfaces.td-fx-dealserver)
|-
|  Delphi || Document management || ||@cap(delphi.document-management)
|-
|  Delphi || Erik (RSP) || Application ||@cap(delphi.erik-rsp.application)
|-
|  Delphi || Erik (Asynch) || Application ||@cap(delphi.erik-asynch.application)
|-
|  Delphi || Erik (RSP + Limit minding) || Application ||@cap(delphi.erik-rsp-limit-minding.application)
|-
|  Delphi || MVP Framework || ||@cap(delphi.mvp-framework)
|-
|  Delphi || Enquiry system || O/R Application ||@cap(delphi.enquiry-system.o-r-application)
|-
|  Delphi || Quick-address interface || O/R Application ||@cap(delphi.quick-address-interface.o-r-application)
|-
|  Delphi || BITA Risk || O/R Application ||@cap(delphi.bita-risk.o-r-application)
|-
|  Delphi || Switch Delta || O/R Application ||@cap(delphi.switch-delta.o-r-application)
|-
|  Delphi || Pack Printing Interface || O/R Application ||@cap(delphi.pack-printing-interface.o-r-application)
|-
|  Delphi || Record Card || O/R Application ||@cap(delphi.record-card.o-r-application)
|-
|  Delphi || Valuation || O/R Application ||@cap(delphi.valuation.o-r-application)
|-
|  Delphi || Performance || O/R Application ||@cap(delphi.performance.o-r-application)
|-
|  Delphi || Document Generation || O/R Application ||@cap(delphi.document-generation.o-r-application)
|-
|  Delphi || Document Management || O/R Application ||@cap(delphi.document-management.o-r-application)
|-
|  Delphi || Order Management || O/R Application ||@cap(delphi.order-management.o-r-application)
|-
|  Delphi || Order management || RPG Transactions ||@cap(delphi.order-management.rpg-transactions)
|-
|  Delphi || Portfolio Management || RPG Transactions ||@cap(delphi.portfolio-management.rpg-transactions)
|-
|  Delphi || Portfolio Management || Application ||@cap(delphi.portfolio-management.application)
|-
|  Delphi || Research admin || Application ||@cap(delphi.research-admin.application)
|-
|  Delphi || Workflow || Application ||@cap(delphi.workflow.application)
|-
|  Delphi || Valuation || RPG Transactions ||@cap(delphi.valuation.rpg-transactions)
|-
|  Delphi || Performance || RPG Transactions ||@cap(delphi.performance.rpg-transactions)
|-
|  Delphi || CGT || RPG Transactions ||@cap(delphi.cgt.rpg-transactions)
|-
|  Delphi || Erik || RPG Transactions ||@cap(delphi.erik.rpg-transactions)
|-
|  Java || Anti Money Laundering / ID&V || Experian connector ||@cap(java.anti-money-laundering-id-v.experian-connector)
|-
|  Java || Anti Money Laundering / ID&V || ID3 Global (formally ProveURU) ||@cap(java.anti-money-laundering-id-v.id3-global-formally-proveuru)
|-
|  Java || Client sites || Fidelity site ||@cap(java.client-sites.fidelity-site)
|-
|  Java || Client sites || Lawsafe ||@cap(java.client-sites.lawsafe)
|-
|  Java || Client sites || Snoopy site ||@cap(java.client-sites.snoopy-site)
|-
|  Java || Client sites || Squirrel ||@cap(java.client-sites.squirrel)
|-
|  Java || Cofunds || ||@cap(java.cofunds)
|-
|  Java || DelphiComms || ||@cap(java.delphicomms)
|-
|  Java || CCM || FIGAROWeb ||@cap(java.ccm.figaroweb)
|-
|  Java || CCM Panels || FIGAROWeb ||@cap(java.ccm-panels.figaroweb)
|-
|  Java || CMS || FIGAROWeb ||@cap(java.cms.figaroweb)
|-
|  Java || Trading || FIGAROWeb ||@cap(java.trading.figaroweb)
|-
|  Java || Authentication (login process, password, passphrase) || FIGAROWeb ||@cap(java.authentication-login-process-password-passphrase.figaroweb)
|-
|  Java || KIIDs || FIGAROWeb ||@cap(java.kiids.figaroweb)
|-
|  Java || Registration || FIGAROWeb ||@cap(java.registration.figaroweb)
|-
|  Java || Framework (application, session etc) || FIGAROWeb ||@cap(java.framework-application-session-etc.figaroweb)
|-
|  Java || Stock Search || FIGAROWeb ||@cap(java.stock-search.figaroweb)
|-
|  Java || Documents || FIGAROWeb ||@cap(java.documents.figaroweb)
|-
|  Java || Secure messaging || FIGAROWeb ||@cap(java.secure-messaging.figaroweb)
|-
|  Java || Agent/Provider account search || FIGAROWeb ||@cap(java.agent-provider-account-search.figaroweb)
|-
|  Java || Downloads || FIGAROWeb ||@cap(java.downloads.figaroweb)
|-
|  Java || Corporate Actions || FIGAROWeb ||@cap(java.corporate-actions.figaroweb)
|-
|  Java || Withdrawals || FIGAROWeb ||@cap(java.withdrawals.figaroweb)
|-
|  Java || Valuation & Summaries || FIGAROWeb ||@cap(java.valuation-summaries.figaroweb)
|-
|  Java || Fantasy portfolio || FIGAROWeb ||@cap(java.fantasy-portfolio.figaroweb)
|-
|  Java || Performance || FIGAROWeb ||@cap(java.performance.figaroweb)
|-
|  Java || UX branding & styling || FIGAROWeb ||@cap(java.ux-branding-styling.figaroweb)
|-
|  Java || Statements || FIGAROWeb ||@cap(java.statements.figaroweb)
|-
|  Java || Transaction history || FIGAROWeb ||@cap(java.transaction-history.figaroweb)
|-
|  Java || Research integration (Ditial Look - Research Centre, Charts & Stock Information) || FIGAROWeb ||@cap(java.research-integration-ditial-look-research-centre-charts-stock-information.figaroweb)
|-
|  Java || Research integration (Morningstar( || FIGAROWeb ||@cap(java.research-integration-morningstar.figaroweb)
|-
|  Java || MSIP || FIGAROWeb ||@cap(java.msip.figaroweb)
|-
|  Java || Reverse Portfolio || FIGAROWeb ||@cap(java.reverse-portfolio.figaroweb)
|-
|  Java || Card payments & PXP integration || FIGAROWeb ||@cap(java.card-payments-pxp-integration.figaroweb)
|-
|  Java || Web Rebalancing || FIGAROWeb ||@cap(java.web-rebalancing.figaroweb)
|-
|  Java || Bulk Orders & Switching || FIGAROWeb ||@cap(java.bulk-orders-switching.figaroweb)
|-
|  Java || FigaroWeb (other) || FIGAROWeb ||@cap(java.figaroweb-other.figaroweb)
|-
|  Java || Generic FIX Engine || ||@cap(java.generic-fix-engine)
|-
|  Java || Java FIX DealServer || ||@cap(java.java-fix-dealserver)
|-
|  Java || JETS || KIIDs ||@cap(java.jets.kiids)
|-
|  Java || JETS || Other ||@cap(java.jets.other)
|-
|  Java || JETS-persistence || ||@cap(java.jets-persistence)
|-
|  Java || JTS || ||@cap(java.jts)
|-
|  Java || LDAP services || ||@cap(java.ldap-services)
|-
|  Java || FIGAROMobile || Trading ||@cap(java.figaromobile.trading)
|-
|  Java || FIGAROMobile || Non-Trading ||@cap(java.figaromobile.non-trading)
|-
|  Java || OAUTH || ||@cap(java.oauth)
|-
|  Java || OMXSCA || ||@cap(java.omxsca)
|-
|  Java || Price Feeds || ||@cap(java.price-feeds)
|-
|  Java || REST API || ||@cap(java.rest-api)
|-
|  Java || Sell Side FIX || ||@cap(java.sell-side-fix)
|-
|  Java || CCM || RPG Transactions ||@cap(java.ccm.rpg-transactions)
|-
|  Java || Bulk Orders & Switching || RPG Transactions ||@cap(java.bulk-orders-switching.rpg-transactions)
|-
|  Java || MSIP || RPG Transactions ||@cap(java.msip.rpg-transactions)
|-
|  Java || Order processing || RPG Transactions ||@cap(java.order-processing.rpg-transactions)
|-
|  Java || Order validation || RPG Transactions ||@cap(java.order-validation.rpg-transactions)
|-
|  Java || Performance || RPG Transactions ||@cap(java.performance.rpg-transactions)
|-
|  RPG || <Unclassified> || RPG Unit Tests ||@cap(rpg.unclassified.rpg-unit-tests)
|-
|  RPG || Accounts || ||@cap(rpg.accounts)
|-
|  RPG || Accounts || GetCash (API) ||@cap(rpg.accounts.getcash-api)
|-
|  RPG || Accounts || GetCItem (API) ||@cap(rpg.accounts.getcitem-api)
|-
|  RPG || Archiving || ||@cap(rpg.archiving)
|-
|  RPG || Audits || ||@cap(rpg.audits)
|-
|  RPG || Authorisation || Roes and Permissions ||@cap(rpg.authorisation.roes-and-permissions)
|-
|  RPG || Authorisation || Accounting code security (old) ||@cap(rpg.authorisation.accounting-code-security-old)
|-
|  RPG || Authorisation || Accounting code security (new) ||@cap(rpg.authorisation.accounting-code-security-new)
|-
|  RPG || CGT || Existing CGT ||@cap(rpg.cgt.existing-cgt)
|-
|  RPG || CGT || New CGT ||@cap(rpg.cgt.new-cgt)
|-
|  RPG || Client Database || General, database ||@cap(rpg.client-database.general-database)
|-
|  RPG || Client Database || CCM engine ||@cap(rpg.client-database.ccm-engine)
|-
|  RPG || Client Database || Green screen client inception ||@cap(rpg.client-database.green-screen-client-inception)
|-
|  RPG || Client Database || TD's Client Maintenance (including APO, upgrades, INA Bridge) ||@cap(rpg.client-database.td-s-client-maintenance-including-apo-upgrades-ina-bridge)
|-
|  RPG || Client Credit || CLICREDIT ||@cap(rpg.client-credit.clicredit)
|-
|  RPG || Client payments || ||@cap(rpg.client-payments)
|-
|  RPG || Commissions and Extra Charges || ||@cap(rpg.commissions-and-extra-charges)
|-
|  RPG || Compliance || ||@cap(rpg.compliance)
|-
|  RPG || Compliance || S18 ||@cap(rpg.compliance.s18)
|-
|  RPG || Compliance || Tax compliance (FATCA) ||@cap(rpg.compliance.tax-compliance-fatca)
|-
|  RPG || Compliance || CMR ||@cap(rpg.compliance.cmr)
|-
|  RPG || Compliance || Qualified Intermediaries (Q) ||@cap(rpg.compliance.qualified-intermediaries-q)
|-
|  RPG || Contracting || Trigger ||@cap(rpg.contracting.trigger)
|-
|  RPG || Contracting || Ernie ||@cap(rpg.contracting.ernie)
|-
|  RPG || CREST || ||@cap(rpg.crest)
|-
|  RPG || Configuration management || ||@cap(rpg.configuration-management)
|-
|  RPG || Contracting || ||@cap(rpg.contracting)
|-
|  RPG || Conversions || ||@cap(rpg.conversions)
|-
|  RPG || Corporate Actions || Generation ||@cap(rpg.corporate-actions.generation)
|-
|  RPG || Corporate Actions || Posting ||@cap(rpg.corporate-actions.posting)
|-
|  RPG || Data export || FEX ||@cap(rpg.data-export.fex)
|-
|  RPG || Deposit interest || ||@cap(rpg.deposit-interest)
|-
|  RPG || Dividends || ||@cap(rpg.dividends)
|-
|  RPG || Exchange Rates || ||@cap(rpg.exchange-rates)
|-
|  RPG || Fees || ||@cap(rpg.fees)
|-
|  RPG || Messaging || Messaging Framework (DEM*) ||@cap(rpg.messaging.messaging-framework-dem)
|-
|  RPG || Messaging || CREST messaging ||@cap(rpg.messaging.crest-messaging)
|-
|  RPG || Messaging || Non-CREST messaging ||@cap(rpg.messaging.non-crest-messaging)
|-
|  RPG || Messaging || Calastone ||@cap(rpg.messaging.calastone)
|-
|  RPG || Messaging || Cofunds ||@cap(rpg.messaging.cofunds)
|-
|  RPG || Messaging || Clearstream (SWIFT) ||@cap(rpg.messaging.clearstream-swift)
|-
|  RPG || Messaging || AllFunds ||@cap(rpg.messaging.allfunds)
|-
|  RPG || Nominees & safe custody || ||@cap(rpg.nominees-safe-custody)
|-
|  RPG || Pack Printing || Transactional & Tax reports extract routines ||@cap(rpg.pack-printing.transactional-tax-reports-extract-routines)
|-
|  RPG || Pack Printing || Valuation extract routines ||@cap(rpg.pack-printing.valuation-extract-routines)
|-
|  RPG || Pack Printing || Performance extract routines ||@cap(rpg.pack-printing.performance-extract-routines)
|-
|  RPG || Pack Printing || Contract noes and transfer forms ||@cap(rpg.pack-printing.contract-noes-and-transfer-forms)
|-
|  RPG || Pack Printing || Pack printing configuration and delivery programs ||@cap(rpg.pack-printing.pack-printing-configuration-and-delivery-programs)
|-
|  RPG || Pack Printing || Pack Printing and XML Infrastructure ||@cap(rpg.pack-printing.pack-printing-and-xml-infrastructure)
|-
|  RPG || Pack Printing || Pack print routing (old) ||@cap(rpg.pack-printing.pack-print-routing-old)
|-
|  RPG || Pack Printing || Pack print routing (new) ||@cap(rpg.pack-printing.pack-print-routing-new)
|-
|  RPG || Performance || Periodic & Shared Artefacts ||@cap(rpg.performance.periodic-shared-artefacts)
|-
|  RPG || Performance || Daily ||@cap(rpg.performance.daily)
|-
|  RPG || Performance || POR946 (API) ||@cap(rpg.performance.por946-api)
|-
|  RPG || Portfolios || ||@cap(rpg.portfolios)
|-
|  RPG || Regular background processes || ||@cap(rpg.regular-background-processes)
|-
|  RPG || Regular Investments (MSIP processing) || ||@cap(rpg.regular-investments-msip-processing)
|-
|  RPG || Settlements || Settlements ||@cap(rpg.settlements.settlements)
|-
|  RPG || Settlements || Settlement Netting ||@cap(rpg.settlements.settlement-netting)
|-
|  RPG || Stock Database || Upload ||@cap(rpg.stock-database.upload)
|-
|  RPG || Stock Database || GetStockUTL (API) ||@cap(rpg.stock-database.getstockutl-api)
|-
|  RPG || Stock Database || Database ||@cap(rpg.stock-database.database)
|-
|  RPG || Stock Database || STKJTS ||@cap(rpg.stock-database.stkjts)
|-
|  RPG || Stock reconciliation || ||@cap(rpg.stock-reconciliation)
|-
|  RPG || System Utilities || ||@cap(rpg.system-utilities)
|-
|  RPG || Tax wrappers || ||@cap(rpg.tax-wrappers)
|-
|  RPG || Transfers || ||@cap(rpg.transfers)
|-
|  RPG || Trade feeds || ||@cap(rpg.trade-feeds)
|-
|  RPG || Trade Restrictions || ||@cap(rpg.trade-restrictions)
|-
|  RPG || Trail commission || ||@cap(rpg.trail-commission)
|-
|  RPG || Transaction reporting || ||@cap(rpg.transaction-reporting)
|-
|  RPG || Translation || ||@cap(rpg.translation)
|-
|  RPG || Unit Trusts || ||@cap(rpg.unit-trusts)
|-
|  RPG || Unit Trusts || Cofunds Interface ||@cap(rpg.unit-trusts.cofunds-interface)
|-
|  RPG || Valuations || GetHold (API) ||@cap(rpg.valuations.gethold-api)
|-
|  RPG || Valuations || GetHoldings (API) ||@cap(rpg.valuations.getholdings-api)
|-
|  RPG || Valuations || GetValuation (API) ||@cap(rpg.valuations.getvaluation-api)
|-
|  RPG || Workflow || Generic Workflow ||@cap(rpg.workflow.generic-workflow)
|-
|  RPG || Workflow || Corporate Action Workflow ||@cap(rpg.workflow.corporate-action-workflow)
|-
|  RPG || Workflow || Transfers Workflow ||@cap(rpg.workflow.transfers-workflow)
|-
|  RPG || Workflow || CRM Workflow ||@cap(rpg.workflow.crm-workflow)
|-
|  Planet Press || Forms and templates || ||@cap(planet-press.forms-and-templates)
|-
|  Planet Press || Core templates || ||@cap(planet-press.core-templates)
|-
|  Delphi || Control library || ||@cap(delphi.control-library)
|-
|  Delphi || MVP Framework || ||@cap(delphi.mvp-framework)
|-
|  Delphi || Application Framework || ||@cap(delphi.application-framework)
|-
|  Delphi || Application Framework || Printing ||@cap(delphi.application-framework.printing)
|-
|  Delphi || Application Framework || Shared Definitions ||@cap(delphi.application-framework.shared-definitions)
|-
|  Delphi || Application Framework || User Interface ||@cap(delphi.application-framework.user-interface)
|-
|  Delphi || Application Framework || Logging ||@cap(delphi.application-framework.logging)
|-
|  Delphi || SS3 || ||@cap(delphi.ss3)
|-
|  Delphi || XA Framework || ||@cap(delphi.xa-framework)
|-
| undefined||undefined|| ||@cap()
|}