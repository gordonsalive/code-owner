/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
var prompt = require('prompt');
var fs = require('fs');
var _ = require("underscore");
var s = require("underscore.string");

//note: String.prototype is part of javascript and contains most of this functionality!

//input filename is a copy and paste from code owners spreadsheet - specialism \t component \t sub-component
//output filename will contain a json array of objects contain original descriptions and the slugified concatenated code ownership tag

prompt.get([
    'input_filename',
'output_filename'], function (err, result) {
    if (err) {
        return onErr(err);
    }
    console.log('filenames input received:');
    console.log('input filename: ' + result.input_filename);
    console.log('output filename: ' + result.output_filename);

    var lines = s.lines(fs.readFileSync('./utils/' + result.input_filename));
    lines = _.map(lines, function (line) {
        var origItems = _.filter(line.split('\t'), function (item) {
            return (item.trim() !== '');
        });
        var items = _.map(origItems, function (item) {
            //return s(item).slugify().value();
            return s.slugify(item);
        });
        return {
            originalDescription: origItems,
            slugifiedDescription: _.reduce(items, function (memo, item) {
                return memo + '.' + item;
            }, '').substring(1)
        };
    });
    console.log(lines);
    fs.writeFile('./utils/' + result.output_filename, JSON.stringify(lines));
});