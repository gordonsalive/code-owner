var Q = require("q");
var Qfs = require("q-io/fs");
var _ = require("underscore");
var fs = require("fs");
var parseString = require('xml2js').parseString;

var get_cucumber_from_junit = require('./scripts/get-cucumber-from-junit');//returns promise
var get_junit_from_cucumber_json = require('./scripts/get-junit-from-cucumber-json');//returns a promise

// Takes all the (junit.)xml files in the passed in directory and squashes them together 
//  into a single xml file (via a cucumber.json file)

var rootDir = process.argv[2] ? process.argv[2] : '.';
rootDir = __dirname + '/' + rootDir;

var files = _getAllFilesFromFolder(rootDir, false, /.*\.xml/); //don't recurse and only get *.xml files
console.log('files = ' + files);

//convert all the xml files into cucumber.json files (we'll compose this and then convert it back to junit.xml)
var runResults = Q.all(
        files.map(function(file) {
            console.log('converting junit to cucumber: ' + file);
            return get_cucumber_from_junit(file);
        })
    )
    .then(function(cucumberResults) {
        //now squash the cucumber json together
        return _.flatten(cucumberResults);
    })
    .then(function(cucumberSquash) {
        //now convert the cucumber squash into one junit.xml
        return get_junit_from_cucumber_json(cucumberSquash);
    })
    .then(function(junitSquash) {
        //now write it out to the second param, or output.xml if none
        outputFile = __dirname + '/' + (process.argv[3] ? process.argv[3] : 'output.xml');
        return Qfs.write(outputFile, junitSquash);
    })
    // .then(function(cucumberResults) {
    //     console.log('results = ' + cucumberResults);
    //     return Q.all(cucumberResults.map(function(cucumberJson){
    //         return get_junit_from_cucumber_json(cucumberJson)
    //     }));
    // })
    // .then(function(junitResults) {
    //     console.log('results = ' + junitResults);
    //     return Q.all(junitResults.map(function(junitXml) {
    //         Qfs.write(...)
    //     }))
    // })
    .fail(function (err) {
        //if any of these calls fails, just skip over them
        console.log("failed!  " + err);
    });


function _getAllFilesFromFolder(dir, isRecurse, regex) {

    console.log('dir = ' + dir + ', recurse = ' + isRecurse + ', regex = ' + regex);
    //var filesystem = require("fs");
    var results = [];

    fs.readdirSync(dir).forEach(function(file) {

        file = dir+'/'+file;
        //console.log('file = ' + file);
        var stat = fs.statSync(file);

        if (stat && stat.isDirectory()) {
            //console.log('its a dir');
            if (isRecurse) {
                results = results.concat(_getAllFilesFromFolder(file));
            }
        } else {
            //console.log('its not a dir');
            if (regex) {
                //console.log('testing (' + file + ') matches regex.');
                if (regex.test(file)) {
                    //console.log('true');
                    results.push(file);
                }
            } else {
                results.push(file);
            }
        }
    });
    //console.log('back.');

    return results;

};