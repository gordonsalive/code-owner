/*jslint browser: true, devel: true, node: true, white: true, plusplus: true, sloppy: true, vars: true, maxlen: 200*/
/*jshint -W030*/
/*jshint expr:true*/
//TODO: need to write automated tests - want to test it correctly update status if no connection, could test retry timings, could test json of the right shape is returned if there is a connection?
console.log('================================================================================');
console.log('Start: ' + new Date().toISOString());
console.log('================================================================================');

//**********
// Author: ALan Gordon
// Date: 28/10/2016
// uses git to fetch figaro-features files, then copies them into figaro-features 
// and pushes them ready for them to be Pickled
//**********

var sys = require('sys');
var fs = require('fs');
var Qfs = require("q-io/fs");
var Q = require("q");
var _ = require("underscore");
//Q.longStackSupport = true;

var read_json = require("./scripts/read-json");
var promisify = require("./scripts/promisify");
var delete_folder_recursive_promise = require("./scripts/delete-folder-recursive-promise");
var exec_promise = require("./scripts/exec-promise");

var DEBUG = 0; //0=off
var WINDOWS = false;

var update_status = require('./scripts/update-status');

var resultsJson = {};


writeOutStatus('failed'); //TODO: does this do anything or just return a promise that is never resolved?

(function doMainStuff() {

    var d = new Date();
    if (DEBUG) console.log('mainLoop:' + d.toTimeString());

    //TODO: this should use repeat instead of setTimeout?
    //or perhaps a proper chron scheduler?

    //console.log('--parsefile..');
    read_json('json/features.json')
        .then(function (featuresFile) {
            //now pull the changes to figaro-features
            var pullPromise;
            if (WINDOWS) {
                pullPromise = exec_promise("pull-figaro-features.bat");
            } else {
                pullPromise = exec_promise("sh pull-figaro-features.sh");
            }
            return pullPromise
                .then(function (result) {
                    return featuresFile;
                });
        })
        .then(function (featuresFile) {
            //first step is to get all the features and copy them into figaro-features
            return promisify(featuresFile)
                .then(function (featuresFile) {
                    return featuresFile.features;
                })
                .then(function (features) {
                    //clean out the features folder
                    //for each feature, delete the contents of it's directory, if it exists
                    return Q.all(_.map(features, function (feature, key) {
                            if (feature.type === 'git') {
                                console.log('delete features/' + key);
                                return delete_folder_recursive_promise("features/" + key);
                            } else {
                                return promisify("");
                            }
                        }))
                        .then(function (result) {
                            console.log('done with deletes from features');
                            return features;
                        });
                })
                .then(function (features) {
                    //for each feature, delete the contents of it's directory, if it exists
                    return Q.all(_.map(features, function (feature, key) {
                            if (feature.type === 'git') {
                                console.log('delete figaro-features/features/' + key);
                                return delete_folder_recursive_promise("figaro-features/features/" + key);
                            } else {
                                return promisify("");
                            }
                        }))
                        .then(function (result) {
                            console.log('done with deletes from figaro-features/features/');
                            return features;
                        });
                })
                .then(function (features) {
                    console.log('clone the repos');
                    //for all the features, get their code
                    return Q.all(_.map(features, function (feature, key) {
                            console.log('clone ' + key);
                            if (feature.type === 'git') {
                                //git clone -b branch repo directory_to_put_it
                                return exec_promise("git clone --depth 1 " +
                                    " -b " + feature['repo-branch'] +
                                    " " + feature.repo +
                                    " features/" + key); //feature["repo-name"]);
                            } else {
                                return promisify("");
                            }
                        }))
                        .then(function (result) {
                            console.log('done getting code into features/name');
                            return features;
                        });
                })
                .then(function (features) {
                    //for all the features, copy their features across
                    return Q.all(_.map(features, function (feature, key) {
                            if (feature.type === 'git') {
                                if (WINDOWS) {
                                    //return exec_promise("xcopy features\\" + feature['repo-name'].replace(/\//g, "\\") + '\\' + 
                                    //feature['from-folder'].replace(/\//g, "\\") + " figaro-features\\features\\" + key + " /E/C");
                                    return exec_promise("xcopy features\\" + key + '\\' + feature['from-folder'].replace(/\//g, "\\") +
                                        " figaro-features\\features\\" + key + "\\ /E/C");
                                } else {
                                    return exec_promise("cp -rf features/" + key + '/' + feature['from-folder'] +
                                        " figaro-features/features/" + key);
                                }
                            } else {
                                return promisify("");
                            }
                        }))
                        .then(function (result) {
                            return features;
                        });
                })
                .then(function (features) {
                    return featuresFile;
                });

        })
        .then(function (featuresFile) {
            //now push the changes to figaro-features
            if (WINDOWS) {
                return exec_promise("push-figaro-features.bat");
            } else {
                return exec_promise("sh push-figaro-features.sh");
            }
        })
        .then(function (result) {
            console.log('--update status..');
            //update status
            return writeOutStatus('succeeded');
        })
        .then(function (result) {
            console.log('..repeat in 1 day');
            /*repeat in 30 seconds*/
            setTimeout(doMainStuff, 24 * 60 * 60 * 1000);
        })
        .fail(function (err) {
            console.error(err);
            //update the status (even if done already)
            writeOutStatus('failed');
            console.log('error, try again in 1 hour');
            //try again in a whlie, leave an interval for things to correct themselves.
            setTimeout(doMainStuff, 60 * 60 * 1000); //try again in 1 hour
        });

})();

function writeOutStatus(connectionStatus) {
    return update_status(connectionStatus, 'feature-docs');
}